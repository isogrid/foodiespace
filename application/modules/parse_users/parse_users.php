<?php

use Category\CategoryApi;
use Currency\Currency;
use SProductsQuery;
use SProductVariantsQuery;
use CMSFactory\ModuleSettings;
use Cart\BaseCart;
use Cart\CartItem;

use parse_users\DataUpdaters\ProductsDataUpdater;
use parse_users\DataUpdaters\BrandsDataUpdater;

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Image CMS
 * Module Frame
 *
 *
 *
 *
 *
 *TRUNCATE `shop_brands`;
 * TRUNCATE `shop_brands_i18n`;
 * TRUNCATE `shop_products`;
 * TRUNCATE `shop_products_i18n`;
 * TRUNCATE `shop_products_rating`;
 * TRUNCATE `shop_products_words`;
 * TRUNCATE `shop_product_categories`;
 * TRUNCATE `shop_product_images`;
 * TRUNCATE `shop_product_properties`;
 * TRUNCATE `shop_product_properties_categories`;
 * TRUNCATE `shop_product_properties_data`;
 * TRUNCATE `shop_product_properties_i18n`;
 * TRUNCATE `shop_product_property_value`;
 * TRUNCATE `shop_product_property_value_i18n`;
 * TRUNCATE `shop_product_variants`;
 * TRUNCATE `shop_product_variants_i18n`;
 * TRUNCATE `shop_product_variants_prices`;
 * TRUNCATE `shop_product_variants_price_types`;
 * TRUNCATE `shop_product_variants_price_type_values`;
 *
 * DELETE FROM `route` WHERE  `type`='product'
 */
class parse_users extends MY_Controller
{

    /**
     * @var ProductsDataUpdater
     */
    protected $productsUpdater;
    protected $brands_data;

    public $delimiter = ';';

    /**
     * CSV enclosure
     * @var string
     */
    public $enclosure = '"';

    public $possibleAttributes = [];
    public $products_data = [];


    private $uploadDir = './uploads/';
    private $csvFileName = 'Category.csv';
    private $xlsFileName = 'Category.xls';
    private $xlsFileName1 = 'Выгрузка на сайт Экостатуса.csv';

    public static $check_event = false;


    public function __construct()
    {
        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('parse_users');
        $this->load->helper('translit');
        $this->time = time();
        $this->settings = ModuleSettings::ofModule('parse_users')->get();
    }

    /**
     * @return CI_User_agent
     */
    public function start_parsing()
    {
        try {
            $this_users = self::parse_file();

            self::create_users($this->users_data);


        } catch (\Exception $e) {

        }

    }

    public function create_users($this_users)
    {
       $roles = \CI::$APP->db->get('shop_rbac_roles');
       foreach($roles->result_array() as $role){

           $role_data[$role['name']]=$role['id'];
       }
        if (\CI::$APP->load->module('mod_cities') != null) {
            $cities = module('mod_cities')->get_AllCities1C();
        }
//        dump($cities);
//        dump($role_data);
//        dd($this_users);
        foreach($this_users as $user){
            $key = random_string('numeric', 5);
            $password =crypt($this->dx_auth->_encode(str_replace('+', '', $user['contact_phone'])));
            $user_data = ['role_id'=>$role_data[$user['group_name']],
                'username'=>$user['lastname'].' '.$user['firstname'],
                'password'=>$password,
                'email'=>$user['email'],
                'modified'=>$user['date_upd'],
                'key'=>$key,
                'phone'=>str_replace('+', '', $user['contact_phone']),
                'city_id'=>$cities[$user['card_city']],
                'birth_date'=>$user['birthday'],
                'card_number'=>$user['card_number'],
                'referer_card_number'=>$user['referer_card_number']
                ];

            \CI::$APP->db->insert('users', $user_data);

        }
dd(111);
    }


    public function parse_file()
    {
        unlink($this->uploadDir . 'Upload UTF1.csv');
        $text = file_get_contents($this->uploadDir . 'users_2018-04-11_15_15_01.csv');
        $text = iconv('WINDOWS-1251', 'UTF-8', $text);


        $fp = fopen($this->uploadDir . 'Upload UTF1.csv', "a"); // Открываем файл в режиме записи
        fwrite($fp, $text);
        fclose($fp); //Закрытие файла
        $excel_file = $this->uploadDir . 'Upload UTF1.csv';


        if (file_exists($excel_file)) {
            $file = @fopen($excel_file, 'r');
        }
        $this->makeAttributesList();
        $cnt = 0;

        while (($row = fgetcsv($file, $length = null, $this->delimiter, $this->enclosure)) !== false) {

            $content[] = array_combine($this->possibleAttributes, array_map('trim', $row));
//            $content[] =  array_map('trim', $row);
            $cnt++;
        }
        fclose($file);
        unset($content['0']);

        $this->users_data = $content;
        return TRUE;
    }


    public function makeAttributesList()
    {
        if (!count($this->possibleAttributes)) {
            $this->possibleAttributes = [
                'card_number' => lang('card_number', 'import_export'),
                'card_city' => lang('card_city', 'import_export'),
                'referer_card_number' => lang('referer_card_number', 'import_export'),
                'firstname' => lang('firstname', 'import_export'),
                'lastname' => lang('lastname', 'import_export'),
                'email' => lang('email', 'import_export'),
                'contact_phone' => lang('contact_phone', 'import_export'),
                'contact_phone1' => lang('contact_phone1', 'import_export'),
                'birthday' => lang('birthday', 'import_export'),
                'date_upd' => lang('date_upd', 'import_export'),
                'group_name' => lang('group_name', 'import_export'),

            ];

        }
        return $this;
    }


    public function parse_fileBig()
    {
        unlink($this->uploadDir . 'TestBig.csv');
        $text = file_get_contents($this->uploadDir . $this->xlsFileName1);

//$text = mb_convert_encoding($text, 'UTF-8', 'AUTO');
//$text = mb_convert_encoding($text, 'UTF-8', 'ANSI');
//$text = mb_convert_encoding($text, 'UTF-8', 'WINDOWS-1251');
        $text = iconv('WINDOWS-1251', 'UTF-8', $text);


        $fp = fopen($this->uploadDir . 'TestBig.csv', "a"); // Открываем файл в режиме записи
        fwrite($fp, $text);
        fclose($fp); //Закрытие файла
        $excel_file = $this->uploadDir . 'TestBig.csv';
//        $excel_file = $this->uploadDir .$this->xlsFileName1;

        if (file_exists($excel_file)) {
            $file = @fopen($excel_file, 'r');
        }
//        $this->makeAttributesList();
        $cnt = 0;

        while (($row = fgetcsv($file, $length = null, $this->delimiter, $this->enclosure)) !== false) {

//            $content[] = array_combine($this->possibleAttributes, array_map('trim', $row));
            $content[] = array_map('trim', $row);
            $cnt++;
        }
        fclose($file);
        dd($content);
        unset($content['0']);

        $this->products_data = $content;
        return TRUE;
    }

    private function saveAsCSVFile()
    {

        $this->load->library(
            'upload',
            [
                'overwrite' => true,
                'upload_path' => $this->uploadDir,
                'allowed_types' => '*',
            ]
        );
        $this->convertXLStoCSV($this->uploadDir . $this->xlsFileName1);
    }

    private function convertXLStoCSV($excel_file1251)
    {

//        $excel_file= self::cp1251_to_utf8($excel_file1251);
//        $excel_file= iconv('CP1251', 'UTF-8', $excel_file1251);
//        $excel_file = mb_convert_encoding($excel_file1251, 'utf-8', mb_detect_encoding($excel_file1251));
//        $excel_file= utf8_encode($excel_file1251);

        $excel_file = $excel_file1251;
        $objReader = PHPExcel_IOFactory::createReaderForFile($excel_file);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($excel_file);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        dd($sheetData);
        foreach ($sheetData as $i) {
            foreach ($i as $j) {
                $toPrint .= '"' . str_replace('"', '""', $j) . '";';
            }
            $toPrint = rtrim($toPrint, ';') . PHP_EOL;
        }
        $filename = '1111111111111.csv';
        fopen($this->uploadDir . $filename, 'w+');
        if (is_writable($this->uploadDir . $filename)) {
            if (!$handle = fopen($this->uploadDir . $filename, 'w+')) {
                echo json_encode(['error' => Factor::ErrorFolderPermission]);
                exit;
            }

            write_file($this->uploadDir . $filename, $toPrint);

            fclose($handle);
        } else {
            showMessage(lang("The file {$filename} is not writable", 'admin'));
        }
    }

    private function cp1251_to_utf8($txt)
    {
        $in_arr = array(
            chr(208), chr(192), chr(193), chr(194),
            chr(195), chr(196), chr(197), chr(168),
            chr(198), chr(199), chr(200), chr(201),
            chr(202), chr(203), chr(204), chr(205),
            chr(206), chr(207), chr(209), chr(210),
            chr(211), chr(212), chr(213), chr(214),
            chr(215), chr(216), chr(217), chr(218),
            chr(219), chr(220), chr(221), chr(222),
            chr(223), chr(224), chr(225), chr(226),
            chr(227), chr(228), chr(229), chr(184),
            chr(230), chr(231), chr(232), chr(233),
            chr(234), chr(235), chr(236), chr(237),
            chr(238), chr(239), chr(240), chr(241),
            chr(242), chr(243), chr(244), chr(245),
            chr(246), chr(247), chr(248), chr(249),
            chr(250), chr(251), chr(252), chr(253),
            chr(254), chr(255)
        );

        $out_arr = array(
            chr(208) . chr(160), chr(208) . chr(144), chr(208) . chr(145),
            chr(208) . chr(146), chr(208) . chr(147), chr(208) . chr(148),
            chr(208) . chr(149), chr(208) . chr(129), chr(208) . chr(150),
            chr(208) . chr(151), chr(208) . chr(152), chr(208) . chr(153),
            chr(208) . chr(154), chr(208) . chr(155), chr(208) . chr(156),
            chr(208) . chr(157), chr(208) . chr(158), chr(208) . chr(159),
            chr(208) . chr(161), chr(208) . chr(162), chr(208) . chr(163),
            chr(208) . chr(164), chr(208) . chr(165), chr(208) . chr(166),
            chr(208) . chr(167), chr(208) . chr(168), chr(208) . chr(169),
            chr(208) . chr(170), chr(208) . chr(171), chr(208) . chr(172),
            chr(208) . chr(173), chr(208) . chr(174), chr(208) . chr(175),
            chr(208) . chr(176), chr(208) . chr(177), chr(208) . chr(178),
            chr(208) . chr(179), chr(208) . chr(180), chr(208) . chr(181),
            chr(209) . chr(145), chr(208) . chr(182), chr(208) . chr(183),
            chr(208) . chr(184), chr(208) . chr(185), chr(208) . chr(186),
            chr(208) . chr(187), chr(208) . chr(188), chr(208) . chr(189),
            chr(208) . chr(190), chr(208) . chr(191), chr(209) . chr(128),
            chr(209) . chr(129), chr(209) . chr(130), chr(209) . chr(131),
            chr(209) . chr(132), chr(209) . chr(133), chr(209) . chr(134),
            chr(209) . chr(135), chr(209) . chr(136), chr(209) . chr(137),
            chr(209) . chr(138), chr(209) . chr(139), chr(209) . chr(140),
            chr(209) . chr(141), chr(209) . chr(142), chr(209) . chr(143)
        );

        $txt = str_replace($in_arr, $out_arr, $txt);
        return $txt;
    }


    public function autoload()
    {
//        if (method_exists(\CMSFactory\Events::create(), 'onShopMakeOrder')) {
//            \CMSFactory\Events::create()->onShopMakeOrder()->setListener('save_order_data_module');
//        } else {
//            \CMSFactory\Events::create()->setListener('save_order_data_module', 'MakeOrder');
//        }

//        if (method_exists(\CMSFactory\Events::create(), 'onShopMakeOrder')) {
//            \CMSFactory\Events::create()->onOrderValidated()->setListener('save_order_data_module');
//        } else {
//            \CMSFactory\Events::create()->setListener('save_order_data_module', 'OrderValidated');
//        }

    }

    public function save_order_data_module($ord_user_data)
    {
        if (!self::$check_event) {
            $cart = BaseCart::getInstance();
            $old_cart = BaseCart::getInstance();
            unset($_SESSION['deleted_cart_items_morion']);
            session_start();

            foreach ($old_cart->getItems()['data'] as $apt_item) {

                $prod_IdDrugStore = SProductVariantsQuery::create()->findOneById($apt_item->id)->getIdDrugStore();
                if (!$prod_IdDrugStore || $prod_IdDrugStore == '' || $prod_IdDrugStore == NULL) {
                    $_SESSION['deleted_cart_items_morion'][] = $apt_item->getData()['id'];
                    $data = [
                        'instance' => CartItem::INSTANCE_PRODUCT,
                        'id' => $apt_item->getData()['id'],
                    ];
                    $old_cart->removeItem($data);

                    continue;
                }

                $prod_IdMorion = SProductVariantsQuery::create()->findOneById($apt_item->id)->getIdMorion();

                $apt_items[$apt_item->getData()['apt_number']][] = [$apt_item, $prod_IdMorion, $prod_IdDrugStore];
            }
            self::rezerv_in_morion($apt_items, $ord_user_data);
            self::$check_event = true;
        }
    }


    public function rezerv_in_morion($apt_items, $ord_user_data)
    {
//dd($apt_items, $ord_user_data);

        $accessKey1 = ModuleSettings::ofModule('parse_users')->get();

        $accessKey = 'AccessKey: ' . $accessKey1['accessKey'] . '';

        // $accessKey = 'AccessKey: b2dfabc564bd4652a61a772c7e42600e';

        $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, 'http://test-spho.pharmbase.com.ua/api/auth');
        curl_setopt($ch, CURLOPT_URL, 'https://spho.pharmbase.com.ua/api/auth');


        // curl_setopt($ch, CURLOPT_URL, 'https://spho.pharmbase.com.ua/api/auth');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $headers = [
            $accessKey
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $server_output = curl_exec($ch);
        curl_close($ch);
        $info = curl_getinfo($ch);


        $old_cart = BaseCart::getInstance();
        foreach ($apt_items as $key => $apt_cart_item) {

            $ch1[$key] = curl_init();
            // curl_setopt($ch1[$key], CURLOPT_URL, 'http://test-spho.pharmbase.com.ua/api/nats/get-reserve');
            curl_setopt($ch1[$key], CURLOPT_URL, 'https://spho.pharmbase.com.ua/api/nats/get-reserve');


            curl_setopt($ch1[$key], CURLOPT_RETURNTRANSFER, true);
            foreach ($apt_cart_item as $one_cart_item) {

                $data_prod_to_morion[$key][] = ['id' => (string)$one_cart_item[1], 'quant' => $one_cart_item[0]->getData()[quantity],
                    // 'price' =>(float)$one_cart_item[0]->getData()[price]/*$one_cart_item[0]->getData()[price] */];
                    'inet_price' => (float)$one_cart_item[0]->getData()[price]/*$one_cart_item[0]->getData()[price] */];

            }

            $postfields[$key] = '{
                  "access_key":  "' . $accessKey1[accessKey] . '", 
                  "phone": "' . $ord_user_data[user_data][phone] . '",
                  "id_structure": "' . $apt_cart_item[0][2] . '",
                  "data": ' . json_encode($data_prod_to_morion[$key]) . '
                }';

            $headers1[$key] = [
                'Token: ' . json_decode($server_output)->token . ''
            ];
            curl_setopt($ch1[$key], CURLOPT_HTTPHEADER, ['Token: ' . json_decode($server_output)->token . '']);
            curl_setopt($ch1[$key], CURLOPT_HTTPHEADER, $headers1[$key]);
            curl_setopt($ch1[$key], CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch1[$key], CURLOPT_POSTFIELDS, $postfields[$key]);
            // curl_setopt($ch1[$key], CURLOPT_TIMEOUT_MS, 1000);

            $server_output1[$key] = curl_exec($ch1[$key]);
            $err = curl_error($ch1[$key]);

            curl_close($curl);
            // dd($server_output1[$key], $err);
            $delimiter = '-------------------------------------------------------------------------------------------------';
            if (!file_exists('morion_api_req.txt')) {
                $req = 'Запрос на Морион ' . PHP_EOL . date("Y-m-d H:i:s") . PHP_EOL . $postfields[$key] . PHP_EOL . PHP_EOL;
                $resp = 'Ответ от Морион ' . PHP_EOL . date("Y-m-d H:i:s") . PHP_EOL . $server_output1[$key] . PHP_EOL . $delimiter . PHP_EOL . PHP_EOL;

                file_put_contents('morion_api_req.txt', $req . $resp);
            } else {
                $content_file = file_get_contents('morion_api_req.txt');

                $req = 'Запрос на Морион ' . PHP_EOL . date("Y-m-d H:i:s") . PHP_EOL . $postfields[$key] . PHP_EOL . PHP_EOL;
                $resp = 'Ответ от Морион ' . PHP_EOL . date("Y-m-d H:i:s") . PHP_EOL . $server_output1[$key] . PHP_EOL . $delimiter . PHP_EOL . PHP_EOL;

                file_put_contents('morion_api_req.txt', $content_file . $req . $resp);
            }
            curl_close($ch1[$key]);

            if (!$server_output1[$key]) {

                foreach ($apt_cart_item as $one_cart_item0) {
                    $_SESSION['deleted_cart_items_morion'] = [$one_cart_item0[0]->getData()['id']];
                    $data = [
                        'instance' => CartItem::INSTANCE_PRODUCT,
                        'id' => $one_cart_item0[0]->getData()['id'],
                    ];
                    $old_cart->removeItem($data);
                    continue;
                }
            }

            if (json_decode($server_output1[$key])->data && json_decode($server_output1[$key])->data != null) {

                foreach ($apt_cart_item as $one_cart_item0) {
                    $_SESSION['deleted_cart_items_morion'][] = $one_cart_item0[0]->getData()['id'];
                    $data1 = [
                        'instance' => CartItem::INSTANCE_PRODUCT,
                        'id' => $one_cart_item0[0]->getData()['id'],
                    ];
                    $old_cart->removeItem($data1);
                    continue;
                }
            }

            if (json_decode($server_output1[$key])->order_no && json_decode($server_output1[$key])->order_no != null) {

                $apt_cart_item['0'] += ['morion_order' => json_decode($server_output1[$key])->order_no, 'order_exp' => json_decode($server_output1[$key])->order_exp];
                $apt_cart_item['0']['0']->data += ['morion_order' => json_decode($server_output1[$key])->order_no, 'order_exp' => json_decode($server_output1[$key])->order_exp];
            }

        }

    }


    public function _install()
    {
        $this->load->dbforge();
        ($this->dx_auth->is_admin()) OR exit;
        $this->db
            ->where('identif', 'parse_users')
            ->update(
                'components', [
                    'settings' => '',
                    'enabled' => 1,
                    'autoload' => 1
                ]
            );

        return TRUE;
    }

    public function _deinstall()
    {
        $this->load->dbforge();
        ($this->dx_auth->is_admin()) OR exit;
    }

}

/*
  Родительский класс для XML обработчиков.
 */




