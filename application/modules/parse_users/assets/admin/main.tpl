

<div class="container">
    <section class="mini-layout">
        <div class="frame_title clearfix">
            <div class="pull-left">
                <span class="help-inline"></span>
                <span class="title">{lang('Настройка соединения', 'parse_brain')}</span>
            </div>
            <div class="pull-right">
                <div class="d-i_b">
                    <span class="help-inline"></span>
                    <a href="{$BASE_URL}admin/components/modules_table" class="t-d_n m-r_15 ">
                        <span class="f-s_14">←</span>
                        <span class="t-d_u">{lang('Back', 'parse_brain')}</span>
                    </a>

                    <button type="button"
                            class="btn btn-small btn-primary action_on formSubmit"
                            data-form="#wishlist_settings_form"
                            data-action="tomain">
                        <i class="icon-ok"></i>{lang('Save', 'parse_brain')}
                    </button>
                </div>
            </div>
        </div>

        <div class="row-fluid m-t_20">
            <form method="post" action="{site_url('admin/components/cp/parse_users/save')}"
                  class="form-horizontal"
                  id="wishlist_settings_form">
                <div class="span6">
                    <table class="table table-striped table-bordered table-hover table-condensed t-l_a">
                        <thead>
                        <tr>
                            <th colspan="6">
                                {lang('Настройка соединения', 'parse_brain')}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6">
                                <div class="inside_padd">


                                    <div class="control-group">
                                        <label class="control-label"
                                               for="settings[login]">{lang('AccessKey', 'parse_brain')}
                                            :</label>
                                        <div class="controls">
                                            <input name="settings[accessKey]" id="settings[login]"
                                                   value="{$settings['accessKey']}" type="text"/>
                                        </div>
                                    </div>


                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                {form_csrf()}
            </form>
        </div>
    </section>
</div>

