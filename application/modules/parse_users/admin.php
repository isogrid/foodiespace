<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
use CMSFactory\ModuleSettings;
use Category\CategoryApi;
use Currency\Currency;
use Currency\AdminCurrency;
use Products\ProductApi; 
use SCategoryQuery;
/**
 * Image CMS
 * Sample Module Admin
 */
class Admin extends  ShopAdminController
{
    private $uploadDir = './uploads/';
    private $csvFileName = 'comments.csv';
    public $delimiter = ';';
    /**
     * Attributes
     * @var array
     */
    public $attributes = '';
    /**
     * Possible attributes
     * @var array
     */
    public $possibleAttributes = [];


    /**
     * CSV enclosure
     * @var string
     */
    public $enclosure = '"';

    public function __construct()
    {
        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('parse_yandex_xml');
    }




    public function index()
    {
//        \CMSFactory\assetManager::create()
//            ->setData('settings', ModuleSettings::ofModule('parse_users')->get())
//
//            ->renderAdmin('main');
    }


    public function save_categiries_settings(){

        if ($this->input->post()) {
            if (ModuleSettings::ofModule('parse_brain')->set('categories_to_parce_prc', $this->input->post('categories_to_parce_prc'))) {
                showMessage(lang('Settings saved', 'parse_brain'), lang('Message', 'parse_brain'));
            }
            $this->cache->delete_all();
        }
    }



    public function save(){

        if ($this->input->post()) {
            if (ModuleSettings::ofModule('parse_users')->set($this->input->post('settings'))) {
                showMessage(lang('Settings saved', 'parse_users'), lang('Message', 'parse_users'));
            }
            $this->cache->delete_all();
        }
    }



}