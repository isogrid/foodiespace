<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
use CMSFactory\ModuleSettings;
use Category\CategoryApi;
use Currency\Currency;
use Currency\AdminCurrency;
use Products\ProductApi; 

/**
 * Image CMS
 * Sample Module Admin
 */
class Admin extends  ShopAdminController
{
    private $uploadDir = './uploads/';
    private $csvFileName = 'comments.csv';
    public $delimiter = ';';
    /**
     * Attributes
     * @var array
     */
    public $attributes = '';
    /**
     * Possible attributes
     * @var array
     */
    public $possibleAttributes = [];


    /**
     * CSV enclosure
     * @var string
     */
    public $enclosure = '"';

    public function __construct()
    {
        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('parse_yandex_xml');
    }


    public function save_file()
    {

        if (count($_FILES)) {

            $this->saveCSVFile();
            chmod($this->uploadDir . $this->csvFileName, 0777);
            $path = $this->uploadDir . strtr($_FILES['userfile']['name'], [' ' => '_']);
            if (isset($path)) {
                $this->lib_admin->log(lang('Loaded import file', 'import_export') . '. File: ' . $_FILES['userfile']['name']);
                unlink($path);
            }
        }
    }

    private function saveCSVFile()
    {
//        $this->takeFileName();

        $this->csvFileName;

        $this->load->library(
            'upload',
            [
                'overwrite' => true,
                'upload_path' => $this->uploadDir,
                'allowed_types' => '*',
            ]
        );

        $fileExt = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);
        if (!in_array($fileExt, ['csv', 'xls', 'xlsx'])) {
            echo json_encode(['error' => lang('Wrong file type. Only csv|xls|xlsx')]);
            return;
        }

        if ($this->upload->do_upload('userfile')) {
            $data = $this->upload->data();

            if (($data['file_ext'] === '.xls') || ($data['file_ext'] === '.xlsx')) {
                $this->convertXLStoCSV($data['full_path']);
                unlink($this->uploadDir . $data['client_name']);
            } else {

                rename($this->uploadDir . str_replace(' ', '_', $data['client_name']), $this->uploadDir . $this->csvFileName);
            }

//            $this->configureImportProcess();
        } else {
            echo json_encode(['error' => $this->upload->display_errors()]);
        }
    }

    private function convertXLStoCSV($excel_file = '')
    {
        $objReader = PHPExcel_IOFactory::createReaderForFile($excel_file);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($excel_file);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        foreach ($sheetData as $i) {
            foreach ($i as $j) {
                $toPrint .= '"' . str_replace('"', '""', $j) . '";';
            }
            $toPrint = rtrim($toPrint, ';') . PHP_EOL;
        }
        $filename = $this->csvFileName;
        fopen($this->uploadDir . $filename, 'w+');
        if (is_writable($this->uploadDir . $filename)) {
            if (!$handle = fopen($this->uploadDir . $filename, 'w+')) {
                echo json_encode(['error' => Factor::ErrorFolderPermission]);
                exit;
            }

            write_file($this->uploadDir . $filename, $toPrint);

            fclose($handle);
        } else {
            showMessage(lang("The file {$filename} is not writable", 'admin'));
        }
    }

    /**
     * @return CI_User_agent
     */
    public function parse_file()
    {
        if (file_exists('./uploads/comments.csv')) {
            $file = @fopen('./uploads/comments.csv', 'r');
        }
        $this->makeAttributesList();
        $file1 = fgetcsv($file, $length = null, $this->delimiter, $this->enclosure);
//        dd($file1);
//        if ((count($this->possibleAttributes) - count(array_diff($this->possibleAttributes, $file1))) == count($this->attributes)) {
//            $this->attributes = $file1;
//        } elseif (count($file1) === count($this->attributes)) {
//            rewind($file1);
//        } else {
////            ImportBootstrap::addMessage(Factor::ErrorPossibleAttrValues);
//            return FALSE;
//        }

//        if ($offers > 0) {
//            $positionStart = $offers - $limit;
        $cnt = 0;
        $iOffer = 0;
        while (($row = fgetcsv($file, $length = null, $this->delimiter, $this->enclosure)) !== false) {
            //                if ($cnt != 0) {
            //                    if ($iOffer < $positionStart) {
            //                        $iOffer++;
            //                    }else{
            //                        $this->content[] = array_combine($this->attributes, array_map('trim', $row));
            //                    }
            //                }
            //                $cnt = 1;
            //            }
            if ($cnt != 0) {
                if ($iOffer) {
                    $iOffer++;
                } else {
                    $content[] = array_combine($this->possibleAttributes, array_map('trim', $row));
                }
            }
            if ($cnt >= 10000) {
                break;
            }
            $cnt++;
        }
//        } else {
//            $cnt = 0;
//            while (($row = fgetcsv($file, $this->maxRowLegth, $this->delimiter, $this->enclosure)) !== false) {
//                if ($cnt != 0) {
//                    $this->countProduct++;
//                }
//                $cnt = 1;
//            }
//            $_SESSION['countProductsInFile'] = $this->countProduct;
//        }
        fclose($file);
//        dd($content);
        $this->save_comments_data($content);
//        return TRUE;
    }

    /**
     * @return CI_User_agent
     */
    public function save_comments_data($content)
    {
        foreach ($content as $comment_datas){
            if(in_array('product', explode('/', $comment_datas['url'])) && in_array('shop', explode('/', $comment_datas['url']))){
                $product = SProductsQuery::create()->findByUrl(end(explode('/', $comment_datas['url'])))->getFirst()->id;
                $ci = &get_instance();
//                $comments_module = $ci->load->module('comments/commentsapi')->renderAsArray($ci->uri->uri_string());
//                $d = new DateTime($comment_datas['date'], new DateTimeZone('Europe/Rome'));
//                $comment_date = $d->getTimestamp();
                $comment_date = strtotime($comment_datas['date']);
                $comment_data = [
                    'module'     => 'shop',
                    'user_id'    => '',//$this->dx_auth->get_user_id(), // 0 if unregistered
                    'user_name'  => $comment_datas['author'],//$this->dx_auth->is_logged_in() ? $this->dx_auth->get_username() : $this->input->post('comment_author'),
                    'user_mail'  => '',//$this->dx_auth->is_logged_in() ? $email->email : $this->input->post('comment_email'),
                    'user_site'  => '',//$this->input->post('comment_site'),
                    'text'       => $comment_datas['comment_post'],
                    'text_plus'  => '',//$comment_text_plus,
                    'text_minus' => '',//$comment_text_minus,
                    'item_id'    => $product,
                    'status'     => 0,//$this->_comment_status(),
                    'agent'      => '',//$this->agent->agent_string(),
                    'user_ip'    => '',//$this->input->ip_address(),
                    'date'       => $comment_date,
                    'rate'       => 5,//$this->input->post('ratec'),
                    'parent'     => '0',//$this->input->post('comment_parent'),
                ];
                $this->db->insert('comments', $comment_data);

            }else{
                $article_core = $this->db
                    ->select('id')
                    ->where('url', end(explode('/', $comment_datas['url'])))
                    ->get('content')->row_array();

                if(is_array($article_core) && !empty($article_core)){

                    $comment_date = strtotime($comment_datas['date']);
                    $comment_data = [
                        'module'     => 'core',
                        'user_id'    => '',//$this->dx_auth->get_user_id(), // 0 if unregistered
                        'user_name'  => $comment_datas['author'],//$this->dx_auth->is_logged_in() ? $this->dx_auth->get_username() : $this->input->post('comment_author'),
                        'user_mail'  => '',//$this->dx_auth->is_logged_in() ? $email->email : $this->input->post('comment_email'),
                        'user_site'  => '',//$this->input->post('comment_site'),
                        'text'       => $comment_datas['comment_post'],
                        'text_plus'  => '',//$comment_text_plus,
                        'text_minus' => '',//$comment_text_minus,
                        'item_id'    => $article_core['id'],
                        'status'     => 0,//$this->_comment_status(),
                        'agent'      => '',//$this->agent->agent_string(),
                        'user_ip'    => '',//$this->input->ip_address(),
                        'date'       => $comment_date,
                        'rate'       => 5,//$this->input->post('ratec'),
                        'parent'     => '0',//$this->input->post('comment_parent'),
                    ];
                }
                $this->db->insert('comments', $comment_data);
            }
        }

    }

    public function makeAttributesList()
    {
        if (!count($this->possibleAttributes)) {
//            $this->possibleAttributes = [
//                'url' => lang('Сслка на страницу', 'import_export'),
//                'date' => lang('Дата комента', 'import_export'),
//                'author' => lang('Автор', 'import_export'),
//                'comment_post' => lang('Комент', 'import_export')
//            ];

            $this->possibleAttributes = [
                'url' => 'url',
                'date' => 'date',
                'author' => 'author',
                'comment_post' => 'comment_post'
            ];

        }
        return $this;
    }

    public function index()
    {
//        dd(ModuleSettings::ofModule('parse_yandex_xml')->get());
        $categiries = ShopCore::app()->SCategoryTree->getTree_();
        \CMSFactory\assetManager::create()
            ->setData('settings', ModuleSettings::ofModule('parse_yandex_xml')->get())
            ->setData('categories', $categiries)
            ->registerScript('script', TRUE)
            ->registerStyle('style', TRUE)
            ->renderAdmin('main');
    }

    public function save()
    {
        // dd($this->input->post());
        if ($this->input->post()) {
            if (ModuleSettings::ofModule('parse_yandex_xml')->set($this->input->post())) {
                showMessage(lang('Settings saved', 'parse_yandex_xml'), lang('Message', 'parse_yandex_xml'));
            }
            $this->cache->delete_all();
        }
    }

}