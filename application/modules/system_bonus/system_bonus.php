<?php


use Cart\BaseCart;
use Cart\CartItem;
use cmsemail\email;
use CMSFactory\assetManager;
use CMSFactory\Events;
use CMSFactory\ModuleSettings;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\PropelException;
use system_bonus\classes\CommentPaidProduct;
use system_bonus\classes\FirstPaidOrder;
use system_bonus\classes\Products;
use system_bonus\classes\RegisterOnSite;
use system_bonus\classes\RegularInterface;
use system_bonus\classes\SystemBonusFactory;
use system_bonus\models\SystemBonusDataValue;
use system_bonus\models\SystemBonusDataValueQuery;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;
use system_bonus\models\SystemBonusValues;

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Image CMS
 * Module SystemBonus
 *
 * @TODO: Сделать автоматическое время сгорания бонусов
 * @TODO: Сделать автоматическую генерацию конвертации бонуса
 *
 * @property System_bonus_model system_bonus_model
 * @author Stk <s.knysh@imagems.net>
 */
class System_bonus extends MY_Controller
{

    /**
     * @var array
     */
    private static $bonus_list = [];

    public function __construct() {

        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('system_bonus');
        $this->load->model('system_bonus_model');
    }

    public static function adminAutoload() {

        Events::create()->onShopAdminAjaxChangeOrderPaid()->setListener('ajaxPaidOrders');
        Events::create()->onShopAdminOrderEdit()->setListener('editOrder');

    }

    /**
     * @return void
     */
    public function autoload() {

        Events::create()->onShopMakeOrder()->setListener('makeOrderUseBonus');
        Events::create()->onAuthUserRegister()->setListener(SystemBonusFactory::REGISTER_ON_SITE);
        Events::create()->onFrontOrderUserCreate()->setListener(SystemBonusFactory::REGISTER_ON_SITE);
        Events::create()->setListener(SystemBonusFactory::COMMENT_PAID_PRODUCT, 'CommentsApi:newPost');

    }

    /**
     * @param SOrders $order
     * @return bool|int
     */
    private static function getOrderNeedBonus(SOrders $order) {

        $setting = ModuleSettings::ofModule('system_bonus')->get();

        if ($setting['consider_delivery']) {

            return self::convertOrderPriceToBonus($setting['consider_discount'] ? $order->getFinalPrice() : $order->getOriginPrice() + (float) $order->getDeliveryPrice());

        } else {

            return self::convertOrderPriceToBonus($setting['consider_discount'] ? $order->getTotalPrice() : $order->getOriginPrice());
        }

    }

    /**
     * @param array $data
     */
    public static function makeOrderUseBonus($data) {

        /** @var SOrders $order */
        $order = $data['order'];

        if (CI::$APP->input->post('use_bonus') != 'on') {
            return;
        }

        $order_need_bonus = (int) (self::getOrderNeedBonus($order) / 2);

        if ($order_need_bonus) {

            $user_bonus_model = self::checkExistBonus($order);

            if (!($user_bonus_model instanceof SystemBonusDataValue)) {
                return;
            }

            $settingPayPath = ModuleSettings::ofModule('system_bonus')->get('pay_part');

            if ($order_need_bonus <= $user_bonus_model->getCountBonus()) {

                self::paidOrderBonus($order, $user_bonus_model, $order_need_bonus);

            } elseif ($settingPayPath == 'on') {

                self::payPathPaidOrderBonus($order, $user_bonus_model);

            } else {

                $message = lang('Order do not paid to bonus for user', 'system_bonus') . '.  Id: ' . $order->getUserId() . ' Email:  ' . $order->getUserEmail() . ' Order_id:' . $order->getId() . lang('Count ned used bonus:', 'system_bonus') . $order_need_bonus;
                CI::$APP->lib_admin->log($message);
            }
        }

    }

    /**
     * @param SOrders $order
     * @param SystemBonusDataValue $userBonusModel
     * @throws PropelException
     */
    private static function payPathPaidOrderBonus(SOrders $order, SystemBonusDataValue $userBonusModel) {

        /** Set order paid and status paid for bonus */
        $order_status = ModuleSettings::ofModule('system_bonus')->get('order_status_id');

        $order->setStatus($order_status);
        $order->setTotalPrice($order->getTotalPrice() - $userBonusModel->getCountBonus());
        $order->save();

        self::addStatusHistory($order->getId(), $order->getUserId(), $order_status, $userBonusModel->getCountBonus());

        $email_data = [
                       'userName'     => $order->getUserFullName(),
                       'message'      => lang('Order pay path to bonus', 'system_bonus') . '. ' . lang('Used bonus', 'system_bonus') .
                ': ' . $userBonusModel->getCountBonus() . '. ' . lang('Need to paid', 'system_bonus') . ': ' . $order->getTotalPriceWithDelivery(),
                       'adminMessage' => lang('Order pay path to bonus', 'system_bonus') . '. ' . lang('Used bonus', 'system_bonus') .
                ': ' . $userBonusModel->getCountBonus() . ' ' . lang('order id', 'system_bonus') . ': ' . $order->getId() . lang('user id', 'system_bonus') . ': ' . $order->getUserId(),
                      ];

        self::sendEmail($email_data, $order->getUserEmail());
        unset($email_data);

        /** Делаем именно 0 что-бы оставалась инфа про юзера */
        $userBonusModel->setCountBonus(0);
        $userBonusModel->save();

        $message = lang('Order pay path to bonus for user', 'system_bonus') . '.  Id: ' . $order->getUserId() . ' Email:  ' . $order->getUserEmail() . ' Order_id:' . $order->getId();
        CI::$APP->lib_admin->log($message);

    }

    /**
     * @param SOrders $order
     * @param SystemBonusDataValue $userBonusModel
     * @param int $orderNeedBonus
     * @throws PropelException
     */
    private static function paidOrderBonus(SOrders $order, SystemBonusDataValue $userBonusModel, $orderNeedBonus) {

        $userBonusModel->setCountBonus($userBonusModel->getCountBonus() - $orderNeedBonus);
        $userBonusModel->save();

        /** Set order paid and status paid for bonus */
        $order_status = ModuleSettings::ofModule('system_bonus')->get('order_status_id');

        if (ModuleSettings::ofModule('system_bonus')->get('consider_delivery') || $order->getDeliveryPrice() == null) {

            $order->setPaid(true);

        } else {
//            $order->setTotalPrice($order->getTotalPrice() - $orderNeedBonus);
            $order->setUseBonuses(true);
        }

        $order->setStatus($order_status);
        $order->save();

        self::addStatusHistory($order->getId(), $order->getUserId(), $order_status, $orderNeedBonus);

        $email_data = [

                       'userName'     => $order->getUserFullName(),
                       'message'      => lang('Order paid to bonus', 'system_bonus') . '. ' . lang('Used bonus', 'system_bonus') .
                ': ' . $orderNeedBonus . ' ' . lang('You bonus balance', 'system_bonus') . ': ' . $userBonusModel->getCountBonus(),
                       'adminMessage' => lang('Order paid to bonus', 'system_bonus') . '. ' . lang('Used bonus', 'system_bonus') .
                ': ' . $orderNeedBonus . ' ' . lang('order id', 'system_bonus') . ': ' . $order->getId() . lang('user id', 'system_bonus') . ': ' . $order->getUserId(),
                      ];

        self::sendEmail($email_data, $order->getUserEmail());
        unset($email_data);

        $message = lang('Order paid to bonus for user', 'system_bonus') . '.  Id: ' . $order->getUserId() . ' Email:  ' . $order->getUserEmail() . ' Order_id:' . $order->getId() . lang('Count used bonus:', 'system_bonus') . $orderNeedBonus;
        CI::$APP->lib_admin->log($message);

    }

    /**
     * @param int $price
     * @return bool|int
     */
    private static function convertOrderPriceToBonus($price) {

        $settings_role = ModuleSettings::ofModule('system_bonus')->get('bonus_point_role');

        /** Бестолковая вещь, так как юзер создается при создании заказа, но пусть будет */
        if (CI::$APP->dx_auth->is_logged_in()) {

            $role_id = CI::$APP->dx_auth->get_role_id();

            if ($settings_role[$role_id]) {

                $count_convert = $settings_role[$role_id];

            } else {

                $count_convert = ModuleSettings::ofModule('system_bonus')->get('bonus_point');

            }

            return (int) $price / $count_convert;
        }
    }

    /**
     * @param SOrders $order
     * @return bool|SystemBonusDataValue
     */
    private static function checkExistBonus(SOrders $order) {

        $user_bonus_data = SystemBonusDataValueQuery::create()
            ->findOneByUserId($order->getUserId());

        if ($user_bonus_data == null) {
            return false;
        }

        return $user_bonus_data;

    }

    /**
     * Set status history
     *
     * @param int $order_id
     * @param int $user_id
     * @param int $order_status
     * @param int $used_bonus
     * @throws PropelException
     */
    public static function addStatusHistory($order_id, $user_id, $order_status, $used_bonus) {

        $order_status_history = new SOrderStatusHistory();
        $order_status_history->setOrderId($order_id);
        $order_status_history->setStatusId($order_status);
        $order_status_history->setUserId($user_id);
        $order_status_history->setComment(lang('Used bonus', 'system_bonus') . ': ' . $used_bonus);
        $order_status_history->setDateCreated(time());
        $order_status_history->save();

    }

    /**
     * @param array $data
     * @param string $userEmail
     */
    public static function sendEmail(array $data, $userEmail) {

        $data['href'] = site_url('/shop/profile');

        email::getInstance()->sendEmail($userEmail, 'system_bonus', $data);

    }

    /**
     * @param array $data
     */
    public static function editOrder($data) {

        /** @var SOrders $model */
        $model = $data['model'];

        /** Бонус за первый оплаченый заказ */
        self::FirstPaidOrder($model);

        /** Если в редактировании модели статус совпадает со старым, шлем лесом  */
        if ($model->getPaid() && $model->getPaid() != $data['old_paid']) {

            self::setBonusList();

            $bonus = self::setOrderProductBonus($model->getOrderProducts());

            if ($bonus > 0) {

                self::addBonusToUser($model->getUserId(), $bonus);
            }
        }
    }

    /**
     * @param SOrders $data
     * @throws Exception
     */
    public static function FirstPaidOrder(SOrders $data) {

        /** @var FirstPaidOrder $bonus_data */
        $bonus_data = SystemBonusFactory::initial(SystemBonusFactory::FIRST_PAID_ORDER);

        $bonus_data->setSetting(
            [
             'user_id' => $data->getUserId(),
             'item_id' => $data->getId(),
            ]
        );

        $count_bonus = $bonus_data->getBonus();

        if ($count_bonus && $data->getUserId()) {

            self::addBonusToUser($data->getUserId(), $count_bonus);

            $bonus_user = SystemBonusDataValueQuery::create()
                ->findOneByUserId($data->getUserId());

            $email_data = [

                           'userName'     => $data->getUserFullName(),
                           'message'      => lang('You accrued bonuses', 'system_bonus') . ' ' . lang('for first paid order', 'system_bonus') . ': ' . site_url('/') .
                    lang('You bonus balance', 'system_bonus') . ': ' . $bonus_user->getCountBonus(),
                           'adminMessage' => lang('Accrued bonuses', 'system_bonus') . ' ' . lang('for first paid order', 'system_bonus') . '. ' . lang('user id', 'system_bonus') . ': ' .
                    $data->getUserId() . ', ' . lang('Order id', 'system_bonus') . ': ' . $data->getId(),
                          ];

            self::sendEmail($email_data, $data->getUserEmail());
            unset($email_data);

            $message = lang('Bonus set count for user', 'system_bonus') . '.  Id: ' . $data->getUserId() . ' Email:  ' . $data->getUserEmail() . ' Order_id:' . $data->getId() . lang('Add bonus:', 'system_bonus') . $count_bonus;
            CI::$APP->lib_admin->log($message);

        }

    }

    /**
     * @param $user_id
     * @param $count_bonus
     * @throws PropelException
     */
    private static function addBonusToUser($user_id, $count_bonus) {

        $model = SystemBonusDataValueQuery::create()
            ->filterByUserId($user_id)
            ->findOneOrCreate();

        if ($model->isNew()) {

            $model->setCountBonus($count_bonus);
        } else {
            $model->setCountBonus($model->getCountBonus() + $count_bonus);
        }

        $model->save();
    }

    /**
     * @param ObjectCollection $data
     * @return int
     */
    private static function setOrderProductBonus(ObjectCollection $data) {

        $bonus_count = 0;

        /** @var SOrderProducts $item */
        foreach ($data as $item) {

            $product = $item->getSProducts();
            $bonus_list = self::$bonus_list;

            if (count($bonus_list) > 0) {

                foreach ($bonus_list as $bonus_type) {

                    /** @var Products $t */
                    $t = SystemBonusFactory::initial($bonus_type);
                    $t->setOrderProducts($item);
                    $t->setProduct($product);

                    if (false === $bonus = $t->getBonus()) {
                        continue;
                    }

                    $bonus_count += $bonus;

                    unset($bonus, $product, $t);
                    break;
                }
            }
        }

        return $bonus_count;
    }

    /**
     *
     * @param array $data
     */
    public static function ajaxPaidOrders($data) {

        self::setBonusList();

        /** @var SOrders $item */
        foreach ($data['model'] as $item) {

            if ($data['paid'] == 1) {
                self::setPaidOrders($item);

            } else {
                self::setMinusOrderBonus($item);
            }
        }
    }

    /**
     * @param SOrders $order
     */
    private static function setPaidOrders(SOrders $order) {

        /** Бонус за первый оплаченый заказ */
        self::FirstPaidOrder($order);

        $bonus = self::setOrderProductBonus($order->getOrderProducts());

        if ($bonus > 0) {

            self::addBonusToUser($order->getUserId(), $bonus);

            $bonus_user = SystemBonusDataValueQuery::create()
                ->findOneByUserId($order->getUserId());

            $email_data = [

                           'userName'     => $order->getUserFullName(),
                           'message'      => lang('You accrued bonuses', 'system_bonus') . ' ' . lang('for paid order', 'system_bonus') . ': ' . site_url('/') .
                    lang('You bonus balance', 'system_bonus') . ': ' . $bonus_user->getCountBonus(),
                           'adminMessage' => lang('Accrued bonuses', 'system_bonus') . ' ' . lang('for paid order', 'system_bonus') . '. ' . lang('user id', 'system_bonus') . ': ' .
                    $order->getUserId() . ', ' . lang('Order id', 'system_bonus') . ': ' . $order->getId(),
                          ];

            self::sendEmail($email_data, $order->getUserEmail());
            unset($email_data);

            $message = lang('Bonus set count for user', 'system_bonus') . '.  Id: ' . $order->getUserId() . ' Email:  ' . $order->getUserEmail() . ' Order_id:' . $order->getId() . lang('Add bonus:', 'system_bonus') . $bonus;
            CI::$APP->lib_admin->log($message);

        }
    }

    /**
     * Отнимает бонусы
     *
     * @param SOrders $order
     */
    private static function setMinusOrderBonus(SOrders $order) {

        $bonus = self::setOrderProductBonus($order->getOrderProducts());

        if ($bonus > 0) {

            self::minusBonusToUser($order->getUserId(), $bonus);

            $message = lang('Bonus count minus for user', 'system_bonus') . '.  Id: ' . $order->getUserId() . ' Email:  ' . $order->getUserEmail() . ' Order_id:' . $order->getId() . ' Minus:' . $bonus;
            CI::$APP->lib_admin->log($message);

        }
    }

    /**
     * @param $user_id
     * @param $count_bonus
     * @throws PropelException
     */
    private static function minusBonusToUser($user_id, $count_bonus) {

        $model = SystemBonusDataValueQuery::create()
            ->findOneByUserId($user_id);

        if ($model) {

            $model->setCountBonus($model->getCountBonus() - $count_bonus);
        }

        $model->save();
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public static function CommentPaidProduct(array $data) {

        /** @var CommentPaidProduct $bonus_data */
        $bonus_data = SystemBonusFactory::initial(SystemBonusFactory::COMMENT_PAID_PRODUCT);

        $user_id = $data['user_info']['id'];

        $bonus_data->setSetting(['user_id' => $user_id, 'item_id' => $data['item_id']]);
        $count_bonus = $bonus_data->getBonus();

        if ($count_bonus && $user_id) {

            self::addBonusToUser($user_id, $count_bonus);

            $bonus_user = SystemBonusDataValueQuery::create()
                ->findOneByUserId($user_id);

            $email_data = [

                           'userName'     => $data['user_info']['username'],
                           'message'      => lang('You accrued bonuses', 'system_bonus') . ' ' . lang('for comment paid product', 'system_bonus') . ': ' . site_url('/') .
                    lang('You bonus balance', 'system_bonus') . ': ' . $bonus_user->getCountBonus(),
                           'adminMessage' => lang('Accrued bonuses', 'system_bonus') . ' ' . lang('for comment paid product', 'system_bonus') . '. ' . lang('user id', 'system_bonus') . ': ' . $user_id,
                          ];

            self::sendEmail($email_data, $data['user_info']['email']);
            unset($email_data);

            $message = lang('Bonus count for user first comment paid product', 'system_bonus') . '.  Id: ' . $user_id . ' Email:  ' . $data['user_info']['email'] . ' Product_id:' . $data['item_id'] . lang('Add bonus:', 'system_bonus') . $count_bonus;
            CI::$APP->lib_admin->log($message);
        }

    }

    /**
     * @param array $data
     * @throws Exception
     */
    public static function RegisterOnSite(array $data) {

        /** @var RegisterOnSite $bonus_data */
        $bonus_data = SystemBonusFactory::initial(SystemBonusFactory::REGISTER_ON_SITE);
        $count_bonus = $bonus_data->getBonus();

        if ($count_bonus && $data['id_user']) {

            self::addBonusToUser($data['id_user'], $count_bonus);

            $email_data = [

                           'userName'     => $data['userName'],
                           'message'      => lang('You accrued bonuses', 'system_bonus') . ' ' . lang('for registration on site', 'system_bonus') . ': ' . site_url('/') .
                    lang('You bonus balance', 'system_bonus') . ': ' . $count_bonus,
                           'adminMessage' => lang('Accrued bonuses', 'system_bonus') . ' ' . lang('for registration on site', 'system_bonus') . '. ' . lang('user id', 'system_bonus') . ': ' . $data['id_user'],
                          ];

            self::sendEmail($email_data, $data['email']);
            unset($email_data);

            $message = lang('Bonus count for user registration', 'system_bonus') . '.  User_id: ' . $data['id_user'] . ' Email:  ' . $data['email'] . lang('Add bonus:', 'system_bonus') . $count_bonus;
            CI::$APP->lib_admin->log($message);

        }

    }

    /**
     * @param SProducts $product
     * @param SProductVariants $variant
     * @return bool|int
     * @throws Exception
     */
    public function getBonusForProductFront(SProducts $product, SProductVariants $variant) {

        self::setBonusList();

        $bonus_list = self::$bonus_list;

        if (count($bonus_list) > 0) {

            foreach ($bonus_list as $bonus_type) {

                /** Смотреть на интерфейс Products */
                $bonus = SystemBonusFactory::initial($bonus_type);
                $bonus->setUseFront(true);
                $bonus->setFrontInfo($product, $variant);

                if ($bonus = (int) $bonus->getBonus()) {

                    return $bonus;
                }
            }

            return false;
        }
    }

    /**
     * @param string $type
     * @param bool $model
     * @return false|int
     * @throws Exception
     */
    public function getBonusForTypeFront($type, $model = false) {

        switch ($type) {

            case SystemBonusFactory::REGISTER_ON_SITE     :
            case SystemBonusFactory::COMMENT_PAID_PRODUCT :
            case SystemBonusFactory::FIRST_PAID_ORDER     :

                /** @var RegularInterface $bonus */
                $bonus = SystemBonusFactory::initial($type);
                return $bonus->getRegularBonus($model);

        }

        return false;
    }

    /**
     * @return void
     * @throws PropelException
     */
    private static function setBonusList() {

        $t = SystemBonusListQuery::create()
            ->filterByActive(true)
            ->orderByPosition()
            ->find();

        $data = [];

        /** @var SystemBonusList $item */
        foreach ($t as $item) {

            if (count($item->getSystemBonusValuess()->getData()) > 0) {
                $data[] = $item->getType();
            }

        }
        $data = array_unique($data);

        if (count($data) > 0) {

            self::$bonus_list = $data;
        }

    }

    /**
     *
     * @return void
     */
    public function getCartInput($formID) {

        if ($this->dx_auth->is_logged_in()) {

            $bonus = SystemBonusDataValueQuery::create()
                ->findOneByUserId($this->dx_auth->get_user_id());

            $payPath = ModuleSettings::ofModule('system_bonus')->get('pay_part');

            $cartPrice = self::convertOrderPriceToBonus(BaseCart::getInstance()->getTotalPrice());

            $bonusCount = 0;

            if ($bonus !== null) {

                $bonusCount = $bonus->getCountBonus();
            }

            /** Проверка, включена ли оплата частями и показывать ли чекбокс */
            $showCheckBox = false;

            if ($cartPrice <= $bonusCount) {
                $showCheckBox = true;
            } else {
                if ($bonusCount > 0) {
                    $showCheckBox = (bool) $payPath;
                }
            }

            $template = assetManager::create()
                ->setData(
                    [
                     'bonus'        => $bonusCount,
                     'showCheckBox' => $showCheckBox,
                     'formID'      => $formID
                    ]
                )
                ->fetchTemplate('cart_input', false);

            echo $template;

        }
    }

    /**
     * @param null|int $id
     * @return int
     */
    public function getBonusForUser($id = null) {

        if ($id === null) {
            $id = $this->dx_auth->get_user_id();
        }

        $bonus = SystemBonusDataValueQuery::create()
            ->findOneByUserId($id);

        return $bonus !== null ? $bonus->getCountBonus() : 0;
    }

    /**
     * @TODO: добавить заполнения rbac ролей.
     *
     * смотреть schema.xml
     */
    public function _install() {

        $this->load->dbforge();

        $this->system_bonus_model->insertEmailPattern();

        $modSystemBonusList = new SystemBonusList();
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field($modSystemBonusList->getDbFields());
        $this->dbforge->create_table('mod_system_bonus_list', TRUE);

        $modSystemBonusValues = new SystemBonusValues();
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field($modSystemBonusValues->getDbFields());
        $this->dbforge->create_table('mod_system_bonus_values', TRUE);

        $modSystemBonusDataValues = new SystemBonusDataValue();
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field($modSystemBonusDataValues->getDbFields());
        $this->dbforge->create_table('mod_system_bonus_data_value', TRUE);

        $this->db->where('name', 'system_bonus')
            ->update('components', ['autoload' => '1', 'enabled' => '1']);

        $orderStatus = $this->addOderStatusInstall();

        ModuleSettings::ofModule('system_bonus')->set(['order_status_id' => $orderStatus->getId()]);
    }

    /**
     * @return SOrderStatuses
     * @throws PropelException
     */
    private function addOderStatusInstall() {

        /** Добавление нового статуса заказа */
        $locale = MY_Controller::defaultLocale();
        $orderStatus = SOrderStatusesQuery::create()
            ->useI18nQuery($locale)
            ->filterByName('Оплачен бонусом')
            ->endUse()
            ->findOne();

        if ($orderStatus == null) {

            $orderStatus = new SOrderStatuses();
            $orderStatus->setName('Оплачен бонусом');
            $orderStatus->setColor('#67cf21');
            $orderStatus->setFontcolor('#ffffff');
            $orderStatus->setLocale($locale);

            $posModel = SOrderStatusesQuery::create()
                ->select('Position')
                ->orderByPosition('Desc')
                ->findOne();

            $orderStatus->setPosition($posModel[0] + 1);
            $orderStatus->save();
        }

        return $orderStatus;
    }

    public function _deinstall() {

        $this->load->dbforge();
        $this->dbforge->drop_table('mod_system_bonus_list');
        $this->dbforge->drop_table('mod_system_bonus_values');
        $this->dbforge->drop_table('mod_system_bonus_data_value');

        $this->system_bonus_model->dropEmailPatterns();
    }

    /**
     * @return int
     */
    public function getCartTotalBonus() {

        $bonusCount = 0;

        self::setBonusList();

        $bonusList = self::$bonus_list;

        if (count($bonusList) > 0) {

            $cartData = BaseCart::getInstance()->getItems('SProducts')['data'];

            /** @var CartItem $item */
            foreach ($cartData as $item) {

                foreach ($bonusList as $bonus_type) {

                    /** @var Products $bonus */
                    $bonus = SystemBonusFactory::initial($bonus_type);
                    $bonus->setUseFront(true);

                    if (false === $countBonus = $bonus->getCartBonus($item)) {
                        continue;
                    }

                    $bonusCount += $countBonus;

                    unset($bonus, $item, $countBonus);
                    break;
                }
            }

        }

        return (int) $bonusCount;

    }

    /**
     * @return int
     */
    public function getBonusRate() {

        if ($this->dx_auth->is_logged_in()) {

            $bonusRolePoint = ModuleSettings::ofModule('system_bonus')->get('bonus_point_role');

            if ($bonusRolePoint[$this->dx_auth->get_role_id()] != null) {

                return $bonusRolePoint[$this->dx_auth->get_role_id()];
            }

            return ModuleSettings::ofModule('system_bonus')->get('bonus_point') ?: 0;
        }

        return 0;
    }

}

/* End of file sample_module.php */