Инструкция по установке модуля.
===============================

вставить в корзине шаблона  <i class="icon-hdd"></i> **{module('system_bonus')->getCartInput()}**
 -------------
 
> **Note:** <i class="icon-refresh"></i> 
 (я вставлял в файл /templates/unishop/shop/includes/cart/cart_checkout.tpl)

-----------------

> - В настройках модуля настроить конвертацию бонусов в валюту по ролям пользователей.
> - Cоздать бонус, на категорию, товар и т.д.

----------------

Бонусы на товары выполняются тогда когда товар оплачивается в заказе.



#### <i class="icon-hdd"></i> Получить количиство бонусов на товар с любой точки сайта.
**{module('system_bonus')->getBonusForProductFront(SProducts $product, SProductVariants $variant)}**



#### <i class="icon-hdd"></i> Получить количиство бонусов по пользователю.
**{module('system_bonus')->getBonusForUser()}** 
> **Note:** <i class="icon-refresh"></i> Если передать $user_id вернет количество бонусов по конкретному пользователю

#### <i class="icon-hdd"></i> Получить общее количество бонусов в корзине
**{module('system_bonus')->getCartTotalBonus()}** 


#### <i class="icon-hdd"></i> Получить количиство по типу бонусов.
**{module('system_bonus')->getBonusForTypeFront($type, $model = false)}** 
> **Note:** <i class="icon-refresh"></i> Если передан $model = true, вернет модель бонуса.

> **Note:** <i class="icon-refresh"></i> Доступные параметры $type 
> -  CommentPaidProduct - Вернет количество бонусов за комментарий к оплаченому заказу
> -  FirstPaidOrder - Вернет количество бонусов за первый оплаченый заказ
> -  RegisterOnSite - Вернет количество бонусов за регистрацию на сайте
        
#### <i class="icon-hdd"></i> Получить курс бонуса относительно валюты
**{module('system_bonus')->getBonusRate()}** 
        