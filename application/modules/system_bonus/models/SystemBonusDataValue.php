<?php

namespace system_bonus\models;

use system_bonus\models\Base\SystemBonusDataValue as BaseSystemBonusDataValue;

/**
 * Skeleton subclass for representing a row from the 'mod_system_bonus_data_value' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemBonusDataValue extends BaseSystemBonusDataValue
{

    /**
     * @return array
     */
    public function getDbFields() {

        return [
                'id'          => [
                                  'type'           => 'INT',
                                  'constraint'     => 11,
                                  'auto_increment' => true,
                                 ],
                'user_id'     => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                  'unique'     => true,
                                 ],
                'count_bonus' => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                 ],
               ];

    }

}