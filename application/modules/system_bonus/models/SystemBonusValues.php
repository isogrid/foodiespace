<?php

namespace system_bonus\models;

use system_bonus\models\Base\SystemBonusValues as BaseSystemBonusValues;

/**
 * Skeleton subclass for representing a row from the 'mod_system_bonus_values' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemBonusValues extends BaseSystemBonusValues
{

    /**
     * @return array
     */
    public function getDbFields() {

        return [
                'id'           => [
                                   'type'           => 'INT',
                                   'constraint'     => 11,
                                   'auto_increment' => true,
                                  ],
                'sys_bonus_id' => [
                                   'type'       => 'INT',
                                   'constraint' => 11,
                                  ],
                'value'        => [
                                   'type'       => 'INT',
                                   'constraint' => 11,
                                  ],
                'value_type'   => [
                                   'type'       => 'VARCHAR',
                                   'constraint' => 100,
                                  ],
               ];

    }

}