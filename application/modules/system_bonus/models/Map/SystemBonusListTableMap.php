<?php

namespace system_bonus\models\Map;

use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;


/**
 * This class defines the structure of the 'mod_system_bonus_list' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SystemBonusListTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system_bonus.models.Map.SystemBonusListTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'Shop';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'mod_system_bonus_list';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\system_bonus\\models\\SystemBonusList';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'system_bonus.models.SystemBonusList';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'mod_system_bonus_list.id';

    /**
     * the column name for the type field
     */
    const COL_TYPE = 'mod_system_bonus_list.type';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'mod_system_bonus_list.name';

    /**
     * the column name for the count_bonus field
     */
    const COL_COUNT_BONUS = 'mod_system_bonus_list.count_bonus';

    /**
     * the column name for the price_type field
     */
    const COL_PRICE_TYPE = 'mod_system_bonus_list.price_type';

    /**
     * the column name for the active field
     */
    const COL_ACTIVE = 'mod_system_bonus_list.active';

    /**
     * the column name for the show_always field
     */
    const COL_SHOW_ALWAYS = 'mod_system_bonus_list.show_always';

    /**
     * the column name for the show_from field
     */
    const COL_SHOW_FROM = 'mod_system_bonus_list.show_from';

    /**
     * the column name for the show_to field
     */
    const COL_SHOW_TO = 'mod_system_bonus_list.show_to';

    /**
     * the column name for the position field
     */
    const COL_POSITION = 'mod_system_bonus_list.position';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Type', 'Name', 'CountBonus', 'PriceType', 'Active', 'ShowAlways', 'ShowFrom', 'ShowTo', 'Position', ),
        self::TYPE_CAMELNAME     => array('id', 'type', 'name', 'countBonus', 'priceType', 'active', 'showAlways', 'showFrom', 'showTo', 'position', ),
        self::TYPE_COLNAME       => array(SystemBonusListTableMap::COL_ID, SystemBonusListTableMap::COL_TYPE, SystemBonusListTableMap::COL_NAME, SystemBonusListTableMap::COL_COUNT_BONUS, SystemBonusListTableMap::COL_PRICE_TYPE, SystemBonusListTableMap::COL_ACTIVE, SystemBonusListTableMap::COL_SHOW_ALWAYS, SystemBonusListTableMap::COL_SHOW_FROM, SystemBonusListTableMap::COL_SHOW_TO, SystemBonusListTableMap::COL_POSITION, ),
        self::TYPE_FIELDNAME     => array('id', 'type', 'name', 'count_bonus', 'price_type', 'active', 'show_always', 'show_from', 'show_to', 'position', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Type' => 1, 'Name' => 2, 'CountBonus' => 3, 'PriceType' => 4, 'Active' => 5, 'ShowAlways' => 6, 'ShowFrom' => 7, 'ShowTo' => 8, 'Position' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'type' => 1, 'name' => 2, 'countBonus' => 3, 'priceType' => 4, 'active' => 5, 'showAlways' => 6, 'showFrom' => 7, 'showTo' => 8, 'position' => 9, ),
        self::TYPE_COLNAME       => array(SystemBonusListTableMap::COL_ID => 0, SystemBonusListTableMap::COL_TYPE => 1, SystemBonusListTableMap::COL_NAME => 2, SystemBonusListTableMap::COL_COUNT_BONUS => 3, SystemBonusListTableMap::COL_PRICE_TYPE => 4, SystemBonusListTableMap::COL_ACTIVE => 5, SystemBonusListTableMap::COL_SHOW_ALWAYS => 6, SystemBonusListTableMap::COL_SHOW_FROM => 7, SystemBonusListTableMap::COL_SHOW_TO => 8, SystemBonusListTableMap::COL_POSITION => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'type' => 1, 'name' => 2, 'count_bonus' => 3, 'price_type' => 4, 'active' => 5, 'show_always' => 6, 'show_from' => 7, 'show_to' => 8, 'position' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mod_system_bonus_list');
        $this->setPhpName('SystemBonusList');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\system_bonus\\models\\SystemBonusList');
        $this->setPackage('system_bonus.models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 11, null);
        $this->addColumn('type', 'Type', 'VARCHAR', true, 255, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('count_bonus', 'CountBonus', 'INTEGER', true, null, null);
        $this->addColumn('price_type', 'PriceType', 'INTEGER', true, 1, 1);
        $this->addColumn('active', 'Active', 'BOOLEAN', true, 1, null);
        $this->addColumn('show_always', 'ShowAlways', 'BOOLEAN', true, 1, null);
        $this->addColumn('show_from', 'ShowFrom', 'INTEGER', false, null, null);
        $this->addColumn('show_to', 'ShowTo', 'INTEGER', false, null, null);
        $this->addColumn('position', 'Position', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SystemBonusValues', '\\system_bonus\\models\\SystemBonusValues', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sys_bonus_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'SystemBonusValuess', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to mod_system_bonus_list     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SystemBonusValuesTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SystemBonusListTableMap::CLASS_DEFAULT : SystemBonusListTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SystemBonusList object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SystemBonusListTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SystemBonusListTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SystemBonusListTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SystemBonusListTableMap::OM_CLASS;
            /** @var SystemBonusList $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SystemBonusListTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SystemBonusListTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SystemBonusListTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SystemBonusList $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SystemBonusListTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_ID);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_TYPE);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_NAME);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_COUNT_BONUS);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_PRICE_TYPE);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_ACTIVE);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_SHOW_ALWAYS);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_SHOW_FROM);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_SHOW_TO);
            $criteria->addSelectColumn(SystemBonusListTableMap::COL_POSITION);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.type');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.count_bonus');
            $criteria->addSelectColumn($alias . '.price_type');
            $criteria->addSelectColumn($alias . '.active');
            $criteria->addSelectColumn($alias . '.show_always');
            $criteria->addSelectColumn($alias . '.show_from');
            $criteria->addSelectColumn($alias . '.show_to');
            $criteria->addSelectColumn($alias . '.position');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SystemBonusListTableMap::DATABASE_NAME)->getTable(SystemBonusListTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SystemBonusListTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SystemBonusListTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SystemBonusListTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SystemBonusList or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SystemBonusList object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusListTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \system_bonus\models\SystemBonusList) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SystemBonusListTableMap::DATABASE_NAME);
            $criteria->add(SystemBonusListTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SystemBonusListQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SystemBonusListTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SystemBonusListTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the mod_system_bonus_list table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SystemBonusListQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SystemBonusList or Criteria object.
     *
     * @param mixed               $criteria Criteria or SystemBonusList object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusListTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SystemBonusList object
        }

        if ($criteria->containsKey(SystemBonusListTableMap::COL_ID) && $criteria->keyContainsValue(SystemBonusListTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SystemBonusListTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SystemBonusListQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SystemBonusListTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SystemBonusListTableMap::buildTableMap();
