<?php
use system_bonus\classes\BrandBonus;
use system_bonus\classes\CategoriesBonus;
use system_bonus\classes\CommentPaidProduct;
use system_bonus\classes\FirstPaidOrder;
use system_bonus\classes\ProductBonus;
use system_bonus\classes\RegisterOnSite;
use system_bonus\classes\SystemBonusFactory;

/**
 * @property CI_DB_active_record $db
 * @property DX_Auth $dx_auth
 */
class System_bonus_model extends CI_Model
{

    private $valueType = [
                          SystemBonusFactory::CATEGORIES_BONUS => 'category',
                          SystemBonusFactory::BRAND_BONUS      => 'brand',
                          SystemBonusFactory::PRODUCT_BONUS    => 'product',
                         ];

    public $setting;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Возвращает список доступных значений,
     * где ключ имя класса, который будет задействован
     *
     * Метод Всея Руси!:)
     *
     * @return array
     */
    public function getAvailableTypes() {

        return [
                SystemBonusFactory::REGISTER_ON_SITE     => lang('Register on site', 'system_bonus'),
                SystemBonusFactory::FIRST_PAID_ORDER     => lang('First paid order', 'system_bonus'),
                SystemBonusFactory::COMMENT_PAID_PRODUCT => lang('Comment for paid product', 'system_bonus'),
                SystemBonusFactory::CATEGORIES_BONUS     => lang('Categories', 'system_bonus'),
                SystemBonusFactory::BRAND_BONUS          => lang('Brands', 'system_bonus'),
                SystemBonusFactory::PRODUCT_BONUS        => lang('Products', 'system_bonus'),
               ];

    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getValueType($key) {

        return $this->valueType[$key];

    }

    /**
     * @return array
     */
    public function getEmailPattern() {

        $data = [
                 'name'                 => 'system_bonus',
                 'type'                 => 'HTML',
                 'user_message_active'  => 1,
                 'admin_message_active' => 1,
                ];

        return $data;
    }

    /**
     * @return array
     */
    private function getEmailPatternI18n() {

        $data = [

                 'id'            => $this->db->insert_id(),
                 'locale'        => 'ru',
                 'theme'         => 'Система бонусов',
                 'user_message'  => '<p>Здравствуйте  $userName$ !<br /><br />$message$</p><p>Просмотреть информацию можете по следующей ссылке в вашем&nbsp;<a href="$href$" target="_blank">профиле</a></p><p>С Уважением команда imageCMS.</p>',
                 'admin_message' => '<p>Здравствуйте Administarator !<br /><br />$adminMessage$</p><p></p>',
                 'description'   => '<p>Отправка сообщений бонусной системы</p>',
                 'variables'     => 'a:4:{s:10:"$userName$";s:31:"Имя пользователя";s:8:"$mesage$";s:18:"Сообщение";s:6:"$href$";s:30:"Адрес на профиль";s:14:"$adminMessage$";s:47:"Сообщение администратору";}',
                ];

        return $data;
    }

    /**
     * @return void
     */
    public function dropEmailPatterns() {

        /** @var CI_DB_result $email_pattern */
        $email_pattern = $this->db->select('id')
            ->from('mod_email_paterns')
            ->where('name', 'system_bonus')
            ->get();

        if ($email_pattern->num_rows() > 0) {

            foreach ($email_pattern->result_array() as $item) {

                $this->db->where('id', $item['id']);
                $this->db->delete(['mod_email_paterns', 'mod_email_paterns_i18n']);
            }

        }
    }

    /**
     * @return void
     */
    public function insertEmailPattern() {

        //@TODO: реализовать шаблон отправки email уведомлений
        $pattern = $this->getEmailPattern();

        $this->db->insert('mod_email_paterns', $pattern);

        $pattern_i18n = $this->getEmailPatternI18n();

        $this->db->insert('mod_email_paterns_i18n', $pattern_i18n);

    }

}