<?php

namespace system_bonus\models;

use system_bonus\models\Base\SystemBonusDataValueQuery as BaseSystemBonusDataValueQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mod_system_bonus_data_value' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemBonusDataValueQuery extends BaseSystemBonusDataValueQuery
{

}