<?php

namespace system_bonus\models;

use Propel\Runtime\Exception\PropelException;
use system_bonus\models\Base\SystemBonusList as BaseSystemBonusList;

/**
 * Skeleton subclass for representing a row from the 'mod_system_bonus_list' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemBonusList extends BaseSystemBonusList
{

    /**
     * @throws PropelException
     */
    public function addToModel() {

        $post = \CI::$APP->input->post();

        $this->setName($post['name']);
        $this->setType($post['type']);
        $this->setCountBonus($post['count_bonus']);

        /** Boolean */
        $this->setShowAlways($post['show_always'] == 'on');
        $this->setActive($post['active'] == 'on');

        if ($post['price_type']) {
            $this->setPriceType($post['price_type']);
        }

        if ($post['show_always'] != 'on') {

            $this->setShowFrom($post['linked']['active_from'] ? strtotime($post['linked']['active_from']) : time());
            $this->setShowTo(strtotime($post['linked']['active_to']));
        }

        if ($this->isNew()) {
            $systemBonusList = SystemBonusListQuery::create()
                ->find();

            /** @var SystemBonusList $item */
            foreach ($systemBonusList as $item) {
                $item->setPosition($item->getPosition() + 1);
                $item->save();

            }
        }

        $this->save();
    }

    /**
     * @param bool|string $format
     * @return bool|int|string
     */
    public function getShowFrom($format = false) {

        $date = parent::getShowFrom();
        return $format ? $this->formatDate($date, $format) : $date;

    }

    /**
     * @param bool|string $format
     * @return bool|int|string
     */
    public function getShowTo($format = false) {

        $date = parent::getShowTo();
        return $format ? $this->formatDate($date, $format) : $date;

    }

    /**
     * Format date
     * @param $unixTimeStamp
     * @param bool $format
     * @return bool|string
     */
    private function formatDate($unixTimeStamp, $format = false) {
        $format = is_string($format) ? $format : 'd-m-Y';
        return $unixTimeStamp ? date($format, $unixTimeStamp) : null;
    }

    /**
     * @return array
     */
    public function rules() {

        $post = \CI::$APP->input->post();

        $fields = [
                   [
                    'field' => 'name',
                    'label' => lang('Title', 'admin'),
                    'rules' => 'required|trim',
                   ],
                   [
                    'field' => 'type',
                    'label' => lang('Type', 'admin'),
                    'rules' => 'required|trim',
                   ],
                   [
                    'field' => 'count_bonus',
                    'label' => lang('Count bonus', 'system_bonus'),
                    'rules' => 'required|trim|numeric',
                   ],
                  ];

        if ($post['show_always'] != 'on') {
            $fields[] = [

                         'field' => 'linked[active_to]',
                         'label' => lang('Date', 'admin'),
                         'rules' => 'required|trim',
                        ];
        }

        if ($post['type'] == 'categories' || $post['type'] == 'brands') {

            $fields[] = [

                         'field' => 'values[]',
                         'label' => lang('Some name input', 'admin'),
                         'rules' => 'required',
                        ];

        }

            return $fields;
    }

    /**
     * @return array
     */
    public function getDbFields() {

        return [
                'id'          => [
                                  'type'           => 'INT',
                                  'constraint'     => 11,
                                  'auto_increment' => true,
                                 ],
                'type'        => [
                                  'type'       => 'VARCHAR',
                                  'constraint' => 255,
                                 ],
                'name'        => [
                                  'type'       => 'VARCHAR',
                                  'constraint' => 255,
                                 ],
                'count_bonus' => [
                                  'type'       => 'INT',
                                  'constraint' => 30,
                                 ],
                'active'      => [
                                  'type'       => 'TINYINT',
                                  'constraint' => 1,
                                 ],
                'show_always' => [
                                  'type'       => 'TINYINT',
                                  'constraint' => 1,
                                 ],
                'show_from'   => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                  'null'       => true,
                                 ],
                'show_to'     => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                  'null'       => true,
                                 ],
                'position'    => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                  'default'    => '0',
                                 ],
                'price_type'  => [
                                  'type'       => 'INT',
                                  'constraint' => 11,
                                  'default'    => '1',
                                 ],

               ];

    }

}