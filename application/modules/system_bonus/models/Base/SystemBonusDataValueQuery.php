<?php

namespace system_bonus\models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use system_bonus\models\SystemBonusDataValue as ChildSystemBonusDataValue;
use system_bonus\models\SystemBonusDataValueQuery as ChildSystemBonusDataValueQuery;
use system_bonus\models\Map\SystemBonusDataValueTableMap;

/**
 * Base class that represents a query for the 'mod_system_bonus_data_value' table.
 *
 *
 *
 * @method     ChildSystemBonusDataValueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSystemBonusDataValueQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method     ChildSystemBonusDataValueQuery orderByCountBonus($order = Criteria::ASC) Order by the count_bonus column
 *
 * @method     ChildSystemBonusDataValueQuery groupById() Group by the id column
 * @method     ChildSystemBonusDataValueQuery groupByUserId() Group by the user_id column
 * @method     ChildSystemBonusDataValueQuery groupByCountBonus() Group by the count_bonus column
 *
 * @method     ChildSystemBonusDataValueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSystemBonusDataValueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSystemBonusDataValueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSystemBonusDataValueQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSystemBonusDataValueQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSystemBonusDataValueQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSystemBonusDataValue findOne(ConnectionInterface $con = null) Return the first ChildSystemBonusDataValue matching the query
 * @method     ChildSystemBonusDataValue findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSystemBonusDataValue matching the query, or a new ChildSystemBonusDataValue object populated from the query conditions when no match is found
 *
 * @method     ChildSystemBonusDataValue findOneById(int $id) Return the first ChildSystemBonusDataValue filtered by the id column
 * @method     ChildSystemBonusDataValue findOneByUserId(int $user_id) Return the first ChildSystemBonusDataValue filtered by the user_id column
 * @method     ChildSystemBonusDataValue findOneByCountBonus(int $count_bonus) Return the first ChildSystemBonusDataValue filtered by the count_bonus column *

 * @method     ChildSystemBonusDataValue requirePk($key, ConnectionInterface $con = null) Return the ChildSystemBonusDataValue by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusDataValue requireOne(ConnectionInterface $con = null) Return the first ChildSystemBonusDataValue matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusDataValue requireOneById(int $id) Return the first ChildSystemBonusDataValue filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusDataValue requireOneByUserId(int $user_id) Return the first ChildSystemBonusDataValue filtered by the user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusDataValue requireOneByCountBonus(int $count_bonus) Return the first ChildSystemBonusDataValue filtered by the count_bonus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusDataValue[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSystemBonusDataValue objects based on current ModelCriteria
 * @method     ChildSystemBonusDataValue[]|ObjectCollection findById(int $id) Return ChildSystemBonusDataValue objects filtered by the id column
 * @method     ChildSystemBonusDataValue[]|ObjectCollection findByUserId(int $user_id) Return ChildSystemBonusDataValue objects filtered by the user_id column
 * @method     ChildSystemBonusDataValue[]|ObjectCollection findByCountBonus(int $count_bonus) Return ChildSystemBonusDataValue objects filtered by the count_bonus column
 * @method     ChildSystemBonusDataValue[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SystemBonusDataValueQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \system_bonus\models\Base\SystemBonusDataValueQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Shop', $modelName = '\\system_bonus\\models\\SystemBonusDataValue', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSystemBonusDataValueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSystemBonusDataValueQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSystemBonusDataValueQuery) {
            return $criteria;
        }
        $query = new ChildSystemBonusDataValueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSystemBonusDataValue|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SystemBonusDataValueTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SystemBonusDataValueTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSystemBonusDataValue A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, user_id, count_bonus FROM mod_system_bonus_data_value WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSystemBonusDataValue $obj */
            $obj = new ChildSystemBonusDataValue();
            $obj->hydrate($row);
            SystemBonusDataValueTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSystemBonusDataValue|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id > 12
     * </code>
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusDataValueTableMap::COL_USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the count_bonus column
     *
     * Example usage:
     * <code>
     * $query->filterByCountBonus(1234); // WHERE count_bonus = 1234
     * $query->filterByCountBonus(array(12, 34)); // WHERE count_bonus IN (12, 34)
     * $query->filterByCountBonus(array('min' => 12)); // WHERE count_bonus > 12
     * </code>
     *
     * @param     mixed $countBonus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function filterByCountBonus($countBonus = null, $comparison = null)
    {
        if (is_array($countBonus)) {
            $useMinMax = false;
            if (isset($countBonus['min'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_COUNT_BONUS, $countBonus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countBonus['max'])) {
                $this->addUsingAlias(SystemBonusDataValueTableMap::COL_COUNT_BONUS, $countBonus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusDataValueTableMap::COL_COUNT_BONUS, $countBonus, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSystemBonusDataValue $systemBonusDataValue Object to remove from the list of results
     *
     * @return $this|ChildSystemBonusDataValueQuery The current query, for fluid interface
     */
    public function prune($systemBonusDataValue = null)
    {
        if ($systemBonusDataValue) {
            $this->addUsingAlias(SystemBonusDataValueTableMap::COL_ID, $systemBonusDataValue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mod_system_bonus_data_value table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusDataValueTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SystemBonusDataValueTableMap::clearInstancePool();
            SystemBonusDataValueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusDataValueTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SystemBonusDataValueTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SystemBonusDataValueTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SystemBonusDataValueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SystemBonusDataValueQuery
