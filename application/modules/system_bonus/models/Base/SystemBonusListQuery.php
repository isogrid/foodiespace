<?php

namespace system_bonus\models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use system_bonus\models\SystemBonusList as ChildSystemBonusList;
use system_bonus\models\SystemBonusListQuery as ChildSystemBonusListQuery;
use system_bonus\models\Map\SystemBonusListTableMap;
use system_bonus\models\Map\SystemBonusValuesTableMap;

/**
 * Base class that represents a query for the 'mod_system_bonus_list' table.
 *
 *
 *
 * @method     ChildSystemBonusListQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSystemBonusListQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildSystemBonusListQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildSystemBonusListQuery orderByCountBonus($order = Criteria::ASC) Order by the count_bonus column
 * @method     ChildSystemBonusListQuery orderByPriceType($order = Criteria::ASC) Order by the price_type column
 * @method     ChildSystemBonusListQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method     ChildSystemBonusListQuery orderByShowAlways($order = Criteria::ASC) Order by the show_always column
 * @method     ChildSystemBonusListQuery orderByShowFrom($order = Criteria::ASC) Order by the show_from column
 * @method     ChildSystemBonusListQuery orderByShowTo($order = Criteria::ASC) Order by the show_to column
 * @method     ChildSystemBonusListQuery orderByPosition($order = Criteria::ASC) Order by the position column
 *
 * @method     ChildSystemBonusListQuery groupById() Group by the id column
 * @method     ChildSystemBonusListQuery groupByType() Group by the type column
 * @method     ChildSystemBonusListQuery groupByName() Group by the name column
 * @method     ChildSystemBonusListQuery groupByCountBonus() Group by the count_bonus column
 * @method     ChildSystemBonusListQuery groupByPriceType() Group by the price_type column
 * @method     ChildSystemBonusListQuery groupByActive() Group by the active column
 * @method     ChildSystemBonusListQuery groupByShowAlways() Group by the show_always column
 * @method     ChildSystemBonusListQuery groupByShowFrom() Group by the show_from column
 * @method     ChildSystemBonusListQuery groupByShowTo() Group by the show_to column
 * @method     ChildSystemBonusListQuery groupByPosition() Group by the position column
 *
 * @method     ChildSystemBonusListQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSystemBonusListQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSystemBonusListQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSystemBonusListQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSystemBonusListQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSystemBonusListQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSystemBonusListQuery leftJoinSystemBonusValues($relationAlias = null) Adds a LEFT JOIN clause to the query using the SystemBonusValues relation
 * @method     ChildSystemBonusListQuery rightJoinSystemBonusValues($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SystemBonusValues relation
 * @method     ChildSystemBonusListQuery innerJoinSystemBonusValues($relationAlias = null) Adds a INNER JOIN clause to the query using the SystemBonusValues relation
 *
 * @method     ChildSystemBonusListQuery joinWithSystemBonusValues($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SystemBonusValues relation
 *
 * @method     ChildSystemBonusListQuery leftJoinWithSystemBonusValues() Adds a LEFT JOIN clause and with to the query using the SystemBonusValues relation
 * @method     ChildSystemBonusListQuery rightJoinWithSystemBonusValues() Adds a RIGHT JOIN clause and with to the query using the SystemBonusValues relation
 * @method     ChildSystemBonusListQuery innerJoinWithSystemBonusValues() Adds a INNER JOIN clause and with to the query using the SystemBonusValues relation
 *
 * @method     \system_bonus\models\SystemBonusValuesQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSystemBonusList findOne(ConnectionInterface $con = null) Return the first ChildSystemBonusList matching the query
 * @method     ChildSystemBonusList findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSystemBonusList matching the query, or a new ChildSystemBonusList object populated from the query conditions when no match is found
 *
 * @method     ChildSystemBonusList findOneById(int $id) Return the first ChildSystemBonusList filtered by the id column
 * @method     ChildSystemBonusList findOneByType(string $type) Return the first ChildSystemBonusList filtered by the type column
 * @method     ChildSystemBonusList findOneByName(string $name) Return the first ChildSystemBonusList filtered by the name column
 * @method     ChildSystemBonusList findOneByCountBonus(int $count_bonus) Return the first ChildSystemBonusList filtered by the count_bonus column
 * @method     ChildSystemBonusList findOneByPriceType(int $price_type) Return the first ChildSystemBonusList filtered by the price_type column
 * @method     ChildSystemBonusList findOneByActive(boolean $active) Return the first ChildSystemBonusList filtered by the active column
 * @method     ChildSystemBonusList findOneByShowAlways(boolean $show_always) Return the first ChildSystemBonusList filtered by the show_always column
 * @method     ChildSystemBonusList findOneByShowFrom(int $show_from) Return the first ChildSystemBonusList filtered by the show_from column
 * @method     ChildSystemBonusList findOneByShowTo(int $show_to) Return the first ChildSystemBonusList filtered by the show_to column
 * @method     ChildSystemBonusList findOneByPosition(int $position) Return the first ChildSystemBonusList filtered by the position column *

 * @method     ChildSystemBonusList requirePk($key, ConnectionInterface $con = null) Return the ChildSystemBonusList by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOne(ConnectionInterface $con = null) Return the first ChildSystemBonusList matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusList requireOneById(int $id) Return the first ChildSystemBonusList filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByType(string $type) Return the first ChildSystemBonusList filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByName(string $name) Return the first ChildSystemBonusList filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByCountBonus(int $count_bonus) Return the first ChildSystemBonusList filtered by the count_bonus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByPriceType(int $price_type) Return the first ChildSystemBonusList filtered by the price_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByActive(boolean $active) Return the first ChildSystemBonusList filtered by the active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByShowAlways(boolean $show_always) Return the first ChildSystemBonusList filtered by the show_always column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByShowFrom(int $show_from) Return the first ChildSystemBonusList filtered by the show_from column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByShowTo(int $show_to) Return the first ChildSystemBonusList filtered by the show_to column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusList requireOneByPosition(int $position) Return the first ChildSystemBonusList filtered by the position column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusList[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSystemBonusList objects based on current ModelCriteria
 * @method     ChildSystemBonusList[]|ObjectCollection findById(int $id) Return ChildSystemBonusList objects filtered by the id column
 * @method     ChildSystemBonusList[]|ObjectCollection findByType(string $type) Return ChildSystemBonusList objects filtered by the type column
 * @method     ChildSystemBonusList[]|ObjectCollection findByName(string $name) Return ChildSystemBonusList objects filtered by the name column
 * @method     ChildSystemBonusList[]|ObjectCollection findByCountBonus(int $count_bonus) Return ChildSystemBonusList objects filtered by the count_bonus column
 * @method     ChildSystemBonusList[]|ObjectCollection findByPriceType(int $price_type) Return ChildSystemBonusList objects filtered by the price_type column
 * @method     ChildSystemBonusList[]|ObjectCollection findByActive(boolean $active) Return ChildSystemBonusList objects filtered by the active column
 * @method     ChildSystemBonusList[]|ObjectCollection findByShowAlways(boolean $show_always) Return ChildSystemBonusList objects filtered by the show_always column
 * @method     ChildSystemBonusList[]|ObjectCollection findByShowFrom(int $show_from) Return ChildSystemBonusList objects filtered by the show_from column
 * @method     ChildSystemBonusList[]|ObjectCollection findByShowTo(int $show_to) Return ChildSystemBonusList objects filtered by the show_to column
 * @method     ChildSystemBonusList[]|ObjectCollection findByPosition(int $position) Return ChildSystemBonusList objects filtered by the position column
 * @method     ChildSystemBonusList[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SystemBonusListQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \system_bonus\models\Base\SystemBonusListQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Shop', $modelName = '\\system_bonus\\models\\SystemBonusList', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSystemBonusListQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSystemBonusListQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSystemBonusListQuery) {
            return $criteria;
        }
        $query = new ChildSystemBonusListQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSystemBonusList|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SystemBonusListTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SystemBonusListTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSystemBonusList A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, type, name, count_bonus, price_type, active, show_always, show_from, show_to, position FROM mod_system_bonus_list WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSystemBonusList $obj */
            $obj = new ChildSystemBonusList();
            $obj->hydrate($row);
            SystemBonusListTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSystemBonusList|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the count_bonus column
     *
     * Example usage:
     * <code>
     * $query->filterByCountBonus(1234); // WHERE count_bonus = 1234
     * $query->filterByCountBonus(array(12, 34)); // WHERE count_bonus IN (12, 34)
     * $query->filterByCountBonus(array('min' => 12)); // WHERE count_bonus > 12
     * </code>
     *
     * @param     mixed $countBonus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByCountBonus($countBonus = null, $comparison = null)
    {
        if (is_array($countBonus)) {
            $useMinMax = false;
            if (isset($countBonus['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_COUNT_BONUS, $countBonus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countBonus['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_COUNT_BONUS, $countBonus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_COUNT_BONUS, $countBonus, $comparison);
    }

    /**
     * Filter the query on the price_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPriceType(1234); // WHERE price_type = 1234
     * $query->filterByPriceType(array(12, 34)); // WHERE price_type IN (12, 34)
     * $query->filterByPriceType(array('min' => 12)); // WHERE price_type > 12
     * </code>
     *
     * @param     mixed $priceType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByPriceType($priceType = null, $comparison = null)
    {
        if (is_array($priceType)) {
            $useMinMax = false;
            if (isset($priceType['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_PRICE_TYPE, $priceType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priceType['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_PRICE_TYPE, $priceType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_PRICE_TYPE, $priceType, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive(true); // WHERE active = true
     * $query->filterByActive('yes'); // WHERE active = true
     * </code>
     *
     * @param     boolean|string $active The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (is_string($active)) {
            $active = in_array(strtolower($active), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the show_always column
     *
     * Example usage:
     * <code>
     * $query->filterByShowAlways(true); // WHERE show_always = true
     * $query->filterByShowAlways('yes'); // WHERE show_always = true
     * </code>
     *
     * @param     boolean|string $showAlways The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByShowAlways($showAlways = null, $comparison = null)
    {
        if (is_string($showAlways)) {
            $showAlways = in_array(strtolower($showAlways), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_ALWAYS, $showAlways, $comparison);
    }

    /**
     * Filter the query on the show_from column
     *
     * Example usage:
     * <code>
     * $query->filterByShowFrom(1234); // WHERE show_from = 1234
     * $query->filterByShowFrom(array(12, 34)); // WHERE show_from IN (12, 34)
     * $query->filterByShowFrom(array('min' => 12)); // WHERE show_from > 12
     * </code>
     *
     * @param     mixed $showFrom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByShowFrom($showFrom = null, $comparison = null)
    {
        if (is_array($showFrom)) {
            $useMinMax = false;
            if (isset($showFrom['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_FROM, $showFrom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showFrom['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_FROM, $showFrom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_FROM, $showFrom, $comparison);
    }

    /**
     * Filter the query on the show_to column
     *
     * Example usage:
     * <code>
     * $query->filterByShowTo(1234); // WHERE show_to = 1234
     * $query->filterByShowTo(array(12, 34)); // WHERE show_to IN (12, 34)
     * $query->filterByShowTo(array('min' => 12)); // WHERE show_to > 12
     * </code>
     *
     * @param     mixed $showTo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByShowTo($showTo = null, $comparison = null)
    {
        if (is_array($showTo)) {
            $useMinMax = false;
            if (isset($showTo['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_TO, $showTo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($showTo['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_TO, $showTo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_SHOW_TO, $showTo, $comparison);
    }

    /**
     * Filter the query on the position column
     *
     * Example usage:
     * <code>
     * $query->filterByPosition(1234); // WHERE position = 1234
     * $query->filterByPosition(array(12, 34)); // WHERE position IN (12, 34)
     * $query->filterByPosition(array('min' => 12)); // WHERE position > 12
     * </code>
     *
     * @param     mixed $position The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterByPosition($position = null, $comparison = null)
    {
        if (is_array($position)) {
            $useMinMax = false;
            if (isset($position['min'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_POSITION, $position['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($position['max'])) {
                $this->addUsingAlias(SystemBonusListTableMap::COL_POSITION, $position['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusListTableMap::COL_POSITION, $position, $comparison);
    }

    /**
     * Filter the query by a related \system_bonus\models\SystemBonusValues object
     *
     * @param \system_bonus\models\SystemBonusValues|ObjectCollection $systemBonusValues the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function filterBySystemBonusValues($systemBonusValues, $comparison = null)
    {
        if ($systemBonusValues instanceof \system_bonus\models\SystemBonusValues) {
            return $this
                ->addUsingAlias(SystemBonusListTableMap::COL_ID, $systemBonusValues->getSysBonusId(), $comparison);
        } elseif ($systemBonusValues instanceof ObjectCollection) {
            return $this
                ->useSystemBonusValuesQuery()
                ->filterByPrimaryKeys($systemBonusValues->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySystemBonusValues() only accepts arguments of type \system_bonus\models\SystemBonusValues or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SystemBonusValues relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function joinSystemBonusValues($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SystemBonusValues');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SystemBonusValues');
        }

        return $this;
    }

    /**
     * Use the SystemBonusValues relation SystemBonusValues object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \system_bonus\models\SystemBonusValuesQuery A secondary query class using the current class as primary query
     */
    public function useSystemBonusValuesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSystemBonusValues($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SystemBonusValues', '\system_bonus\models\SystemBonusValuesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSystemBonusList $systemBonusList Object to remove from the list of results
     *
     * @return $this|ChildSystemBonusListQuery The current query, for fluid interface
     */
    public function prune($systemBonusList = null)
    {
        if ($systemBonusList) {
            $this->addUsingAlias(SystemBonusListTableMap::COL_ID, $systemBonusList->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mod_system_bonus_list table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusListTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += $this->doOnDeleteCascade($con);
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SystemBonusListTableMap::clearInstancePool();
            SystemBonusListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusListTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SystemBonusListTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += $c->doOnDeleteCascade($con);

            SystemBonusListTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SystemBonusListTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Query classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param ConnectionInterface $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected function doOnDeleteCascade(ConnectionInterface $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $this
        $objects = ChildSystemBonusListQuery::create(null, $this)->find($con);
        foreach ($objects as $obj) {


            // delete related SystemBonusValues objects
            $query = new \system_bonus\models\SystemBonusValuesQuery;

            $query->add(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $obj->getId());
            $affectedRows += $query->delete($con);
        }

        return $affectedRows;
    }

} // SystemBonusListQuery
