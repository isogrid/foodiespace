<?php

namespace system_bonus\models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use system_bonus\models\SystemBonusValues as ChildSystemBonusValues;
use system_bonus\models\SystemBonusValuesQuery as ChildSystemBonusValuesQuery;
use system_bonus\models\Map\SystemBonusValuesTableMap;

/**
 * Base class that represents a query for the 'mod_system_bonus_values' table.
 *
 *
 *
 * @method     ChildSystemBonusValuesQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSystemBonusValuesQuery orderBySysBonusId($order = Criteria::ASC) Order by the sys_bonus_id column
 * @method     ChildSystemBonusValuesQuery orderByValue($order = Criteria::ASC) Order by the value column
 * @method     ChildSystemBonusValuesQuery orderByValueType($order = Criteria::ASC) Order by the value_type column
 *
 * @method     ChildSystemBonusValuesQuery groupById() Group by the id column
 * @method     ChildSystemBonusValuesQuery groupBySysBonusId() Group by the sys_bonus_id column
 * @method     ChildSystemBonusValuesQuery groupByValue() Group by the value column
 * @method     ChildSystemBonusValuesQuery groupByValueType() Group by the value_type column
 *
 * @method     ChildSystemBonusValuesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSystemBonusValuesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSystemBonusValuesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSystemBonusValuesQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSystemBonusValuesQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSystemBonusValuesQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSystemBonusValuesQuery leftJoinSystemBonusList($relationAlias = null) Adds a LEFT JOIN clause to the query using the SystemBonusList relation
 * @method     ChildSystemBonusValuesQuery rightJoinSystemBonusList($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SystemBonusList relation
 * @method     ChildSystemBonusValuesQuery innerJoinSystemBonusList($relationAlias = null) Adds a INNER JOIN clause to the query using the SystemBonusList relation
 *
 * @method     ChildSystemBonusValuesQuery joinWithSystemBonusList($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SystemBonusList relation
 *
 * @method     ChildSystemBonusValuesQuery leftJoinWithSystemBonusList() Adds a LEFT JOIN clause and with to the query using the SystemBonusList relation
 * @method     ChildSystemBonusValuesQuery rightJoinWithSystemBonusList() Adds a RIGHT JOIN clause and with to the query using the SystemBonusList relation
 * @method     ChildSystemBonusValuesQuery innerJoinWithSystemBonusList() Adds a INNER JOIN clause and with to the query using the SystemBonusList relation
 *
 * @method     \system_bonus\models\SystemBonusListQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSystemBonusValues findOne(ConnectionInterface $con = null) Return the first ChildSystemBonusValues matching the query
 * @method     ChildSystemBonusValues findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSystemBonusValues matching the query, or a new ChildSystemBonusValues object populated from the query conditions when no match is found
 *
 * @method     ChildSystemBonusValues findOneById(int $id) Return the first ChildSystemBonusValues filtered by the id column
 * @method     ChildSystemBonusValues findOneBySysBonusId(int $sys_bonus_id) Return the first ChildSystemBonusValues filtered by the sys_bonus_id column
 * @method     ChildSystemBonusValues findOneByValue(int $value) Return the first ChildSystemBonusValues filtered by the value column
 * @method     ChildSystemBonusValues findOneByValueType(string $value_type) Return the first ChildSystemBonusValues filtered by the value_type column *

 * @method     ChildSystemBonusValues requirePk($key, ConnectionInterface $con = null) Return the ChildSystemBonusValues by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusValues requireOne(ConnectionInterface $con = null) Return the first ChildSystemBonusValues matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusValues requireOneById(int $id) Return the first ChildSystemBonusValues filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusValues requireOneBySysBonusId(int $sys_bonus_id) Return the first ChildSystemBonusValues filtered by the sys_bonus_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusValues requireOneByValue(int $value) Return the first ChildSystemBonusValues filtered by the value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSystemBonusValues requireOneByValueType(string $value_type) Return the first ChildSystemBonusValues filtered by the value_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSystemBonusValues[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSystemBonusValues objects based on current ModelCriteria
 * @method     ChildSystemBonusValues[]|ObjectCollection findById(int $id) Return ChildSystemBonusValues objects filtered by the id column
 * @method     ChildSystemBonusValues[]|ObjectCollection findBySysBonusId(int $sys_bonus_id) Return ChildSystemBonusValues objects filtered by the sys_bonus_id column
 * @method     ChildSystemBonusValues[]|ObjectCollection findByValue(int $value) Return ChildSystemBonusValues objects filtered by the value column
 * @method     ChildSystemBonusValues[]|ObjectCollection findByValueType(string $value_type) Return ChildSystemBonusValues objects filtered by the value_type column
 * @method     ChildSystemBonusValues[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SystemBonusValuesQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \system_bonus\models\Base\SystemBonusValuesQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Shop', $modelName = '\\system_bonus\\models\\SystemBonusValues', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSystemBonusValuesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSystemBonusValuesQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSystemBonusValuesQuery) {
            return $criteria;
        }
        $query = new ChildSystemBonusValuesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSystemBonusValues|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SystemBonusValuesTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SystemBonusValuesTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSystemBonusValues A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sys_bonus_id, value, value_type FROM mod_system_bonus_values WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSystemBonusValues $obj */
            $obj = new ChildSystemBonusValues();
            $obj->hydrate($row);
            SystemBonusValuesTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSystemBonusValues|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sys_bonus_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySysBonusId(1234); // WHERE sys_bonus_id = 1234
     * $query->filterBySysBonusId(array(12, 34)); // WHERE sys_bonus_id IN (12, 34)
     * $query->filterBySysBonusId(array('min' => 12)); // WHERE sys_bonus_id > 12
     * </code>
     *
     * @see       filterBySystemBonusList()
     *
     * @param     mixed $sysBonusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterBySysBonusId($sysBonusId = null, $comparison = null)
    {
        if (is_array($sysBonusId)) {
            $useMinMax = false;
            if (isset($sysBonusId['min'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $sysBonusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sysBonusId['max'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $sysBonusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $sysBonusId, $comparison);
    }

    /**
     * Filter the query on the value column
     *
     * Example usage:
     * <code>
     * $query->filterByValue(1234); // WHERE value = 1234
     * $query->filterByValue(array(12, 34)); // WHERE value IN (12, 34)
     * $query->filterByValue(array('min' => 12)); // WHERE value > 12
     * </code>
     *
     * @param     mixed $value The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterByValue($value = null, $comparison = null)
    {
        if (is_array($value)) {
            $useMinMax = false;
            if (isset($value['min'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_VALUE, $value['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($value['max'])) {
                $this->addUsingAlias(SystemBonusValuesTableMap::COL_VALUE, $value['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_VALUE, $value, $comparison);
    }

    /**
     * Filter the query on the value_type column
     *
     * Example usage:
     * <code>
     * $query->filterByValueType('fooValue');   // WHERE value_type = 'fooValue'
     * $query->filterByValueType('%fooValue%', Criteria::LIKE); // WHERE value_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $valueType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterByValueType($valueType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($valueType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SystemBonusValuesTableMap::COL_VALUE_TYPE, $valueType, $comparison);
    }

    /**
     * Filter the query by a related \system_bonus\models\SystemBonusList object
     *
     * @param \system_bonus\models\SystemBonusList|ObjectCollection $systemBonusList The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function filterBySystemBonusList($systemBonusList, $comparison = null)
    {
        if ($systemBonusList instanceof \system_bonus\models\SystemBonusList) {
            return $this
                ->addUsingAlias(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $systemBonusList->getId(), $comparison);
        } elseif ($systemBonusList instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SystemBonusValuesTableMap::COL_SYS_BONUS_ID, $systemBonusList->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySystemBonusList() only accepts arguments of type \system_bonus\models\SystemBonusList or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SystemBonusList relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function joinSystemBonusList($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SystemBonusList');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SystemBonusList');
        }

        return $this;
    }

    /**
     * Use the SystemBonusList relation SystemBonusList object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \system_bonus\models\SystemBonusListQuery A secondary query class using the current class as primary query
     */
    public function useSystemBonusListQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSystemBonusList($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SystemBonusList', '\system_bonus\models\SystemBonusListQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSystemBonusValues $systemBonusValues Object to remove from the list of results
     *
     * @return $this|ChildSystemBonusValuesQuery The current query, for fluid interface
     */
    public function prune($systemBonusValues = null)
    {
        if ($systemBonusValues) {
            $this->addUsingAlias(SystemBonusValuesTableMap::COL_ID, $systemBonusValues->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mod_system_bonus_values table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusValuesTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SystemBonusValuesTableMap::clearInstancePool();
            SystemBonusValuesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SystemBonusValuesTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SystemBonusValuesTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SystemBonusValuesTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SystemBonusValuesTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SystemBonusValuesQuery
