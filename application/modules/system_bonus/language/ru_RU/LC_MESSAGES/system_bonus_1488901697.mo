��    `                            -  
   4     ?     G  /   L  !   |     �     �     �     �     �          (     =     D  
   K     V     d     }     �     �     �     �     �     �     �                    %      7     X     a     u     �     �     �  #   �  
   �     �     �  #   �     	     #	     7	     T	      l	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     
  !   +
     M
     c
     h
     y
     �
     �
     �
     �
     �
     �
     �
                    ,     >     S     Y  	   ^  
   h     s     �     �     �     �     �     �     �               -     2     ;     @     C     K  �  ]     -     M     ^     z     �  {   �  O     E   d  
   �  3   �  ,   �  O     C   f  *   �     �     �     �  %     ;   ,  #   h  B   �  #   �  9   �  \   -  @   �     �     �  ,   �     $     -     <  9   Y  "   �  (   �  (   �  *        3     D  ;   Y  '   �  %   �     �  M   �     I  (   Y  =   �  9   �  [   �  $   V  .   {     �  !   �     �     �       &     .   A  -   p  @   �  ,   �                @     `  4   |     �  !   �  (   �  .        >     N     [  .   y  :   �  0   �          %  %   ,  '   R  9   z  (   �     �  &   �  "        @  >   X  /   �  "   �  +   �               )     :     =  *   Y   Accrued bonuses Active Add bonus: Article Base Bonus count for user first comment paid product Bonus count for user registration Bonus count minus for user Bonus point Bonus point for role Bonus point for user Bonus point of default currency Bonus set count for user Bonus success delete Brands Cancel Categories Changes saved Comment for paid product Consider delivery Consider discount Count bonus Count bonus  for user Count ned used bonus: Count used bonus: Create Create and go back Create system bonus Date Delete Do not correct id Do not selected bonus for delete Duration Empty system bonus. Error date format! First paid order Go back Information List of bonuses successfuly updated List users Need to paid Only numeric Order do not paid to bonus for user Order id Order paid to bonus Order paid to bonus for user Order pay path to bonus Order pay path to bonus for user Page not found Pay part bonus Percent of the price Positions saved Price type Product name Products Register on site Remove system bonus Remove system bonus? Remove the selected system bonus? Removing system bonus Save Save and go back Selected products Selecting a product Setting success saved Settings Show always Show in all brands Show in all categories Some name input Status System bonus System bonus create System bonus edit System bonus setting Title Type Use bonus Used bonus User bonus list View system bonus Visualization You accrued bonuses You bonus balance choose a date for comment paid product for first paid order for paid order for registration on site from order id show to user id user not selected Project-Id-Version: 
Report-Msgid-Bugs-To: 
Last-Translator: <>
Language-Team: <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2017-03-07 05:48+0200
PO-Revision-Date: 2017-03-07 05:48+0200
Language: 
X-Poedit-KeywordsList: _;gettext;gettext_noop;lang
X-Poedit-Basepath: ../../..
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.5.7
X-Poedit-Language: 
X-Poedit-Country: 
X-Poedit-SearchPath-0: .
 Добавлены бонусы Активная Добавить бонус Артикул Базовые Нчисление бонусов пользователю, за коментарий к оплаченому товару  Начисление бонусов за регистрацию на сайте Минусование бонусов для пользователя Бонус Количество бонусов для роли Бонус для пользователей Количество бонусов для валюты по умолчанию Начисление бонусов для пользователя Бонусы успешно удалены Бренды Отменить Категории Изменения сохранены Комментарий к оплаченому товару Учитывать доставку Учитывать скидку при оплате бонусом Количество бонусов Список бонусов по пользователю Количество необходимых для использования бонусов Количество использованных бонусов Создать Создать и выйти Создать систему бонусов Дата Удалить Не корректный id Не выбраны  бонусы для удаления Продолжительность Система бонусов пуста Неверный диапазон дат Первый оплаченый заказ Вернутся Информация Список бонусов успешно обновлен Список пользователей Необходимо оплатить Только цифры Заказ не оплачен бонусом для пользователя id Заказа Заказ оплачен бонусом Заказ оплачен бонусом пользовеля Заказ частично оплачен бонусом Заказ частично оплачен бонусами для пользователя Страница не найдена Частичная оплата бонусом Процент от цены Позиции сохранены Тип цены Имя товара Товары Регистрация на сайте Удаление системы бонусов Удалить систему бонусов? Удалить выбранные системы бонусов? Удалить систему бонусов Сохранить Сохранить и выйти Выбранные товары Выберите товар Настройки успешно сохранены Настройки Показывать всегда Показывать все бренды Показывать все категории Имя ввод Статус Система бонусов Создание системы бонусов Редактирование системы бонусов Настройки системы бонусов Название Тип Использовать бонусы Использовано бонусов Список бонусов по пользователю Список систем бонусов Визуализация Вам начислены бонусы Ваш баланс бонусов выбрать дату за коментарий к оплаченому товару за первый оплаченый заказ за оплаченый заказ за регистрацию на сайте с id заказа показать к id пользователя пользователь не выбран 