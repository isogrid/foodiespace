 $(document).on('document:initialized', function () {

    $('.system_bonus_status').on('click', function () {
        var id = $(this).attr('data-id');

        $.ajax({
            type: 'POST',
            url: '/admin/components/init_window/system_bonus/ajaxChangeActiveStatus/' + id,
            success: function (data) {
                $('.notifications').append(data);
            }
        });
    });


    $('#permanentCheck').on('click', function () {

        if ( $('#show_always').is(':checked') != true) {

            $('#hideDate').hide();
        } else {

            $('#hideDate').show();
        }

        }
    );

    /**
     * @TODO: добавить обнуление значений полей при смене значения селекта
     */
    $('#type').on('change', function () {

        var val = $('#type').val(),
            bonus_id = $('#bonus_id').val();

        $.ajax({
            type: 'GET',
            //data: {type: val},
            url: '/admin/components/init_window/system_bonus/render_form/' + val + (bonus_id ? '/' + bonus_id : '') ,
            success: function (data) {

                $("#change_bonus_info").html(data);

                initChosenSelect();

                $(document).trigger('loadProduct');


            }
        });
    });

    $('#users').on('change', function () {

        var val = $('#users').val();

        if (val != 0) {

            $.ajax({
                type: 'GET',
                url: '/admin/components/init_window/system_bonus/get_user_bonus/' + val,
                success: function (data) {

                    $("#bonus_point_user").val(data);
                    $("#bonus_point_user").attr('name', "bonus_point_user" + "[" + val + "]");

                    initChosenSelect()
                }
            });

        } else {
            $("#bonus_point_user").attr('name', "bonus_point_user");
            $("#bonus_point_user").val(val);
            initChosenSelect()
        }

    });

    $(function(){
        $(document).trigger('loadProduct');
    });


    $(document).on('click', '[data-delete-product]', function(event){
        var syblings = $(this).closest('tr').siblings();

        if (syblings.length == 0) {

            $(this).closest('#relatedProductsNames').css('display','none');
            $(this).closest('tr').remove();

        } else {

            $(this).closest('tr').remove();
        }

        event.preventDefault();
    });


    $(document).on('loadProduct', function (){

        $('#RelatedProductsBonus').autocomplete({
            minChars: 0,
            source: function (request, response) {

                $.ajax({
                    url: '/admin/components/init_window/system_bonus/autocomplete',
                    dataType: 'json',
                    type: 'POST',
                    data: {
                        limit: 20,
                        q: request.term,
                        ids: getAddedRelatedBonusProductsIds()
                    },
                    success: function (data) {
                        response(data);
                    }
                })
            },
            select: function (event, ui) {
                $('<tr id="tpm_row' + ui.item.id + '" class="item-accessories"><td>\n\
                <button class="btn btn-small my_btn_s pull-left m-r_10" data-delete-product data-rel="tooltip" data-title="' + langs.remove + '" data-kid="' + ui.item.id + '"><i class="icon-trash"></i></button>\n\
                <div class="photo_album-v">' +
                    (ui.item.photo ? '<img src="' + ui.item.photo + '" class="img-polaroid" style="width: 100px;max-height: 100%;float: left; margin-right: 15px;">' : '<img src="' + (base_url + 'templates/administrator/images/select-picture.png') + '" class="img-polaroid" style="width: 100px;max-height: 100%;float: left;margin-right: 15px;">')
                    +
                    '<div class="o_h">\n\
                    <a href="'+ ui.item.href +'">' + ui.item.label + '</a>\n\
                <div>' +
                    '<b>' + ui.item.price + '</b>'
                    + '</div>\n\
                <input type="hidden" name="values[]" value="' + ui.item.id + '">\n\
                </div>\n\
                </div>\n\
                </td></tr>').prependTo($('#relatedProductsNames tbody'));
                $('#relatedProductsNames').show();
            },
            close: function (event, ui) {
                $(this).attr('value', '');
            }
        });
    });




    function getAddedRelatedBonusProductsIds() {
        var inputs = $("#relatedProductsNames input[name='values[]']");
        var idsString = '';

        $(inputs).each(function () {
            idsString += this.value + ",";
        });

        return idsString;
    }


});