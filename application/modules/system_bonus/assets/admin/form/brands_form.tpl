<div class="control-group">

    <label class="control-label" for="values">{lang('Brands', 'admin')}:</label>
    <div class="controls">
        <select name="values[]" id="values" multiple>
            <option value="all">- {lang('Show in all brands','system_bonus')} -</option>

            {foreach $data as $item}
                <option value="{echo $item->getId()}" {if in_array($item->getId(), $selected)} selected="selected"{/if} >{echo $item->getName()}</option>
            {/foreach}

        </select>
    </div>
</div>

<div class="control-group">

    <label class="control-label" for="price_type">{lang('Price type','system_bonus')}:<span class="must">*</span></label>

    <div class="controls">
        <select name="price_type" id="price_type">

            <option value="1"{if $price_type == 1} selected {/if}>{lang('Count bonus','system_bonus')}</option>
            <option value="2"{if $price_type == 2} selected {/if}>{lang('Percent of the price','system_bonus')}</option>
        </select>

    </div>
</div>
