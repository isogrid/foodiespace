<div class="control-group">
    <div class="inside_padd">
        <div class="form-horizontal">
            <div class="control-group">
                <label class="control-label">{lang('Selecting a product','admin')}:</label>
                <div class="controls">
                    <input type="text" id="RelatedProductsBonus"/>
                    <span class="help-block">{lang('ID', 'admin')} / {lang('Product name', 'admin')} / {lang('Article', 'admin')}</span>
                </div>
            </div>

                <div id="relatedProductsNames" style="margin-top: 10px; {if count($selected) == 0} display: none; {/if}"  >
                    <div class="control-group">
                        <label class="control-label">{lang('Visualization','admin')}:</label>
                        <div class="controls">
                            <table class="table table-bordered content_small_td t-l_a w_auto">
                                <thead>
                                <tr>
                                    <th>{lang('Selected products', 'admin')}</th>
                                </tr>
                                </thead>
                                <tbody>

                                {foreach $selected as $product}
                                    <tr id="tpm_row{echo $product['id']}" class="item-accessories">
                                        <td>
                                            <button class="btn btn-small my_btn_s pull-left m-r_10" data-rel="tooltip" data-delete-product>
                                                    <i class="icon-trash"></i></button>
                                            <div class="photo_album-v"><img src="{$product['photo']}" class="img-polaroid" style="width: 100px;max-height: 100%;float: left; margin-right: 15px;">
                                                <div class="o_h">
                                                    <a href="{echo $product['href']}">{echo $product['label']}</a>
                                                    <div>
                                                        <b> {echo $product['price']} </b>
                                                    </div>
                                                    <input type="hidden" name="values[]" value="{echo $product['id']}">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

</div>

    </div>
<div class="control-group">

    <label class="control-label" for="price_type">{lang('Price type','system_bonus')}:<span class="must">*</span></label>

    <div class="controls">

        <select name="price_type" id="price_type">

            <option value="1"{if $price_type == 1} selected {/if}>{lang('Count bonus','system_bonus')}</option>
            <option value="2"{if $price_type == 2} selected {/if}>{lang('Percent of the price','system_bonus')}</option>

        </select>

    </div>
</div>
