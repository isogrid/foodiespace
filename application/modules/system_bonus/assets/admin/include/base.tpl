<table class="table  table-bordered table-hover table-condensed content_big_td">
    <thead>
    <tr>
        <th colspan="6">
            {lang('Information','admin')}
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td colspan="9" class="number">

            <div class="control-group">
                <label class="control-label" for="Name">{lang('Bonus point of default currency','system_bonus')}:<span
                            class="must">*</span></label>

                <div class="controls">
                    <div class="input-append input-prepend">
                        <span class="add-on">{lang('Bonus point', 'system_bonus')} =</span>
                        <input class="span2" name="bonus_point" id="bonus_point" type="text"
                               title="{lang('Only numeric', 'system_bonus')}"

                               onkeyup="checkLenghtStr('bonus_point', 11, 2, event.keyCode);"
                               value="{if $settings['bonus_point']}{echo preg_replace('/\.?0*$/','',number_format($settings['bonus_point'], 5, ".", ""))}{/if}">


                        <span class="add-on"> {$CS}</span>
                    </div>
                </div>

            </div>

            {if count($roles) > 0}

                {foreach $roles as $role}

                    <div class="control-group">
                        <label class="control-label">{lang('Bonus point for role','system_bonus')} {echo $role['name']}:</label>
                        <div class="controls">
                            <div class="input-append input-prepend">
                                <span class="add-on">{lang('Bonus point', 'system_bonus')} =</span>
                                <input class="span2" name="bonus_point_role[{echo $role['id']}]" id="bonus_point_role_{$role['id']}" type="text"
                                       onkeyup="checkLenghtStr('bonus_point_role_{$role['id']}', 11, 2, event.keyCode);" title="{lang('Only numeric', 'system_bonus')}"
                                       value="{if $settings['bonus_point_role'][$role['id']]}{echo preg_replace('/\.?0*$/','',number_format($settings['bonus_point_role'][$role['id']], 5, ".", ""))}{/if}">


                                <span class="add-on"> {$CS}</span>
                            </div>
                        </div>

                    </div>

                {/foreach}

            {/if}


            <div class="control-group">
                <label class="control-label" for="Name">{lang('Pay part bonus','system_bonus')}:</label>

                <div class="controls">
                    <div class="input-append input-prepend">
                        <input class="span2" name="pay_part" {if $settings['pay_part']} checked {/if} type="checkbox">
                    </div>
                </div>

            </div>
            <div class="control-group">
                <label class="control-label" for="consider_discount">{lang('Consider discount','system_bonus')}:</label>

                <div class="controls">
                    <div class="input-append input-prepend">
                        <input class="span2" name="consider_discount" {if $settings['consider_discount']} checked {/if} type="checkbox">
                    </div>
                </div>

            </div>
            <div class="control-group">
                <label class="control-label" for="consider_delivery">{lang('Consider delivery','system_bonus')}:</label>

                <div class="controls">
                    <div class="input-append input-prepend">
                        <input class="span2" name="consider_delivery" {if $settings['consider_delivery']} checked {/if} type="checkbox">
                    </div>
                </div>

            </div>

        </td>
    </tr>
    </tbody>
</table>
