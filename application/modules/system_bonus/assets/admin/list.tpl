<div class="container">
    <!-- ---------------------------------------------------Блок видалення---------------------------------------------------- -->
    <div class="modal hide fade modal_del">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>{lang('Remove system bonus','system_bonus')}</h3>
        </div>
        <div class="modal-body">
            <p>{lang('Remove the selected system bonus?','system_bonus')}</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-primary  "
               onclick="delete_function.deleteFunctionConfirm('{site_url('/admin/components/init_window/system_bonus/delete')}')">{lang('Delete','admin')}</a>
            <a href="#" class="btn" onclick="$('.modal').modal('hide');">{lang('Cancel','admin')}</a>
        </div>
    </div>


    <div id="delete_dialog" title="{lang('Removing system bonus','system_bonus')}" style="display: none">
        {lang('Remove system bonus?','system_bonus')}
    </div>
    <!-- ---------------------------------------------------Блок видалення---------------------------------------------------- -->
    <div>
        <section class="mini-layout">
            <div class="frame_title clearfix">
                <div class="pull-left">
                    <span class="help-inline"></span>
                    <span class="title">{lang('View system bonus','system_bonus')}</span>
                </div>
                <div class="pull-right">
                    <div class="d-i_b">
                        <a href="{site_url('/admin/components/modules_table')}"
                           class="t-d_n m-r_15 pjax"><span class="f-s_14">←</span> <span
                                    class="t-d_u">{lang('Go back','admin')}</span></a>
                        <a class="btn btn-small btn-primary"
                           href="{site_url('/admin/components/init_window/system_bonus/settings')}">{lang('Settings','system_bonus')}</a>
                        <a class="btn btn-small btn-primary"
                           href="{site_url('/admin/components/init_window/system_bonus/show_user_list')}">{lang('User bonus list','system_bonus')}</a>
                        <a class="btn btn-small btn-success"
                           href="{site_url('/admin/components/init_window/system_bonus/create')}"><i
                                    class="icon-plus-sign icon-white"></i>{lang('Create system bonus','system_bonus')}
                        </a>
                        <button type="button" class="btn btn-small btn-danger disabled action_on"
                                onclick="delete_function.deleteFunction()"><i
                                    class="icon-trash"></i>{lang('Delete','admin')}</button>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                {if count($models) > 0}
                    <table class="table  table-bordered table-hover table-condensed t-l_a">
                        <thead>
                        <tr>
                            <th class="t-a_c span1">
                                    <span class="frame_label">
                                        <span class="niceCheck b_n">
                                            <input type="checkbox"/>
                                        </span>
                                    </span>
                            </th>
                            <th>{lang('ID','admin')}</th>
                            <th>{lang('Title','admin')}</th>
                            <th>{lang('Type','system_bonus')}</th>
                            <th>{lang('Count bonus','system_bonus')}</th>
                            <th>{lang('Duration','system_bonus')}</th>
                            <th>{lang('Status', 'admin')}</th>
                        </tr>
                        </thead>
                        <tbody class="sortable save_positions"
                               data-url="/admin/components/init_window/system_bonus/save_positions">

                        {foreach $models as $model}
                            <tr id="currency_tr{echo $model->getId()}">
                                <td class="t-a_c">
                                        <span class="frame_label">
                                            <span class="niceCheck b_n">
                                                <input type="checkbox" name="ids" value="{echo $model->getId()}"/>
                                            </span>
                                        </span>
                                </td>
                                <td><span>{echo $model->getId()}</span></td>
                                <td class="share_alt">
                                    <a href="{site_url('/admin/components/init_window/system_bonus/edit/' . $model->getId())}"
                                       class="title d_i"
                                       data-rel="tooltip" data-placement="top">{echo $model->getName()}</a>
                                </td>
                                <td>
                                    {echo $availableType[$model->getType()]}
                                </td>
                                <td>
                                    {echo $model->getCountBonus()} {echo $model->getPriceType() == 2?'%':''}
                                </td>
                                <td>
                                    {if $model->getShowAlways() == true}
                                        {lang('Show always', 'system_bonus')}
                                    {else :}
                                        {echo date('d-m-Y', $model->getShowFrom()) . ' - ' . date('d-m-Y', $model->getShowTo())}
                                    {/if}
                                </td>
                                <td>
                                    <div class="frame_prod-on_off" data-rel="tooltip" data-placement="top"
                                         data-original-title="{lang('show','admin')}">
                                        <span class="prod-on_off system_bonus_status {if $model->getActive() != 1} disable_tovar{/if}"
                                              data-id="{echo $model->getId()}"></span>
                                    </div>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                {else:}
                    <div class="alert alert-info" style="margin-bottom: 18px; margin-top: 18px;">
                        {lang('Empty system bonus.','system_bonus')}
                    </div>
                {/if}
            </div>
            <div class="clearfix">
            </div>
        </section>
    </div>
</div>