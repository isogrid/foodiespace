<section class="mini-layout">
    <div class="frame_title clearfix">
        <div class="pull-left">
            <span class="help-inline"></span>
            <span class="title">{lang('Count bonus  for user','system_bonus')}</span>
        </div>
        <div class="pull-right">
            <div class="d-i_b">
                <a href="{site_url('/admin/components/init_window/system_bonus')}"
                   class="t-d_n m-r_15 pjax"><span class="f-s_14">←</span> <span
                            class="t-d_u">{lang('Go back','admin')}</span></a>
                <button type="button" class="btn btn-small btn-primary formSubmit" data-form="#sys_bonus"
                        data-submit><i class="icon-ok icon-white"></i>{lang('Save','admin')}</button>
            </div>
        </div>

    </div>
    <form method="post" action="{site_url('/admin/components/init_window/system_bonus/save_user_bonus_edit')}" class="form-horizontal" id="sys_bonus">
        <table class="table  table-bordered table-hover table-condensed content_big_td">
            <thead>
            <tr>
                <th colspan="6">
                    {lang('Information','admin')}
                </th>
            </tr>
            </thead>


            <tbody>
            <tr>
                <td colspan="9">
                    <div class="control-group">
                        <label class="control-label">{lang('List users','system_bonus')}</label>
                        <div class="controls">
                            <select name='users' id="users">
                                <option value="0">{lang('user not selected', 'system_bonus')}</option>
                                {foreach $users as $user}
                                    <option value="{echo $user['id']}">{echo $user['username']}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">{lang('Bonus point for user','system_bonus')}</label>
                        <div class="controls">
                                <input name="bonus_point_user" id="bonus_point_user" type="text" onkeypress="validateN()" title="{lang('Only numeric', 'system_bonus')}" value="">
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        {form_csrf()}
    </form>
</section>