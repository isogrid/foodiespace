<section class="mini-layout">
    <div class="frame_title clearfix">
        <div class="pull-left">
            <span class="help-inline"></span>
            <span class="title">{lang('System bonus edit','system_bonus')}</span>
        </div>
        <div class="pull-right">
            <div class="d-i_b">
                <a href="{site_url('/admin/components/init_window/system_bonus')}"
                   class="t-d_n m-r_15 pjax"><span class="f-s_14">←</span> <span
                            class="t-d_u">{lang('Go back','admin')}</span></a>
                <button type="button" class="btn btn-small btn-primary formSubmit" data-form="#sys_bonus"
                        data-submit><i class="icon-ok icon-white"></i>{lang('Save','admin')}</button>
                <button type="button" class="btn btn-small formSubmit" data-form="#sys_bonus"
                        data-action="tomain"><i class="icon-check"></i>{lang('Save and go back','admin')}</button>
            </div>
        </div>

    </div>
    <form method="post" action="{site_url('/admin/components/init_window/system_bonus/edit/' . $model->getId())}" class="form-horizontal" id="sys_bonus">
        <div class="tab-content">
            <div class="tab-pane active" id="params">
                <table class="table  table-bordered table-hover table-condensed content_big_td">
                    <thead>
                    <tr>
                        <th colspan="6">
                            {lang('Information','admin')}
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="9">

                            <div class="control-group">
                                <label class="control-label" for="Name">{lang('Title','admin')}:<span
                                            class="must">*</span></label>
                                <div class="controls">
                                    <input type="text" name="name" required="required" value="{echo $model->getName()}"/>
                                </div>

                            </div>

                            <div class="control-group">

                                <label class="control-label" for="price_types">{lang('Type','admin')}:</label>
                                <div class="controls">
                                    <select name="type" id="type">

                                        {foreach $availableType as  $key => $type}
                                            <option value="{echo $key}" {if $model->getType() == $key} selected="selected" {/if} >{echo $type}</option>
                                        {/foreach}

                                    </select>
                                </div>

                            </div>

                            <div class="control-group" id="change_bonus_info">
                                {if $form}

                                    {echo $form}

                                {/if}
                            </div>



                            <div class="control-group">

                                <label class="control-label" for="Name">{lang('Count bonus','system_bonus')}:<span
                                            class="must">*</span></label>
                                <div class="controls">
                                    <input type="text" name="count_bonus" onkeypress="validateN()" required="required" value="{echo $model->getCountBonus()}"/>
                                </div>
                            </div>

                            <div class="controls">

                                <label class="frame_label no_connection " id="permanentCheck">
                                    {lang('Show always', 'system_bonus')}

                                    <span class="niceCheck b_n">
                                        <input name="show_always" id="show_always" {if $model->getShowAlways() == true} checked {/if} type="checkbox"/>
                                    </span>
                                </label>

                            </div>

                            <div class="control-group " id="hideDate" {if $model->getShowAlways() == true} style="display: none"  {/if}>
                                <label class="control-label"
                                       for="showInCategory">{lang('Date', 'system_bonus')}:
                                </label>
                                <div class="controls">
                                    <label class="v-a_m"
                                           style="width:115px;margin-right:10px; display: inline-block;margin-bottom:0px;">
                                        <span class="o_h d_b p_r">
                                            <input type="text" data-placement="top"
                                                   data-original-title="{lang('choose a date','system_bonus')}"
                                                   data-rel="tooltip" class="datepicker " name="linked[active_from]"
                                                   value="{echo $model->getShowFrom('d-m-Y')}"
                                                   placeholder="{lang('from','admin')}">
                                            <i class="icon-calendar"></i>
                                        </span>
                                    </label>
                                    <label class="v-a_m" style="width:115px; display: inline-block;margin-bottom:0px;">
                                        <span class="o_h d_b p_r">
                                            <input type="text" data-placement="top"
                                                   data-original-title="{lang('choose a date','system_bonus')}"
                                                   data-rel="tooltip" class="datepicker " name="linked[active_to]"
                                                   value="{echo $model->getShowTo('d-m-Y')}"
                                                   placeholder="{lang('to','admin')}">
                                            <i class="icon-calendar"></i>
                                        </span>
                                    </label>
                                </div>
                            </div>

                            <div class="control-group">

                                <div class="controls">

                                    <label class="frame_label no_connection " id="permanentCheck">
                                        {lang('Active', 'system_bonus')}

                                        <span class="niceCheck b_n">
                                        <input name="active" id="active" {if $model->getActive() == true} checked {/if} type="checkbox"/>
                                    </span>
                                    </label>
                                </div>

                            </div>
                            <input type="hidden" id="bonus_id" value="{echo $model->getId()}">

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        {form_csrf()}
    </form>
</section>