<section class="mini-layout">
    <div class="frame_title clearfix">
        <div class="pull-left">
            <span class="help-inline"></span>
            <span class="title">{lang('System bonus setting','system_bonus')}</span>
        </div>
        <div class="pull-right">
            <div class="d-i_b">
                <a href="{site_url('/admin/components/init_window/system_bonus')}"
                   class="t-d_n m-r_15 pjax"><span class="f-s_14">←</span> <span
                            class="t-d_u">{lang('Go back','admin')}</span></a>
                <button type="button" class="btn btn-small btn-primary formSubmit" data-form="#sys_bonus"
                        data-submit><i class="icon-ok icon-white"></i>{lang('Save','admin')}</button>
            </div>
        </div>

    </div>
    <form method="post" action="{site_url('/admin/components/init_window/system_bonus/save_setting')}" class="form-horizontal" id="sys_bonus">
        <div class="clearfix">
            <div class="btn-group myTab m-t_20 pull-left" data-toggle="buttons-radio">
                <a href="#base" class="btn btn-small active">{lang('Base','system_bonus')}</a>
            </div>
        </div>

        <div class="tab-content">
            <div class="tab-pane active" id="base">
                {include_tpl('include/base.tpl')}
            </div>
        </div>
        {form_csrf()}
    </form>
</section>