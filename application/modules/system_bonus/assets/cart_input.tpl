<div class="form__field form__field--hor form__field--hor-lg form__field--static">
    <div class="form__label">
        {lang('Use bonus', 'system_bonus')}
    </div>
    <div class="form__inner">
        <div class="delivery-radio__field" data-cart-delivery--item>

            {if $showCheckBox}
            <div class="delivery-radio__control">
                <input type="checkbox" name="use_bonus" {if $formID && $formID !=''}form="{echo $formID}"{/if} >
            </div>
            {/if}

            Використати бонуси ({echo $bonus})

        </div><!-- /.content -->
        <div class="delivery_bonus_shit_description">
            Ви можете використати бонуси для оплати цього замовлення (до 50% вартості замовлення). Оплата бонусами не розповсюджується на алкогольні напої
        </div>
    </div>
</div>
