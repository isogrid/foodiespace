<?php

namespace system_bonus\classes;

use SOrdersQuery;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

class CommentPaidProduct extends SystemBonusFactory implements RegularInterface
{

    private $setting = [];

    /**
     * @param bool $model
     * @return bool|int|SystemBonusList
     */
    public function getRegularBonus($model = false) {

        $bonusModel = $this->getBonusModel(SystemBonusFactory::COMMENT_PAID_PRODUCT);

        if ($model === true) {

            return $bonusModel;
        }

        if ($bonusModel != null) {

            return $bonusModel->getCountBonus();
        }

        return false;
    }

    /**
     * @return bool|int
     */
    public function getBonus() {

        if (false === $this->checkPaidProduct()) {

            return false;
        }

        if (false === $this->checkCommentsExists()) {

            return false;
        }

        $bonus = $this->getBonusModel(SystemBonusFactory::COMMENT_PAID_PRODUCT);

        if ($bonus != null) {

            return $bonus->getCountBonus();
        }

        return false;

    }

    /**
     * проверка на существование товара в оплаченых заказах по пользователю
     *
     * @return bool
     */
    private function checkPaidProduct() {

        $order = SOrdersQuery::create()
            ->useSOrderProductsQuery()
            ->filterByProductId($this->getSetting('item_id'))
            ->endUse()
            ->filterByPaid(1)
            ->findOneByUserId($this->getSetting('user_id'));

        if ($order == null) {
            return false;
        }

        return true;

    }

    /**
     * @param null $key
     * @return string|array
     */
    private function getSetting($key = null) {

        if ($key != null) {

            return $this->setting[$key];

        }
        return $this->setting;
    }

    /**
     * проверка на наличие существующих коментов на товаре, по пользователю
     *
     * @return bool
     */
    private function checkCommentsExists() {

        /** @var \CI_DB_result $user_comments */
        $user_comments = \CI::$APP->db->get_where(
            'comments',
            [
             'user_id' => $this->getSetting('user_id'),
             'item_id' => $this->getSetting('item_id'),
            ]
        );

        if ($user_comments->num_rows() > 0) {
            return false;
        }

        return true;

    }

    /**
     * @param array $setting
     */
    public function setSetting($setting) {

        $this->setting = $setting;
    }

}