<?php

namespace system_bonus\classes;

use Cart\CartItem;
use CMSFactory\assetManager;
use Propel\Runtime\Exception\PropelException;
use SCategoryQuery;
use SOrderProducts;
use SProducts;
use SProductVariants;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

/**
 * Class CategoriesBonus
 * @package system_bonus\classes
 */
class CategoriesBonus extends SystemBonusFactory implements Products
{
    use ProductBonusTrait;

    /**
     *
     * @param null $bonus_id
     * @return string
     */
    public function getForm($bonus_id = null) {

        if ($bonus_id != null) {
            $selected = $this->getSelectedVal($bonus_id);
        }

        return assetManager::create()
            ->setData(
                [
                 'data'       => SCategoryQuery::create()->orderByPosition()->find(),
                 'selected'   => $selected['selected'] ?: [],
                 'price_type' => $selected['price_type'] ?: false,
                ]
            )
            ->fetchAdminTemplate('form/categories_form', false);
    }

    public function getValue() {
        return $this->getProduct()->getCategoryId();
    }

    /**
     * @return string
     */
    public function getType() {
        return SystemBonusFactory::CATEGORIES_BONUS;
    }

    /**
     * @return string
     */
    public function getValueType() {
        return 'category';
    }

}