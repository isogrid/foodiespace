<?php namespace system_bonus\classes;

use Cart\CartItem;
use Propel\Runtime\Exception\PropelException;
use SOrderProducts;
use SProducts;
use SProductVariants;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

trait ProductBonusTrait
{

    /**
     * @var SProducts
     */
    protected $product;

    /**
     * @var SProductVariants
     */
    protected $variant;

    /**
     * @var SOrderProducts
     */
    protected $order_products;

    /**
     * @param int $offset
     * @return bool|SystemBonusList
     */
    public function getBonusModel($offset = 0) {

        $bonus = SystemBonusListQuery::create()
            ->filterByActive(true)
            ->useSystemBonusValuesQuery()
            ->filterByValue($this->getValue())
            ->filterByValueType($this->getValueType())
            ->endUse()
            ->_if($offset)
            ->offset($offset)
            ->_endif()
            ->orderByPosition()
            ->findOneByType($this->getType());

        if ($bonus) {
            return $this->isActive($bonus) ? $bonus : $this->getBonusModel(++$offset);
        }

    }

    /**
     * @return bool|int
     * @throws PropelException
     */
    public function getBonus() {

        if (!($this->getProduct() instanceof SProducts)) {
            return false;
        }

        $bonus = $this->getBonusModel();

        if ($bonus) {

            /** Если тип цены процент */
            if ($bonus->getPriceType() == SystemBonusFactory::PERCENT_BONUS_TYPE) {

                return $this->getBonusPercentType($bonus->getCountBonus(), $this->chooseTypePercentPrice()) * $this->getQuantity();
            }

            return $bonus->getCountBonus() * $this->getQuantity();
        }

        return false;
    }

    /**
     * @param CartItem $cartItem
     * @return int|bool
     */
    public function getCartBonus(CartItem $cartItem) {

        $this->setVariant($cartItem->getItemTypeConcrete()->model);
        $this->setProduct($this->getVariant()->getSProducts());
        $this->setQuantity($cartItem->data['quantity']);

        $bonus = $this->getBonusModel();

        if ($bonus) {

            /** Если тип цены процент */
            if ($bonus->getPriceType() == SystemBonusFactory::PERCENT_BONUS_TYPE) {

                return $this->getBonusPercentType($bonus->getCountBonus(), $this->chooseTypePercentPrice()) * $this->getQuantity();
            }

            return $bonus->getCountBonus() * $this->getQuantity();
        }

        return false;
    }

    /**
     * @return float|mixed
     * @throws PropelException
     */
    public function chooseTypePercentPrice() {
        return $this->isUseFront() ? $this->getVariant()->getPrice() : $this->getOrderProduct()->getPrice();
    }

    /**
     * @return int
     */
    public function getQuantity() {
        return $this->isUseFront() ? $this->quantity : $this->getOrderProduct()->getQuantity();
    }

    /**
     * @param bool $quantity
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    /**
     * @return SProducts
     */
    public function getProduct() {

        return $this->product;
    }

    /**
     * @param SProducts $product
     */
    public function setProduct(SProducts $product) {

        $this->product = $product;
    }

    /**
     * @return boolean
     */
    public function isUseFront() {
        return $this->useFront;
    }

    /**
     * @param boolean $useFront
     */
    public function setUseFront($useFront) {
        $this->useFront = $useFront;
    }

    /**
     * @param SOrderProducts $order_products
     */
    public function setOrderProducts(SOrderProducts $order_products) {

        $this->order_products = $order_products;
    }

    /**
     * @return SOrderProducts
     */
    public function getOrderProduct() {

        return $this->order_products;
    }

    /**
     * @return SProductVariants
     */
    public function getVariant() {
        return $this->variant;
    }

    /**
     * @param SProductVariants $variant
     */
    public function setVariant(SProductVariants $variant) {
        $this->variant = $variant;
    }

    /**
     * @param SProducts $product
     * @param SProductVariants $variant
     */
    public function setFrontInfo(SProducts $product, SProductVariants $variant) {

        $this->setProduct($product);
        $this->setVariant($variant);

    }

}