<?php

namespace system_bonus\classes;

use system_bonus\models\SystemBonusList;

/**
 * Class RegisterOnSite
 * @package system_bonus\classes
 */
class RegisterOnSite extends SystemBonusFactory implements RegularInterface
{

    /**
     * @param bool $model
     * @return bool|int|SystemBonusList
     */
    public function getRegularBonus($model = false) {

        $bonusModel = $this->getBonusModel(SystemBonusFactory::REGISTER_ON_SITE);

        if ($model === true) {

            return $bonusModel;
        }

        if ($bonusModel != null) {

            return $bonusModel->getCountBonus();
        }

        return false;
    }

    /**
     * @return int
     */
    public function getBonus() {

        $model = $this->getBonusModel(SystemBonusFactory::REGISTER_ON_SITE);

        if ($model != null) {

            return $model->getCountBonus();
        }

        return false;

    }

}