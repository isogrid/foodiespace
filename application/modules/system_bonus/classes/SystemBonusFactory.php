<?php
/**
 * pattern Facade
 */

namespace system_bonus\classes;

use Exception;
use system_bonus\models\Map\SystemBonusValuesTableMap;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;
use system_bonus\models\SystemBonusValuesQuery;


/**
 * Class SystemBonusFactory
 *
 * @package system_bonus\classes
 * @property RegisterOnSite
 * @property BrandBonus
 * @property CategoriesBonus
 * @property CommentPaidProduct
 * @property FirstPaidOrder
 * @property ProductBonus
 */
abstract class SystemBonusFactory
{
    const PERCENT_BONUS_TYPE   = 2;

    const REGISTER_ON_SITE     = 'RegisterOnSite';
    const BRAND_BONUS          = 'BrandBonus';
    const CATEGORIES_BONUS     = 'CategoriesBonus';
    const COMMENT_PAID_PRODUCT = 'CommentPaidProduct';
    const FIRST_PAID_ORDER     = 'FirstPaidOrder';
    const PRODUCT_BONUS        = 'ProductBonus';

    /**
     * @var array
     */
    private static $classes = [
                               self::REGISTER_ON_SITE     => RegisterOnSite::class,
                               self::BRAND_BONUS          => BrandBonus::class,
                               self::CATEGORIES_BONUS     => CategoriesBonus::class,
                               self::COMMENT_PAID_PRODUCT => CommentPaidProduct::class,
                               self::FIRST_PAID_ORDER     => FirstPaidOrder::class,
                               self::PRODUCT_BONUS        => ProductBonus::class,
                              ];

    /**
     * @var bool
     */
    protected $useFront = false;

    /**
     * @var int
     */
    protected $quantity = 1;

    /**
     * @param string $bonusType
     * @abstract
     * @return SystemBonusFactory
     * @throws Exception
     */
    public static function initial($bonusType) {

        if (class_exists(self::$classes[$bonusType])) {
            return new self::$classes[$bonusType]();
        } else {
            throw new Exception('Class not found');
        }
    }

    /**
     * @return int
     */
    abstract public function getBonus();

    /**
     * @param int $bonus_id
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    protected function getSelectedVal($bonus_id) {

        $model = SystemBonusListQuery::create()
            ->setComment(__METHOD__)
            ->findOneById($bonus_id);

        $price_type = $model->getPriceType();

        $selected = $model->getSystemBonusValuess(
            SystemBonusValuesQuery::create()
                ->withColumn(SystemBonusValuesTableMap::COL_VALUE, 'value')
                ->select(['value'])
        )->toArray();

        return [
                'price_type' => $price_type,
                'selected'   => $selected,

               ];
    }

    /**
     * @param int $percent
     * @param int $totalPrice
     * @return int
     */
    protected function getBonusPercentType($percent, $totalPrice) {

        return (int) ($totalPrice * $percent / 100);

    }

    /**
     * @param SystemBonusList $bonus
     * @return bool
     */
    final protected function isActive(SystemBonusList $bonus) {

        if ($bonus->getShowAlways()) {

            return true;

        }

        return $bonus->getShowFrom() < time() && time() < $bonus->getShowTo();
    }

    /**
     * @param string $bonusType
     * @param int $offset
     * @return bool|SystemBonusList
     */
    public function getBonusModel($bonusType, $offset = 0) {

        $bonus = SystemBonusListQuery::create()
            ->filterByActive(true)
            ->orderByPosition()
            ->_if($offset)
            ->offset($offset)
            ->_endif()
            ->findOneByType($bonusType);

        if ($bonus == null) {

            return false;
        }

        if (false == $this->isActive($bonus)) {

            $bonus = $this->getBonusModel($bonusType, ++$offset);

        }

        return $bonus;

    }

}