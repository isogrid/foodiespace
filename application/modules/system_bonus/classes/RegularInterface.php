<?php

namespace system_bonus\classes;

use system_bonus\models\SystemBonusList;

interface RegularInterface
{

    /**
     * @param bool $model
     * @return bool|SystemBonusList
     */
    public function getRegularBonus($model = false);

}