<?php

namespace system_bonus\classes;

use Propel\Runtime\ActiveQuery\Criteria;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

class FirstPaidOrder extends SystemBonusFactory implements RegularInterface
{

    private $setting = [];

    /**
     * @param bool $model
     * @return bool|int|SystemBonusList
     */
    public function getRegularBonus($model = false) {

        $bonusModel = $this->getBonusModel(SystemBonusFactory::FIRST_PAID_ORDER);

        if ($model === true) {

            return $bonusModel;
        }

        if ($bonusModel != null) {

            return $bonusModel->getCountBonus();
        }

        return false;
    }

    /**
     * @return bool|int
     */
    public function getBonus() {

        if (false == $this->checkOrders()) {

            return false;
        }

        $bonus = $this->getBonusModel(SystemBonusFactory::FIRST_PAID_ORDER);

        if ($bonus != null) {

            return $bonus->getCountBonus();
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkOrders() {

        $order = \SOrdersQuery::create()
            ->filterById($this->getSetting('item_id'), Criteria::NOT_EQUAL)
            ->filterByPaid(1)
            ->findOneByUserId($this->getSetting('user_id'));

        if ($order != null) {
            return false;
        }
        return true;
    }

    /**
     * @param null $key
     * @return string|array
     */
    private function getSetting($key = null) {

        if ($key != null) {

            return $this->setting[$key];

        }
        return $this->setting;
    }

    /**
     * @param array $setting
     */
    public function setSetting($setting) {

        $this->setting = $setting;
    }

}