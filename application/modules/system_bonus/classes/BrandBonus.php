<?php


namespace system_bonus\classes;

use Cart\CartItem;
use CMSFactory\assetManager;
use Propel\Runtime\Exception\PropelException;
use SBrandsQuery;
use SOrderProducts;
use SProducts;
use SProductVariants;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

/**
 * Class BrandBonus
 * @package system_bonus\classes
 */
class BrandBonus extends SystemBonusFactory implements Products
{

    use ProductBonusTrait;

    /**
     * @param null $bonus_id
     * @return string
     */
    public function getForm($bonus_id = null) {

        if ($bonus_id != null) {
            $selected = $this->getSelectedVal($bonus_id);
        }

        return assetManager::create()
            ->setData(
                [
                 'data'       => SBrandsQuery::create()->orderByPosition()->find(),
                 'selected'   => $selected['selected'] ?: [],
                 'price_type' => $selected['price_type'] ?: false,
                ]
            )
            ->fetchAdminTemplate('form/brands_form', false);
    }

    public function getValue() {
        return $this->getProduct()->getBrandId();
    }

    /**
     * @return string
     */
    public function getType() {
        return SystemBonusFactory::BRAND_BONUS;
    }

    /**
     * @return string
     */
    public function getValueType() {
        return 'brand';
    }

}