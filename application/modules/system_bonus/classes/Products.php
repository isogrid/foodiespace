<?php

namespace system_bonus\classes;

use Cart\CartItem;
use SOrderProducts;
use SProducts;
use SProductVariants;

/**
 * Interface Products
 * @package system_bonus\classes
 */
interface Products
{

    /**
     * @return string
     */
    public function getType();

    /**
     * @return string
     */
    public function getValueType();

    /**
     * @return string
     */
    public function getValue();

    /**
     * @param SProducts $product
     * @return void
     */
    public function setProduct(SProducts $product);

    /**
     * @return SProducts
     */
    public function getProduct();

    /**
     * @return SOrderProducts
     */
    public function getOrderProduct();

    /**
     * @param SOrderProducts $order_products
     * @return void
     */
    public function setOrderProducts(SOrderProducts $order_products);

    /**
     * @param null|int $bonus_id
     * @return mixed
     */
    public function getForm($bonus_id = null);

    /**
     * @param SProducts $product
     * @param SProductVariants $variant
     * @return void
     */
    public function setFrontInfo(SProducts $product, SProductVariants $variant);

    /**
     * @return SProductVariants
     */
    public function getVariant();

    /**
     * @param SProductVariants $variant
     * @return void
     */
    public function setVariant(SProductVariants $variant);

    /**
     * @return float
     */
    public function chooseTypePercentPrice();

    /**
     * @param CartItem $cartItem
     * @return int
     */
    public function getCartBonus(CartItem $cartItem);

    /**
     * @return boolean
     */
    public function isUseFront();

    /**
     * @param boolean $useFront
     */
    public function setUseFront($useFront);

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param boolean $quantity
     */
    public function setQuantity($quantity);

}