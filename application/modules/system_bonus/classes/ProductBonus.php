<?php

namespace system_bonus\classes;

use Cart\CartItem;
use CMSFactory\assetManager;
use Propel\Runtime\Exception\PropelException;
use SOrderProducts;
use SProducts;
use SProductsQuery;
use SProductVariants;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;

/**
 * Class ProductBonus
 * @package system_bonus\classes
 */
class ProductBonus extends SystemBonusFactory implements Products
{


    use ProductBonusTrait;

    public function getForm($bonus_id = null) {

        if ($bonus_id != null) {
            $selected = $this->getSelectedVal($bonus_id);
            $select_prod = SProductsQuery::create()
                ->findPks($selected['selected']);

            /** @var \SProducts $item */
            foreach ($select_prod as $product) {

                $data[] = [
                           'id'       => $product->getId(),
                           'label'    => $product->getId() . ' - ' . $product->getName(),
                           'name'     => $product->getName(),
                           'photo'    => $product->getFirstVariant()->getSmallPhoto(),
                           'category' => $product->getCategoryId(),
                           'price'    => (string) emmet_money($product->getFirstVariant()->getFinalPrice()),
                           'href'     => site_url('/admin/components/run/shop/products/edit/' . $product->getId()),
                          ];

            }

        }

        $t = assetManager::create()
            ->setData(
                [
                 'selected'   => $data ?: [],
                 'price_type' => $selected['price_type'] ?: false,
                ]
            )
            ->fetchAdminTemplate('form/products_form', false);

        return $t;
    }

    public function getValue() {
        return $this->getProduct()->getId();
    }

    /**
     * @return string
     */
    public function getType() {
        return SystemBonusFactory::PRODUCT_BONUS;
    }

    /**
     * @return string
     */
    public function getValueType() {
        return 'product';
    }
}