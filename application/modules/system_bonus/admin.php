<?php

use CMSFactory\assetManager;
use CMSFactory\ModuleSettings;
use Map\SBrandsTableMap;
use Map\SCategoryTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;
use system_bonus\classes\Products;
use system_bonus\classes\SystemBonusFactory;
use system_bonus\models\SystemBonusDataValueQuery;
use system_bonus\models\SystemBonusList;
use system_bonus\models\SystemBonusListQuery;
use system_bonus\models\SystemBonusValues;
use system_bonus\models\SystemBonusValuesQuery;

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Class Admin
 *
 * @TODO: добавить дефолтные значения
 *
 *
 * @property System_bonus_model system_bonus_model
 */
class Admin extends BaseAdminController
{

    public function __construct() {

        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('system_bonus');
        $this->load->model('system_bonus_model');
        assetManager::create()->registerScript('script');

    }

    /**
     * @TODO: Добавить быстрый фильтр и пагинацию
     */
    public function index() {

        $model = SystemBonusListQuery::create()
            ->orderByPosition()
            ->find();

        $availableType = $this->system_bonus_model->getAvailableTypes();

        assetManager::create()
            ->setData(
                [
                 'models'        => $model,
                 'availableType' => $availableType,
                ]
            )
            ->renderAdmin('list');
    }

    /**
     * @return void
     */
    public function create() {

        $availableType = $this->system_bonus_model->getAvailableTypes();

        $firstElement = array_shift(array_flip($availableType));

        /** Возвращает форму по первому елементу типа, часто бывает пустой **/
        $form = $this->render_form($firstElement);

        assetManager::create()
            ->setData(
                [
                 'form'          => $form,
                 'availableType' => $availableType,
                ]
            )
            ->renderAdmin('create');
    }

    /**
     * @param null|string $type
     * @param null|int $bonus_id
     * @return string
     */
    public function render_form($type = null, $bonus_id = null) {
        switch ($type) {

            case SystemBonusFactory::PRODUCT_BONUS     :
            case SystemBonusFactory::BRAND_BONUS       :
            case SystemBonusFactory::CATEGORIES_BONUS  :

                /** @var Products $t */
                $t = SystemBonusFactory::initial($type);

                return $t->getForm($bonus_id);
        }
        return false;
    }

    /**
     * @return void
     */
    public function save() {

        $post = $this->input->post();

        $model = new SystemBonusList();

        $this->load->library('form_validation');
        $this->form_validation->set_rules($model->rules());

        if ($this->form_validation->run() == false) {
            showMessage(validation_errors(), '', 'r');
            return;
        }

        if ($post['show_always'] != 'on' && strtotime($post['linked']['active_from']) >= strtotime($post['linked']['active_to'])) {
            showMessage(lang('Error date format!', 'system_bonus'), '', 'r');
            return;
        }

        $model->addToModel();

        /** insert selected values */
        if ($post['values']) {
            $this->checkSelectedAll($post['values'], $model->getId());
        }

        if ($post['action'] == 'tomain') {
            pjax('/admin/components/init_window/system_bonus');
        } else {
            pjax('/admin/components/init_window/system_bonus/edit/' . $model->getId());
        }

    }

    /**
     * @param array $values
     * @param int $bonus_id
     * @return void
     */
    private function checkSelectedAll(array $values, $bonus_id) {

        if (in_array('all', $values)) {

            $this->saveAllBonusSelectValues($bonus_id);
        } else {

            $this->saveSystemBonusValue($values, $bonus_id);
        }

    }

    /**
     * @param int $bonus_id
     * @throws PropelException
     * @return void
     */
    private function saveAllBonusSelectValues($bonus_id) {

        $valueType = $this->system_bonus_model->getValueType($this->input->post('type'));

        switch ($valueType) {

            case 'category':

                $data = SCategoryQuery::create()
                    ->withColumn(SCategoryTableMap::COL_ID, 'id')
                    ->select(['id'])
                    ->find()
                    ->toArray();

                break;
            case 'brand' :

                $data = SBrandsQuery::create()
                    ->withColumn(SBrandsTableMap::COL_ID, 'id')
                    ->select(['id'])
                    ->find()
                    ->toArray();

                break;
        }

        if (is_array($data)) {

            $this->saveSystemBonusValue($data, $bonus_id, $valueType);

        }

    }

    /**
     * @param array $data
     * @param int $bonus_id
     * @param string|null $value_type
     * @throws PropelException
     * @return void
     */
    private function saveSystemBonusValue(array $data, $bonus_id, $value_type = null) {

        $valueType = $value_type ?: $this->system_bonus_model->getValueType($this->input->post('type'));

        foreach ($data as $value) {
            $values = new SystemBonusValues();

            $values->setSysBonusId($bonus_id);
            $values->setValue($value);
            $values->setValueType($valueType);
            $values->save();
        }
    }

    /**
     * @param int $id
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function edit($id) {

        if (!$id) {
            $core_error = new ShopAdminController();
            $core_error->error404(lang('Page not found', 'admin'));
        }

        $model = SystemBonusListQuery::create()
            ->findPk($id);

        if ($model == null) {
            return;
        }

        if ($this->input->post()) {

            $post = $this->input->post();

            $this->load->library('form_validation');
            $this->form_validation->set_rules($model->rules());

            if ($this->form_validation->run() == false) {
                showMessage(validation_errors(), '', 'r');
                return;
            }

            if ($post['show_always'] != 'on' && strtotime($post['linked']['active_from']) >= strtotime($post['linked']['active_to'])) {
                showMessage(lang('Error date format!', 'system_bonus'), '', 'r');
                return;
            }

            $model->addToModel();

            SystemBonusValuesQuery::create()
                ->filterBySysBonusId($model->getId())
                ->delete();

            /** insert selected values */
            if ($post['values']) {

                $this->checkSelectedAll($post['values'], $model->getId());
            }

            if ($this->input->post('action') == 'tomain') {
                pjax('/admin/components/init_window/system_bonus');
            } else {
                pjax('/admin/components/init_window/system_bonus/edit/' . $model->getId());
            }

        } else {

            /** Возвращает форму по первому елементу типа, часто бывает пустой **/
            $form = $this->render_form($model->getType(), $model->getId());

            assetManager::create()
                ->setData(
                    [
                     'model'         => $model,
                     'form'          => $form,
                     'availableType' => $this->system_bonus_model->getAvailableTypes(),
                    ]
                )
                ->renderAdmin('edit');
        }
    }

    /**
     * @param $id
     * @return void
     */
    public function ajaxChangeActiveStatus($id) {

        if (!$id) {
            showMessage(lang('Do not correct id', 'system_bonus', '', 'r'));
            return;
        }

        $model = SystemBonusListQuery::create()
            ->findPk($id);

        if (count($model) > 0) {

            $model->setActive(!$model->getActive());
            $model->save();
            showMessage(lang('Changes saved', 'admin'));

        }

    }

    /**
     * @return void|boolean
     */
    public function save_positions() {

        $positions = $this->input->post('positions');

        if (count($positions) == 0) {
            return false;
        }

        foreach ($positions as $key => $val) {

            $systemBonusList = SystemBonusListQuery::create()
                ->findPk($val);

            if ($systemBonusList) {
                $systemBonusList->setPosition($key);
                $systemBonusList->save();
            }
        }
        showMessage(lang('Positions saved', 'admin'));
    }

    /**
     * @return void
     */
    public function delete() {

        $ids = $this->input->post('ids');

        if (!is_array($ids)) {

            showMessage(lang('Do not selected bonus for delete', 'system_bonus'));
            return;

        }

        SystemBonusListQuery::create()
            ->findPks($ids)
            ->delete();

        showMessage(lang('Bonus success delete', 'system_bonus'));
        return;
    }

    /**
     * @return void
     */
    public function settings() {

        $settings = ModuleSettings::ofModule('system_bonus')->get();

        /** @var CI_DB_result $users */
        $users = $this->db->select(['users.id', 'users.username', 'mod_system_bonus_data_value.count_bonus'])
            ->from('users')
            ->join('mod_system_bonus_data_value', 'mod_system_bonus_data_value.user_id = users.id')
            ->get();

        /** @var CI_DB_result $query */
        $query = $this->db->get('shop_rbac_roles');

        assetManager::create()
            ->setData(
                [
                 'roles'         => $query->num_rows() > 0 ? $query->result_array() : [],
                 'availableType' => $this->system_bonus_model->getAvailableTypes(),
                 'settings'      => $settings,
                 'users'         => $users->num_rows() > 0 ? $users->result_array() : [],
                 'CS'            => \Currency\Currency::create()->getSymbol(),
                ]
            )
            ->renderAdmin('settings');

    }

    /**
     * @return void
     */
    public function show_user_list() {

        /** @var CI_DB_result $users */
        $users = $this->db->select(['users.id', 'users.username', 'mod_system_bonus_data_value.count_bonus'])
            ->from('users')
            ->join('mod_system_bonus_data_value', 'mod_system_bonus_data_value.user_id = users.id', 'left')
            ->order_by('id', 'ASC')
            ->get();

        assetManager::create()
            ->setData(
                [
                 'users' => $users->num_rows() > 0 ? $users->result_array() : [],
                ]
            )
            ->renderAdmin('user_bonus_list');
    }

    /**
     * @return void
     */
    public function save_user_bonus_edit() {

        $bonus_point_user = $this->input->post('bonus_point_user');

        if (is_array($bonus_point_user)) {

            foreach ($bonus_point_user as $key => $item) {

                $userBonus = SystemBonusDataValueQuery::create()
                    ->filterByUserId($key)
                    ->findOneOrCreate();
                $userBonus->setCountBonus($item);
                $userBonus->save();
            }

        }

        showMessage(lang('List of bonuses successfuly updated'));
    }

    /**
     * @return void
     */
    public function save_setting() {

        if ($post = $this->input->post()) {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('bonus_point', lang('Bonus point of default currency', 'system_bonus'), 'required|trim');

            /** Валидация по полям ролей пользователей  */
            foreach (array_keys($post['bonus_point_role']) as $key) {
                $this->form_validation->set_rules('bonus_point_role[' . $key . ']', lang('Bonus point for role', 'system_bonus'), 'trim');
            }

            if ($this->form_validation->run() == false) {
                showMessage(validation_errors(), '', 'r');
                return;
            }

            $post = $this->input->post();

            $dataSetting = ModuleSettings::ofModule('system_bonus')->get();
            $dataSetting['bonus_point'] = $post['bonus_point'];
            $dataSetting['bonus_point_role'] = $post['bonus_point_role'];
            $dataSetting['pay_part'] = $post['pay_part'] ?: false;
            $dataSetting['consider_discount'] = $post['consider_discount'] ?: false;
            $dataSetting['consider_delivery'] = $post['consider_delivery'] ?: false;

            ModuleSettings::ofModule('system_bonus')->set($dataSetting);
            showMessage(lang('Setting success saved', 'system_bonus'));
            return;
        }

    }

    /**
     * @param $id
     * @return int|void
     */
    public function get_user_bonus($id) {

        if (!$id) {
            return false;
        }

        $user = SystemBonusDataValueQuery::create()
            ->findOneByUserId($id);

        if ($user != null) {

            return $user->getCountBonus();
        }
    }

    /**
     * @return string
     */
    final public function autocomplete() {

        $locale = MY_Controller::getCurrentLocale();
        $products = SProductsQuery::create()
            ->joinWithI18n($locale);

        if ($this->input->post('ids')) {
            $selected_id = explode(',', rtrim($this->input->post('ids'), ','));
            $products->filterById($selected_id, Criteria::NOT_IN);
        }

        $searched = trim($this->input->post('q'));

        if ($searched) {
            $product_data = $products
                ->joinWithProductVariant()
                ->condition('numberCondition', 'ProductVariant.Number LIKE ?', '%' . $searched . '%')
                ->condition('nameCondition', 'SProductsI18n.Name LIKE ?', '%' . $searched . '%')
                ->condition('idtCondition', 'SProducts.Id LIKE ?', '%' . $searched . '%')
                ->where(['numberCondition', 'nameCondition', 'idtCondition'], Criteria::LOGICAL_OR)
                ->distinct()
                ->find();

            $response = [];

            if ($product_data->count() > 0) {

                foreach ($product_data as $product) {

                    $response[] = [
                                   'id'       => $product->getId(),
                                   'label'    => $product->getId() . ' - ' . $product->getName(),
                                   'name'     => $product->getName(),
                                   'photo'    => $product->getFirstVariant()->getSmallPhoto(),
                                   'category' => $product->getCategoryId(),
                                   'price'    => (string) emmet_money($product->getFirstVariant()->getFinalPrice()),
                                   'href'     => site_url('/admin/components/run/shop/products/edit/' . $product->getId()),
                                  ];

                }
            }

            return json_encode($response);
        }
    }

}