��    _                         
   &     1     6  6   L     �  "   �  1   �     �     �          +     2     A  "   M     p     �     �     �  !   �  &   �        $     5   =     s     |     �     �     �  	   �     �     �     �     �     	     	     %	     ;	  	   Q	  I   [	     �	  
   �	     �	     �	     �	  %   �	     
     
     
     2
     ?
     F
     c
     |
  1   �
     �
     �
  "   �
     �
     	       
        (     1     A  .   Y  +   �     �     �     �  "   �  %     $   ?  #   d  t   �     �          $     >     V     m     �     �     �     �     �  
   �     �     �  !   �          +  .   @     o  �  ~  2   N     �     �  ,   �  �   �     [  N   i  d   �  .     O   L  2   �     �  $   �     	  :   !  >   \     �     �     �  N   �  K   @  ,   �  4   �  q   �     `     i     r     �     �     �     �  1   �  .   	  /   8  1   h     �  1   �      �       �        �     �     �     �       N   )  
   x  <   �      �     �  
   �  F   
  B   Q     �  �   �     -     M  1   ^  /   �     �     �     �     �  !   	     +  E   J  9   �     �     �  3     6   <  <   s  <   �  <   �  �   *  %     '   .  #   V  *   z  L   �  .   �  L   !     n  1   u     �  #   �  -   �        "      I   @   *   �      �   .   �      �    Accept license agreement Add design Back Bad archive structure Be careful, installing demo data can damage your data. Buy Can not delete installed template. Can not delete template. Check files permissions. Can not install banner. Can not install custom field. Can not upload template. Cancel Changes saved. Choose file Class Template has no |0| property Click to select the image Component Name Component body Component name Component settings can not update Component settings successfuly updated Component |0| not found Component |component_name| is broken Current installed template can not be installed again DB Error Date Demo online Description Download Enter URL Error Error - template not specified Error on adding to DB Failed to download the archive Full demo data install Install Install full demodata Install with demodata Installed Installing full demo data can destroy all your database and uploads data. License agreement Local file Logo Logo & Favicon Message Module for simpler templates managing No No input data specified No params.xml file No templates Number One or more dependency error Only image can be loaded Pay Please use function siteinfo() with the parameter Repository templates Save Specified zip file does not exists Start typing name here... Status Support from System Pay Template Template Editor Template already exists Template demo data was successfully installed. Template does not exist in templates folder Template does not exists. Template list Template name is not set. Template was successfully deleted. Template was successfully downloaded. Template was successfully installed. Template was successfully uploaded. Templates are the intellectual property, so if you <br /> want to install it, you must accept the license agreement. Templates manager Templates properties This module is not exists This module is required This module is wishful This widget is required This widget is wishful Type Unable to extract archive Upload Upload template User Guide Version Wrong file type Wrong filetype. Zip-archives only XML-file read error Бесплатный Дизайн интернет-магазина Платный Project-Id-Version: 
Report-Msgid-Bugs-To: 
Last-Translator: <>
Language-Team: <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2018-06-18 09:51+0300
PO-Revision-Date: 2018-06-18 09:51+0300
Language: 
X-Poedit-KeywordsList: _;gettext;gettext_noop;lang
X-Poedit-Basepath: ../../..
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 1.5.7
X-Poedit-Language: 
X-Poedit-Country: 
X-Poedit-SearchPath-0: .
 Приймаємо ліцензійна угода Додати дизайн Тому Погана структура архіву Будьте обережні, установка демо-даних може призвести до пошкодження даних. В кошик Ви не можете видалити встановлений шаблон. Ви не можете видалити шаблон. Перевірте права на файли. Не можу встановити банер. Не можу встановити користувальницькі поля. Не можу завантажити шаблон. Скасування Зміни зберігаються. Вибрати файл Шаблон класу не має {0} власності Натисніть, щоб вибрати зображення Ім'я Компонента Компонент тіла Ім'я компонента Параметри компонентів можна не оновлювати Компонент налаштування успішно оновлено Компонент |0| не знайдено Компонент |component_name| зламаний Поточний встановлений шаблон не може бути встановлена заново DB Error Дата Демо онлайн Опис Завантажити Введіть URL-адресу Помилка Помилка - шаблон не вказано Помилка на додавання в БД Не зміг завантажити архів Повна установка демо-даних Встановити Встановити повні демо-дані Встановити з demodata Встановлено Установка повна демо-дані можуть знищити всі ваші бази даних і додавання даних. Ліцензійна угода Локальний файл Логотип Логотип І Favicon Повідомлення Модуль для спрощення управління шаблонами Немає Ніяких вхідних даних, зазначених Немає файла params.xml Жодних шаблонів Номер Один або декілька залежностей помилка Тільки образ може бути завантажений Оплатити Будь ласка, використовуйте функцію електронної пошти : siteinfo() з параметром Сховище шаблонів Зберегти Зазначений zip-файл не існує Почніть вводити ім'я тут... Статус Підтримка Система Оплати Шаблон Редактор Шаблонів Шаблон вже існує Шаблон демо-дані успішно встановлено. Шаблон не існує в теку шаблонів Шаблону не існує. Список дизайнів Ім'я шаблону не встановлено. Шаблон був успішно видалений. Шаблон був успішно завантажений. Шаблон був успішно встановлений. Шаблон був успішно завантажений. Шаблони є інтелектуальною власністю, тому, якщо ви <br /> хочете, щоб встановити його, ви повинні прийняти ліцензійну угоду. Управління дизайном Налаштування дизайну Цей модуль не існує Цей модуль вимагається Цей модуль є прийняття бажаного за дійсне Цей віджет є обов'язковим Цей віджет є прийняття бажаного за дійсне Тип Не вдається вилучити архів Завантажити Завантажити шаблон Керівництво Користувача Версія Невірний Тип файлу Неправильний Тип файлу. Zip-архіви тільки XML-файл помилка читання Безкоштовний Дизайн інтернет-магазину Платний 