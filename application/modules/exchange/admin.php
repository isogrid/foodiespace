<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

use CMSFactory\assetManager as AssetManager;
use CMSFactory\ModuleSettings;


use Currency\Currency;
use Map\SProductVariantPriceTableMap;
use Map\SProductVariantPriceTypeTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\PropelException;
use VariantPriceType\BaseVariantPriceType;

class Admin extends BaseAdminController
{

    /**
     *
     * @var array
     */
    protected $ignoreExisting;

    public function __construct() {

        parent::__construct();
        $this->ignoreExisting = [
            'products'    => lang('Products', 'exchange'),
            'description' => lang('Description', 'exchange'),
            'categories'  => lang('Categories', 'exchange'),
            'properties'  => lang('Properties', 'exchange'),
        ];
        //        \ShopController::checkVar();
        //        \ShopController::checkLicensePremium();
        $this->locale = MY_Controller::getCurrentLocale();
        $lang = new MY_Lang();
        $lang->load('exchange');
        //        $this->load->config('exchange');

        $this->tempDir = './uploads/cmlTemp';
        if (!is_dir($this->tempDir)) {
            mkdir($this->tempDir);
        }
    }

    public function index() {
        $variantPriceTypeData = SProductVariantPriceTypeQuery::create()
            ->setComment(__METHOD__)
            ->joinWithCurrency(Criteria::LEFT_JOIN)
            ->joinWithSProductVariantPriceTypeValue(Criteria::LEFT_JOIN)
            ->orderByPosition()
            ->find();


        $data = [
            'settings'       => $this->get1CSettings(),
            'statuses'       => $this->get_orders_statuses(),
            'ignoreExisting' => $this->ignoreExisting,
            'variantPriceTypeData'=>$variantPriceTypeData

        ];

        AssetManager::create()
            ->setData($data)
            ->registerScript('admin')
            ->registerStyle('admin')
            ->renderAdmin('settings');
    }

    private function get1CSettings() {

        return ModuleSettings::ofModule('exchange')->get();
    }

    private function get_orders_statuses() {

        return $this->db->query(
            "SELECT * FROM `shop_order_statuses`
            JOIN `shop_order_statuses_i18n` ON shop_order_statuses.id=shop_order_statuses_i18n.id
            WHERE `locale`='" . $this->locale . "'"
        )->result_array();
    }

    public function update_settings() {

        $config = $this->input->post('1CSettings');

        if (!$config) {
            showMessage(lang('No config data provided', '', 'r', 'exchange'));
            return;
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('1CSettings[filesize]', lang('Size of the file being loaded at the same time', 'exchange'), 'integer|required');

        if ($config['email'] != null) {
            $this->form_validation->set_rules('1CSettings[email]', lang('Email Administrator for sending important safety mistakes', 'exchange'), 'valid_email');
        }

        if ($this->form_validation->run() == false) {
            showMessage(validation_errors(), '', 'r');
            return;
        }

        ModuleSettings::ofModule('exchange')->set($config);
        $this->lib_admin->log(lang('1C settings was edited', 'exchange'));

        showMessage(lang('Settings saved', 'exchange'));
    }

    public function startImagesResize() {

        ShopCore::app()->SWatermark->updateWatermarks(true);
        showMessage(lang('Images saved', 'exchange'));
    }

    /**
     * Set shop categories for products
     * @throws Exception
     */
    public function setAdditionalCats() {

        $products = $this->db->select('shop_products.category_id, shop_products.id, shop_category.full_path_ids, shop_category.parent_id')
            ->join('shop_category', 'shop_category.id = shop_products.category_id')
            ->get('shop_products')
            ->result();

        $this->db->truncate('shop_product_categories');
        foreach ($products as $product) {
            foreach (unserialize($product->full_path_ids) as $fpi) {
                $this->db->set('category_id', $fpi);
                $this->db->set('product_id', $product->id);
                try {
                    $this->db->insert('shop_product_categories');
                    if ($this->db->_error_message()) {
                        throw new Exception("product {$product->id} already in this category $fpi");
                    }
                } catch (Exception $e) {
                    echo $e->getMessage() . PHP_EOL;
                }
            }
            if (!in_array($product->parent_id, unserialize($product->full_path_ids))) {
                $this->db->set('category_id', $product->parent_id);
                $this->db->set('product_id', $product->id);

                $this->db->insert('shop_product_categories');
            }
            if (!in_array($product->category_id, unserialize($product->full_path_ids))) {
                $this->db->set('category_id', $product->category_id);
                $this->db->set('product_id', $product->id);

                $this->db->insert('shop_product_categories');
            }
        }

        $this->lib_admin->log(lang('1C additional categories was started', 'exchange'));
        redirect('/admin/components/cp/exchange');
    }

    public function clear($type) {

        switch ($type) {
            case 'error':
                write_file($this->tempDir . 'error_log.txt', '');
                break;
            case 'log':
                write_file($this->tempDir . 'log.txt', '');
                break;
            case 'time':
                write_file($this->tempDir . 'time.txt', '');
                break;
        }
    }

    public function log($type) {

        switch ($type) {
            case 'error':
                $txt = read_file($cmlTempFilePath . 'error_log.txt');
                $txt = explode("\n", $txt);
                if (count($txt) > 40000) {
                    $txt = array_slice($txt, count($txt) - 20000);
                }
                foreach ($txt as $value) {
                    preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $value, $ip);
                    $text .= preg_replace('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', '<br><b>' . $ip[0] . '</b>', $value) . '<br>';
                }
                $title = lang('Errors log', 'exchange');
                break;
            case 'log':
                $txt = read_file($cmlTempFilePath . 'log.txt');
                $txt = explode("\n", $txt);
                if (count($txt) > 40000) {
                    $txt = array_slice($txt, count($txt) - 20000);
                }
                foreach ($txt as $value) {
                    $text .= $value . '<br>';
                }
                $title = lang('Query log', 'exchange');
                break;
            case 'time':
                $txt = read_file($cmlTempFilePath . 'time.txt');
                $txt = explode("\n", $txt);
                if (count($txt) > 40000) {
                    $txt = array_slice($txt, count($txt) - 20000);
                }
                foreach ($txt as $value) {
                    $text .= $value . '<br>';
                }
                $title = lang('Time log', 'exchange');
                break;
        }

        $this->lib_admin->log(lang('1C log was cleared', 'exchange'));
        \CMSFactory\assetManager::create()
            ->setData('type', $type)
            ->setData('text', trim($text, '<br>'))
            ->setData('title', $title)
            ->renderAdmin('Log');
    }

}