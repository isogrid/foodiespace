<?php

namespace exchange\classes;

use CMSFactory\ModuleSettings;

/**
 *
 *
 * @author kolia
 */
class UserBonus extends ExchangeBase
{

    protected $users_for_compare = [];

    /**
     *
     */
    protected function import_()
    {

        \CI::$APP->load->helper('translit');

        // preparing data array for insert, splitting on new and existing properties
        $this->getAllUsers();


    }

    /**
     * Parsing properties. Separation on new and existing.
     * Preparing arrays (insert & update) for db
     */
    protected function getAllUsers()
    {

        $users = \CI::$APP->db->get('users');
        \CI::$APP->db->query('TRUNCATE TABLE  `mod_system_bonus_data_value`');
        if (\CI::$APP->db->_error_message() && \CI::$APP->db->_error_message() != null && \CI::$APP->db->_error_message() != '') {
            dd(111, \CI::$APP->db->_error_message());
        }

        if ($users && $users->num_rows > 0) {
            foreach ($users->result_array() as $user) {
                if ($user['card_number'] != null && $user['card_number'] != ''/*&& $user['referer_card_number'] != ''*/) {
                    $this->users_for_compare[$user['card_number']] = $user;
                }
            }
        } else {
            echo 'not found users';
            exit;
        }
        $roles = \CI::$APP->db->get('shop_rbac_roles');
        foreach ($roles->result_array() as $role) {
            $role_data[$role['id']] = $role['name'];
        }
        $role_data = array_flip($role_data);
//        dump($role_data);
        foreach ($this->importData as $user_bonus) {
            $user_buus_data = (array)$user_bonus;
//            dd($user_buus_data);
            if (key_exists($user_buus_data['card_number'], $this->users_for_compare)) {
//                if(key_exists($user_buus_data['group_name'], $role_data)){
                $new_user_data[$user_buus_data['card_number']] = [
                    'id' => $this->users_for_compare[$user_buus_data['card_number']]['id'],
                    'role_id' => $role_data[$user_buus_data['group_name']],
                    'have_a_card' => $user_buus_data['ЕстьКлубнаяКартка']];
//                }
//                $new_user_data[$user_buus_data['card_number']] =['have_a_card'=>$user_buus_data['ЕстьКлубнаяКартка']];
                $bonus_adata[] = ['user_id' => $this->users_for_compare[$user_buus_data['card_number']]['id'],
                    'count_bonus' => (int)$user_buus_data['sum_bonuses']];
            }
//            dd($new_user_data, $bonus_adata);
        }

        \CI::$APP->db->insert_batch( 'mod_system_bonus_data_value', $bonus_adata);

        if (\CI::$APP->db->_error_message() && \CI::$APP->db->_error_message() != null && \CI::$APP->db->_error_message() != '') {
            dd(222, \CI::$APP->db->_error_message(), \CI::$APP->db->last_query());
        }

        $this->updateBatch('users', $new_user_data, 'id');
    }

}