<?php

namespace exchange\classes;

use CMSFactory\ModuleSettings;
use CustomFieldsData;
use CustomFieldsDataQuery;
use SProductsQuery;


use Currency\Currency;
use Map\SProductVariantPriceTableMap;
use Map\SProductVariantPriceTypeTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\PropelException;
use VariantPriceType\BaseVariantPriceType;
use SProductVariantPriceTypeQuery;
use SProductVariantsQuery;
use SProductVariantPriceQuery;
use SProductVariantPrice;

/**
 *
 *
 * @author kolia
 */
class Prices extends ExchangeBase
{

    protected function import_()
    {
        $data = [];
        $i = 0;
        $purchasePriceId = false;
        $mainPriceId = false;
        $purchasePrice = false;
        $mainPrice = false;
        $price_add = false;


        $price_type = ModuleSettings::ofModule('exchange')->get('price');
        /*Для доп. цен*/
        $variantPriceTypeData = SProductVariantPriceTypeQuery::create()
            ->setComment(__METHOD__)
            ->joinWithCurrency(Criteria::LEFT_JOIN)
            ->joinWithSProductVariantPriceTypeValue(Criteria::LEFT_JOIN)
            ->orderByPosition()
            ->find();

        foreach ($variantPriceTypeData as $type_prise_shop) {
            $types_prise_shop[$type_prise_shop->getId()] = $type_prise_shop->getNameType();
        }
        if($types_prise_shop==null){
            $types_prise_shop=['00000'=>'0000'];
        }
        /*END Для доп. цен*/
        $one_c_price_types = (array)$this->xml->ПакетПредложений->ТипыЦен;
        $one_c_price_types1 = $one_c_price_types['ТипЦены'];
        $priceIds =[];
        foreach ($one_c_price_types1 as $kk => $type) {
            foreach ($types_prise_shop as $key => $price) {
                (string)$type->Наименование == 'Прих' && $purchasePriceId = (string)$type->Ид;
                (string)$type->Наименование == 'Розд' && $mainPriceId = (string)$type->Ид;
                /*Для доп. цен*/
                (string)$type->Наименование == $price && $priceIds[$key] = (string)$type->Ид;
                (string)$type->Наименование != $price && $priceIdsNew[(string)$type->Ид] = (string)$type->Ид;
                (string)$type->Наименование != $price && $priceIdsNew1[(string)$type->Ид] = [(string)$type->Наименование, (string)$type->Валюта, (string)$type->Ид];
                /*END Для доп. цен*/
            }
        }

        $final_new_price_tupes_to_shop = array_diff($priceIdsNew, array_merge($priceIds, [$mainPriceId, $purchasePriceId]));


        if (count($final_new_price_tupes_to_shop) > 0) {
            foreach ($final_new_price_tupes_to_shop as $n_k => $tupe_n) {

                $prc_type = new \SProductVariantPriceType();

                $cur = \SCurrenciesQuery::create()->findOneByCode($priceIdsNew1[$n_k]['1'])==null
                    ?\SCurrenciesQuery::create()->findOneByMain(1)->getId()
                    :\SCurrenciesQuery::create()->findOneByCode($priceIdsNew1[$n_k]['1'])->getId();
                $prc_type->setNameType($priceIdsNew1[$n_k]['0']);
                $prc_type->setCurrencyId($cur);
                $prc_type->setStatus('1');
                $prc_type->setPriceType('1');
                $prc_type->save();
                $priceIds[$prc_type->getId()]=$n_k;
            }
        }



        foreach ($this->importData as $offer) {

            foreach ($offer->Цены->Цена as $one) {
                ((string)$one->ИдТипаЦены === $purchasePriceId) && $purchasePrice = str_replace(',', '.', (string)$one->ЦенаЗаЕдиницу);
                ((string)$one->ИдТипаЦены === $mainPriceId) && $mainPrice = str_replace(',', '.', (string)$one->ЦенаЗаЕдиницу);
                ((string)$one->Валюта) && $curr_1c_mainPrc = str_replace(',', '.', (string)$one->Валюта);
                /*Для доп. цен*/
                foreach ($priceIds as $key1 => $price_id) {

                    ((string)$one->ИдТипаЦены === $price_id) && $price_add[$key1] = str_replace(',', '.', (string)$one->ЦенаЗаЕдиницу);
                    ((string)$one->Валюта) && $curr_1c = str_replace(',', '.', (string)$one->Валюта);
                }
                /*END Для доп. цен*/
            }


            $price = $mainPrice ?: str_replace(',', '.', (string)$mainPrice ?: (string)$offer->Цены->Цена->ЦенаЗаЕдиницу);
            $data[$i]['price_in_main'] = $price;

            $currency = $this->db->select('*')
                ->where('code', $curr_1c_mainPrc)
                ->or_where('symbol', $curr_1c_mainPrc)
                ->or_where('name', $curr_1c_mainPrc)
                ->get('shop_currencies')->row_array();

            $data[$i]['currency'] = $currency['id'];

            $data[$i]['price'] = str_replace(',', '.', round($price / $currency['rate'], 2));

            if (ModuleSettings::ofModule('exchange')->get('purchcePriceFieldId') && $purchasePrice) {
                $this->setPurchacePrice((string)$offer->Ид, $purchasePrice);
            }

            if (property_exists($offer, 'Склад')) {
                $data[$i]['stock'] = (int)xml_attribute($offer->Склад, 'КоличествоНаСкладе');
            } elseif (property_exists($offer, 'Количество')) {
                $data[$i]['stock'] = (int)$offer->Количество;
            }
            $price_adds['prices'] = $price_add;
            $data[$i]['external_id'] = (string)$offer->Ид . '-city-' . $this->city_id;


            /*Для доп. цен*/
            $this->saveVariantsPrices((string)$offer->Ид . '-city-' . $this->city_id, $price_adds);
            /*END Для доп. цен*/

            $i++;
        }

        $this->updateBatch('shop_product_variants', $data, 'external_id');
    }

    public function saveVariantsPrices($var_ext_id, $price_adds)
    {

        $variant = SProductVariantsQuery::create()
            ->findOneByExternalId($var_ext_id);

        if (!$variant || $variant == null) {
            return;
        }

        SProductVariantPriceQuery::create()
            ->findByVarId($variant->getId())
            ->delete();

        $prices = array_diff($price_adds, ['']);

        $price_types = $this->getPriceTypesData(array_keys($prices['prices']));

        foreach ($prices['prices'] as $type_id => $price) {
            $model = new SProductVariantPrice();


            if ($price_types[$type_id]['price_type'] == BaseVariantPriceType::PRICE_TYPE_PERCENT) {

                $final = Currency::create()
                    ->toMain(BaseVariantPriceType::getPercent((string)$price, $variant->getPriceInMain()), $variant->getSCurrencies()->getId());
            }

            $model->addToModel(
                [
                    'var_id' => $variant->getId(),
                    'type_id' => $type_id,
                    'price' => $price,
                    'prod_id' => $variant->getProductId(),
                    // конвертирует в стринг так как float не пишет в таблицу как double  а рубает по запятой
                    'final' => (string)str_replace(',', '.', $final) ?: Currency::create()->toMain((string)$price, $price_types[$type_id]['currency']),
                ]
            );

        }
    }

    private function getPriceTypesData(array $arrayKeys)
    {

        $priceType = SProductVariantPriceTypeQuery::create()
            ->withColumn(SProductVariantPriceTypeTableMap::COL_ID, 'id')
            ->withColumn(SProductVariantPriceTypeTableMap::COL_PRICE_TYPE, 'price_type')
            ->withColumn(SProductVariantPriceTypeTableMap::COL_CURRENCY_ID, 'currency')
            ->select(['id', 'price_type', 'currency'])
            ->findPks($arrayKeys)->toArray();

        $data = [];

        foreach ($priceType as $item) {
            $data[$item['id']] = [
                'price_type' => $item['price_type'],
                'currency' => $item['currency'],
            ];
        }

        return $data;
    }

    /**
     * @param string $externalId
     * @param string $purchasePrice
     */
    protected function setPurchacePrice($externalId, $purchasePrice)
    {

        $fieldId = ModuleSettings::ofModule('exchange')->get('purchcePriceFieldId');
        $product = SProductsQuery::create()->setComment(__METHOD__)->findOneByExternalId($externalId);
        if ($product) {
            $productId = $product->getId();
            $cf = CustomFieldsDataQuery::create()->setComment(__METHOD__)->filterByentityId($productId)->filterByfieldId($fieldId)->findOne();

            if (!$cf) {
                $cf = new CustomFieldsData();
                $cf->setfieldId($fieldId);
                $cf->setentityId($productId);
                $cf->setLocale(\MY_Controller::defaultLocale());
            }
            $cf && $cf->setdata($purchasePrice)->save();
        }
    }

}