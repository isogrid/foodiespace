<?php

/**
 * error
 */
$lang['error_user_message_doesnt_send'] = 'Сообщение пользователю не отправлено';

/**
 * form validation
 */
$lang['Template_name'] = 'Название шаблона';
$lang['From'] = 'От кого';
$lang['From_sms'] = 'От кого(sms)';
$lang['Template_theme'] = 'Тема шаблона';
$lang['Template_user_mail'] = 'Шаблон письма пользователю';
$lang['Template_admin_mail'] = 'Шаблон письма администратору';
$lang['Admin_address'] = 'Адресс администратора';

/**
 * Mail Test
 */
$lang['Check_sms_sending'] = 'Проверка отправки почты.';

/**
 * admin
 */
$lang['Template_created'] = "Шаблон создан";
$lang['Template_edited'] = "Шаблон отредактирован";
$lang['Admin_sms'] = 'Емейл администратора';
$lang['Sender_eamil'] = 'Емейл отправителя';
$lang['Wraper'] = 'Обгортка';
$lang['Message'] = 'Сообщение';
$lang['Field'] = 'Поле';
$lang['must_contain_variable'] = 'должно содержать переменную';
$lang['Settings_saved'] = 'Настройки сохранены';

/**
 ******************************** Tempalets
 *
 * Create and Edit template
 */

$lang['Creating_mail_template'] = 'Создание шаблона письма';
$lang['Template_name_only_latin'] = 'Название шаблона письма (только латиница)';
$lang['Theme'] = 'Тема';
$lang['Message_type'] = 'Тип письма';
$lang['Send_sms_to_user'] = 'Отправлять письмо пользователю';
$lang['Send_sms_to_admin'] = 'Отправлять письмо администратору';
$lang['Yes'] = 'Да';
$lang['No'] = 'Нет';
$lang['Template_description'] = 'Описание шаблона';
$lang['Sms_template_editing'] = 'Редактирование шаблона письма';
$lang['Template_variables'] = 'Переменные шаблона';
$lang['Template_settings'] = 'Настройки шаблона';

/**
 * Edit template variables
 */

$lang['Variables'] = 'Переменние';
$lang['Values'] = 'Значения';
$lang['Edit'] = 'Редактировать';
$lang['Delete'] = 'Удалить';
$lang['Add_new_variable'] = 'Добавить новую переменную';


/**
 * list template
 */
$lang['Template_sms_deleting'] = 'Удаление шаблона письма';
$lang['Delete_selected_templates'] = 'Удалить выбраные шаблоны';
$lang['Template_list_view'] = 'Просмотр списка шаблонов';
$lang['Cancel'] = 'Отмена';

$lang['Create_template'] = 'Создать шаблон';
$lang['Settings'] = 'Настройки';
$lang['Description'] = 'Описание';
$lang['Template_list_empty'] = 'Список шаблонов пустой';
$lang['Use_wraper'] = 'Использовать обгортку';

/**
 *Settings
 */
$lang['Server_path_to_sendmail'] = 'Серверный путь к Sendmail';
$lang['Port'] = 'Порт';
$lang['Protocol'] = 'Протокол';
$lang['Install_standart_templates'] = 'Установить стандартние шаблони';
$lang['sms_sent'] = 'Ваше сообщение успешно отправлено';









?>
