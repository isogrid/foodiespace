<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
use CMSFactory\Events;
use CMSFactory\assetManager;

/**
 * @package alfasms
 * @property alfasms_model $alfasms_model
 * Image CMS
 * Smss
 */
class Alfasms extends MY_Controller
{
    public static $check_event = false;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('alfasms_model');
        $lang = new MY_Lang();
        $lang->load('alfasms');
    }

    public static function adminAutoload()
    {
        parent::adminAutoload();
        \CMSFactory\Events::create()->setListener("onShopAdminOrdersEdit", "ShopAdminOrders:edit");
        \CMSFactory\Events::create()->setListener("onShopAdminOrdersCreate", "ShopAdminOrder:create");
//        \CMSFactory\Events::create()->setListener("onShopOrdersCreate", "Cart:MakeOrder");
    }

    public function autoload()
    {
//         \CMSFactory\Events::create()->setListener("onShopOrdersCreate", "Cart:MakeOrder");
        Events::create()->onShopMakeOrder()->setListener('oonShopOrdersCreate');
        Events::create()->onAuthUserPreRegister()->setListener('sms_userPreRegister');
//        \CMSFactory\Events::create()->onAuthUserPreRegister()->setListener('save_order_data_module');

    }

    public static function sms_userPreRegister($post_data)
    {
        if (!self::$check_event) {

            $exist_user_temp = \CI::$APP->db->where('phone', $post_data['phone'])
                ->or_where('email', $post_data['email'])
                ->get('user_temp');
            if (!$exist_user_temp || $exist_user_temp->num_rows() <= 0) {

                $key = random_string('nozero', 5);
//                setcookie("sms_username", $post_data['username'], time() + 300,'',base_url());
//                setcookie("sms_email", $post_data['email'], time() + 300,'',base_url());
//                setcookie("sms_reg_key", md5($key), time() + 300,'',base_url());
//                setcookie("sms_reg_key_or", $key, time() + 300,'',base_url());
//                setcookie("sms_reg_phone_num", $post_data['phone'], time() + 300,'',base_url());
//                setcookie("password_reg", $post_data['password'], time() + 300,'',base_url());
//                setcookie("date_reg_birth", $post_data['date'], time() + 300,'',base_url());
//                setcookie("sity_id_reg", $post_data['city'], time() + 300,'',base_url());
//                setcookie("sms_crypt_phone_num", md5($post_data['phone']), time() + 300,'',base_url());
                \alfasms\sms::getInstance()->sendSmsRegKey($post_data['phone'], 'sendSmsRegKey', $key);

                \CI::$APP->db->insert('user_temp', ['username' => $post_data['username'],
                    'password' => $post_data['password'],
                    'email' => $post_data['email'],
                    'activation_key' => $key,
                    'phone' => $post_data['phone'],
                    'date_birth' => $post_data['date'],
                    'city' => $post_data['city'],
                    'akt_key_kreation' => time()
                ]);
                \CI::$APP->session->set_flashdata('user_preregister_data', $post_data);
                return assetManager::create()
                    ->registerScript('for_auth');

            } else {
                \CI::$APP->session->set_flashdata('user_preregister_data', $post_data);
                return assetManager::create()
                    ->setData('user_posted_data', [$post_data])
                    ->setData('errors', true)
                    ->registerScript('for_auth');
//                return [$post_data, assetManager::create() ->registerScript('for_auth')];
            }
            self::$check_event = true;
        }
    }

    public function confirmcode()
    {

        $exist_user_temp = \CI::$APP->db->where('phone', $this->input->post('phone1'))
            ->or_where('email', $this->input->post('email1'))
            ->or_where('activation_key', $this->input->post('sms_code'))
            ->get('user_temp');

        if ($exist_user_temp && $exist_user_temp->num_rows() > 0) {
            $exist_user_temp = $exist_user_temp->result_array();
            if ($exist_user_temp['0']['activation_key'] == $this->input->post('sms_code')) {
                $this->dx_auth->register($this->input->post('username1'), $this->input->post('password1'),
                    $this->input->post('email1'), '', $this->input->post('sms_code'), $this->input->post('phone1'));

//                $modules = CI::$APP->template->template_vars['modules'];
//                if (array_key_exists('mod_cities', $modules)) {
//                    $cities = module('mod_cities')->get_cities_register();
//                    if (key_exists($this->input->post('city1'), $cities)) {
                \CI::$APP->db->where('phone LIKE', $this->input->post('phone1'))->set(['city_id' => (int)$this->input->post('city1'),
                    'birth_date' => $this->input->post('date1')])
                    ->update('users');
                if (\CI::$APP->db->_error_message() && \CI::$APP->db->_error_message() != null && \CI::$APP->db->_error_message() != '') {
                    dd(\CI::$APP->db->_error_message());
                }
                \CI::$APP->db->where('phone', $this->input->post('phone1'))
                    ->or_where('email', $this->input->post('email1'))
                    ->delete('user_temp');
//                    }
//                }

                echo assetManager::create()
                    ->setData('success', true)
//                ->registerScript('for_auth')
                    ->render('formRegSMS');
            } else {

                return assetManager::create()
                    ->setData('errors', true)
//                ->registerScript('for_auth')
                    ->render('formRegSMS');
            }
        } else {

            return assetManager::create()
                ->setData('errors', true)
//                ->registerScript('for_auth')
                ->render('formRegSMS');
        }
    }

    public
    function repeat_getting_Code()
    {

        if ($this->input->post('repeat') == 'repeat') {
            $post_data = $this->input->post();
            $user_preregister_data = ['username' => $post_data['username1'],
                'password' => $post_data['password1'],
                'email' => $post_data['email1'],
                'phone' => $post_data['phone1'],
                'date_birth' => $post_data['date1'],
                'city' => $post_data['city1']];


            \CI::$APP->session->set_flashdata('user_preregister_data', $user_preregister_data);
            $exist_user_temp = \CI::$APP->db->where('phone', $this->input->post('phone1'))
                ->or_where('email', $this->input->post('email1'))
                ->get('user_temp');
            if ($exist_user_temp && $exist_user_temp->num_rows() > 0) {
                $exist_user_temp = $exist_user_temp->result_array();
                $exist_user_temp = $exist_user_temp['0'];
//                dd($exist_user_temp['akt_key_kreation'] + 300 , time());
                if ($exist_user_temp['akt_key_kreation'] + 300 > time()) {
                    \CI::$APP->session->set_flashdata('user_preregister_data', $user_preregister_data);
//                echo assetManager::create()
//                    ->setData('repeat_error', true)
//                    ->registerScript('for_auth')
//                    ->render('formRegSMS');

                    return 'wait';
                }

//            if ($user_preregister_data && $user_preregister_data['phone'] !=null && $user_preregister_data['phone']!='') {
                \alfasms\sms::getInstance()->sendSmsRegKey($this->input->post('phone1'), 'sendSmsRegKey', $exist_user_temp['activation_key']);
//                echo assetManager::create()
//                    ->setData('repeat_success', true)
//                   ->registerScript('for_auth')
//                    ->render('formRegSMS');
                \CI::$APP->db->where('phone', $this->input->post('phone1'))
                    ->or_where('email', $this->input->post('email1'))
                    ->set('akt_key_kreation', time())
                    ->update('user_temp');

                return 'true';
            } else {

                \CI::$APP->session->set_flashdata('user_preregister_data', $user_preregister_data);
//                echo assetManager::create()
//                    ->setData('repeat_error', true)
//                    ->registerScript('for_auth')
//                    ->render('formRegSMS');
                return 'false';
            }
        }

    }


    public
    static function oonShopOrdersCreate($arg)
    {

        if (!self::$check_event) {


            $orderId = $arg['order']->getId();
            $user_phone = $arg['order']->user_phone;
            $statusHistory = SOrderStatusHistoryQuery::create()
                ->filterByOrderId((int)$orderId)
                ->find();
            foreach ($statusHistory as $history) {

            }
            $statusComment = $history->comment;
            $StatusDeliveryId = SOrdersQuery::create()->filterById($orderId)->findOne()->getStatus();
            $StatusDeliveryName = SOrderStatusesQuery::create()->filterById($StatusDeliveryId)->findOne()->getName();

            if ($arg['order']->getPaid() == TRUE) {
                $statusPaid = 'Оплачен';
            }
            if ($arg['order']->getPaid() == FALSE) {
                $statusPaid = 'Не Оплачен';
            }
            $delivery_price = $arg['order']->delivery_price;
            $discount = $arg['order']->discount;
            $origin_price = $arg['order']->origin_price;
            $final_price = $arg['order']->origin_price - $discount + $delivery_price - $arg['order']->comulativ - $arg['order']->gift_cert_price;
            $currency = \Currency\Currency::create()->getSymbol();
            $products = SOrdersQuery::create()->filterById($orderId)->findOne()->getOrderProducts();

            /** Begining creating the table * */
            $tdStyle = "\n";
            $productsForSms = '';
            $total = 0;
            // adding product rows
            foreach ($products as $item) {
                $curTotal = $item->price * $item->quantity;
                $total += $curTotal;
                $productsForSms .= 'Продукт' . '-' . $item->product_name . '-' . $item->variant_name . ';  ' . 'Количество' . '-' . $item->quantity . ';  ' . 'Цена' . '-' . $item->price . $defaultCurrency . ';  ' . 'Cумма' . '-' . $curTotal . $defaultCurrency . $tdStyle;

            }

            // if there is a discount
            if ($discount) {
                $total -= $discount;
                $productsForSms .= 'Сумма скидки' . $discount . '-' . $defaultCurrency . $tdStyle;
            }

            // total row
            $productsForSms .= 'Общая сумма' . '-' . $total . ' ' . $defaultCurrency;
            $productsForSms .= '';
            $SmsData = array(
                'orderId' => $orderId,
                'userName' => $arg['order']->getUserFullName(),
                'statusComment' => $statusComment,
                'StatusDeliveryName' => $StatusDeliveryName,
                'statusPaid' => $statusPaid,
                'delivery_price' => $delivery_price . ' ' . $currency . '.',
                'discount' => $discount . ' ' . $currency . '.',
                'origin_price' => $origin_price . ' ' . $currency,
                'final_price' => $final_price . ' ' . $currency . '.',
                'Comment' => $_POST['Comment'],
//            'custom_field' => $_POST['custom_field']['104'],
                'productsForSms' => $productsForSms,
            );

            self::$check_event = true;
            \alfasms\sms::getInstance()->sendSms($user_phone, 'make_order', $SmsData);
        }
    }


    public
    static function onShopAdminOrdersCreate($arg)
    {
        $orderId = $arg['order']->getId();
        $user_phone = $arg['order']->user_phone;
        $statusHistory = SOrderStatusHistoryQuery::create()
            ->filterByOrderId((int)$orderId)
            ->find();
        foreach ($statusHistory as $history) {

        }
        $statusComment = $history->comment;
        $StatusDeliveryId = SOrdersQuery::create()->filterById($orderId)->findOne()->getStatus();
        $StatusDeliveryName = SOrderStatusesQuery::create()->filterById($StatusDeliveryId)->findOne()->getName();

        if ($arg['order']->getPaid() == TRUE) {
            $statusPaid = 'Оплачен';
        }
        if ($arg['order']->getPaid() == FALSE) {
            $statusPaid = 'Не Оплачен';
        }
        $delivery_price = $arg['order']->delivery_price;
        $discount = $arg['order']->discount;
        $origin_price = $arg['order']->origin_price;
        $final_price = $arg['order']->origin_price - $discount + $delivery_price - $arg['order']->comulativ - $arg['order']->gift_cert_price;
        $currency = \Currency\Currency::create()->getSymbol();
        $products = SOrdersQuery::create()->filterById($orderId)->findOne()->getOrderProducts();

        /** Begining creating the table * */
        $tdStyle = "\n";
        $productsForSms = '';
        $total = 0;
        // adding product rows
        foreach ($products as $item) {
            $curTotal = $item->price * $item->quantity;
            $total += $curTotal;
            $productsForSms .= 'Продукт' . '-' . $item->product_name . '-' . $item->variant_name . ';  ' . 'Количество' . '-' . $item->quantity . ';  ' . 'Цена' . '-' . $item->price . $defaultCurrency . ';  ' . 'Cумма' . '-' . $curTotal . $defaultCurrency . $tdStyle;

        }

        // if there is a discount
        if ($discount) {
            $total -= $discount;
            $productsForSms .= 'Сумма скидки' . $discount . '-' . $defaultCurrency . $tdStyle;
        }

        // total row
        $productsForSms .= 'Общая сумма' . '-' . $total . ' ' . $defaultCurrency;
        $productsForSms .= '';
        $SmsData = array(
            'orderId' => $orderId,
            'statusComment' => $statusComment,
            'StatusDeliveryName' => $StatusDeliveryName,
            'statusPaid' => $statusPaid,
            'delivery_price' => $delivery_price . ' ' . $currency . '.',
            'discount' => $discount . ' ' . $currency . '.',
            'origin_price' => $origin_price . ' ' . $currency . '.',
            'final_price' => $final_price . ' ' . $currency . '.',
            'Comment' => $_POST['Comment'],
            'custom_field' => $_POST['custom_field']['104'],
            'productsForSms' => $productsForSms,
        );

        \alfasms\sms::getInstance()->sendSms($user_phone, 'admin_create_order', $SmsData);

    }

    public
    static function onShopAdminOrdersEdit($arg)
    {
        if ($_POST['Notify'] == 'on') {
            $orderId = $arg['model']->getId();
            $user_phone = $arg['model']->user_phone;
            $statusHistory = SOrderStatusHistoryQuery::create()
                ->filterByOrderId((int)$orderId)
                ->find();
            foreach ($statusHistory as $history) {

            }
            $statusComment = $history->comment;
            $StatusDeliveryId = SOrdersQuery::create()->filterById($orderId)->findOne()->getStatus();
            $StatusDeliveryName = SOrderStatusesQuery::create()->filterById($StatusDeliveryId)->findOne()->getName();

            if ($arg['model']->getPaid() == TRUE) {
                $statusPaid = 'Оплачен';
            }
            if ($arg['model']->getPaid() == FALSE) {
                $statusPaid = 'Не Оплачен';
            }
            $delivery_price = $arg['model']->delivery_price;
            $discount = $arg['model']->discount;
            $origin_price = $arg['model']->origin_price;
            $final_price = $arg['model']->origin_price - $discount + $delivery_price - $arg['model']->comulativ - $arg['model']->gift_cert_price;
            $currency = \Currency\Currency::create()->getSymbol();

            $SmsData = array(
                'orderId' => $orderId,
                'statusComment' => $statusComment,
                'StatusDeliveryName' => $StatusDeliveryName,
                'statusPaid' => $statusPaid,
                'delivery_price' => $delivery_price . ' ' . $currency . '.',
                'discount' => $discount . ' ' . $currency . '.',
                'origin_price' => $origin_price . ' ' . $currency . '.',
                'final_price' => $final_price . ' ' . $currency . '.',
                'Comment' => $_POST['Comment'],
                'custom_field' => $_POST['custom_field']['102']
            );

            \alfasms\sms::getInstance()->sendSms($user_phone, 'change_order_status', $SmsData);
        }
    }

    public
    function _install()
    {
        $this->alfasms_model->install();
    }

    public
    function _deinstall()
    {
        $this->alfasms_model->deinstall();
    }

}

/* End of file sms.php */
