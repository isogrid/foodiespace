<?php

namespace alfasms\classes;

/**
 * Image CMS
 * Module Wishlist
 * @property \alfasms_model $alfasms_model
 * @property \DX_Auth $dx_auth
 * @property \CI_URI $uri
 * @property \CI_DB_active_record $db
 * @property \CI_Input $input
 * @version 1.0 big start!
 */
class ParentSms extends \MY_Controller {
    /**
     * Sms sender name
     * @var string
     */

    /**
     * Receiver sms
     * @var string
     */
    protected $send_to;

    /**
     * Sms theme
     * @var string
     */
    protected $theme;

    /**
     * Sms message
     * @var string
     */
    protected $message;

    /**
     * sms protocol
     * @var string
     */
    protected $protocol;

    /**
     * sms port
     * @var int
     */

    /**
     * Server path to Sendsms
     * @var string
     */
    protected $smspath;

    /**
     * Array of errors
     * @var array
     */
    public $errors = array();

    /**
     * Array of data
     * @var array
     */
    public $data_model = array();

    /**
     * List of accepted params
     * @var array
     */
    public $accepted_params = array(
        'name',
        'theme',
        'user_message',
        'user_message_active',
        'admin_message',
        'admin_message_active',
        'admin_sms',
    );

    public function __construct() {

        parent::__construct();
        $this->load->module('core');
        $this->load->model('../modules/alfasms/models/alfasms_model');
        $lang = new \MY_Lang();
        $lang->load('alfasms');
    }

    /**
     * replace variables in patern and wrap it
     *
     * @param array $variables
     * @param string $patern
     * @return string
     */
    public function replaceVariables($patern, $variables) {

        foreach ($variables as $variable => $replase_value) {
            $patern = str_replace('$' . $variable . '$', $replase_value, $patern);
        }
        $wraper = $this->alfasms_model->getWraper();
        if ($wraper) {
            $patern = str_replace('$content', $patern, $wraper);
        }
        return $patern;
    }



    public function sendSmsRegKey($send_to, $patern_name, $variables) {
        $default_settings = $this->alfasms_model->getSettings();
        $arrayForUser1 = array(
            'version' => 'http',
            'login' => $default_settings['login'],
            'password' => $default_settings['password'],
            'command' => 'send',
            'from' => $default_settings['from'],
            'to' => $send_to,
            'message' => 'Код регистрации на сайте - '.$variables
        );
        /* return */$this->alfasms_model->sendingAllSms($default_settings['smspath'], $arrayForUser1);


    }
    /**
     * send sms
     *
     * @param string $send_to - recepient sms
     * @param string $patern_name - sms patern  name
     * @param array $variables - variables to raplase in message:
     *   $variables = array('$user$' => 'UserName')
     * @return bool
     */
    public function sendSms($send_to, $patern_name, $variables) {



            //Getting settings
            $patern_settings = $this->alfasms_model->getPaternSettings($patern_name);

            $default_settings = $this->alfasms_model->getSettings();
            //Prepare settings into correct array for initialize library
            if ($patern_settings) {
                foreach ($patern_settings as $key => $value) {
                    if (!$value) {
                        if ($default_settings[$key]) {
                            $patern_settings[$key] = $default_settings[$key];
                        }
                    }
                }
            }
            
              if($variables['custom_field'] != '' && $patern_settings['user_message_active'] == '1'){
                  $text = htmlspecialchars($this->replaceVariables(strip_tags($patern_settings['user_message']), $variables));
                  if($variables['custom_field'] != ''){
                      $text = $text./*"\r\n"*/' ТТН № '.$variables['custom_field'];
                  }
                   $array1 = array(
                       'version' => 'http',
                       'login' => $default_settings['login'],
                       'password' => $default_settings['password'],
                       'command' => 'send',
                       'from' => $default_settings['from'],
                       'to' => $send_to,
                       'message' => $text
                   );
                   /* return */$this->alfasms_model->sendingAllSms($default_settings['smspath'], $array1);

               }

            //Sending user sms if active in options
            if ($patern_settings['user_message_active'] == '1' && !$variables['custom_field']) {

//            $text = htmlspecialchars($this->replaceVariables(strip_tags($patern_settings['user_message']), $variables));
                $text = htmlspecialchars($this->replaceVariables(strip_tags($patern_settings['user_message']), $variables));
                if($variables['Comment'] != ''){
                    $text = $text."\r\n".$variables['Comment'];
                }

//            $token = $this->alfasms_model->get_token();
                $arrayForUser1 = array(
                    'version' => 'http',
                    'login' => $default_settings['login'],
                    'password' => $default_settings['password'],
                    'command' => 'send',
                    'from' => $default_settings['from'],
                    'to' => $send_to,
                    'message' => $text
                );
                /* return */$this->alfasms_model->sendingAllSms($default_settings['smspath'], $arrayForUser1);

                //Registering event is success
            }

            //Sending administrator sms if active in options
            if ($patern_settings['admin_message_active'] == '1') {

//            $text = htmlspecialchars($this->replaceVariables(strip_tags($patern_settings['admin_message']), $variables));
//            $token = $this->alfasms_model->get_token();
                $text = htmlspecialchars($this->replaceVariables(strip_tags($patern_settings['admin_message']), $variables));
                if($variables['Comment'] != ''){
                    $text = $text."\r\n".$variables['Comment'];
                }
                $arrayForUser = array(
                    'version' => 'http',
                    'login' => $default_settings['login'],
                    'password' => $default_settings['password'],
                    'command' => 'send',
                    'from' => $default_settings['from'],
                    'to' => $patern_settings['admin_alfasms'],
                    'message' => $text
                    //"test"=>1
                );
                /* return */$this->alfasms_model->sendingAllSms($default_settings['smspath'], $arrayForUser);

                //Registering event is success
            }

            //Returning status
            if ($this->errors) {
                return FALSE;
            } else {
                return TRUE;
            }


       
    }

    /**
     *
     * @param array $data keys from list:
     * 'name',
     * 'from',
     * 'from_sms',
     * 'theme',
     * 'type',
     * 'user_message',
     * 'user_message_active',
     * 'admin_message',
     * 'admin_message_active',
     * 'admin_sms',
     * 'description'
     * @return boolean
     */
    public function create($data = array()) {
        if ($_POST) {
            $this->form_validation->set_rules('sms_name', lang('Template name', 'alfasms'), 'required|alpha_dash');
            $this->form_validation->set_rules('sms_theme', lang('Template theme', 'alfasms'), 'required');

            if ($_POST['usersmsTextRadio']) {
                $this->form_validation->set_rules('usersmsText', lang('Template user sms', 'alfasms'), 'required');
            }

            if ($_POST['adminsmsTextRadio']) {
                $this->form_validation->set_rules('adminsmsText', lang('Template admin sms', 'alfasms'), 'required');
            }

            $this->form_validation->set_rules('admin_sms', lang('Admin address', 'alfasms'), 'valid_sms');

            if ($this->form_validation->run($this) == FALSE) {
                $this->errors = validation_errors();
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            if (is_array($data) && !empty($data)) {
                foreach ($data as $key => $d) {
                    if (!in_array($key, $this->accepted_params)) {
                        unset($data[$key]);
                    }
                }

                $this->data_model = $data;
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     *
     * @param array $data keys from list:
     * 'name',
     * 'from',
     * 'from_sms',
     * 'theme',
     * 'type',
     * 'user_message',
     * 'user_message_active',
     * 'admin_message',
     * 'admin_message_active',
     * 'admin_sms',
     * 'description'
     * @param int $id ID of element
     * @return boolean
     */
    public function edit($id, $data = array()) {
        if ($_POST) {
            $this->form_validation->set_rules('theme', lang('Template theme', 'alfasms'), 'required');

            if ($_POST['usersmsTextRadio']) {
                $this->form_validation->set_rules('usersmsText', lang('Template user sms', 'alfasms'), 'required');
            }

            if ($_POST['adminsmsTextRadio']) {
                $this->form_validation->set_rules('adminsmsText', lang('Template admin sms', 'alfasms'), 'required');
            }

            $this->form_validation->set_rules('admin_sms', lang('Admin address', 'alfasms'), 'valid_sms');

            if ($this->form_validation->run($this) == FALSE) {
                $this->errors = validation_errors();
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            if (is_array($data) && !empty($data)) {
                foreach ($data as $key => $d) {
                    if (!in_array($key, $this->accepted_params)) {
                        unset($data[$key]);
                    }
                }

                $this->data_model = $data;
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function delete($ids) {
        return $this->alfasms_model->deleteTemplateByID($ids);
    }

    /**
     * test sms sending
     *
     * @param array $config cohfiguration options for sending sms:
     * 'protocol',
     * 'smtp_port',
     * 'type',
     * 'smspath'
     */
//    public function smsTest($config) {
//        $test = '1';
//        $array = array(
//            "login" => $config['user'],
//            "sha512" => hash('sha512', $config['password'] . $token . $config['apiid']),
//            "token" => $token,
//            "to" => $config['recipient'],
//            "text" => $config['text']
//        );
//        return $this->alfasms_model->sendingAllSms($config['source'], $array, $test);
//
//    }

    public function smsTest($source, $configur)
    {

        $test = '1';
        //  $token = $this->sms_biz_model->get_token();
//        $array = array(
//            "login" => $config['user'],
//            "sha512" => hash('sha512', $config['password'] . $token . $config['apiid']),
//            "token" => $token,
//            "to" => $config['recipient'],
//            "text" => $config['text']
//        );

        return $this->alfasms_model->sendingAllSms($source, $configur, $test);
        // вывод результата в браузер для удобства чтения обрамлен в textarea
    }

    public function getAllTemplates() {
        return $this->alfasms_model->getAllTemplates();
    }

    public function getSettings() {
        return $this->alfasms_model->getSettings();
    }

    public function getTemplateById($id, $locale) {
        return $this->alfasms_model->getTemplateById($id, $locale);
    }

    public function setSettings($settings) {
        return $this->alfasms_model->setSettings($settings);
    }

    public function deleteVariable($template_id, $variable, $locale) {
        return $this->alfasms_model->deleteVariable($template_id, $variable, $locale);
    }

    public function updateVariable($template_id, $variable, $variableNewValue, $oldVariable, $locale) {
        return $this->alfasms_model->updateVariable($template_id, $variable, $variableNewValue, $oldVariable, $locale);
    }

    public function addVariable($template_id, $variable, $variableValue, $locale) {
        return $this->alfasms_model->addVariable($template_id, $variable, $variableValue, $locale);
    }

    public function getTemplateVariables($template_id, $locale) {
        return $this->alfasms_model->getTemplateVariables($template_id, $locale);
    }

}
