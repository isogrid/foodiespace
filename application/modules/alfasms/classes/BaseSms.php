<?php

namespace alfasms\classes;

/**
 * Image CMS
 * Module alfasms
 * @property \DX_Auth $dx_auth
 * @property \CI_URI $uri
 * @property \CI_DB_active_record $db
 * @property \CI_Input $input
 */
class BaseSms extends ParentSms {

    public function __construct() {
        parent::__construct();
        $lang = new \MY_Lang();
        $lang->load('alfasms');
    }

    public function create($params = array()) {
        if (parent::create($params = array())) {

            if ($_POST) {
                $data['name'] = $this->input->post('sms_name');
                $data['theme'] = $this->input->post('sms_theme');

                $data['usersmsText'] = $this->input->post('usersmsText');
                $data['user_message_active'] = $this->input->post('usersmsTextRadio');
                $data['admin_message'] = $this->input->post('adminsmsText');
                $data['admin_message_active'] = $this->input->post('adminsmsTextRadio');
                $data['admin_sms'] = $this->input->post('admin_sms');

                $id = $this->alfasms_model->create($data);
            } else {
                $id = $this->alfasms_model->create($this->data_model);
            }
            return $id;
        } else {
            return FALSE;
        }
    }

    public function edit($id, $params = array(), $locale) {
        
        if (parent::edit($id, $params = array())) {
            if ($this->input->post()) {

                $data['name'] = $this->input->post('sms_name');
                $data['theme'] = $this->input->post('theme');

                $data['usersmsText'] = $this->input->post('usersmsText');
                $data['user_message_active'] = $this->input->post('usersmsTextRadio');
                $data['admin_message'] = $this->input->post('adminsmsText');
                $data['admin_message_active'] = $this->input->post('adminsmsTextRadio');
                $data['admin_sms'] = $this->input->post('admin_sms');

                $this->alfasms_model->edit($id, $data, $locale);
            } else {
                $this->alfasms_model->edit($id, $this->data_model, $locale);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * send sms
     * @param type $send_to
     * @param type $patern_name
     * @param type $variables
     * @return boolean
     */
    public function sendSms($send_to, $patern_name, $variables) {
        if (parent::sendSms($send_to, $patern_name, $variables)) {
            return TRUE;
        } else {
            return $this->errors;
        }
    }
    public function sendSmsRegKey($send_to, $patern_name, $variables) {
        if (parent::sendSmsRegKey($send_to, $patern_name, $variables)) {
            return TRUE;
        } else {
            return $this->errors;
        }
    }


    /**
     * test sms sending
     * @return string
     */
    public function smsTest() {
        $this->recipient = $this->input->post('recipient');
        $this->text = $this->input->post('text');
        $this->description = $this->input->post('theme');
        //$this->protocol = $this->input->post('protocol');
        //$this->start_time = $this->input->post('start_time');
        ////$this->end_time = $this->input->post('end_time');
        //$this->rate = $this->input->post('rate');
        //$this->lifetime = $this->input->post('lifetime');
        $this->user = $this->input->post('user');
        $this->password = $this->input->post('password');
        $this->source = $this->input->post('source');
        $this->apiid = $this->input->post('apiid');


        $source = $this->source;

        $configur = array(
            'version' => 'http',
            'login' => $this->user,
            'password' => $this->password,
            'command'=>'send',
            'from'=>$this->input->post('from'),
            'to'=>$this->user,

//            'apiid' => $this->apiid,
//            'recipient' => $this->recipient,
            'message' => htmlspecialchars(strip_tags($this->text)));

        //  'description' => htmlspecialchars($this->replaceVariables(strip_tags($this->description))),);
//dd($configur);

        return parent::smsTest($source, $configur);
    }

}
