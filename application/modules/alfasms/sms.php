<?php

namespace alfasms;

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Image CMS
 * Smss
 */
class sms extends classes\BaseSms {

    protected static $_instance;

    public function __construct() {
        parent::__construct();
        $this->load->module('core');
    }

    private function __clone() {

    }

    /**
     *
     * @return sms
     */
    public static function getInstance() {
                
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    

}

/* End of file sms.php */
