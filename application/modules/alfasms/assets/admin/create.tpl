<section class="mini-layout">
    <div class="frame_title clearfix">
        <div class="pull-left">
            <span class="help-inline"></span>
            <span class="title">{lang('Creating sms template', 'alfasms')}</span>
        </div>
        <div class="pull-right">
            <div class="d-i_b">
                <a href="{$BASE_URL}admin/components/cp/alfasms/index" class="t-d_n m-r_15 pjax">
                    <span class="f-s_14">←</span>
                    <span class="t-d_u">{lang('Go back', 'alfasms')}</span>
                </a>
                <button type="button" class="btn btn-small formSubmit btn-success" data-form="#sms_form" data-action="save">
                    <i class="icon-ok icon-white"></i>{lang('Create', 'alfasms')}
                </button>
                <button type="button" class="btn btn-small formSubmit" data-form="#sms_form" data-action="tomain">
                    <i class="icon-edit"></i>{lang('Create and exit', 'alfasms')}
                </button>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div class="row-fluid">
            <form action="{$BASE_URL}admin/components/cp/alfasms/create" id="sms_form" method="post" class="form-horizontal">
                <table class="table table-striped table-bordered table-hover table-condensed content_big_td">
                    <thead>
                    <th>{lang('Settings', 'alfasms')}</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="inside_padd">
                                    <div class="row-fluid">
                                        <div class="control-group">
                                            <label class="control-label" for="comcount">{lang('Template name (only latin)', 'alfasms')}:</label>
                                            <div class="controls">
                                                <input id="comcount" type="text" required class="required" name="sms_name" value=""/>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="comcount4">{lang('Theme', 'alfasms')}:</label>
                                            <div class="controls">
                                                <input id="comcount4" type="text" required class="required" name="sms_theme" value=""/>
                                            </div>
                                        </div>

                                        

                                        <div class="control-group">
                                            <label class="control-label" for="usersmsText">{lang('Template user sms', 'alfasms')}:</label>
                                            <div class="controls">
                                                <textarea class="" name="usersmsText" id="usersmsText" ></textarea>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="usersmsTextRadio">{lang('Send sms to user', 'alfasms')}:</label>
                                            <div class="controls">
                                                &nbsp; {lang('Yes', 'alfasms')} &nbsp;
                                                <span class="frame_label">
                                                    <span class="niceRadio b_n">
                                                        <input type="radio" name="usersmsTextRadio" value="1" checked="checked" id="usersmsTextRadio"/>
                                                    </span>
                                                </span>
                                                &nbsp; {lang('No', 'alfasms')} &nbsp;
                                                <span class="frame_label">
                                                    <span class="niceRadio b_n">
                                                        <input type="radio" name="usersmsTextRadio" value="0" id="usersmsTextRadio"/>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="adminsmsText">{lang('Admin sms template', 'alfasms')}:</label>
                                            <div class="controls">
                                                <textarea class="" name="adminsmsText" id="adminsmsText"></textarea>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="adminsmsTextRadio">{lang('Send sms to admin', 'alfasms')}:</label>
                                            <div class="controls">
                                                &nbsp; {lang('Yes', 'alfasms')} &nbsp;
                                                <span class="frame_label">
                                                    <span class="niceRadio b_n">
                                                        <input type="radio" name="adminsmsTextRadio" value="1" checked="checked" id="adminsmsTextRadio"/>
                                                    </span>
                                                </span>
                                                &nbsp; {lang('No', 'alfasms')} &nbsp;
                                                <span class="frame_label">
                                                    <span class="niceRadio b_n">
                                                        <input type="radio" name="adminsmsTextRadio" value="0" id="adminsmsTextRadio"/>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label class="control-label" for="comcount3">{lang('Admin address', 'alfasms')}:</label>
                                            <div class="controls">
                                                <input id="comcount3" type="text" name="admin_sms" value=""/>
                                            </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                {form_csrf()}
            </form>
        </div>
    </div>
</section>
