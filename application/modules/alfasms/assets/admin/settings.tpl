<div class="container">
    <section class="mini-layout">
        <div class="frame_title clearfix">
            <div class="pull-left">
                <span class="help-inline"></span>
                <span class="title">{lang('Settings', 'alfasms')}<br/>{lang('On your accaount is: ', 'alfasms')}{$settings['res']}{lang(' RUR', 'alfasms')}</span>
            </div>
            <div class="pull-right">
                <div class="d-i_b">
                    <a href="{$BASE_URL}admin/components/init_window/alfasms"
                       class="t-d_n m-r_15 pjax">
                        <span class="f-s_14">←</span>
                        <span class="t-d_u">{lang('Back', 'alfasms')}</span>
                    </a>
                    <button type="button"
                            class="btn btn-small btn-primary action_on formSubmit"
                            data-form="#wishlist_settings_form"
                            data-action="tomain">
                        <i class="icon-ok"></i>{lang('Save', 'alfasms')}
                    </button>
                </div>
            </div>
        </div>
        <form method="post" action="{site_url('admin/components/cp/alfasms/update_settings')}" class="form-horizontal" id="wishlist_settings_form">
            <table class="table table-striped table-bordered table-hover table-condensed t-l_a">
                <thead>
                    <tr>
                        <th colspan="6">
                            {lang('Settings', 'alfasms')}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="6">
                            <div class="inside_padd">

                               {/*} <div class="control-group">
                                    <div class="controls">
                                        <a href="{site_url('admin/components/cp/alfasms/import_templates')}" style="float:right">{lang('Install standart templates', 'alfasms')}</a>
                                    </div>
                                </div>{ */}



                                <div class="control-group">
                                    <label class="control-label" for="settings[admin_sms]">{lang('SMS to admin for test', 'alfasms')}:</label>
                                    <div class="controls">
                                        <input type = "text" name = "settings[admin_sms]" required class="textbox_short required" value="{$settings['admin_sms']}" id="admin_sms"/>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="settings[theme]">{lang('Theme', 'alfasms')}:</label>
                                    <div class="controls">
                                        <input type = "text" name = "settings[theme]" required class="textbox_short required" value="{$settings['theme']}" id="theme"/>
                                    </div>
                                </div>



                                <div class="control-group"> 
                                     <label class="control-label" for="settings[description]">{lang('Text description', 'alfasms')}:</label>
                                    <div class="controls">
                                        <textarea name='settings[description]' class="elRTE" required  id="description">{$settings['description']}</textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="settings[smspath]">{lang('Server path to API_SMS', 'alfasms')}:</label>
                                    <div class="controls">
                                        <input type = "text" name = "settings[smspath]" class="textbox_short" value="{$settings['smspath']}" id="smspath"/>
                                    </div>
                                </div>

                                {/*}<div class="control-group">
                                    <label class="control-label" for="settings[protocol]">{lang('Alfaname', 'alfasms')}:</label>

                                    <div class="controls">
                                        <input type = "text" name = "settings[protocol]" class="textbox_short" value="{$settings['protocol']}" id="protocol"/>
                                    </div>

                                </div>{ */}

                                <div class="control-group">
                                    <label class="control-label" for="settings[login]">{lang('login', 'alfasms')}:</label>

                                    <div class="controls">
                                        <input type = "text" name = "settings[login]" class="textbox_short" value="{$settings['login']}" id="login"/>
                                    </div>

                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="settings[password]">{lang('password', 'alfasms')}:</label>

                                    <div class="controls">
                                        <input type = "password" name = "settings[password]" class="textbox_short" value="{$settings['password']}" id="password"/>
                                    </div>

                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="settings[api_id]">{lang('api_id', 'alfasms')}:</label>

                                    <div class="controls">
                                        <input type = "text" name = "settings[api_id]" class="textbox_short" value="{$settings['api_id']}" id="api_id"/>
                                    </div>

                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="settings[api_id]">{lang('From (not numbers)', 'alfasms')}:</label>

                                    <div class="controls">
                                        <input type = "text" name = "settings[from]" class="textbox_short" value="{$settings['from']}" id="from"/>
                                    </div>

                                </div>

                                <div class="control-group" >
                                    <div class="controls ">
                                        <button type="button" class="btn" onclick="smsTest()" >
                                            <i class="icon-envelope"></i>
                                            {lang('Check sms sending', 'alfasms')}
                                        </button>
                                    </div>
                                    <br>
                                    <div class="controls ">
                                        <button type="button" class="btn delete_image smsTestResultsHide">
                                            <i class="icon-remove"></i>
                                        </button>
                                        <div class="smsTestResults">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            {form_csrf()}
        </form>
    </section>
</div>