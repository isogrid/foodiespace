<section class="mini-layout">
    <div class="frame_title clearfix">
        <div class="pull-left">
            <span class="help-inline"></span>
            <span class="title">{lang('Sms template editing', 'alfasms')}</span>
        </div>
        <div class="pull-right">
            <div class="d-i_b">
                <a href="{$BASE_URL}admin/components/cp/alfasms/index" class="t-d_n m-r_15 pjax">
                    <span class="f-s_14">←</span>
                    <span class="t-d_u">{lang('Go back', 'alfasms')}</span>
                </a>
                <button type="button" class="btn btn-small formSubmit btn-primary" data-form="#sms_form" data-action="save">
                    <i class="icon-ok icon-white"></i>{lang('Save', 'alfasms')}
                </button>
                <button type="button" class="btn btn-small formSubmit" data-form="#sms_form" data-action="tomain">
                    <i class="icon-edit"></i>{lang('Save and go back', 'alfasms')}
                </button>
                {echo create_language_select($languages, $locale, "/admin/components/cp/alfasms/edit/" . $model['id'])}
            </div>
        </div>
    </div>
    <div class="content_big_td row-fluid">
        <div class="clearfix">
            <div class="btn-group myTab m-t_20 pull-left" data-toggle="buttons-radio">
                <a href="#settings" class="btn btn-small active" onclick="SmsTemplateVariables.updateVariablesList($(this), '{$model['id']}', '{echo $locale}')">{lang('Template settings', 'alfasms')}</a>
                <a href="#variables" class="btn btn-small">{lang('Template variables', 'alfasms')}</a>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="inside_padd">
                    <form action="{$BASE_URL}admin/components/cp/alfasms/edit/{$model['id']}/{echo $locale}" id="sms_form" method="post" class="form-horizontal">
                        <table class="table table-striped table-bordered table-hover table-condensed content_big_td">
                            <thead>
                            <th>{lang('Settings', 'alfasms')}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="inside_padd">
                                            <div class="row-fluid">
                                                <div class="control-group">
                                                    <label class="control-label" for="comcount">{lang('Template name(only latin)', 'alfasms')}:</label>
                                                    <div class="controls">
                                                        <input id="comcount" type="text" name="sms_name" value="{$model['name']}" disabled="disabled"/>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="comcount4">{lang('Theme', 'alfasms')}:</label>
                                                    <div class="controls">
                                                        <input id="comcount4" type="text" name="theme" class="required" required value="{$model['theme']}"/>
                                                    </div>
                                                </div>



                                                <div class="control-group">
                                                    <div class="control-label">
                                                        <label class="" for="usersmsText">{lang('Template user sms', 'alfasms')}:</label>
                                                        <div class="m-t_15">
                                                            <select name="sms_variables[]" multiple="multiple" id="usersmsVariables" size="20" class="notchosen">
                                                                {foreach $variables as $variable => $variableValue}
                                                                    <option title="{echo $variableValue}" value="{echo $variable}">{echo $variableValue}</option>
                                                                {/foreach}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="controls">
                                                        <textarea class="elRTE" name="usersmsText" id="usersmsText">{$model['user_message']}</textarea>
                                                    </div>

                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="usersmsTextRadio">{lang('Send sms to user', 'alfasms')}:</label>
                                                    <div class="controls">
                                                        &nbsp; {lang('Yes', 'alfasms')} &nbsp;
                                                        <span class="frame_label">
                                                            <span class="niceRadio b_n">
                                                                <input type="radio" name="usersmsTextRadio" value="1" {if $model['user_message_active']}checked="checked"{/if} id="usersmsTextRadio"/>
                                                            </span>
                                                        </span>
                                                        &nbsp; {lang('No', 'alfasms')} &nbsp;
                                                        <span class="frame_label">
                                                            <span class="niceRadio b_n">
                                                                <input type="radio" name="usersmsTextRadio" value="0" {if !$model['user_message_active']}checked="checked"{/if} id="usersmsTextRadio"/>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <div class="control-label">
                                                        <label class="" for="adminsmsText">{lang('Admin sms template', 'alfasms')}: </label>
                                                        <div class="m-t_15">
                                                            <select name="sms_variables[]" multiple="multiple" id="adminsmsVariables" size="20" class="notchosen">
                                                                {foreach $variables as $variable => $variableValue}
                                                                    <option title="{echo $variableValue}" value="{echo $variable}">{echo $variableValue}</option>
                                                                {/foreach}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="controls">
                                                        <textarea class="elRTE" name="adminsmsText" id="adminsmsText">{$model['admin_message']}</textarea>
                                                    </div>

                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="adminsmsTextRadio">{lang('Send sms to admin', 'alfasms')}:</label>
                                                    <div class="controls">
                                                        &nbsp; {lang('Yes', 'alfasms')} &nbsp;
                                                        <span class="frame_label">
                                                            <span class="niceRadio b_n">
                                                                <input type="radio" name="adminsmsTextRadio" value="1" {if $model['admin_message_active']}checked="checked"{/if} id="adminsmsTextRadio"/>
                                                            </span>
                                                        </span>
                                                        &nbsp; {lang('No', 'alfasms')} &nbsp;
                                                        <span class="frame_label">
                                                            <span class="niceRadio b_n">
                                                                <input type="radio" name="adminsmsTextRadio" value="0" {if !$model['admin_message_active']}checked="checked"{/if} id="adminsmsTextRadio"/>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="comcount3">{lang('Admin address', 'alfasms')}:</label>
                                                    <div class="controls">
                                                        <input id="comcount3" type="text" name="admin_sms" value="{$model['admin_alfasms']}"/>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        {form_csrf()}
                    </form>

                </div>
            </div>
            <div class="tab-pane active" id="variables">

                <div class="inside_padd">
                    <table class="table table-striped table-bordered table-hover table-condensed content_big_td variablesTable t-l_a">
                        <thead>
                        <th>{lang('Variables', 'alfasms')}</th>
                        <th>{lang('Values', 'alfasms')}</th>
                        <th>{lang('Edit', 'alfasms')}</th>
                        <th>{lang('Delete', 'alfasms')}</th>
                        </thead>
                        {foreach $variables as $variable => $variable_value}
                            {//var_dumps($variable)}
                            {//exit()}
                            <tr>
                                <td class="span5">
                                    <div class="variable">
                                        {echo $variable}
                                    </div>
                                    <input type="text" name="variableEdit" class="variableEdit" style="display: none"/>
                                </td>
                                <td class="span5">
                                    <div class="variableValue">
                                        {echo $variable_value}
                                    </div>
                                    <input type="text" name="variableValueEdit" class="variableValueEdit" style="display: none"/>
                                </td>
                                <td style="width: 100px">
                                    <button class="btn my_btn_s btn-small btn-success editVariable" type="button">
                                        <i class="icon-edit"></i>
                                    </button>
                                    <button data-update="count" onclick="SmsTemplateVariables.update($(this), '{$model['id']}', '{echo $variable}', '{echo $locale}')" class="btn btn-small refreshVariable" type="button" style="display: none;">
                                        <i class="icon-refresh"></i>
                                    </button>
                                </td>
                                <td class="span1">
                                    <button class="btn my_btn_s btn-small btn-danger " type="button" onclick="SmsTemplateVariables.delete({$model['id']}, '{echo $variable}', $(this), '{echo $locale}')">
                                        <i class="icon-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        <tr class="addVariableContainer" style="display: none">
                            <td class="span5">
                                <input type="text" name="variableEdit" class="variableEdit"/>
                            </td>
                            <td class="span5">
                                <input type="text" name="variableValueEdit" class="variableValueEdit"/>
                            </td>
                            <td style="width: 100px" colspan="2">
                                <button data-update="count" onclick="SmsTemplateVariables.add($(this), {$model['id']}, '', '{echo $locale}')" data-variable=""  class="btn btn-small" type="button" style="display: block; margin-top: 4px;margin-left: 4px">
                                    <i class="icon-plus"></i>
                                </button>
                            </td>
                        </tr>
                    </table>
                    <button  class="btn btn-small btn-success addVariable">
                        <i class="icon-plus icon-white"></i>&nbsp;{lang('Add new variable', 'alfasms')}
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>