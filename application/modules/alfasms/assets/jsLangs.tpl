<script>
langs["Error"] = '<?php echo lang("Error", "alfasms", FALSE)?>';
langs["Variable is not removed"] = '<?php echo lang("Variable is not removed", "alfasms", FALSE)?>';
langs["Message"] = '<?php echo lang("Message", "alfasms", FALSE)?>';
langs["Variable successfully removed"] = '<?php echo lang("Variable successfully removed", "alfasms", FALSE)?>';
langs["Variable is not updated"] = '<?php echo lang("Variable is not updated", "alfasms", FALSE)?>';
langs["Variable successfully updated"] = '<?php echo lang("Variable successfully updated", "alfasms", FALSE)?>';
langs["Variable is not added"] = '<?php echo lang("Variable is not added", "alfasms", FALSE)?>';
langs["Variable successfully added"] = '<?php echo lang("Variable successfully added", "alfasms", FALSE)?>';
langs["Enter variable"] = '<?php echo lang("Enter variable", "alfasms", FALSE)?>';
langs["Variable should contain only Latin characters"] = '<?php echo lang("Variable should contain only Latin characters", "alfasms", FALSE)?>';
langs["Variable must be surrounded by $"] = '<?php echo lang("Variable must be surrounded by $", "alfasms", FALSE)?>';
langs["Variable must have a value"] = '<?php echo lang("Variable must have a value", "alfasms", FALSE)?>';
</script>