<div id="bpopup" class="reg-confirm__inner modal">

    <!-- Modal Header -->
    {view('includes/modal/modal_header.tpl', [
    'title' => tlang('Подтверждение регистрации')
    ])}

    <form class="form" action="{site_url('alfasms/confirmcode')}" method="post"
          data-form-ajax="formRegSMS"
    >

        <input type="hidden" name="username1" value="{echo $user_posted_data['username']}">
        <input type="hidden" name="email1" value="{echo $user_posted_data['email']}">
        <input type="hidden" name="phone1" value="{echo $user_posted_data['phone']}">
        <input type="hidden" name="date1" value="{echo $user_posted_data['date']}">
        <input type="hidden" name="city1" value="{echo $user_posted_data['city']}">
        <input type="hidden" name="password1" value="{echo $user_posted_data['password']}">

        <!-- Modal Content -->
        <div class="modal__content">

            {if $_GET['repeat_error']=='false'}
                <div class="form__messages">
                    <div class="message message--error">
                        <ul class="message__list">
                            {tlang('Время ожидания истекло. Заполните форму регистрации еще раз')}
                        </ul>
                    </div>
                </div>
            {literal}
                <script type="text/javascript">
                    setTimeout(function () {
                        location.href = '{/literal}{echo base_url('/auth/register')}{literal}'
                    }, 3000)

                </script>
            {/literal}

            {/if}
            {if $_GET['repeat_error']=='wait'}
                <div class="form__messages">
                    <div class="message message--error">
                        <ul class="message__list">
                            {tlang('Код был отправлен ранее. Oжидайте до 5 минут, если код не придет, повторите попытку')}
                        </ul>
                    </div>
                </div>
            {/if}
            {if $_GET['repeat_error']=='true'}
                <div class="form__messages">
                    <div class="message message--success">
                        <ul class="message__list">
                            <li class="message__item">{tlang('Код отправлен повторно')}</li>
                        </ul>
                    </div>
                </div>
            {/if}

            {if $errors}
                <div class="form__messages">
                    <div class="message message--error">
                        <ul class="message__list">
                            <li class="message__item">{tlang('Не верно введен код')}</li>
                        </ul>
                    </div>
                </div>
            {/if}


            {if !$success}

            <div class="form">

                <!-- User Name field -->
                {view('includes/forms/input-base.tpl', [
                'label' => tlang('Введите код из SMS'),
                'type' => 'text',
                'name' => 'sms_code',
                'value' => '',
                'required' => true
                ])}


                {else:}
                <div class="typo">{ $success}</div>
                <div class="form__messages">
                    <div class="message message--success">
                        <ul class="message__list">
                            <li class="message__item">{tlang('Регистрация прошла успешно')}</li>
                        </ul>
                    </div>
                </div>

                {literal}
                <script type="text/javascript">
                    setTimeout(function () {
                        location.href = '{/literal}{echo shop_url('profile')}{literal}'
                    }, 3000)

                </script>

                {/literal}

                {/if}

            </div><!-- /.modal__content -->

            <!-- Modal Footer -->
            <div class="modal__footer">
                <div class="modal__footer-row">
                    <div class="modal__footer-btn hidden-xs">
                        <button class="btn btn-default" type="reset" data-modal-close>{tlang('Close')}</button>
                    </div>

                    {if !$success}
                        <div class="modal__footer-btn">

                            <div class="form__inner">
                                <input id="merchantType" class="btn btn-primary"
                                       onclick="repeat_getting_Code.update($(this))"
                                       value="{tlang('Получить код еще раз')}"
                                       data-modal1="formRegSMS">
                            </div>
                        </div>
                        <div class="modal__footer-btn">

                            <button class="btn product-buy__btn--buy" type="submit"
                                    data-product-button--loader="">{tlang('Отправить')}<i
                                        class="button--loader hidden" data-button-loader="loader"></i>
                            </button>
                        </div>
                    {/if}

                </div>
            </div>


            {form_csrf()}
    </form>

</div><!-- /.modal -->
