<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Image CMS
 * Sms Module Admin
 * @property Cache $cache
 */
class Admin extends BaseAdminController {

    /**
     * Object of Sms class
     * @var Sms
     */
    public $sms;

    public function __construct() {
        parent::__construct();
        $this->sms = \alfasms\sms::getInstance();
        $lang = new MY_Lang();
        $lang->load('alfasms');
    }

    public function index() {
        \CMSFactory\assetManager::create()
                ->setData('models', $this->sms->getAllTemplates())
                ->renderAdmin('list');
    }

    public function settings() {
        \CMSFactory\assetManager::create()
                ->registerScript('sms', TRUE)
                ->registerStyle('style')
                ->setData('settings', $this->sms->getSettings())
                ->renderAdmin('settings');
    }

    public function create() {
        if ($_POST) {
            $id = $this->sms->create();
            if ($id) {
                showMessage(lang('Template created', 'alfasms'));
                if ($this->input->post('action') == 'tomain') {
                    pjax('/admin/components/cp/alfasms/index');
                } else {
                    pjax('/admin/components/cp/alfasms/edit/' . $id . '#settings');
                }
            } else {
                showMessage($this->sms->errors, '', 'r');
            }
        } else
            \CMSFactory\assetManager::create()
                    ->registerScript('sms', TRUE)
                    ->setData('settings', $this->sms->getSettings())
                    ->renderAdmin('create');
    }

    public function smsTest() {
        lang('sms_sent', 'admin');
        echo $this->sms->smsTest($this->input->post());
    }

    public function delete() {
        $this->sms->delete($_POST['ids']);
        showMessage(lang('Sms template deleted', 'alfasms'), lang('Message', 'alfasms'));
    }

    public function edit($id, $locale = null) {
        if (null === $locale)
            $locale = chose_language();

        $model = $this->sms->getTemplateById($id, $locale);
        if (!$model) {
            $this->load->module('core');
            $this->core->error_404();
            exit;
        }
        $variables = unserialize($model['variables']);

        if ($this->input->post()) {
            if ($this->sms->edit($id, array(), $locale)) {
                showMessage(lang('Template edited', 'alfasms'));

                if ($this->input->post('action') == 'tomain')
                    pjax('/admin/components/cp/alfasms/index');
            }
            else {
                showMessage($this->sms->errors, '', 'r');
            }
        } else
            \CMSFactory\assetManager::create()
                    ->setData('locale', $locale)
                    ->setData('languages', $this->db->get('languages')->result_array())
                    ->setData('model', $model)
                    ->setData('variables', $variables)
                    ->registerScript('sms', TRUE)
                    ->renderAdmin('edit');
    }

    /**
     * update settings for sms
     */
    public function update_settings() {

        if ($_POST) {
            $this->form_validation->set_rules('settings[admin_sms]', lang('Admin sms', 'alfasms'), 'required|xss_clean|valid_sms');
            $this->form_validation->set_rules('settings[theme]', lang('From sms', 'alfasms'), 'xss_clean|required');

            if ($_POST['settings']['wraper_activ'])
                $this->form_validation->set_rules('settings[wraper]', lang('Wraper', 'alfasms'), 'required|xss_clean|callback_wraper_check');
            else
                $this->form_validation->set_rules('settings[wraper]', lang('Wraper', 'alfasms'), 'xss_clean');

            if ($this->form_validation->run($this) == FALSE) {
                showMessage(validation_errors(), lang('Message', 'alfasms'), 'r');
            } else {
                if ($this->sms->setSettings($_POST['settings']))
                    showMessage(lang('Settings saved', 'alfasms'), lang('Message', 'alfasms'));
            }

            $this->cache->delete_all();
        }
    }

    public function wraper_check($wraper) {
        if (preg_match('/\$content/', htmlentities($wraper))) {
            return TRUE;
        } else {
            $this->form_validation->set_message('wraper_check', lang('Field', 'alfasms') . ' %s ' . lang('Must contain variable', 'alfasms') . ' $content');
            return FALSE;
        }
    }

    public function deleteVariable($locale = null) {
        if (null === $locale)
            $locale = chose_language();
        $template_id = $this->input->post('template_id');
        $variable = $this->input->post('variable');

        return $this->sms->deleteVariable($template_id, $variable, $locale);
    }

    public function updateVariable($locale = null) {
        if (null === $locale)
            $locale = chose_language();
        $template_id = $this->input->post('template_id');
        $variable = $this->input->post('variable');
        $variableNewValue = $this->input->post('variableValue');
        $oldVariable = $this->input->post('oldVariable');
        return $this->sms->updateVariable($template_id, $variable, $variableNewValue, $oldVariable, $locale);
    }

    public function addVariable($locale = null) {
        if (null === $locale)
            $locale = chose_language();
        $template_id = $this->input->post('template_id');
        $variable = $this->input->post('variable');
        $variableValue = $this->input->post('variableValue');

        if ($this->sms->addVariable($template_id, $variable, $variableValue, $locale)) {
            \CMSFactory\assetManager::create()
                    ->setData('template_id', $template_id)
                    ->setData('variable', $variable)
                    ->setData('variable_value', $variableValue)
                    ->setData('locale', $locale)
                    ->render('newVariable', true);
        } else {
            return FALSE;
        }
    }

    public function getTemplateVariables($locale = null) {
        if (null === $locale)
            $locale = chose_language();
        $template_id = $this->input->post('template_id');
        $variables = $this->sms->getTemplateVariables($template_id, $locale);
        if ($variables) {
            return \CMSFactory\assetManager::create()
                            ->setData('variables', $variables)
                            ->render('variablesSelectOptions', true);
        } else {
            return FALSE;
        }
    }

    /**
     * import templates from file
     */
    public function import_templates() {
        $this->db->where_in('id', array(1, 2, 3, 4, 5, 6, 7, 8, 9))->delete('mod_sms_paterns');
        $this->db->where_in('id', array(1, 2, 3, 4, 5, 6, 7, 8, 9))->delete('mod_sms_paterns_i18n');

        $file = $this->load->file(dirname(__FILE__) . '/models/paterns.sql', true);
        $this->db->query($file);

        $file = $this->load->file(dirname(__FILE__) . '/models/patterns_i18n.sql', true);
        $this->db->query($file);
        redirect('/admin/components/cp/alfasms/');
    }

}
