


<div class="container">
    <section class="mini-layout">
        <div class="frame_title clearfix">
            <div class="pull-left">
                <span class="help-inline"></span>
                <span class="title">{lang('mod_cities management', 'mod_cities')}</span>
            </div>
            <div class="pull-right">

                <div class="d-i_b">
                    <a class="btn btn-small " href="{$BASE_URL}admin/components/cp/mod_cities/main_price_options">
                        <i class="icon-wrench"></i>
                        {lang('Выбор минимального количества товара для покупки по ролям пользователей', 'parse_yandex_xml')}
                    </a>
                </div>

        </div>

    </section>

</div>

