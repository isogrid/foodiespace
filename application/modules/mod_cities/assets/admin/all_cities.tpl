<div class="container">


    <section class="mini-layout">
        <div class="frame_title clearfix">
            <div class="pull-left">
                <span class="help-inline"></span>
                <span class="title">{lang('Список городов', 'mod_cities')}</span>
            </div>
            <div class="pull-right">
                <div class="d-i_b">
                    <a href="{$BASE_URL}admin/components/cp/" class="t-d_n m-r_15 pjax">
                        <span class="f-s_14">←</span>
                        <span class="t-d_u">{lang('Go back', 'mod_cities')}</span>
                    </a>
                </div>


            </div>
        </div>
        <div class="tab-pane" id="variables">
            <div class="inside_padd">
                <table class="table  table-bordered table-hover table-condensed variablesTable t-l_a">
                    <thead>
                    <th>{lang('Название города', 'mod_cities')}</th>
                    <th>{lang('Транслит названия города', 'mod_cities')}</th>
                    <th>{lang('Активен', 'mod_cities')}</th>
                    <th>{lang('Edit', 'mod_cities')}</th>
                    <th>{lang('Delete', 'mod_cities')}</th>
                    </thead>
                    {foreach $all_cities as $id_city => $city}
                        <tr>
                            <td class="span5">
                                <div class="cityName">
                                    {echo  $city['city_name']}
                                </div>

                                <input type="text" name="cityNameEdit" class="cityNameEdit" style="display: none"/>
                            </td>
                            <td class="span5">
                                <div class="cityTranslitName">
                                    {echo  $city['city_translit_name']}
                                </div>

                                <input type="text" name="cityTranslitNameEdit" class="cityTranslitNameEdit"
                                       style="display: none"/>
                            </td>
                            <td class="span5">
                                <div class="activeCityName">
                                    {if $city['active'] =='1'}
                                        {lang('Активен', 'mod_cities')}
                                    {else:}
                                        {lang('Не активен', 'mod_cities')}
                                    {/if}
                                </div>
                                <input type="checkbox" name="activeCityNameEdit" class="activeCityNameEdit"
                                       style="display: none"/>
                            </td>
                            <td style="width: 100px">
                                <button class="btn my_btn_s btn-small editVariable" type="button">
                                    <i class="icon-edit"></i>
                                </button>
                                <button data-update="count"
                                        onclick="providerVariables.update($(this),  '{echo $id_city}', '{echo $locale}')"
                                        class="btn btn-small refreshVariable my_btn_s" type="button"
                                        style="display: none;">
                                    <i class="icon-ok"></i>
                                </button>
                            </td>
                            <td class="span1">
                                <button class="btn my_btn_s btn-small btn-danger " type="button"
                                        onclick="providerVariables.delete('{echo $id_city}', $(this), '{echo $locale}')">
                                    <i class="icon-trash"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    <tr class="addVariableContainer" style="display: none">
                        <td class="span5">
                            <input type="text" name="variableEdit" class="variableEdit"/>

                        </td>
                        <td class="span5">
                            <input type="text" name="variableTranslitEdit" class="variableTranslitEdit"/>

                        </td>
                        <td class="span5">

                                 <input   type="checkbox" class="variableValueEdit"/>

                        </td>
                        <td style="width: 100px" colspan="2">
                            <button data-update="count"
                                    onclick="providerVariables.add($(this), '{echo $locale}');"
                                    data-variable="" class="btn btn-small" type="button"
                                    style="display: block; margin-top: 4px;margin-left: 4px">
                                <i class="icon-plus"></i>
                            </button>
                        </td>
                    </tr>
                </table>
                <button class="btn btn-small btn-success addVariable">
                    <i class="icon-plus icon-white"></i>&nbsp;{lang('Add new city', 'mod_cities')}
                </button>
            </div>
        </div>
</div>
</section>
</div>