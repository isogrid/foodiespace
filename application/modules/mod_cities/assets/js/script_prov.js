$(document).ready(
    function () {
        $('body').on(
            'click',
            'table.variablesTable .editVariable',
            function () {
                var editor = $(this).closest('tr').find('div.cityName');

                var editValue = $.trim(editor.text());
                editor.empty();
                editor.parent().find('.cityNameEdit').css('display', 'block').val(editValue);
                // editor.parent().find('.priceMainEdit').chosen();

                var editor = $(this).closest('tr').find('div.cityTranslitName');
                var editValue = $.trim(editor.text());
                editor.empty();
                editor.parent().find('.cityTranslitNameEdit').css('display', 'block').val(editValue);

                var editorChekcbox = $(this).closest('tr').find('div.activeCityName');
                var editValueChekcbox = $.trim(editorChekcbox.text());
                editorChekcbox.empty();
                editorChekcbox.parent().find('.activeCityNameEdit').css('display', 'block');


                $(this).css('display', 'none');
                $(this).closest('tr').find('.refreshVariable').css('display', 'block');

            }
        );

        $('body').on(
            'click',
            '.addVariable',
            function () {
                $('.addVariableContainer').show();

                // $(".niceCheck").niceCheck();
                $(this).hide();
            }
        );
    }
);

var providerVariables = {
    insertVariable: function (curElem) {
        var curEditor = $(curElem).closest('.control-group').find('div[id*="tinymce"].mce-edit-area');
        var insertedValue = ' ' + $(curElem).val() + ' ';
        $(curElem).closest('.control-group').find('iframe').contents().find('body').trigger('focus');

        if (tinyMCE.activeEditor) {
            var activeEditor = tinyMCE.activeEditor.contentAreaContainer;
            tinyMCE.execCommand("mceInsertContent", false, insertedValue);
        } else {
            $(curElem).closest('.control-group').find('textarea').insertAtCaret(insertedValue);
        }

    },

    delete: function (variable, curElement, locale) {



        $.ajax(
            {
                type: 'POST',
                data: {

                    variable: variable
                },
                url: '/admin/components/cp/mod_cities/deleteProvider/',
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not removed'), 'r');
                        return false;
                    }
                    curElement.closest('tr').remove();
                    showMessage(lang('Message'), lang('Variable successfully removed'));
                }
            }
        );
    },
    update: function (curElement, oldVariable, locale) {
        var closestTr = curElement.closest('tr');
        var variable = closestTr.find('.cityNameEdit');
        var variableTranslitEdit = closestTr.find('.cityTranslitNameEdit');
        var variableValue = curElement.closest('tr').find('.activeCityNameEdit');

        this.validateVariable(variable.val(), variableTranslitEdit.val());

        $.ajax(
            {
                type: 'POST',
                data: {
                    variable: $.trim(variable.val()),
                    variableTranslitEdit: $.trim(variableTranslitEdit.val()),
                    variableValue: $.trim(variableValue.is(':checked')),
                    oldVariable: oldVariable,
                },
                url: '/admin/components/cp/mod_cities/updateProv/' ,
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not updated'), 'r');
                        return false;
                    }
                    closestTr.find('.priceMain').text(variable.val());
                    closestTr.find('.price_main_percentValue').text(variableValue.val());
                    variable.css('display', 'none');
                    variableValue.css('display', 'none');
                    closestTr.find('.editVariable').css('display', 'block');
                    closestTr.find('.refreshVariable').css('display', 'none');
                    showMessage(lang('Message'), lang('Variable successfully updated'));
                    window.location.reload()
                }
            }
        );
    },
    add: function (curElement,  locale) {
        var variable = curElement.closest('tr').find('.variableEdit');
        var variableValue = curElement.closest('tr').find('.variableValueEdit');
        var variableTranslitEdit = curElement.closest('tr').find('.variableTranslitEdit');

        this.validateVariable(variable.val(), variableTranslitEdit.val());

        $.ajax(
            {
                type: 'POST',
                data: {
                    variable: $.trim(variable.val()),
                    variableTranslitEdit: $.trim(variableTranslitEdit.val()),
                    variableValue: $.trim(variableValue.is(':checked')),

                },
                url: '/admin/components/cp/mod_cities/addCity/' + locale,
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not added'), 'r');
                        return false;
                    }
                    curElement.parent('div').find('.typeVariable').val('');
                    $('.addVariableContainer').css('display', 'none');
                    $('.addVariableContainer').find('input').val('');
                    $('.addVariable').show();
                    $(data).insertBefore('table.variablesTable .addVariableContainer');
                    showMessage(lang('Message'), lang('Variable successfully added'));
                    window.location.reload()
                }
            }
        );
    },
    updateVariablesList: function (curElement, template_id, locale) {
        if (!curElement.hasClass('active')) {
            $.ajax(
                {
                    type: 'POST',
                    data: {
                        template_id: template_id
                    },
                    url: '/admin/components/cp/mod_cities/getTemplateVariables/' + locale,
                    success: function (data) {
                        $('#userMailVariables').html(data);
                        $('#adminMailVariables').html(data);
                    }
                }
            );
        }
    },
    validateVariable: function (variable, variableValue) {
        var variable = $.trim(variable);
        var variableValue = $.trim(variableValue);

        if (!variable) {
            showMessage(lang('Error'), lang('Enter variable'), 'r');
            exit;
        }

        if (variableValue.match(/[а-яА-Я]{1,}/)) {
            showMessage(lang('Error'), lang('Variable should contain only Latin characters'), 'r');
            exit;
        }



        if (!variableValue) {
            showMessage(lang('Error'), lang('Variable must have a value'), 'r');
            exit;
        }
    }
};


var artculVariables = {
    insertVariable: function (curElem) {
        var curEditor = $(curElem).closest('.control-group').find('div[id*="tinymce"].mce-edit-area');
        var insertedValue = ' ' + $(curElem).val() + ' ';
        $(curElem).closest('.control-group').find('iframe').contents().find('body').trigger('focus');

        if (tinyMCE.activeEditor) {
            var activeEditor = tinyMCE.activeEditor.contentAreaContainer;
            tinyMCE.execCommand("mceInsertContent", false, insertedValue);
        } else {
            $(curElem).closest('.control-group').find('textarea').insertAtCaret(insertedValue);
        }

    },
    delete: function (variable, curElement, locale) {
        $.ajax(
            {
                type: 'POST',
                data: {

                    variable: variable
                },
                url: '/admin/components/cp/mod_cities/deleteArticul/',
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not removed'), 'r');
                        return false;
                    }
                    curElement.closest('tr').remove();
                    showMessage(lang('Message'), lang('Variable successfully removed'));
                }
            }
        );
    },
    update: function (curElement, oldVariable, locale) {
        var closestTr = curElement.closest('tr');
        var variable = closestTr.find('.priceMainEdit');
        var variableValue = closestTr.find('.price_main_percentValueEdit');

        this.validateVariable(variable.val(), variableValue.val());

        $.ajax(
            {
                type: 'POST',
                data: {
                    variable: $.trim(variable.val()),
                    variableValue: $.trim(variableValue.val()),
                    oldVariable: oldVariable,
                },
                url: '/admin/components/cp/mod_cities/updateArticul/' ,
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not updated'), 'r');
                        return false;
                    }
                    closestTr.find('.priceMain').text(variable.val());
                    closestTr.find('.price_main_percentValue').text(variableValue.val());
                    variable.css('display', 'none');
                    variableValue.css('display', 'none');
                    closestTr.find('.editVariable').css('display', 'block');
                    closestTr.find('.refreshVariable').css('display', 'none');
                    showMessage(lang('Message'), lang('Variable successfully updated'));
                }
            }
        );
    },
    add: function (curElement,  locale) {
        var variable = curElement.closest('tr').find('.variableEdit');
        var variableValue = curElement.closest('tr').find('.variableValueEdit');

        this.validateVariable(variable.val(), variableValue.val());

        $.ajax(
            {
                type: 'POST',
                data: {
                    variable: $.trim(variable.val()),
                    variableValue: $.trim(variableValue.val()),

                },
                url: '/admin/components/cp/mod_cities/addArticul/' + locale,
                success: function (data) {
                    if (!data) {
                        showMessage(lang('Error'), lang('Variable is not added'), 'r');
                        return false;
                    }
                    curElement.parent('div').find('.typeVariable').val('');
                    $('.addVariableContainer').css('display', 'none');
                    $('.addVariableContainer').find('input').val('');
                    $('.addVariable').show();
                    $(data).insertBefore('table.variablesTable .addVariableContainer');
                    showMessage(lang('Message'), lang('Variable successfully added'));
                    window.location.reload()
                }
            }
        );
    },
    updateVariablesList: function (curElement, template_id, locale) {
        if (!curElement.hasClass('active')) {
            $.ajax(
                {
                    type: 'POST',
                    data: {
                        template_id: template_id
                    },
                    url: '/admin/components/cp/mod_cities/getTemplateVariables/' + locale,
                    success: function (data) {
                        $('#userMailVariables').html(data);
                        $('#adminMailVariables').html(data);
                    }
                }
            );
        }
    },
    validateVariable: function (variable, variableValue) {
        var variable = $.trim(variable);
        var variableValue = $.trim(variableValue);

        if (!variable) {
            showMessage(lang('Error'), lang('Enter variable'), 'r');
            exit;
        }

        if (variable.match(/[а-яА-Я]{1,}/)) {
            showMessage(lang('Error'), lang('Variable should contain only Latin characters'), 'r');
            exit;
        }



        if (!variableValue) {
            showMessage(lang('Error'), lang('Variable must have a value'), 'r');
            exit;
        }
    }
};