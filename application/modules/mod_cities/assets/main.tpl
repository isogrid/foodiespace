{ $all_cities = $all_cities}
{foreach $all_cities as $city}
    {$cities[$city['id']] = $city}
{/foreach}

<div class="user-panel__item user-panel__item--bg user-panel__item--currency">
    <div class="user-panel__link">
        {if $_COOKIE["front_selected_city_id"] && array_key_exists($_COOKIE["front_selected_city_id"], $cities)}
            {echo $cities[$_COOKIE["front_selected_city_id"]]['city_name']}
        {else:}
        {/*}{echo SCurrenciesQuery::create()->findByMain('1')['0']->code}{ */}{lang('Виберіть місто')}
        {/if}

        <i class="user-panel__arrow user-panel__arrow--down">
            <svg class="svg-icon">
                <use xlink:href="#svg-icon__arrow-down"></use>
            </svg>
        </i>

    </div>
    <div class="user-panel__drop user-panel__drop--rtl">
        <ul class="overlay">
            <form action="" data-currensy-select-form>


                {if count($cities) >0}

                    {foreach $cities as $one_city}

                        {if $_COOKIE["front_selected_city_id"]  != $one_city['id']  && $_COOKIE["front_selected_city_id"]}

                            <li class="overlay__item">

                                <label class="overlay__link"  onclick="city_front.set_temp_main({echo $one_city['id']})"  for="lang-{echo $one_city['id']}">
                                    {echo $one_city['city_name']}
                                </label>
                                <input type="hidden" id="lang-{echo $one_city['id']}" value="{echo $one_city['id']}">
                            </li>
                        {elseif !$_COOKIE["front_selected_city_id"] }

                            <li class="overlay__item">

                                <label class="overlay__link"  onclick="city_front.set_temp_main({echo $one_city['id']})"  for="lang-{echo $one_city['id']}">
                                    {echo $one_city['city_name']}
                                </label>
                                <input type="hidden" id="lang-{echo $one_city['id']}" value="{echo $one_city['id']}">
                            </li>
                        {/if}
                    {/foreach}

                {/if}
            </form>
        </ul>
    </div>

</div>

{literal}
<script type="application/javascript">

    var city_front = {
        set_temp_main: function (cur_id) {
            document.cookie = "front_selected_city_id=" +cur_id +"; path=/"; // куки без даты окончания, - удалятся после закрития браузера(сеессии)
//					window.location.reload();
            $.ajax(
                {
                    type: 'POST',
                    data: {
                        change: true,

                    },
                    url: '/mod_cities/emptyCartOnchangeCity',
                    success: function (data) {

                        window.location.href ='{/literal}{echo site_url()}{literal}'+data;
                    }
                }
            );

            //* window.location.href ='{/literal}{echo site_url($CI->uri->uri_string())}{literal}';*/
            // window.location.href ='{/literal}{echo site_url()}{literal}';
        }
    }
</script>
{/literal}