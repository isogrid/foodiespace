<?php

use mod_cities\classes\DataGetter\ConfigXMLReaderUrl;

use Currency\Currency;
use Products\ProductApi;
use CMSFactory\ModuleSettings;
use MediaManager\Image;


(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Image CMS
 * Module Frame
 */
class Mod_cities extends MY_Controller
{

    public static $check_event = false;
    public function __construct()
    {
        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('mod_cities');
        $this->load->helper('translit');

        $this->settings = ModuleSettings::ofModule('mod_cities')->get();
        $this->load->model('mod_cities_model');

    }

    public function get_cities()
    {

        $all_cities = $this->mod_cities_model->getAllCities('active');
        if (count($all_cities) <= 0 || !$all_cities) {
            return;
        }
        return \CMSFactory\assetManager::create()
            ->setData('all_cities', $all_cities)
//            ->registerScript('script', TRUE)
//            ->registerStyle('style', TRUE)
            ->fetchTemplate('main');

    }
    public function get_cities_register()
    {

        $all_cities = $this->mod_cities_model->getAllCities('active');
        if (count($all_cities) <= 0 || !$all_cities) {
            return;
        }
        return \CMSFactory\assetManager::create()
            ->setData('all_cities', $all_cities)
//            ->registerScript('script', TRUE)
//            ->registerStyle('style', TRUE)
            ->fetchTemplate('city_register');

    }

    public function get_AllCities1C()
    {

        $all_cities = $this->mod_cities_model->getAllCities();
        if (count($all_cities) <= 0 || !$all_cities) {
            return;
        }else{
            foreach ($all_cities as $city_id=>$city){
                $naew_city_array[strtolower($city['city_translit_name'])]= $city_id;
            }
        }
        return $naew_city_array;

    }

    public function get_AllCities()
    {

        $all_cities = $this->mod_cities_model->getAllCities();
        if (count($all_cities) <= 0 || !$all_cities) {
            return;
        }
        return $all_cities;

    }

    public function emptyCartOnchangeCity()
    {
        $cart = new \Cart\BaseCart();
        $cart->removeAll();

        $page_url = end(explode('/', $_SERVER['HTTP_REFERER']));
        $exist_rote = $this->db->where('url', $page_url)->get('route');
        if($exist_rote && $exist_rote->num_rows()>0){
            $exist_rotes=$exist_rote->result_array();
            if($exist_rotes['0']['type'] == 'product'){
                $prod = $this->db->where('product_id', $exist_rotes['0']['entity_id'])->get('shop_product_variants');
                if($prod && $prod->num_rows()>0){
                    $prod=$prod->result_array();

                    $prodN =   $this->db->where('number', $prod['0']['number'])
                        ->where('city_id', $_COOKIE['front_selected_city_id'])
                        ->get('shop_product_variants');
                    if($prodN && $prodN->num_rows()>0){
                        $rote = SProductsQuery::create()->findOneById($prodN->result_array()['0']['product_id'])->getRouteUrl();
                    return  $rote;
                    }else{
                        return ;
                    }
                }else{
                    return ;
                }
            }elseif($exist_rotes['0']['type'] == 'shop_category'){
                return ;
            }else{
                return ;
            }
        }else{
            return ;
        }


    }


    public function autoload()
    {
        if (method_exists(\CMSFactory\Events::create(), 'onShopMakeOrder')) {
            \CMSFactory\Events::create()->onShopMakeOrder()->setListener('save_order_data_module');
        } else {
            \CMSFactory\Events::create()->setListener('save_order_data_module', 'MakeOrder');
        }

    }

    public static function adminAutoload()
    {
        parent::adminAutoload();
//        \CMSFactory\Events::create()->onShopAdminOrderCreate()->setListener('oonShopOrdersCreate');
//        \CMSFactory\Events::create()->onShopAdminOrderEdit()->setListener('oonShopOrdersEdit');
//        \CMSFactory\Events::create()->onShopAdminOrderAddProdQuantity()->setListener('adminOrdAddQuantity');
//        \CMSFactory\Events::create()->onShopAdminOrderDelProd()->setListener('adminOrdDelProd');
    }

    public function save_order_data_module($arg)
    {

        if (!self::$check_event) {
            if ($_COOKIE["front_selected_city_id"]) {
                $modules = \CI::$APP->template->template_vars['modules'];
                if (array_key_exists('mod_cities', $modules)) {
                    $cities = module('mod_cities')->get_AllCities();
                    if ($cities && $cities[$_COOKIE["front_selected_city_id"]] != null) {
                        $city_filter = true;
                    } else {
                        $city_filter = false;
                    }
                } else {
                    $city_filter = false;
                }
            } else {
                $city_filter = false;
            }
            \CI::$APP->db
                ->where('id', $arg['order']->getId())
                ->set(['city_id' => $_COOKIE["front_selected_city_id"]])
                ->update('shop_orders');


            self::$check_event = true;
        }
    }


    public function _install()
    {
        $this->load->dbforge();
        ($this->dx_auth->is_admin()) OR exit;
        $this->db
            ->where('identif', 'mod_cities')
            ->update(
                'components', [
                    'settings' => '',
                    'enabled' => 1,
                    'autoload' => 1
                ]
            );


        $fields_api = array(
            'id' => array(
                'type' => 'INT',
                'auto_increment' => TRUE
            ),
            'city_name' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false
            ),
            'city_translit_name' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false
            ),
            'active' => array(
                'type' => 'TINYINT',
            ),
        );

        $this->dbforge->add_field($fields_api);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('mod_cities');

        $this->db->query('ALTER TABLE  `shop_category` ADD  `city_id` INTEGER ( 50 ) NULL DEFAULT NULL');
        $this->db->query('ALTER TABLE  `shop_products` ADD  `city_id` INTEGER( 50 ) NULL DEFAULT NULL');
        $this->db->query('ALTER TABLE  `shop_product_variants` ADD  `city_id` INTEGER( 50 ) NULL DEFAULT NULL');
        $this->db->query('ALTER TABLE  `shop_orders` ADD  `city_id`  INTEGER( 50 ) NULL DEFAULT NULL');
        $this->db->query('ALTER TABLE  `users` ADD  `city_id` INTEGER( 50 ) NULL DEFAULT NULL');


        return TRUE;
    }

    public function _deinstall()
    {

        $this->load->dbforge();
        ($this->dx_auth->is_admin()) OR exit;
        $this->dbforge->drop_table('mod_cities');
    }

}

/*
  Родительский класс для XML обработчиков.
 */




