<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
use CMSFactory\ModuleSettings;

/**
 * Image CMS
 * Sample Module Admin
 */
class Admin extends BaseAdminController
{

    public function __construct()
    {
        parent::__construct();
        $lang = new MY_Lang();
        $lang->load('mod_cities');
        $this->settings = ModuleSettings::ofModule('mod_cities')->get();
        $this->load->model('mod_cities_model');
    }

    public function index()
    {

        $all_cities = $this->mod_cities_model->getAllCities();
        \CMSFactory\assetManager::create()
            ->setData('all_cities', $all_cities)
            ->registerScript('script_prov', TRUE)
//            ->registerStyle('style', TRUE)
            ->renderAdmin('all_cities');
    }


    public function addCity()
    {

        if ($this->input->post('variableTranslitEdit')) {
           return  $this->mod_cities_model->addCity($this->input->post('variable'),$this->input->post('variableTranslitEdit'),$this->input->post('variableValue'));

            return true;
        }
    }

    public function updateProv()
    {

        if ($this->input->post('variableValue') && $this->input->post('variableTranslitEdit')) {

            return  $this->mod_cities_model->updateCity($this->input->post('variable'),$this->input->post('variableTranslitEdit'),$this->input->post('variableValue'),$this->input->post('oldVariable'));

        }
    }

    public function deleteProvider()
    {
//dd($this->input->post('variable'));

        if ($this->input->post('variable')) {

            return  $this->mod_cities_model->deleteCity($this->input->post('variable'));

            $this->cache->delete_all();
        }
    }




}