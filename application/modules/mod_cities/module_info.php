<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

$com_info = [
    'menu_name' => lang('Управление городами','mod_cities'), // Menu name
    'description' => 'Настройка Управление городами',            // Module Description
    'admin_type' => 'window',       // Open admin class in new window or not. Possible values window/inside
    'window_type' => 'xhr',         // Load method. Possible values xhr/iframe
    'w' => 600,                     // Window width
    'h' => 550,                     // Window height
    'version' => '0.1',             // Module version
    'author' => 'partner@@imagecms.net',  // Author info
    'icon_class' => 'fa  fa-spin'
];

/* End of file module_info.php */