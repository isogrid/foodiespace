<?php


/**
 * Class ug_model
 * @property CI_DB_active_record db
 */
class mod_cities_model extends CI_Model
{


    /**
     * @return array
     */
    public function getPriceTypes()
    {
        return $this->priceTypes;
    }



    public function getAllCities($active)
    {

        if($active =='active'){
            $result = $this->db->where('active', 1);
            $result = $result->order_by('id');
            $result = $result->get('mod_cities');
        }else{
            $result = $this->db->order_by('id');
            $result = $result->get('mod_cities');
        }

        $result = $result->num_rows() > 0 ? $result->result_array() : [];

        $ids = array_column($result, 'id');

        return array_combine($ids, $result);
    }
    public function addCity($name, $translit_name, $active)
    {
        $data = [
            'city_name' => $name,
            'city_translit_name' =>$translit_name,
            'active' => $active=='true'?1:0,
        ];
 
         $this->db->insert('mod_cities',$data);

        if ($this->db->_error_message() && $this->db->_error_message() != null && $this->db->_error_message() != '') {
            return false;
        }
        return true;
    }
    public function updateCity($name, $translit_name, $active, $id)
    {
        $data = [
            'city_name' => $name,
            'city_translit_name' =>$translit_name,
            'active' => $active=='true'?1:0,
        ];
        $city = $this->db->where('id', $id)->get('mod_cities');

        if($city && $city->num_rows()>0){
            $this->db->where('id', $id)->set($data)->update('mod_cities');
        }


        if ($this->db->_error_message() && $this->db->_error_message() != null && $this->db->_error_message() != '') {
            return false;
        }
        return true;
    }
    public function deleteCity($id)
    {

        $city = $this->db->where('id', $id)->get('mod_cities');

        if($city && $city->num_rows()>0){
            $this->db->where('id', $id)->delete('mod_cities');
        }


        if ($this->db->_error_message() && $this->db->_error_message() != null && $this->db->_error_message() != '') {
            return false;
        }
        return true;
    }


}