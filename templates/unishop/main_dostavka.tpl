<!DOCTYPE html>
<html lang="{current_language()}">
<head>

  <!-- Page meta params. Should always be placed before any others head info -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Meta data -->
  <title>{$site_title}</title>
  <meta name="description" content="{$site_description}">
  <meta name="keywords" content="{$site_keywords}">
  <meta name="generator" content="ImageCMS">

  <!-- Final compiled and minified stylesheet -->
  <!--
  * !WARNING! Do not modify final.min.css file! It has been generated automatically
  * All changes will be lost when sources are regenerated!
  * Use Sass files _src/scss instead. Read more here http://docs.imagecms.net/rabota-s-shablonom-multishop/rabota-s-css-i-javasctipt-dlia-razrabotchikov
  -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i&amp;subset=cyrillic-ext" rel="stylesheet">
  <link rel="stylesheet" href="{$THEME}_css/final.min.css">
  <link rel="stylesheet" href="{$THEME}_css/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="{$THEME}_css/selectric.css">
  <link href="{$THEME}/fonts/styles.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--
  * Uncomment this file if you want to put custom styles and do not want to use Sass and Gulp
  -->
  <!-- <link rel="stylesheet" href="{$THEME}_css/custom.css"> -->

  <!-- Shortcut icons -->
  <link rel="shortcut icon" href="{siteinfo('siteinfo_favicon_url')}" type="image/x-icon">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/js/jquery.timepicker.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="/js/jquery.timepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
</head>
<body class="page">

<!-- Main content frame -->
<div class="page__body" data-page-pushy-container>
  <div class="page__wrapper">

    <header class="page__hgroup">
      <!-- Header -->
      {view('includes/header.tpl')}

      <!-- Main Navigation -->
      <div class="page__mainnav-hor hidden-xs hidden-sm">
        <div class="page__container">
          <!-- condition getOpenLevels() == all allows to output mega menu in case of appropriate admin settings -->
          {if getOpenLevels() == 'all'}
            {load_catalog_menu('navs/catalog_cols')}
          {else:}
            {load_catalog_menu('navs/catalog_tree')}
          {/if}
        </div>
      </div>

    </header>

<div class="page_shapka dostavka_bg">
    <!-- Bread Crumbs -->
    {widget('breadcrumbs')}

    <!-- Post title -->
    <div class="content__header">
      <h1 class="content__title">
        {$page.title}
      </h1>
     <!-- <div class="prev_text_shapka">{$page.prev_text}</div>-->
    </div>
</div>
    <div class="page__content" style="padding-bottom: 0 !important;">
      {$content}
    </div>

      {view('includes/language_popup.tpl', ['mod_cities' => $mod_cities])}
  </div><!-- .page__wrapper -->

  <!-- Footer -->
  <footer class="page__fgroup">
    <div class="footer_inside">
    {view('includes/footer.tpl')}
    </div>
  </footer>

</div><!-- .page__body -->

<!-- Mobile slide frame -->
<div class="page__mobile" data-page-pushy-mobile>
  {view('includes/mobile_frame.tpl')}
</div>

<!-- Site background overlay when mobile menu is open -->
<div class="page__overlay hidden" data-page-pushy-overlay></div>
<div class="page-overlay hidden" data-page-overlay></div>
<!-- Final compiled and minified JS -->
<script src="{$THEME}_js/vendor.min.js"></script>
<script src="{$THEME}_js/final.min.js"></script>
<script src="{$THEME}_js/jquery.mCustomScrollbar.js"></script>
<script src="{$THEME}_js/jquery.selectric.js"></script>
<script src="{$THEME}_js/tabs.js"></script>
{literal}
<script>
$(function() {
  $('select').selectric();
});
$(".brands_inner").mCustomScrollbar({
});
</script>
{/literal}


<!--
* Uncomment this file if you want to put custom styles and do not want to use Gulp build
-->
<!-- <script src="{$THEME}_js/custom.js"></script> -->
<!-- Social networks login module styles init -->
{if array_key_exists('socauth', $modules)}
  {tpl_register_asset('socauth/css/style.css', 'before')}
  {if !$CI->dx_auth->is_logged_in()}
    {tpl_register_asset('socauth/js/socauth.js', 'after')}
  {/if}
{/if}
</body>
</html>