<div class="content">
    <div class="content__container">

        <div class="content__header">
            <h1 class="content__title">
                {tlang('Create Account')}
            </h1>
        </div>
        <div class="content__row">
            <div class="row">
                <div class="col-sm-7">
                    <form class="form form--bg" action="{site_url('auth/register')}" method="post"
                           >

                        <!-- Error Messages -->
                        {if validation_errors()}
                            <div class="form__messages">
                                <div class="message message--error">
                                    <ul class="message__list">
                                        {validation_errors('<li class="message__item">', '</li>')}
                                    </ul>
                                </div>
                            </div>
                        {/if}

                        <!-- Login in via Social Networks -->
                        {if array_key_exists('socauth', $modules)}
                            <div class="form__field">
                                <div class="form__label">{tlang('Sign in with')}</div>
                                <div class="form__inner">
                                    {module('socauth')->renderLogin();}
                                </div>
                            </div>
                        {/if}

                        <!-- Name field -->
                        {view('includes/forms/input-base.tpl', [
                        'label' => tlang('Name'),
                        'type' => 'text',
                        'name' => 'username',
                        'value' => get_value('username'),
                        'required' => true
                        ])}

                        {view('includes/forms/input-base.tpl', [
                        'label' => 'Прізвище',
                        'type' => 'text',
                        'name' => 'lastname',
                        'value' => get_value('lastname'),
                        'required' => true
                        ])}

                        {view('includes/forms/input-base.tpl', [
                        'label' => 'По-батькові',
                        'type' => 'text',
                        'name' => 'surname',
                        'value' => get_value('surname'),
                        'required' => true
                        ])}

                        <!-- Email field -->
                        {view('includes/forms/input-base.tpl', [
                        'label' => tlang('E-mail'),
                        'type' => 'email',
                        'name' => 'email',
                        'value' => get_value('email'),
                        'required' => false
                        ])}
                         {view('includes/forms/input-base.tpl', [
                        'label' => tlang('Телефон'),
                        'type' => 'phone',
                        'name' => 'phone',
                        'value' => get_value('phone'),
                        'desc' => 'Ваш телефон буде використовуватись при авторизації',
                        'required' => true,
                             'placeholder'=>'380950000000'
                        ])}
                        {view('includes/forms/input-base.tpl', [
                        'label' => tlang('Дата народження'),
                        'type' => 'date',
                        'name' => 'date',
                        'value' => get_value('date'),
                        'desc' => 'Знижка на день народження',
                        'required' => true
                        ])}
                        {view('includes/forms/input-base.tpl', [
                        'label' => 'Місто',
                        'type' => 'text',
                        'name' => 'city',
                        'value' => get_value('city'),
                        'required' => false
                        ])}

                        <!--{if array_key_exists('mod_cities', $modules)}
                            { module('mod_cities')->get_cities_register()}
                        {/if}-->

                        <!-- Custom fields -->
                        {foreach ShopCore::app()->CustomFieldsHelper->getCustomFielsdAsArray('user') as $field}

                            <!-- Text input field type. field_type_id == 0 -->
                            {if $field['field_type_id'] == 0 || $field['id'] == 123}

                                {view('includes/forms/input-base.tpl', [
                                'label' => $field['field_label'],
                                'type' => 'text',
                                'name' => 'custom_field[' . $field['id'] . ']',
                            'value' => get_value($field['field_name']),
                            'required' => $field['is_required'],
                            'desc' => $field['field_description']
                            ])}

                                <!-- File input field type. field_type_id == 3 -->
                            {elseif $field['field_type_id'] == 3}
                                <!-- Textarea field type. field_type_id == 1 -->
                            {else:}

                                {view('includes/forms/textarea-base.tpl', [
                                'label' => $field['field_label'],
                                'name' => 'custom_field[' . $field['id'] . ']',
                            'value' => get_value($field['field_name']),
                            'required' => $field['is_required'],
                            'desc' => $field['field_description'],
                            'rows' => 5
                            ])}

                            {/if}

                        {/foreach}

                        <!-- Password field -->
                        {view('includes/forms/input-base.tpl', [
                        'label' => tlang('Password'),
                        'type' => 'password',
                        'name' => 'password',
                        'value' => '',
                        'required' => true
                        ])}

                        <!-- Confirm password field -->
                        {view('includes/forms/input-base.tpl', [
                        'label' => tlang('Reenter password'),
                        'type' => 'password',
                        'name' => 'confirm_password',
                        'value' => '',
                        'required' => true
                        ])}

                        <div class="form__field">
                            <div class="form__label"></div>
                            <div class="form__inner">
                                <label class="form__checkbox">
                                    <span class="form__checkbox-field">
                                        <input type="checkbox" name="receive_spam" value="1" checked>
                                    </span>
                                    <span class="form__checkbox-inner">
                                      <span class="form__checkbox-title">Я хочу отримувати розсилку з новинами та акціями</span>
                                    </span>
                                </label>
                            </div>
                        </div>

                        <!-- Submit button -->
                        {/*}<div class="form__field">
                            <div class="form__label"></div>
                            <div class="form__inner">
                                <input id="merchantType" class="btn btn-primary" type="submit"
                                       value="{tlang('Create Account')}"
                                       data-modal1="formRegSMS">
                            </div>
                        </div>
                        <input type="hidden" name="template" value="formRegSMS">
                        { */}
                        <div class="form__field">
                            <div class="form__label"></div>
                            <div class="form__inner">
                                <button class="btn btn-primary reg-confirm__btn {if isset($_COOKIE["BtnDis"])}hidden{/if}" type="submit"
                                        href="#bpopup" data-modal-submit data-modal-type="inline" data-vefication-button--loader>
                                    {tlang('Create Account')}
                                    <svg class="svg-icon svg-icon--in-btn svg-icon--spinner hidden" data-button-loader="loader">
                                        <use xlink:href="{$THEME}_img/sprite.svg#svg-icon__refresh"></use>
                                    </svg>
                                </button>
                                <button class="btn btn-primary {if !isset($_COOKIE["BtnDis"])}hidden{/if}" type="button" href="#b-popup" data-open-popup-btn data-modal-type="inline">
                                    {tlang('Create Account')}
                                </button>
                            </div>
                        </div>


                        <!-- Additional info -->
                        <div class="form__field">
                            <div class="form__label"></div>
                            <div class="form__inner">
                                <div class="form__desc">{tlang('Already Have an Account?')}<br><a class="form__link"
                                                                                                  href="{site_url('auth')}">{tlang('Sign in')}</a>
                                    / <a class="form__link"
                                         href="{site_url('auth/forgot_password')}">{tlang('Forgot password?')}</a>
                                </div>
                            </div>
                        </div>

                        {form_csrf()}
                    </form>

                </div>
            </div>
        </div>


    </div><!-- /.content__container -->
</div><!-- /.content -->
