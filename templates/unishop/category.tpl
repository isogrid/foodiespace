<div class="content">
  <div class="content__container">

    <!-- Category title -->
    <div class="content__header">
      <h1 class="content__title">
        {$category.name}
      </h1>
    </div>

    <!-- Category description -->
    {if trim($category.short_desc)}
      <div class="content__row">
        <div class="typo">{$category.short_desc}</div>
      </div>
    {/if}

    <!-- Category post list BEGIN -->
    <div class="content__row">
      {if count($pages) > 0}
      <div class="row row--ib row--vindent-s">
        {foreach $pages as $item}
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <article class="small-post">
              {if trim($item.field_list_image) != ""}
                <a class="small-post__image" href="{site_url($item.full_url)}">
                  <img src="{$item.field_list_image}" alt="{$item.title}">
                </a>
              {/if}
              <div class="small-post__inner">
                <time class="small-post__date"
                      datetime="{date('Y-m-d', $item.publish_date)}">{tpl_locale_date('d F Y', $item.publish_date)}</time>
                <div class="small-post__title">
                  <a class="small-post__title-link" href="{site_url($item.full_url)}">{$item.title}</a>
                </div>
                {if trim($item.prev_text) != ""}
                  <div class="small-post__desc">
                    <div class="typo typo--sub-color">{$item.prev_text}</div>
                  </div>
                  <a class="small-post__readmore" href="{site_url($item.full_url)}">{tlang('Read more')}</a>
                {/if}
              </div>
            </article>
          </div>
        {/foreach}
      </div>
      {else:}
        <p class="typo">{tlang('There are no items to display. Please come back later!')}</p>
      {/if}

      <!-- Category pagination BEGIN -->
      {if $pagination}
        <div class="content__pagination">
          {$pagination}
        </div>
      {/if}
    </div><!-- /.content__row -->

  </div><!-- /.content__container -->
</div><!-- /.content -->