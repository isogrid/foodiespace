<!-- Top Headline -->
<div class="page__headline hidden-xs hidden-sm">
  <div class="page__container">

    <div class="row row--ib row--ib-mid">
      <div class="col-md-6">
        <div class="page__top-menu">
          <div class="inline_block" >
          {if array_key_exists('mod_cities', $modules)}
              { echo $mod_cities = module('mod_cities')->get_cities()}
          {/if}
          </div>
          <div class="inline_block">
          {load_menu('info_header')}
          </div>

        </div>
      </div>
      <div class="col-md-6 clearfix">
        <div class="page__user-bar">
          {view('includes/header_toolbar.tpl')}
        </div>
      </div>
    </div>

  </div>
</div>


<!-- Main Header -->
<div class="page__header">
  <div class="page__container">

    <div class="row row--ib row--ib-mid">
      <!-- Hamburger menu -->
      <div class="col-xs-3 visible-xs-inline-block visible-sm-inline-block">
        <button class="btn-mobile-icon" data-page-mobile-btn>
          <svg class="svg-icon">
            <use xlink:href="{$THEME}_img/sprite.svg#svg-icon__hamburger"></use>
          </svg>
        </button>
        <button class="btn-mobile-icon hidden" data-page-mobile-btn>
          <svg class="svg-icon">
            <use xlink:href="{$THEME}_img/sprite.svg#svg-icon__close-bold"></use>
          </svg>
        </button>
      </div>
      <!-- Logo -->
      <div class="col-xs-6 col-md-3 col-lg-2 col--align-center col--align-left-md logotype">
        {if siteinfo('siteinfo_logo') != ""}
          {if $CI->core->core_data['data_type'] != 'main'}<a href="{site_url('')}">{/if}
          <img src="{echo siteinfo('siteinfo_logo')}" alt="{echo $CI->core->settings['site_title']}">
          {if $CI->core->core_data['data_type'] != 'main'}</a>{/if}
        {/if}
      </div>
      <!-- Phones and call-back -->
      
      <!-- Schedule -->
      <div class="col-lg-3 col-lg-push-3 hidden-xs hidden-sm hidden-md">
        <div class="work_time">
          {if trim(siteinfo('schedule')) != ""}
                {echo siteinfo('schedule')}
          {/if}
        </div>
      </div>

      <div class="col-md-3 col-lg-2 col-md-push-3 col-lg-push-3 hidden-xs hidden-sm">
        <div class="phone_head">
          <div class="phone_number">
            {if trim(siteinfo('mainphone')) != ""}
                {nl2br(siteinfo('mainphone'))}
              {/if}
          </div>
          <div class="call_me"><a href="/callbacks" data-modal="callbacks_modal" rel="nofollow">Передзвоніть мені</a></div>
        </div>
      </div>
      <!-- Cart -->
      <div class="col-xs-3 col-md-1 col-lg-2 col-md-push-4 col-lg-push-3 clearfix">
        {if !ShopCore::app()->SSettings->useCatalogMode()}
        <div class="pull-right" data-ajax-inject="cart-header">
          {view('shop/includes/cart/cart_header.tpl', ['model' => \Cart\BaseCart::getInstance()])}
        </div>
        {/if}
      </div>
      <!-- Search -->
      <div class="col-xs-12 col-md-5 col-lg-3 col-md-pull-5 col-lg-pull-7 col--spacer-sm">
        {view('shop/includes/search_and_brand/autocomplete.tpl')}
      </div>
    </div>

  </div>
</div>