{$options = $data;}
{$value_label = $data[1];}
{$name_label = $data[2];}
{$required = $required ? "required" : ""}


<div class="form__field">
    {if $label}
      <div class="form__label">
          {$label}
          {if $required}<i class="form__require-mark"></i>{/if}
      </div>
    {/if}
  <div class="form__inner">
    <select class="form-control" name="{$name}" {$required} id="merchantType">
      {foreach $options as $sityId=>$option}
      <option value="{$sityId}" selected="">{$option['city_name']}</option>
      {/foreach}
    </select>    
    {if trim(strip_tags($desc)) != ""}
    <div class="form__info form__info--help">{$desc}</div>
    {/if}
    {if get_error($name)}
    <div class="form__info form__info--error">{get_error($name)}</div>
    {/if}
  </div>
</div>