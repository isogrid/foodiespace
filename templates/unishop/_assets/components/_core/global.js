;(function ($) {

    /* Remove ajax loader */
    $(document).on('ajaxStop', function () {
        $.mlsAjax.preloaderHide();
    });

    /* Hover to click on touch devices. Double click to link */
    $('[data-global-doubletap]').doubleTapToGo();

    // if (document.cookie.indexOf('front_selected_city_id') == -1) {
    //     // $('[data-language-popup]').removeClass('hidden');
    //     // $('[data-page-overlay]').removeClass('hidden');
    //     // $('html').addClass('disable-scroll');
    // }

})(jQuery);