{if $totalProducts > 0 || tpl_is_filtered($minPrice,  $maxPrice) || count($category->getTree()) == 0}
  <div class="content">
    <div class="content__container">
      <div class="row">

        <!-- Left BEGIN -->
        <div class="col-sm-4 col-md-3 col-lg-2">

          <!-- Sub categories -->
          <div class="hidden-xs">
            {view('shop/includes/category/category_subnav.tpl')}
          </div>

          <!-- Filter toggle button on mobile devices -->
          <div class="content__sidebar-item visible-xs">
            <button class="btn btn-default btn-block" data-filter-toggle--btn>
            <span data-filter-toggle--btn-text>{tlang('Show filter')}
              <i class="btn-default__ico btn-default__ico--down">
                <svg class="svg-icon"><use xlink:href="{$THEME}_img/sprite.svg#svg-icon__arrow-down"></use></svg>
              </i>
            </span>
              <span class="hidden" data-filter-toggle--btn-text>{tlang('Hide filter')}
                <i class="btn-default__ico btn-default__ico--top">
                <svg class="svg-icon"><use xlink:href="{$THEME}_img/sprite.svg#svg-icon__arrow-top"></use></svg>
              </i>
            </span>
            </button>
          </div>

          <!-- Filter -->
          <div class="hidden-xs" data-filter-toggle--filter>
            {module('smart_filter')->init();}
          </div>
        </div>
        <!-- END Left -->

        <!-- Center BEGIN -->
        <div class="col-sm-8 col-md-9 col-lg-10">

          <div class="row">
          <div class="col-md-4 col-sm-4 col-lg-4">
          <!-- Category title -->
          <div class="content__header">
            <h1 class="content__title">
              {$title}
            </h1>
            <span class="content__hinfo">
            {tlang('Result')}:
              <i class="content__hinfo-number">{$totalProducts}</i>
              {echo SStringHelper::Pluralize($totalProducts, array(tlang('pluralize item 1'), tlang('pluralize item 2'), tlang('pluralize item 3')))}
          </span>
          </div>
        </div>
        <div class="col-md-8 col-sm-8 col-lg-8 catalog_actions">
          <div class="inline_div">
          <div class="change-view">
                <button class="change-view__button"
                        data-catalog-view-item="card"
                        {if !$_COOKIE['catalog_view'] || $_COOKIE['catalog_view'] == 'card'}disabled{/if}>
                  <i class="change-view__icon"
                     title={tlang("Card")}><svg class="svg-icon"><use xlink:href="{$THEME}_img/sprite.svg#svg-icon__table"></use></svg></i>
                </button>
                <button class="change-view__button"
                        data-catalog-view-item="snippet"
                        {if $_COOKIE['catalog_view'] == 'snippet'}disabled{/if}>
                  <i class="change-view__icon"
                     title={tlang("List")}><svg class="svg-icon"><use xlink:href="{$THEME}_img/sprite.svg#svg-icon__list"></use></svg></i>
                </button>
              </div>
        </div>

        <div class="inline_div">
          {$loc_per_page_items = tpl_per_page_array()}
          {if count($loc_per_page_items) > 1}
            
              <label class="catalog-toolbar__label hidden-xs hidden-sm" for="catalog-per-page">{tlang('Per page')}</label>
              <div class="catalog-toolbar__field">
                <select style="display: none;" class="form-control input-sm"
                        id="catalog-per-page"
                        form="catalog-form"
                        name="user_per_page"
                        data-catalog-perpage-select>
                  {foreach $loc_per_page_items as $per_page_item}
                    <option value="{$per_page_item}"
                            {if tpl_per_page_selected($per_page_item)}selected{/if}
                            {if tpl_per_page_selected($per_page_item, false)}data-catalog-default{/if}>
                      {$per_page_item}
                    </option>
                  {/foreach}
                </select>
              </div>
            </>
          {/if}
        </div>

          <div class="inline_div sort_block">
          {$loc_sorting_list = ShopCore::app()->SSettings->getSortingFront()}
          {if $loc_sorting_list}
              <label class="catalog-toolbar__label hidden-xs hidden-sm" for="catalog-sort-by">{tlang('Sort by')}</label>
                {$loc_current_sort = tpl_get_default_sorting($parent_default_order)}
                {$loc_default_sort = tpl_get_default_sorting($parent_default_order, false)}
                <div class="catalog-toolbar__field">
                <select style="display: none;" class="custom-select form-control input-sm"
                        id="catalog-sort-by"
                        form="catalog-form"
                        name="order"
                        data-catalog-order-select>
                  {foreach $loc_sorting_list as $key => $order}
                    <option value="{$order.get}"
                            {if $loc_current_sort == $order.get}selected{/if}
                            {if $loc_default_sort == $order.get}data-catalog-default{/if}
                    >{$order.name_front}</option>
                  {/foreach}
                </select>
              </div>
          {/if}
        </div>
        
        
          





<!-- COLS -->
          </div>

          </div>

          <!-- Horisontal banner -->
          {if $loc_banner = getBanner('catalog_horisontal_banner', 'object')}
            <div class="content__row content__row--sm">
              {view('xbanners/banners/banner_simple.tpl', [
              'parent_banner' => $loc_banner
              ])}
            </div>
          {/if}

          <!-- Filter selected results -->
          {view('smart_filter/includes/filter_results.tpl')}

          <!-- Product list -->
          <div class="content__row">
            {view('shop/includes/category/category_products.tpl')}
          </div>


          <!-- Category description -->
          {if trim($category->getDescription()) != "" and $page_number < 2}
            <div class="content__row">
              <div class="typo">{echo trim($category->getDescription())}</div>
            </div>
          {/if}

        </div><!-- /.col -->



        <!-- END Center -->

      </div>
    </div><!-- /.content__container -->
    {view('shop/includes/category/category_form.tpl')}
  </div>
  <!-- /.content -->
{else:}
  {view('shop/section.tpl')}
{/if}