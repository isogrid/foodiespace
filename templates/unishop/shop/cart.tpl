<div class="content">
  <div class="content__container" data-cart-scope data-ajax-inject="cart-empty">

    <div class="content__header">
      <h1 class="content__title">
        {tlang('One Step Checkout')}
      </h1>
    </div>

    {if count($items) > 0}
      <div class="row">

        <!-- Order cart -->
        <div class="col-sm-6 col-sm-push-6 col-md-7 col-lg-6 col-md-push-5 col-lg-push-6">
          <div class="cart-frame">
            <div class="cart-frame__header">
              <div class="cart-frame__title">{tlang('Order summary')}</div>
            </div>
            <div class="cart-frame__inner"
                 data-cart-summary="page"
                 data-cart-summary--url="{shop_url('cart')}">
              {view('shop/includes/cart/cart_summary.tpl', [
                'parent_type' => 'order',
              'parent_coupon' => true
              ])}
            </div>
          </div>
        </div>

        <!-- Order form -->
        <div class="col-sm-6 col-sm-pull-6 col-md-5 col-lg-6 col-md-pull-7 col-lg-pull-6 col--spacer-xs">
          {if !$CI->dx_auth->is_logged_in()}
            <div class="content__row content__row--sm">
              <div class="typo">
                {tlang('Returning customer?')}
                <a href="{site_url('auth')}"
                   data-modal="login_popup">{tlang('Sign in')}
                </a>
              </div>
            </div>
          {/if}
          <div class="content__row">
            <!-- Ordering form -->
            {view('shop/includes/cart/cart_checkout.tpl')}
          </div>
        </div>

      </div>
    {else:}
      <div data-ajax-grab="cart-empty">
        <div class="content__header">
          <h1 class="content__title">
            {tlang('Cart is currently empty')}
          </h1>
        </div>
        <div class="content__row">
          <p class="typo">{tlang('You have no items in your shopping cart')}</p>
        </div>
        <div class="content__row">
          <a class="btn btn-primary" href="{site_url('')}">{tlang('Continue Shopping')}</a>
        </div>
      </div>
    {/if}

    <!--
        Insert Header cart template via Ajax
    -->
    <div class="hidden" data-ajax-grab="cart-header">
      {view('shop/includes/cart/cart_header.tpl', ['model' => $cart])}
    </div>

  </div><!-- /.content__container -->
</div><!-- /.content -->
{literal}
<script type="text/javascript">
  $(function() {
      console.log($.datepicker.regional['uk']);
    $("input[name=custom_field\\[131\\]]").datepicker({
        closeText: "Закрити",
        prevText: "&#x3C;",
        nextText: "&#x3E;",
        currentText: "Сьогодні",
        monthNames: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень",
            "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
        monthNamesShort: ["Січ", "Лют", "Бер", "Кві", "Тра", "Чер",
            "Лип", "Сер", "Вер", "Жов", "Лис", "Гру"],
        dayNames: ["неділя", "понеділок", "вівторок", "середа", "четвер", "п’ятниця", "субота"],
        dayNamesShort: ["нед", "пнд", "вів", "срд", "чтв", "птн", "сбт"],
        dayNamesMin: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        weekHeader: "Тиж",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    });
    $("input[name=custom_field\\[132\\]").parent().parent().hide();
    $("input[name=custom_field\\[133\\]").parent().parent().hide();
    $("input[name=custom_field\\[135\\]").parent().parent().hide();
    // $("input[name=custom_field\\[136\\]").parent().parent().hide();
    $("input[name=custom_field\\[137\\]").parent().parent().hide();
    var options = [
        'Вінницька обл.',
        'Волинська обл.',
        'Дніпропетровська обл.',
        'Донецька обл.',
        'Житомирська обл.',
        'Закарпатська обл.',
        'Запорізька обл.',
        'Івано-Франківська обл.',
        'Київська обл.',
        'Кіровоградська обл.',
        'Луганська обл.',
        'Львівська обл.',
        'Миколаївська обл.',
        'Одеська обл.',
        'Полтавська обл.',
        'Рівненська обл.',
        'Сумська обл.',
        'Тернопільська обл.',
        'Харківська обл.',
        'Херсонська обл.',
        'Хмельницька обл.',
        'Черкаська обл.',
        'Чернівецька обл.',
        'Чернігівська обл.',
    ];
    var options_string = '';
    for (var k in options) {
        options_string += '<option value="' + options[k] + '">' + options[k] + '</option>';
    }
      $('input[name=custom_field\\[135\\]').parent().prepend(
          '<select name="region_select">'+options_string+'</select>'
      );
    $('input[name=custom_field\\[135\\]').hide();

    $(document).on('change', 'select[name=region_select]', function () {
        $('input[name=custom_field\\[135\\]').val($(this).val());
    })
  });
  $('label[for=deliveryMethod_67]').parent().find('.delivery-radio__info').last().remove();
  $(document).on('change', 'input[name=deliveryMethodId]', function () {
     if ($(this).val() == 67) {
         $("input[name=custom_field\\[135\\]").parent().parent().show();
         // $("input[name=custom_field\\[136\\]").parent().parent().show();
         $("input[name=custom_field\\[137\\]").parent().parent().show();
         $('#dateFrom').parent().parent().hide();
     } else {
         $("input[name=custom_field\\[135\\]").parent().parent().hide();
         // $("input[name=custom_field\\[136\\]").parent().parent().hide();
         $("input[name=custom_field\\[137\\]").parent().parent().hide();
         $('#dateFrom').parent().parent().show();
     }
  });
  $('#dateFrom').timepicker({ 'step': 15, 'timeFormat': 'H:i', 'minTime': '10:00am'});
  $('#dateTo').timepicker({
      'timeFormat': 'H:i',
      'minTime': '10:00am',
      'step': function(i) {
          return (i%2) ? 15 : 45;
      }
  });
  $('#dateTo').on('changeTime', function() {
    $("input[name=custom_field\\[132\\]").val($('#dateFrom').val());
  });
  $('#dateFrom').on('changeTime', function() {
    $("input[name=custom_field\\[133\\]").val($('#dateTo').val());
  });
</script>
{/literal} 