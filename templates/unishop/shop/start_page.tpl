<div class="slider_home"></div>
<div class="gray_section front-section">
  <div class="title_block_inside"><h3 class="h3title_front">Ми вважаємо, що їжа повинна бути смачною та вишуканою, 
  викликати у вас незабутні емоції і відчуття.</h3></div>
  <p>Саме таку їжу та напої ми обираємо для вас в Еліт Клаб.</p>
  <p>Від наших класичних джемів та конфітюрів до спецій, сирів та випічки.
  І все це у вражаючому упакуванні.</p>

  <div class="categories_front content__container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-20">
        <div class="front_category">
          <div class="front_category_inside">
            <h5>Алкоголь</h5><a href="/Lviv_alkogol-134"><img src="/uploads/shop/products/large/alcogol.png" /></a>
            <a class="go_to_catalog" href="/Lviv_alkogol-134">Перейти в каталог</a>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-20">
        <div class="front_category">
          <div class="front_category_inside">
            <h5>Делікатеси</h5><a href="/Lviv_delikatesi-105"><img src="/uploads/shop/products/large/delicates.png" /></a>
            <a class="go_to_catalog" href="/Lviv_delikatesi-105">Перейти в каталог</a>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-20">
        <div class="front_category">
          <div class="front_category_inside">
            <h5>Подарункові набори</h5><a href="/Lviv_podarunkovi-sertifikati-97"><img src="/uploads/shop/products/large/nabory.png" /></a>
            <a class="go_to_catalog" href="/Lviv_podarunkovi-sertifikati-97">Перейти в каталог</a>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-20">
        <div class="front_category">
          <div class="front_category_inside">
            <h5>Тютюнові вироби</h5><a href="/Lviv_tiutiunovi-virobi-99"><img src="/uploads/shop/products/large/tabak.png" /></a>
            <a class="go_to_catalog" href="/Lviv_tiutiunovi-virobi-99">Перейти в каталог</a>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-20">
        <div class="front_category">
          <div class="front_category_inside">
            <h5>Упакування</h5><a href="/Lviv_upakuvannia-104"><img src="/uploads/shop/products/large/upak.png" /></a>
            <a class="go_to_catalog" href="/Lviv_upakuvannia-104">Перейти в каталог</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  

<div class="start-page">
	<div class="row">
  <div class="content__container">
    {widget('special_products')}
    {widget('products_hits')}
</div>
</div>
</div>

<div class="front_video">
  <div class="row">
    <div class="content__container">
      <div class="col-md-6 col-lg-6 col-sm-6">
        <div class="resp-container">
          <iframe src="https://www.youtube.com/embed/5-2JGLCE42g?rel=0" frameborder="0" allow="autoplay; encrypted-media" rel="0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-md-6 col-lg-6 col-sm-6">
        <h4>Дізнайтесь нашу історію</h4>
        <p>Elite Club - це клуб поціновувачів вишуканих напоїв та елітної гастрономії з усього світу.
У нас ви знайдете понад 5000 найменувань товарів власного імпорту з Італії, Іспанії, Франції.<br><br>
Нашою місією є відкривати клієнтам світ вишуканої їжі та напоїв, дивувати їхні смаки та допомагати в створенні чудової та незабутньої атмосфери за будь-якої нагоди: чи то святкування важливої події чи звичайна вечеря в колі близьких людей.<br>
Їжа - це наша велика пристрасть і ми щодня працюємо над тим, щоб надавати нашим клієнтам найширший асортимент товарів за найвигіднішими цінами. Всі товари сертифіковані та відповідають найвищим вимогам щодо якості.</p>
      </div>
    </div>
  </div>
</div>

<div class="start-page">
  <div style="display: none;" id="popular_products">
  {widget('popular_products')}
  </div>

  



  <div class="content__container">
      <div class="row">
        <div class="title_block_inside"><h3 class="h3title_front">Блог</h3></div>
        <div>{widget('latest_news')}</div>
      </div>
    </div>
  </div>

<div class="our_shops">
  <div class="content__container">
      <div class="row">
        <div class="title_block_inside"><h3 class="h3title_front">Де розташовані наші магазини</h3></div>
        <div class="block_adress highlight">
          <div class="block_adress_header"><span class="span1"></span>Львів<span class="span2"></span></div>
          <div class="block_adress_content">
            <div class="row">
              <div class="col-lg-6">
                <div>м. Львів, вул. Саксаганського, 20<br><a href="https://goo.gl/maps/fWpz783A91S2" target="_blank">Відкрити в Google Maps</a></div>
                (067) 341 24 60<br>
                з 10-00 до 22-00
              </div>
              <div class="col-lg-6">
                <div>м. Львів, вул. Княгині Ольги, 98 а<br><a href="https://goo.gl/maps/vdsgVDahtgJ2" target="_blank">Відкрити в Google Maps</a></div>
                (067) 343 69 21<br>
                з 10-00 до 22-00
              </div>
            </div>
          </div>
        </div>
        <div class="block_adress">
          <div class="block_adress_header"><span class="span1"></span>Київ<span class="span2"></span></div>
          <div class="block_adress_content">
            <div class="row">
              <div class="col-lg-6">
                <div>м. Київ, пр. Героїв Сталінграда, 24<br><a href="https://goo.gl/maps/CUfgEJc5qGo" target="_blank">Відкрити в Google Maps</a></div>
                (067) 340 76 22<br>
                з 10-00 до 23-00
              </div>
            </div>
          </div>
        </div>
        <div class="block_adress">
          <div class="block_adress_header"><span class="span1"></span>Одеса<span class="span2"></span></div>
          <div class="block_adress_content">
            <div class="row">
              <div class="col-lg-6">
                <div>м. Одеса, вул. Маршала Говорова, 10 б<br><a href="https://goo.gl/maps/Q3CLpKoXypw" target="_blank">Відкрити в Google Maps</a></div>
                (099) 777 45 81<br>
                (067) 777 45 81<br>
                з 09-00 до 21-00
              </div>
              <div class="col-lg-6">
                <div>м. Одеса, пр. Академіка Глушко, 14/5<br><a href="https://goo.gl/maps/cGqgJNSTfsp" target="_blank">Відкрити в Google Maps</a></div>
                (068) 517 14 51<br>
                <div>м. Одеса, «7 кілометр», вул. Базова, 20<br><a href="https://goo.gl/maps/6Prko9oY4Zw" target="_blank">Відкрити в Google Maps</a></div>
                з 08-00 до 16-00<br>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

{literal}
<script>
$( ".block_adress_header" ).click(function() {
  $(this).parent(".block_adress").toggleClass( "highlight" );
});
</script>
{/literal}




<div class="subscribe">
  <div class="row">
    <div class="content__container">
      <div class="text_title">Залиште свою електронну адресу, <br>щоб отримувати наші ексклюзивні пропозиції</div>
      <div>
        <form>
          <input type="text" name="name" placeholder="Ваше ім'я">
          <input type="email" name="mail" placeholder="Ваш e-mail">
          <button>Підписатись</button>
        </form>
      </div>
    </div>
  </div>
</div>
