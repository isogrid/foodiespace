<div class="content" style="max-width: 70%; margin: 0 auto;">
  <div class="content__container">

    <!-- Post title -->
    <div class="content__header" style="margin: 0 -25px; text-align: center;">
      
        <div class="title_block_inside" style="text-align: center;"><h3 class="h3title_front" style="text-align: center;  padding-top: 30px !important;">{$page.title}</h3></div>

        <img style="width: 100%;" src='{$page["field_list_image"]}'>
    </div>

    <div style="border-bottom: 1px solid #eee; padding-bottom: 12px; margin-bottom: 12px;">
    Опубліковано: {tpl_locale_date('d F Y', $page.publish_date)}
    </div>

      <!-- Post description -->
      <div class="content__row">
        <div class="typo">{$page.full_text}</div>
      </div>

      <!-- Comments -->
      {if $page.comments_status == 1}
        <div class="content__row">
          <section class="frame-content">
            <div class="frame-content__header">
              <h2 class="frame-content__title">{tlang('Comments')}</h2>
            </div>
            <div class="frame-content__inner"
                 data-comments>
              {tpl_load_comments()}
            </div>
          </section>
        </div>
      {/if}

    </div><!-- /.content__container -->
  </div><!-- /.content -->