<div class="content">
  <div class="content__container">

      <!-- Post description -->
      <div class="content__row">
        <div class="typo">{$page.full_text}</div>
      </div>

    </div><!-- /.content__container -->
  </div><!-- /.content -->