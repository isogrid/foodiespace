! function(t) {
    t.initInputMask = function() {
        t("[data-inputmask-phone]").inputmask({
            mask: "+38 (099) 999-99-99",
            clearMaskOnLostFocus: !1
        })
    }, t("[data-modal-submit]").on("click", function() {
        return Inputmask.isValid(t("[data-inputmask-phone]").val(), {
            alias: "+38 (099) 999-99-99"
        }) ? (t("[data-inputmask-phone]").removeClass("form-control--error"), !0) : (t("[data-inputmask-phone]").addClass("form-control--error"), !1)
    }), t("[data-phone-validate]").on("click", function() {
        return Inputmask.isValid(t("[data-inputmask-phone]").val(), {
            alias: "+38 (099) 999-99-99"
        }) ? (t("[data-inputmask-phone]").removeClass("form-control--error"), !0) : (t("[data-inputmask-phone]").addClass("form-control--error"), !1)
    }), t.initInputMask()
}(jQuery),
    function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : "object" == typeof exports ? module.exports = t($ || require("jquery")) : t(jQuery)
    }(function(t) {
        "use strict";

        function e(e, a) {
            this.element = e, this.options = t.extend({}, n, a);
            var o = this.options.locale;
            void 0 !== this.options.locales[o] && t.extend(this.options, this.options.locales[o]), this.init()
        }

        function a(e) {
            if (!t(e.target).parents().hasClass("jq-selectbox") && "OPTION" != e.target.nodeName && t("div.jq-selectbox.opened").length) {
                var a = t("div.jq-selectbox.opened"),
                    n = t("div.jq-selectbox__search input", a),
                    r = t("div.jq-selectbox__dropdown", a);
                a.find("select").data("_" + o).options.onSelectClosed.call(a), n.length && n.val("").keyup(), r.hide().find("li.sel").addClass("selected"), a.removeClass("focused opened dropup dropdown")
            }
        }
        var o = "styler",
            n = {
                idSuffix: "-styler",
                filePlaceholder: "Файл не выбран",
                fileBrowse: "Обзор...",
                fileNumber: "Выбрано файлов: %s",
                selectPlaceholder: "Выберите...",
                selectSearch: !1,
                selectSearchLimit: 10,
                selectSearchNotFound: "Совпадений не найдено",
                selectSearchPlaceholder: "Поиск...",
                selectVisibleOptions: 0,
                selectSmartPositioning: !0,
                locale: "ru",
                locales: {
                    en: {
                        filePlaceholder: "No file selected",
                        fileBrowse: "Browse...",
                        fileNumber: "Selected files: %s",
                        selectPlaceholder: "Select...",
                        selectSearchNotFound: "No matches found",
                        selectSearchPlaceholder: "Search..."
                    }
                },
                onSelectOpened: function() {},
                onSelectClosed: function() {},
                onFormStyled: function() {}
            };
        e.prototype = {
            init: function() {
                function e() {
                    void 0 !== o.attr("id") && "" !== o.attr("id") && (this.id = o.attr("id") + n.idSuffix), this.title = o.attr("title"), this.classes = o.attr("class"), this.data = o.data()
                }
                var o = t(this.element),
                    n = this.options,
                    r = !(!navigator.userAgent.match(/(iPad|iPhone|iPod)/i) || navigator.userAgent.match(/(Windows\sPhone)/i)),
                    i = !(!navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/(Windows\sPhone)/i));
                if (o.is(":checkbox")) {
                    var l = function() {
                        var a = new e,
                            n = t('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>').attr({
                                id: a.id,
                                title: a.title
                            }).addClass(a.classes).data(a.data);
                        o.after(n).prependTo(n), o.is(":checked") && n.addClass("checked"), o.is(":disabled") && n.addClass("disabled"), n.click(function(t) {
                            t.preventDefault(), o.triggerHandler("click"), n.is(".disabled") || (o.is(":checked") ? (o.prop("checked", !1), n.removeClass("checked")) : (o.prop("checked", !0), n.addClass("checked")), o.focus().change())
                        }), o.closest("label").add('label[for="' + o.attr("id") + '"]').on("click.styler", function(e) {
                            t(e.target).is("a") || t(e.target).closest(n).length || (n.triggerHandler("click"), e.preventDefault())
                        }), o.on("change.styler", function() {
                            o.is(":checked") ? n.addClass("checked") : n.removeClass("checked")
                        }).on("keydown.styler", function(t) {
                            32 == t.which && n.click()
                        }).on("focus.styler", function() {
                            n.is(".disabled") || n.addClass("focused")
                        }).on("blur.styler", function() {
                            n.removeClass("focused")
                        })
                    };
                    l(), o.on("refresh", function() {
                        o.closest("label").add('label[for="' + o.attr("id") + '"]').off(".styler"), o.off(".styler").parent().before(o).remove(), l()
                    })
                } else if (o.is(":radio")) {
                    var s = function() {
                        var a = new e,
                            n = t('<div class="jq-radio"><div class="jq-radio__div"></div></div>').attr({
                                id: a.id,
                                title: a.title
                            }).addClass(a.classes).data(a.data);
                        o.after(n).prependTo(n), o.is(":checked") && n.addClass("checked"), o.is(":disabled") && n.addClass("disabled"), t.fn.commonParents = function() {
                            var e = this;
                            return e.first().parents().filter(function() {
                                return t(this).find(e).length === e.length
                            })
                        }, t.fn.commonParent = function() {
                            return t(this).commonParents().first()
                        }, n.click(function(e) {
                            if (e.preventDefault(), o.triggerHandler("click"), !n.is(".disabled")) {
                                var a = t('input[name="' + o.attr("name") + '"]');
                                a.commonParent().find(a).prop("checked", !1).parent().removeClass("checked"), o.prop("checked", !0).parent().addClass("checked"), o.focus().change()
                            }
                        }), o.closest("label").add('label[for="' + o.attr("id") + '"]').on("click.styler", function(e) {
                            t(e.target).is("a") || t(e.target).closest(n).length || (n.triggerHandler("click"), e.preventDefault())
                        }), o.on("change.styler", function() {
                            o.parent().addClass("checked")
                        }).on("focus.styler", function() {
                            n.is(".disabled") || n.addClass("focused")
                        }).on("blur.styler", function() {
                            n.removeClass("focused")
                        })
                    };
                    s(), o.on("refresh", function() {
                        o.closest("label").add('label[for="' + o.attr("id") + '"]').off(".styler"), o.off(".styler").parent().before(o).remove(), s()
                    })
                } else if (o.is(":file")) {
                    var c = function() {
                        var a = new e,
                            r = o.data("placeholder");
                        void 0 === r && (r = n.filePlaceholder);
                        var i = o.data("browse");
                        void 0 !== i && "" !== i || (i = n.fileBrowse);
                        var l = t('<div class="jq-file"><div class="jq-file__name">' + r + '</div><div class="jq-file__browse">' + i + "</div></div>").attr({
                            id: a.id,
                            title: a.title
                        }).addClass(a.classes).data(a.data);
                        o.after(l).appendTo(l), o.is(":disabled") && l.addClass("disabled");
                        var s = o.val(),
                            c = t("div.jq-file__name", l);
                        s && c.text(s.replace(/.+[\\\/]/, "")), o.on("change.styler", function() {
                            var t = o.val();
                            if (o.is("[multiple]")) {
                                t = "";
                                var e = o[0].files.length;
                                if (e > 0) {
                                    var a = o.data("number");
                                    void 0 === a && (a = n.fileNumber), t = a = a.replace("%s", e)
                                }
                            }
                            c.text(t.replace(/.+[\\\/]/, "")), "" === t ? (c.text(r), l.removeClass("changed")) : l.addClass("changed")
                        }).on("focus.styler", function() {
                            l.addClass("focused")
                        }).on("blur.styler", function() {
                            l.removeClass("focused")
                        }).on("click.styler", function() {
                            l.removeClass("focused")
                        })
                    };
                    c(), o.on("refresh", function() {
                        o.off(".styler").parent().before(o).remove(), c()
                    })
                } else if (o.is('input[type="number"]')) {
                    var d = function() {
                        var a = new e,
                            n = t('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>').attr({
                                id: a.id,
                                title: a.title
                            }).addClass(a.classes).data(a.data);
                        o.after(n).prependTo(n).wrap('<div class="jq-number__field"></div>'), o.is(":disabled") && n.addClass("disabled");
                        var r, i, l, s = null,
                            c = null;
                        void 0 !== o.attr("min") && (r = o.attr("min")), void 0 !== o.attr("max") && (i = o.attr("max")), l = void 0 !== o.attr("step") && t.isNumeric(o.attr("step")) ? Number(o.attr("step")) : Number(1);
                        var d = function(e) {
                            var a, n = o.val();
                            t.isNumeric(n) || (n = 0, o.val("0")), e.is(".minus") ? a = Number(n) - l : e.is(".plus") && (a = Number(n) + l);
                            var s = (l.toString().split(".")[1] || []).length;
                            if (s > 0) {
                                for (var c = "1"; c.length <= s;) c += "0";
                                a = Math.round(a * c) / c
                            }
                            t.isNumeric(r) && t.isNumeric(i) ? a >= r && a <= i && o.val(a) : t.isNumeric(r) && !t.isNumeric(i) ? a >= r && o.val(a) : !t.isNumeric(r) && t.isNumeric(i) ? a <= i && o.val(a) : o.val(a)
                        };
                        n.is(".disabled") || (n.on("mousedown", "div.jq-number__spin", function() {
                            var e = t(this);
                            d(e), s = setTimeout(function() {
                                c = setInterval(function() {
                                    d(e)
                                }, 40)
                            }, 350)
                        }).on("mouseup mouseout", "div.jq-number__spin", function() {
                            clearTimeout(s), clearInterval(c)
                        }).on("mouseup", "div.jq-number__spin", function() {
                            o.change().trigger("input")
                        }), o.on("focus.styler", function() {
                            n.addClass("focused")
                        }).on("blur.styler", function() {
                            n.removeClass("focused")
                        }))
                    };
                    d(), o.on("refresh", function() {
                        o.off(".styler").closest(".jq-number").before(o).remove(), d()
                    })
                } else if (o.is("select")) {
                    var u = function() {
                        function l(t) {
                            var e = t.prop("scrollHeight") - t.outerHeight(),
                                a = null,
                                o = null;
                            t.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll", function(n) {
                                a = n.originalEvent.detail < 0 || n.originalEvent.wheelDelta > 0 ? 1 : -1, ((o = t.scrollTop()) >= e && a < 0 || o <= 0 && a > 0) && (n.stopPropagation(), n.preventDefault())
                            })
                        }

                        function s() {
                            for (var t = 0; t < c.length; t++) {
                                var e = c.eq(t),
                                    a = "",
                                    o = "",
                                    r = "",
                                    i = "",
                                    l = "",
                                    s = "",
                                    u = "",
                                    f = "",
                                    p = "";
                                e.prop("selected") && (o = "selected sel"), e.is(":disabled") && (o = "disabled"), e.is(":selected:disabled") && (o = "selected sel disabled"), void 0 !== e.attr("id") && "" !== e.attr("id") && (i = ' id="' + e.attr("id") + n.idSuffix + '"'), void 0 !== e.attr("title") && "" !== c.attr("title") && (l = ' title="' + e.attr("title") + '"'), void 0 !== e.attr("class") && (u = " " + e.attr("class"), p = ' data-jqfs-class="' + e.attr("class") + '"');
                                var h = e.data();
                                for (var m in h) "" !== h[m] && (s += " data-" + m + '="' + h[m] + '"');
                                o + u !== "" && (r = ' class="' + o + u + '"'), a = "<li" + p + s + r + l + i + ">" + e.html() + "</li>", e.parent().is("optgroup") && (void 0 !== e.parent().attr("class") && (f = " " + e.parent().attr("class")), a = "<li" + p + s + ' class="' + o + u + " option" + f + '"' + l + i + ">" + e.html() + "</li>", e.is(":first-child") && (a = '<li class="optgroup' + f + '">' + e.parent().attr("label") + "</li>" + a)), d += a
                            }
                        }
                        var c = t("option", o),
                            d = "";
                        if (o.is("[multiple]")) {
                            if (i || r) return;
                            ! function() {
                                var a = new e,
                                    n = t('<div class="jq-select-multiple jqselect"></div>').attr({
                                        id: a.id,
                                        title: a.title
                                    }).addClass(a.classes).data(a.data);
                                o.after(n), s(), n.append("<ul>" + d + "</ul>");
                                var r = t("ul", n),
                                    i = t("li", n),
                                    u = o.attr("size"),
                                    f = r.outerHeight(),
                                    p = i.outerHeight();
                                void 0 !== u && u > 0 ? r.css({
                                    height: p * u
                                }) : r.css({
                                    height: 4 * p
                                }), f > n.height() && (r.css("overflowY", "scroll"), l(r), i.filter(".selected").length && r.scrollTop(r.scrollTop() + i.filter(".selected").position().top)), o.prependTo(n), o.is(":disabled") ? (n.addClass("disabled"), c.each(function() {
                                    t(this).is(":selected") && i.eq(t(this).index()).addClass("selected")
                                })) : (i.filter(":not(.disabled):not(.optgroup)").click(function(e) {
                                    o.focus();
                                    var a = t(this);
                                    if (e.ctrlKey || e.metaKey || a.addClass("selected"), e.shiftKey || a.addClass("first"), e.ctrlKey || e.metaKey || e.shiftKey || a.siblings().removeClass("selected first"), (e.ctrlKey || e.metaKey) && (a.is(".selected") ? a.removeClass("selected first") : a.addClass("selected first"), a.siblings().removeClass("first")), e.shiftKey) {
                                        var n = !1,
                                            r = !1;
                                        a.siblings().removeClass("selected").siblings(".first").addClass("selected"), a.prevAll().each(function() {
                                            t(this).is(".first") && (n = !0)
                                        }), a.nextAll().each(function() {
                                            t(this).is(".first") && (r = !0)
                                        }), n && a.prevAll().each(function() {
                                            if (t(this).is(".selected")) return !1;
                                            t(this).not(".disabled, .optgroup").addClass("selected")
                                        }), r && a.nextAll().each(function() {
                                            if (t(this).is(".selected")) return !1;
                                            t(this).not(".disabled, .optgroup").addClass("selected")
                                        }), 1 == i.filter(".selected").length && a.addClass("first")
                                    }
                                    c.prop("selected", !1), i.filter(".selected").each(function() {
                                        var e = t(this),
                                            a = e.index();
                                        e.is(".option") && (a -= e.prevAll(".optgroup").length), c.eq(a).prop("selected", !0)
                                    }), o.change()
                                }), c.each(function(e) {
                                    t(this).data("optionIndex", e)
                                }), o.on("change.styler", function() {
                                    i.removeClass("selected");
                                    var e = [];
                                    c.filter(":selected").each(function() {
                                        e.push(t(this).data("optionIndex"))
                                    }), i.not(".optgroup").filter(function(a) {
                                        return t.inArray(a, e) > -1
                                    }).addClass("selected")
                                }).on("focus.styler", function() {
                                    n.addClass("focused")
                                }).on("blur.styler", function() {
                                    n.removeClass("focused")
                                }), f > n.height() && o.on("keydown.styler", function(t) {
                                    38 != t.which && 37 != t.which && 33 != t.which || r.scrollTop(r.scrollTop() + i.filter(".selected").position().top - p), 40 != t.which && 39 != t.which && 34 != t.which || r.scrollTop(r.scrollTop() + i.filter(".selected:last").position().top - r.innerHeight() + 2 * p)
                                }))
                            }()
                        } else ! function() {
                            var i = new e,
                                u = "",
                                f = o.data("placeholder"),
                                p = o.data("search"),
                                h = o.data("search-limit"),
                                m = o.data("search-not-found"),
                                v = o.data("search-placeholder"),
                                g = o.data("smart-positioning");
                            void 0 === f && (f = n.selectPlaceholder), void 0 !== p && "" !== p || (p = n.selectSearch), void 0 !== h && "" !== h || (h = n.selectSearchLimit), void 0 !== m && "" !== m || (m = n.selectSearchNotFound), void 0 === v && (v = n.selectSearchPlaceholder), void 0 !== g && "" !== g || (g = n.selectSmartPositioning);
                            var b = t('<div class="jq-selectbox jqselect"><div class="jq-selectbox__select"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>').attr({
                                id: i.id,
                                title: i.title
                            }).addClass(i.classes).data(i.data);
                            o.after(b).prependTo(b);
                            var y = b.css("z-index");
                            y = y > 0 ? y : 1;
                            var w = t("div.jq-selectbox__select", b),
                                x = t("div.jq-selectbox__select-text", b),
                                Y = c.filter(":selected");
                            s(), p && (u = '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + v + '"></div><div class="jq-selectbox__not-found">' + m + "</div>");
                            var S = t('<div class="jq-selectbox__dropdown">' + u + "<ul>" + d + "</ul></div>");
                            b.append(S);
                            var X = t("ul", S),
                                T = t("li", S),
                                $ = t("input", S),
                                C = t("div.jq-selectbox__not-found", S).hide();
                            T.length < h && $.parent().hide(), "" === c.first().text() && c.first().is(":selected") && !1 !== f ? x.text(f).addClass("placeholder") : x.text(Y.text());
                            var k = 0,
                                W = 0;
                            if (T.css({
                                    display: "inline-block"
                                }), T.each(function() {
                                    var e = t(this);
                                    e.innerWidth() > k && (k = e.innerWidth(), W = e.width())
                                }), T.css({
                                    display: ""
                                }), x.is(".placeholder") && x.width() > k) x.width(x.width());
                            else {
                                var R = b.clone().appendTo("body").width("auto"),
                                    L = R.outerWidth();
                                R.remove(), L == b.outerWidth() && x.width(W)
                            }
                            k > b.width() && S.width(k), "" === c.first().text() && "" !== o.data("placeholder") && T.first().hide();
                            var A = b.outerHeight(!0),
                                j = $.parent().outerHeight(!0) || 0,
                                H = X.css("max-height"),
                                E = T.filter(".selected");
                            if (E.length < 1 && T.first().addClass("selected sel"), void 0 === T.data("li-height")) {
                                var D = T.outerHeight();
                                !1 !== f && (D = T.eq(1).outerHeight()), T.data("li-height", D)
                            }
                            var M = S.css("top");
                            if ("auto" == S.css("left") && S.css({
                                    left: 0
                                }), "auto" == S.css("top") && (S.css({
                                    top: A
                                }), M = A), S.hide(), E.length && (c.first().text() != Y.text() && b.addClass("changed"), b.data("jqfs-class", E.data("jqfs-class")), b.addClass(E.data("jqfs-class"))), o.is(":disabled")) return b.addClass("disabled"), !1;
                            w.click(function() {
                                if (t("div.jq-selectbox").filter(".opened").length && n.onSelectClosed.call(t("div.jq-selectbox").filter(".opened")), o.focus(), !r) {
                                    var e = t(window),
                                        a = T.data("li-height"),
                                        i = b.offset().top,
                                        s = e.height() - A - (i - e.scrollTop()),
                                        d = o.data("visible-options");
                                    void 0 !== d && "" !== d || (d = n.selectVisibleOptions);
                                    var u = 5 * a,
                                        f = a * d;
                                    d > 0 && d < 6 && (u = f), 0 === d && (f = "auto");
                                    var p = function() {
                                        S.height("auto").css({
                                            bottom: "auto",
                                            top: M
                                        });
                                        var t = function() {
                                            X.css("max-height", Math.floor((s - 20 - j) / a) * a)
                                        };
                                        t(), X.css("max-height", f), "none" != H && X.css("max-height", H), s < S.outerHeight() + 20 && t()
                                    };
                                    !0 === g || 1 === g ? s > u + j + 20 ? (p(), b.removeClass("dropup").addClass("dropdown")) : (function() {
                                        S.height("auto").css({
                                            top: "auto",
                                            bottom: M
                                        });
                                        var t = function() {
                                            X.css("max-height", Math.floor((i - e.scrollTop() - 20 - j) / a) * a)
                                        };
                                        t(), X.css("max-height", f), "none" != H && X.css("max-height", H), i - e.scrollTop() - 20 < S.outerHeight() + 20 && t()
                                    }(), b.removeClass("dropdown").addClass("dropup")) : !1 === g || 0 === g ? s > u + j + 20 && (p(), b.removeClass("dropup").addClass("dropdown")) : (S.height("auto").css({
                                        bottom: "auto",
                                        top: M
                                    }), X.css("max-height", f), "none" != H && X.css("max-height", H)), b.offset().left + S.outerWidth() > e.width() && S.css({
                                        left: "auto",
                                        right: 0
                                    }), t("div.jqselect").css({
                                        zIndex: y - 1
                                    }).removeClass("opened"), b.css({
                                        zIndex: y
                                    }), S.is(":hidden") ? (t("div.jq-selectbox__dropdown:visible").hide(), S.show(), b.addClass("opened focused"), n.onSelectOpened.call(b)) : (S.hide(), b.removeClass("opened dropup dropdown"), t("div.jq-selectbox").filter(".opened").length && n.onSelectClosed.call(b)), $.length && ($.val("").keyup(), C.hide(), $.keyup(function() {
                                        var e = t(this).val();
                                        T.each(function() {
                                            t(this).html().match(new RegExp(".*?" + e + ".*?", "i")) ? t(this).show() : t(this).hide()
                                        }), "" === c.first().text() && "" !== o.data("placeholder") && T.first().hide(), T.filter(":visible").length < 1 ? C.show() : C.hide()
                                    })), T.filter(".selected").length && ("" === o.val() ? X.scrollTop(0) : (X.innerHeight() / a % 2 != 0 && (a /= 2), X.scrollTop(X.scrollTop() + T.filter(".selected").position().top - X.innerHeight() / 2 + a))), l(X)
                                }
                            }), T.hover(function() {
                                t(this).siblings().removeClass("selected")
                            });
                            var _ = T.filter(".selected").text();
                            T.filter(":not(.disabled):not(.optgroup)").click(function() {
                                o.focus();
                                var e = t(this),
                                    a = e.text();
                                if (!e.is(".selected")) {
                                    var r = e.index();
                                    r -= e.prevAll(".optgroup").length, e.addClass("selected sel").siblings().removeClass("selected sel"), c.prop("selected", !1).eq(r).prop("selected", !0), _ = a, x.text(a), b.data("jqfs-class") && b.removeClass(b.data("jqfs-class")), b.data("jqfs-class", e.data("jqfs-class")), b.addClass(e.data("jqfs-class")), o.change()
                                }
                                S.hide(), b.removeClass("opened dropup dropdown"), n.onSelectClosed.call(b)
                            }), S.mouseout(function() {
                                t("li.sel", S).addClass("selected")
                            }), o.on("change.styler", function() {
                                x.text(c.filter(":selected").text()).removeClass("placeholder"), T.removeClass("selected sel").not(".optgroup").eq(o[0].selectedIndex).addClass("selected sel"), c.first().text() != T.filter(".selected").text() ? b.addClass("changed") : b.removeClass("changed")
                            }).on("focus.styler", function() {
                                b.addClass("focused"), t("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide()
                            }).on("blur.styler", function() {
                                b.removeClass("focused")
                            }).on("keydown.styler keyup.styler", function(t) {
                                var e = T.data("li-height");
                                "" === o.val() ? x.text(f).addClass("placeholder") : x.text(c.filter(":selected").text()), T.removeClass("selected sel").not(".optgroup").eq(o[0].selectedIndex).addClass("selected sel"), 38 != t.which && 37 != t.which && 33 != t.which && 36 != t.which || ("" === o.val() ? X.scrollTop(0) : X.scrollTop(X.scrollTop() + T.filter(".selected").position().top)), 40 != t.which && 39 != t.which && 34 != t.which && 35 != t.which || X.scrollTop(X.scrollTop() + T.filter(".selected").position().top - X.innerHeight() + e), 13 == t.which && (t.preventDefault(), S.hide(), b.removeClass("opened dropup dropdown"), n.onSelectClosed.call(b))
                            }).on("keydown.styler", function(t) {
                                32 == t.which && (t.preventDefault(), w.click())
                            }), a.registered || (t(document).on("click", a), a.registered = !0)
                        }()
                    };
                    u(), o.on("refresh", function() {
                        o.off(".styler").parent().before(o).remove(), u()
                    })
                } else o.is(":reset") && o.on("click", function() {
                    setTimeout(function() {
                        o.closest("form").find("input, select").trigger("refresh")
                    }, 1)
                })
            },
            destroy: function() {
                var e = t(this.element);
                e.is(":checkbox") || e.is(":radio") ? (e.removeData("_" + o).off(".styler refresh").removeAttr("style").parent().before(e).remove(), e.closest("label").add('label[for="' + e.attr("id") + '"]').off(".styler")) : e.is('input[type="number"]') ? e.removeData("_" + o).off(".styler refresh").closest(".jq-number").before(e).remove() : (e.is(":file") || e.is("select")) && e.removeData("_" + o).off(".styler refresh").removeAttr("style").parent().before(e).remove()
            }
        }, t.fn[o] = function(a) {
            var n = arguments;
            if (void 0 === a || "object" == typeof a) return this.each(function() {
                t.data(this, "_" + o) || t.data(this, "_" + o, new e(this, a))
            }).promise().done(function() {
                var e = t(this[0]).data("_" + o);
                e && e.options.onFormStyled.call()
            }), this;
            if ("string" == typeof a && "_" !== a[0] && "init" !== a) {
                var r;
                return this.each(function() {
                    var i = t.data(this, "_" + o);
                    i instanceof e && "function" == typeof i[a] && (r = i[a].apply(i, Array.prototype.slice.call(n, 1)))
                }), void 0 !== r ? r : this
            }
        }, a.registered = !1
    }),
    function(t) {
        function e(t, e) {
            if (!(t.originalEvent.touches.length > 1)) {
                t.preventDefault();
                var a = t.originalEvent.changedTouches[0],
                    o = document.createEvent("MouseEvents");
                o.initMouseEvent(e, !0, !0, window, 1, a.screenX, a.screenY, a.clientX, a.clientY, !1, !1, !1, !1, 0, null), t.target.dispatchEvent(o)
            }
        }
        if (t.support.touch = "ontouchend" in document, t.support.touch) {
            var a, o = t.ui.mouse.prototype,
                n = o._mouseInit,
                r = o._mouseDestroy;
            o._touchStart = function(t) {
                !a && this._mouseCapture(t.originalEvent.changedTouches[0]) && (a = !0, this._touchMoved = !1, e(t, "mouseover"), e(t, "mousemove"), e(t, "mousedown"))
            }, o._touchMove = function(t) {
                a && (this._touchMoved = !0, e(t, "mousemove"))
            }, o._touchEnd = function(t) {
                a && (e(t, "mouseup"), e(t, "mouseout"), this._touchMoved || e(t, "click"), a = !1)
            }, o._mouseInit = function() {
                var e = this;
                e.element.bind({
                    touchstart: t.proxy(e, "_touchStart"),
                    touchmove: t.proxy(e, "_touchMove"),
                    touchend: t.proxy(e, "_touchEnd")
                }), n.call(e)
            }, o._mouseDestroy = function() {
                var e = this;
                e.element.unbind({
                    touchstart: t.proxy(e, "_touchStart"),
                    touchmove: t.proxy(e, "_touchMove"),
                    touchend: t.proxy(e, "_touchEnd")
                }), r.call(e)
            }
        }
    }(jQuery),
    function(t) {
        t.mlsAjax = {
            preloaderShow: function(t) {
                "frame" == t.type && t.frame.attr("data-loader-frame", "1").append('<i class="spinner-circle"></i>'), "text" == t.type && t.frame.html(t.frame.data("loader"))
            },
            preloaderHide: function() {
                t("[data-loader-frame]").removeAttr("data-loader-frame").find(".spinner-circle").remove()
            },
            loadResponseFrame: function(e, a) {
                var o = t(e).find(a.selector).children();
                t(a).html(o)
            },
            transferData: function(e) {
                t(e).find("[data-ajax-grab]").each(function() {
                    var e = t(this).data("ajax-grab"),
                        a = t(this).html();
                    t('[data-ajax-inject="' + e + '"]').html(a)
                })
            }
        }
    }(jQuery),
    function(t) {
        t.mlsCart = {
            loadSummaryJson: function(e, a) {
                var o = e.attr("data-cart-summary--href"),
                    n = e.closest("[data-cart-summary]"),
                    r = n.attr("data-cart-summary--url"),
                    i = n.attr("data-cart-summary--tpl"),
                    l = t("[data-cart-summary]"),
                    s = t('[data-cart-summary="modal"]'),
                    c = t('[data-cart-summary="page"]');
                t.ajax({
                    url: o,
                    type: e.attr("method") ? e.attr("method") : "get",
                    data: e.serialize(),
                    dataType: "json",
                    beforeSend: function() {
                        t.mlsAjax.preloaderShow({
                            type: "frame",
                            frame: l
                        })
                    },
                    success: function() {
                        c.size() > 0 && s.size() > 0 && (l = s, t.ajax({
                            url: r,
                            data: {
                                deliveryMethodId: t("[data-cart-delivery--input]:checked").val()
                            },
                            type: "POST",
                            success: function(e) {
                                t.mlsAjax.loadResponseFrame(e, c)
                            }
                        })), t.ajax({
                            url: r,
                            data: {
                                template: i,
                                deliveryMethodId: t("[data-cart-delivery--input]:checked").val()
                            },
                            type: "POST",
                            success: function(e) {
                                a.loadFrame && t.mlsAjax.loadResponseFrame(e, l), a.transferData && t.mlsAjax.transferData(e), a.toggleAddToCartButton && ("string" != typeof a.variantId && a.variantId.length > 0 ? a.variantId.forEach(function(e) {
                                    t.mshProduct.editCartButtons(e, 0)
                                }) : t.mshProduct.editCartButtons(a.variantId, 0)), a.toggleKitButton && a.kitList.toggleClass("hidden")
                            }
                        })
                    }
                })
            }
        }
    }(jQuery),
    function(t) {
        t.mlsCompare = {
            productsEqualHeight: function() {
                Array.prototype.slice.call(document.querySelectorAll("[data-compare-category]")).forEach(function(e) {
                    var a = Array.prototype.slice.call(e.querySelectorAll("[data-compare-product]")),
                        o = a.map(function(t) {
                            return t.style.height = "auto", t.getBoundingClientRect().height
                        }).reduce(function(t, e) {
                            return Math.max(t, e)
                        });
                    a.forEach(function(t) {
                        t.style.height = o + "px"
                    }), t("[data-compare-category]").removeAttr("data-loader-frame").find(".spinner-circle").remove()
                }.bind(this))
            }
        }
    }(jQuery), $(document).ready(function() {
    $("[data-ellipsis]").each(function() {
        var t = $(this);
        t.dotdotdot({
            watch: "window",
            tolerance: 5,
            height: t.data("ellipsis")
        })
    })
}), screen.width > 768 && $("[data-formstyler]").styler({
    selectSmartPositioning: !1
}),
    function(t) {
        t.mlsMedia = {
            zoomImage: function() {
                var e = t("[data-zoom-image]");
                e.trigger("zoom.destroy"), e.each(function() {
                    var a = t(this),
                        o = a.attr("data-zoom-image"),
                        n = a.siblings("[data-zoom-wrapper]");
                    a.zoom({
                        url: o,
                        target: n,
                        touch: !1,
                        onZoomIn: function() {
                            n.removeClass("hidden")
                        },
                        onZoomOut: function() {
                            n.addClass("hidden")
                        },
                        callback: function() {
                            var o = t(this);
                            a.width() >= o.width() && a.height() >= o.height() && e.trigger("zoom.destroy")
                        }
                    })
                })
            },
            magnificGalley: function(e, a) {
                e = e || 0, (a = a || t("[data-magnific-galley]")).each(function() {
                    var a, o, n = t(this),
                        r = n.find("[data-magnific-galley-main]"),
                        i = n.find("[data-magnific-galley-thumb]"),
                        l = [];
                    i.size() > 0 && (i.each(function() {
                        var e = {
                            src: t(this).attr("href")
                        };
                        l.push(e)
                    }), a = l.splice(0, e), o = l.concat(a)), r.magnificPopup({
                        items: o,
                        type: "image",
                        gallery: {
                            enabled: !0,
                            tCounter: "%curr% of %total%"
                        },
                        overflowY: "hidden",
                        image: {
                            titleSrc: "data-magnific-galley-title"
                        }
                    })
                })
            }
        }
    }(jQuery);
var mlsMegamenu = {
    renderCols: function() {
        ! function() {
            var t = document.querySelectorAll("[data-megamenu-item]");
            Array.prototype.forEach.call(t, function(t) {
                var e = t.querySelector("[data-megamenu-wrap]"),
                    a = t.querySelector("[data-megamenu-coll]"),
                    o = t.querySelectorAll("[data-megamenu-coll-item]"),
                    n = function(t) {
                        return Array.prototype.reduce.call(t, function(t, e) {
                            var a = e.dataset.megamenuCollItem;
                            return a > t ? a : t
                        }, 1)
                    }(o);
                if (!(n <= 1)) {
                    var r = function(t, e) {
                        for (var a = [], o = 2; o <= e; o++) a[o] = t.cloneNode(!1), a[o].dataset.megamenuColl = o;
                        return a
                    }(a, n);
                    ! function(t, e) {
                        Array.prototype.forEach.call(t, function(t) {
                            t.dataset.megamenuCollItem > 1 && e[t.dataset.megamenuCollItem].appendChild(t)
                        })
                    }(o, r), r.forEach(function(t) {
                        e.appendChild(t)
                    })
                }
            })
        }()
    },
    equalHeight: function(t, e) {
        ! function(t, e) {
            var a = t.offsetHeight;
            Array.prototype.forEach.call(e, function(t) {
                t.style.minHeight = a + "px"
            })
        }(t, e)
    }
};
! function(t) {
    t.mlsModal = function(e) {
        t.magnificPopup.close(), t.magnificPopup.open({
            items: {
                src: e.src
            },
            type: e.type || "ajax",
            ajax: {
                settings: {
                    data: e.data
                }
            },
            callbacks: e.callbacks || {
                parseAjax: function(a) {
                    e.transferData && t.mlsAjax.transferData(a.data)
                }
            },
            showCloseBtn: !1,
            modal: !1
        })
    }, t(document).on("click", "[data-modal]", function(e) {
        e.preventDefault();
        var a = t(this);
        t.mlsModal({
            src: a.attr("href"),
            type: a.data("modal-type"),
            data: {
                template: a.data("modal")
            }
        })
    }), t(document).on("click", "[data-modal-close]", function(e) {
        e.preventDefault(), t.magnificPopup.close()
    })
}(jQuery),
    function(t) {
        t.mshProduct = {
            loadCartButton: function(t) {
                var e = t.closest("[data-product-scope]"),
                    a = t.attr("data-product-variant--id"),
                    o = e.find("[data-product-button--form]"),
                    n = Number(t.attr("data-product-variant--in-cart")),
                    r = e.find("[data-product-button--add]"),
                    i = e.find("[data-product-button--quantity]"),
                    l = e.find("[data-product-button--view]");
                o.attr("data-product-button--variant", a), n ? (r.add(i).addClass("hidden"), l.removeClass("hidden")) : (r.add(i).removeClass("hidden"), l.addClass("hidden"))
            },
            editCartButtons: function(e, a) {
                var o = t('[data-product-variant--id="' + e + '"]'),
                    n = t('[data-product-button--variant="' + e + '"]').attr("data-product-button--variant");
                o.size() > 0 ? o.each(function() {
                    var e = t(this).attr("data-product-variant--id");
                    t(this).attr("data-product-variant--in-cart", a), t(this).is(":checked") && e === n && t.mshProduct.loadCartButton(t(this))
                }) : t("[data-product-button--variant=" + e + "]").find("[data-product-button-item]").toggleClass("hidden")
            },
            getVariant: function(t) {
                var e;
                switch (t.attr("data-product-variant")) {
                    case "select":
                        e = t.find("option:checked");
                        break;
                    default:
                        e = t
                }
                return e
            }
        }
    }(jQuery),
    function(t) {
        t.mlsSlider = {
            getCols: function(e) {
                console.log();
                var a = [550, 767, 991, 1199],
                    o = !!e && e.split(",");
                return !!t.isArray(o) && (o.shift(), o.length > 0 && t.map(o, function(t, e) {
                        return {
                            breakpoint: a[e],
                            settings: {
                                slidesToShow: parseInt(t)
                            }
                        }
                    }))
            },
            getFirstCol: function(t) {
                return !!t && t.split(",") ? parseInt(t.split(",")[0]) : 2
            }
        }
    }(jQuery),
    function(t) {
        "use strict";
        t(document).on("click", "[data-slide-btn]", function(e) {
            t(e.currentTarget).closest("[data-slide-container]").find("[data-slide-content]").slideToggle()
        })
    }(jQuery), $.mlsTime = {
    countdown: {
        init: function(t) {
            for (var e = document.querySelectorAll(t.scope), a = function(t, e, a) {
                var o = $.mlsTime.countdown.getTimeLeft(t);
                $.mlsTime.countdown.renderTimeLeft(o, e), a && o.total <= 0 && clearInterval(a)
            }, o = 0; o < e.length; o++) ! function() {
                var n = e[o].getAttribute(t.expireDateAttribute),
                    r = e[o].querySelectorAll(t.item),
                    i = setInterval(function() {
                        a(n, r, i)
                    }, 1e3)
            }()
        },
        getTimeLeft: function(t) {
            function e(t, e) {
                for (var a = t + ""; a.length < e;) a = "0" + a;
                return a
            }
            var a = Date.parse(t) - Date.parse(new Date),
                o = e(Math.floor(a / 1e3 % 60), 2),
                n = e(Math.floor(a / 1e3 / 60 % 60), 2),
                r = e(Math.floor(a / 36e5 % 24), 2);
            return {
                total: a,
                days: Math.floor(a / 864e5),
                hours: r,
                minutes: n,
                seconds: o
            }
        },
        renderTimeLeft: function(t, e) {
            for (var a, o, n = 0; n < e.length; n++) a = Object.keys(e[n].dataset)[0], o = e[n].dataset[a], e[n].innerHTML = t[o]
        }
    }
},
    function(t) {
        t.mlsWishList = {
            moveListValidation: function(e) {
                var a = e.find('input[type="radio"]'),
                    o = e.find("[data-wishlist-new-radio]"),
                    n = e.find("[data-wishlist-new-input]"),
                    r = !1;
                return a.each(function() {
                    t(this).not("[data-wishlist-new-radio]").is(":checked") ? r = !0 : o.is(":checked") && "" != t.trim(n.val()) && (r = !0)
                }), r
            }
        }
    }(jQuery),
    function(t) {
        var e = t("[form]").get(0),
            a = !window.ActiveXObject && "ActiveXObject" in window;
        e && window.HTMLFormElement && e.form instanceof HTMLFormElement && !a || (t.fn.appendField = function(e) {
            if (this.is("form")) {
                !t.isArray(e) && e.name && e.value && (e = [e]);
                var a = this;
                return t.each(e, function(e, o) {
                    t("<input/>").attr("type", "hidden").attr("name", o.name).val(o.value).appendTo(a)
                }), a
            }
        }, t("form[id]").submit(function(e) {
            var a = t(this),
                o = t("[form=" + a.attr("id") + "]").serializeArray();
            a.appendField(o)
        }).each(function() {
            var e = this,
                a = t(e),
                o = t("[form=" + a.attr("id") + "]");
            o.filter("button, input").filter("[type=reset],[type=submit]").click(function() {
                var a = this.type.toLowerCase();
                "reset" === a ? (e.reset(), o.each(function() {
                    this.value = this.defaultValue, this.checked = this.defaultChecked
                }).filter("select").each(function() {
                    t(this).find("option").each(function() {
                        this.selected = this.defaultSelected
                    })
                })) : a.match(/^submit|image$/i) && t(e).appendField({
                        name: this.name,
                        value: this.value
                    }).submit()
            })
        }), "function" != typeof Array.prototype.forEach && (Array.prototype.forEach = function(t, e) {
            if ("number" == typeof this.length && "function" == typeof t && "object" == typeof this)
                for (var a = 0; a < this.length; a++) {
                    if (!(a in this)) return;
                    t.call(e || this, this[a], a, this)
                }
        }))
    }(jQuery), svg4everybody(),
    function(t) {
        var e = {
                scope: "[data-accordion-tabs]",
                item: "[data-accordion-tabs-item]",
                link: "[data-accordion-tabs-link]",
                content: "[data-accordion-tabs-content]"
            },
            a = t(e.scope);
        a.each(function() {
            var a = t(this),
                o = a.find(e.item).first();
            o.find(e.link).addClass("js-active"), o.find(e.content).addClass("js-open"), a.find(".js-init-active").removeClass("js-init-active")
        }), a.on("click", e.link, function(e) {
            e.preventDefault();
            var a = t(e.delegateTarget),
                o = t(this);
            o.hasClass("js-active") || (a.find(".js-open").removeClass("js-open").hide(), o.next().toggleClass("js-open").toggle(), a.find(".js-active").removeClass("js-active"), o.addClass("js-active"), t("[data-slider-slides]").slick("setPosition"))
        })
    }(jQuery), $('[data-slider="bargain"]').each(function() {
    var t = $(this),
        e = t.find("[data-slider-slides]").attr("data-slider-slides");
    t.find("[data-slider-slides]").slick({
        dots: !1,
        arrows: !0,
        infinite: !1,
        adaptiveHeight: !0,
        slidesToShow: $.mlsSlider.getFirstCol(e),
        autoplay: !1,
        autoplaySpeed: 3e3,
        swipeToSlide: !0,
        rows: 1,
        prevArrow: t.find("[data-slider-arrow-left]").removeClass("hidden"),
        nextArrow: t.find("[data-slider-arrow-right]").removeClass("hidden"),
        responsive: $.mlsSlider.getCols(e)
    }).find("[data-slider-slide]").removeClass("hidden")
}), jQuery.mshButtons = {
    addLoader: function(t) {
        setTimeout(function() {
            t.attr("disabled", "disabled").find('[data-button-loader="loader"]').removeClass("hidden")
        }, 0)
    },
    removeLoader: function(t) {
        t.removeAttr("disabled").find('[data-button-loader="loader"]').addClass("hidden")
    }
}, $(document).on("click", '[data-button-loader="button"]', function() {
    $.mshButtons.addLoader($(this))
}), $(document).on("submit", "[data-cart-summary--quantity]", function(t) {
    t.preventDefault(), $.mlsCart.loadSummaryJson($(this), {
        loadFrame: !0,
        transferData: !0,
        toggleAddToCartButton: !1,
        toggleKitButton: !1
    })
}), $(document).on("change", "[data-cart-summary--quantity-field]", function() {
    $(this).closest("[data-cart-summary--quantity]").trigger("submit")
}), $(document).on("click", "[data-cart-summary--delete]", function(t) {
    t.preventDefault();
    var e = $(this).attr("data-cart-summary--item-id"),
        a = $('[data-product-kit--id="' + e + '"]');
    $.mlsCart.loadSummaryJson($(this), {
        loadFrame: !0,
        transferData: !0,
        toggleAddToCartButton: !0,
        toggleKitButton: !0,
        variantId: e,
        kitList: a
    })
}), $(document).on("click", "[data-cart-summary--delete-all]", function(t) {
    t.preventDefault();
    var e = $(this).closest("[data-cart-summary]"),
        a = [],
        o = [];
    e.find("[data-cart-summary--item-id]").each(function() {
        a.push($(this).attr("data-cart-summary--item-id"))
    }), e.find("[data-cart-summary--delete-kit]").each(function() {
        o.push($(this).attr("data-cart-summary--item-id"))
    }), o.forEach(function(t) {
        $('[data-product-kit--id="' + t + '"]').toggleClass("hidden")
    }), $.mlsCart.loadSummaryJson($(this), {
        loadFrame: !0,
        transferData: !0,
        toggleAddToCartButton: !0,
        variantId: a
    })
}), $(document).on("submit", "[data-cart-summary--coupon]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $("[data-cart-summary]");
    $.ajax({
        url: e.attr("action"),
        type: e.attr("method") ? e.attr("method") : "get",
        data: e.serialize(),
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: a
            })
        },
        success: function(t) {
            $.mlsAjax.loadResponseFrame(t, a), $.mlsAjax.transferData(t)
        }
    })
}), $(document).on("change", "[data-cart-delivery--input]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = e.attr("data-cart-delivery--href"),
        o = $("[data-cart-summary]"),
        n = e.attr("data-cart-delivery--payment-href");
    $.ajax({
        url: a,
        type: "POST",
        data: e.serialize(),
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: o
            }), $.mlsAjax.preloaderShow({
                type: "frame",
                frame: e.closest($("[data-cart-delivery]"))
            })
        },
        success: function(t) {
            $.mlsAjax.loadResponseFrame(t, o), $.mlsAjax.transferData(t), $("[data-cart-delivery--spoiler]").addClass("hidden"), e.closest("[data-cart-delivery--item]").find("[data-cart-delivery--spoiler]").removeClass("hidden")
        }
    }), $.ajax({
        url: n,
        type: "POST",
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: e.closest("[data-cart-checkout-form]").find("[data-cart-delivery--payment]")
            })
        },
        success: function(t) {
            "use strict";
            e.closest("[data-cart-checkout-form]").find("[data-cart-delivery--payment]").html(t)
        }
    })
}), $(document).on("submit", "[data-cart-checkout-form]", function() {
    $(this).find("[data-cart-checkout-form-button]").prop("disabled", !0)
}), $(document).on("ready", function() {
    $("#paidForm").attr("target", "_blank")
}), $(document).on("click", "[data-catalog-view-item]", function(t) {
    var e = $(this).attr("data-catalog-view-item");
    t.preventDefault(), document.cookie = "catalog_view=" + e + ";path=/", window.location.reload()
}), $(document).on("change", "[data-catalog-order-select]", function() {
    $("#catalog-form").submit(), $('[form="catalog-form"]').attr("disabled", !0)
}), $(document).on("change", "[data-catalog-perpage-select]", function() {
    $("#catalog-form").submit(), $('[form="catalog-form"]').attr("disabled", !0)
}),
    function() {
        function t() {
            o.classList.add(n), a.classList.add("hidden")
        }
        var e = document.querySelector("[data-catalog-btn]"),
            a = document.querySelector("[data-catalog-btn-overlay]"),
            o = document.querySelector("[data-catalog-btn-menu]"),
            n = "is-hidden";
        if (o) {
            e.addEventListener("click", function() {
                o.classList.toggle(n), a.classList.toggle("hidden")
            }), a.addEventListener("click", function() {
                t()
            });
            var r = null;
            window.addEventListener("resize", function() {
                clearTimeout(r), r = setTimeout(function(e) {
                    window.innerWidth < e && t()
                }.bind(null, 991), 300)
            })
        }
    }(), $(document).on("submit", "[data-comments-form]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = e.closest("[data-comments]"),
        o = '[data-comments-form="' + e.attr("data-comments-form") + '"]',
        n = o + " [data-comments-success]",
        r = o + " [data-comments-error-frame]",
        i = o + " [data-comments-error-list]";
    $.ajax({
        url: e.attr("data-comments-form-url"),
        type: "post",
        data: e.serialize(),
        dataType: "json",
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: a
            })
        },
        success: function(t) {
            "error" == t.answer ? (a.find(n).addClass("hidden"), a.find(i).html(t.validation_errors), a.find(r).removeClass("hidden"), a.find(o + " [data-captcha-img]").html(t.cap_image)) : $.ajax({
                url: e.attr("data-comments-form-list-url"),
                method: "post",
                dataType: "json",
                success: function(t) {
                    a.html(t.comments), a.find(r).addClass("hidden"), a.find(n).removeClass("hidden")
                }
            })
        }
    })
}), $(document).on("click", "[data-comments-reply-link]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = e.closest("[data-comments-post]").find("[data-comments-form-wrapper]"),
        o = e.closest("[data-comments]").find('[data-reply-form] [data-comments-form="reply"]').clone(),
        n = e.closest("[data-comments-post]").attr("data-comments-post");
    o.find("[data-comments-success], [data-comments-error-frame]").addClass("hidden"), o.find("[data-comments-parent]").val(n), a.toggleClass("hidden").html(o)
}), $(document).on("click", "[data-comments-vote-url]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = e.attr("data-comments-vote-url"),
        o = e.closest("[data-comments-post]").attr("data-comments-post"),
        n = e.find("[data-comments-vote-value]");
    $.ajax({
        url: a,
        type: "post",
        data: {
            comid: o
        },
        dataType: "json",
        success: function(t) {
            n.html(t.y_count ? t.y_count : t.n_count)
        }
    })
}),
    function() {
        $.mlsCompare.productsEqualHeight();
        var t = null;
        window.addEventListener("resize", function() {
            clearTimeout(t), t = setTimeout(function() {
                $.mlsCompare.productsEqualHeight()
            }, 500)
        })
    }(), $(document).on("click", '[data-compare-scope="add_to"] [data-compare-add]', function(t) {
    t.preventDefault();
    var e = $(this),
        a = $(this).closest("[data-compare-scope]").find("[data-compare-add], [data-compare-open]");
    $.ajax({
        url: e.attr("data-compare-add"),
        type: "get",
        dataType: "json",
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "text",
                frame: e
            })
        },
        success: function(t) {
            t.success && (a.toggleClass("hidden"), $("[data-compare-total]").html(t.count), t.count > 0 && $("[data-compare-removeclass]").each(function() {
                var t = $(this);
                t.removeClass(t.data("compare-removeclass"))
            }))
        }
    })
}), $.mlsTime.countdown.init({
    scope: "[data-countdown]",
    item: "[data-countdown-item]",
    expireDateAttribute: "data-countdown"
}), $(document).on("submit", "[data-form-ajax]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $('[data-form-ajax="' + e.attr("data-form-ajax") + '"]');
    $.ajax({
        url: e.attr("action"),
        type: e.attr("method") ? e.attr("method") : "get",
        data: e.serialize(),
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: e
            })
        },
        success: function(t) {
            $.mlsAjax.loadResponseFrame(t, a), $.mlsAjax.transferData(t)
        }
    })
}), $(document).on("click", "[data-form-quantity-control]", function(t) {
    t.preventDefault();
    var e = $(this).closest("[data-form-quantity]"),
        a = e.find("[data-form-quantity-field]"),
        o = Number(a.val().replace(",", ".")),
        n = $(this).attr("data-form-quantity-control"),
        r = e.find("[data-form-quantity-step]").attr("data-form-quantity-step"),
        i = !1 !== Boolean(r) ? Number(r.replace(",", ".")) : 1,
        l = e.attr("data-form-quantity-submit");
    "minus" == n && (o = o > i ? o - i : i), "plus" == n && (o = o >= i ? o + i : i), a.val(o), void 0 !== l && $(this).trigger("submit")
}), $("[data-gallery-image]").magnificPopup({
    delegate: "[data-gallery-image-item]",
    type: "image",
    mainClass: "gallery-default",
    gallery: {
        enabled: !0,
        tCounter: "%curr% of %total%"
    },
    overflowY: "hidden",
    image: {
        titleSrc: "data-gallery-image-title"
    }
}),
    function() {
        var t = !0;
        $("#mailer-form").submit(function(e) {
            if (!t) return !1;
            t = !1;
            var a = $(this),
                o = a.serialize(),
                n = a.attr("data-action");
            $.ajax({
                url: n,
                type: "POST",
                data: o,
                beforeSend: function() {
                    a.find("[data-mailer-spinner]").removeClass("hidden")
                },
                success: function(e) {
                    a.html(e), t = !0
                }
            }), e.preventDefault()
        })
    }(), document.addEventListener("DOMContentLoaded", function() {
    if (null != document.querySelector("[data-megamenu-container]")) {
        document.querySelector("[data-megamenu-container]").querySelectorAll("[data-megamenu-wrap]");
        mlsMegamenu.renderCols()
    }
}), $("[data-nav-setactive-scope]").each(function() {
    var t = $(this).find("[data-nav-setactive-link]"),
        e = $("[data-product-cat-url]").attr("data-product-cat-url");
    t.map(function(t, a) {
        return a.href == window.location.href || a.href == e ? $(this).closest("[data-nav-setactive-item]") : null
    }).each(function() {
        var t = $(this),
            e = t.parents("[data-nav-setactive-item]");
        $(t).add(e).addClass("is-active")
    })
}), $(document).on("submit", "[data-product-button--form]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = e.find("[data-product-button--loader]"),
        o = e.attr("data-product-button--variant"),
        n = e.attr("data-product-button--path") + "/" + o,
        r = e.attr("data-product-button--modal-url"),
        i = e.attr("data-product-button--modal-template");
    $.ajax({
        url: n,
        data: e.serialize(),
        type: e.attr("method"),
        dataType: "json",
        beforeSend: function() {
            $.mshButtons.addLoader(a)
        },
        complete: function() {
            $.mshButtons.removeLoader(a)
        },
        success: function() {
            $.mlsModal({
                src: r,
                data: {
                    template: i
                },
                transferData: !0
            }), $.mshProduct.editCartButtons(o, 1)
        }
    })
}), $(document).on("click", "[data-product-kit]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $('[data-product-kit--id="' + e.attr("data-product-kit") + '"]'),
        o = e.attr("href"),
        n = e.attr("data-product-kit--modal-url"),
        r = e.attr("data-product-kit--modal-template");
    $.ajax({
        url: o,
        data: e.serialize(),
        type: "get",
        beforeSend: function() {
            $.mshButtons.addLoader(e)
        },
        complete: function() {
            $.mshButtons.removeLoader(e)
        },
        success: function() {
            $.mlsModal({
                src: n,
                data: {
                    template: r
                },
                transferData: !0
            }), a.toggleClass("hidden")
        }
    })
}), $(document).on("change", "[data-product-variant]", function(t) {
    t.preventDefault();
    var e = $.mshProduct.getVariant($(this)),
        a = e.closest("[data-product-scope]"),
        o = e.attr("data-product-variant--id"),
        n = a.find("[data-product-photo-main-thumb]").first().closest("[data-product-photo-thumb]"),
        r = n.closest("[data-product-photo-scope]").find("[data-product-photo-thumb]"),
        i = e.attr("data-product-variant--origin-val"),
        l = e.attr("data-additional-prices"),
        s = a.find("[data-one-click-btn]").first(),
        c = a.find("[data-one-click-scope]").first(),
        d = e.attr("data-product-variant-bonus-points"),
        u = e.attr("data-product-variant-bonus-label"),
        f = $("[data-opt-price-available]").attr("data-opt-price-available");
    if ($.mshProduct.loadCartButton(e), a.find("[data-product-photo]").first().attr("src", e.attr("data-product-variant--photo")), a.find("[data-product-photo-main-thumb]").first().attr("src", e.attr("data-product-variant--thumb")), n.attr("href", e.attr("data-product-variant--photo-link")), r.removeAttr("data-product-photo-thumb-active"), n.attr("data-product-photo-thumb-active", ""), $.mlsMedia.magnificGalley(), a.find("[data-product-photo-link]").first().attr("href", e.attr("data-product-variant--photo-link")), a.find("[data-zoom-image]").first().attr("data-zoom-image", e.attr("data-product-variant--photo-link")), $.mlsMedia.zoomImage(), a.find("[data-product-number]").first().html(e.attr("data-product-variant--number")), e.attr("data-product-variant--stock") > 0 ? (a.find("[data-product-available]").removeClass("hidden"), a.find("[data-product-unavailable]").addClass("hidden")) : (a.find("[data-product-available]").addClass("hidden"), a.find("[data-product-unavailable]").removeClass("hidden"), a.find("[data-product-notify]").first().attr("data-product-notify-variant", o)), a.find("[data-product-price--main]").first().html(e.attr("data-product-variant--price")), a.find("[data-product-price--coins]").first().html(e.attr("data-product-variant--coins")), "true" == f && (a.find("[data-product-opt-price--main]").first().html(e.attr("data-product-variant--opt-val")), a.find("[data-product-opt-price--coins]").first().html(e.attr("data-product-variant--opt-coins"))), i && (a.find("[data-product-price--origin-val]").first().html(e.attr("data-product-variant--origin-val")), a.find("[data-product-price--origin-coins]").first().html(e.attr("data-product-variant--origin-coins"))), l) {
        var p = l.split("|"),
            h = a.find("[data-product-price--addition-list]").first();
        (h = h.siblings("[data-product-price--addition-list]").andSelf()).each(function(t) {
            var e = $(this),
                a = p[t].split("^"),
                o = a[0],
                n = a[1];
            e.find("[data-product-price--addition-value]").first().html(o), e.find("[data-product-price--addition-coins]").first().html(n)
        })
    }
    s.attr("data-one-click-variant", o), 0 == e.attr("data-product-variant--stock") ? c.addClass("hidden") : c.removeClass("hidden"), d > 0 ? (a.find("[data-bonus]").first().removeClass("hidden"), a.find("[data-bonus-points]").first().html(d), a.find("[data-bonus-label]").first().html(u)) : a.find("[data-bonus]").first().addClass("hidden"), $("[data-pricespy-variant]").attr("data-pricespy-variant", o)
}), $(document).on("click", "[data-product-notify]", function(t) {
    t.preventDefault();
    var e = $(this);
    $.mlsModal({
        src: $(this).attr("href"),
        data: {
            ProductId: e.attr("data-product-notify"),
            VariantId: e.attr("data-product-notify-variant")
        }
    })
}),
    function(t) {
        var e = t("[data-catalog-nav-item]"),
            a = t("[data-view-all-btn]");
        e.length > 10 && a.removeClass("hidden"), a.on("click", function(t) {
            t.preventDefault(), e.removeClass("footer__item--catalogue"), a.addClass("hidden")
        })
    }(jQuery), $(document).on("submit", "[data-profile-ajax]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $('[data-profile-ajax~="' + e.attr("data-profile-ajax") + '"]');
    $.ajax({
        url: e.attr("action"),
        type: e.attr("method") ? e.attr("method") : "get",
        data: e.serialize(),
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: a
            })
        },
        success: function(t) {
            $.mlsAjax.loadResponseFrame(t, a), $.mlsAjax.transferData(t);
            var e = a.find("[data-profile-button]");
            e.size() > 0 && ($.mshButtons.addLoader(e), setTimeout(function() {
                location.assign(location.href)
            }, 2e3))
        }
    })
}), $(function() {
    var t = $("[ data-page-pushy-mobile]"),
        e = $("[ data-page-pushy-container]"),
        a = $("[data-page-pushy-overlay]"),
        o = "page__mobile--js-open",
        n = "page__body--js-pushed",
        r = $("[data-page-mobile-btn]");
    r.click(function() {
        a.toggleClass("hidden"), t.toggleClass(o), e.toggleClass(n), r.toggleClass("hidden")
    }), a.click(function() {
        a.addClass("hidden"), t.removeClass(o), e.removeClass(n), r.toggleClass("hidden")
    })
}), $("[data-tabs-scope]").on("click", "[data-tabs-target]", function(t) {
    var e = $(this),
        a = e.closest("[data-tabs-scope]"),
        o = e.attr("data-tabs-target"),
        n = a.find('[data-tabs-container="' + o + '"]');
    n.addClass("tabs__section--active"), n.siblings().removeClass("tabs__section--active"), e.parent().addClass("tabs__item--active"), e.parent().siblings().removeClass("tabs__item--active"), $("[data-slider-slides]").slick("setPosition"), t.preventDefault()
}), $("[data-tabs-container]:first-child").each(function() {
    $(this).addClass("tabs__section--active")
}), $(function() {
    $(".tab-nav li:first").addClass("select"), $(".tab-panels>div").hide().filter(":first").show(), $(".tab-nav a").click(function() {
        return $(".tab-panels>div").hide().filter(this.hash).show(), $(".tab-nav li").removeClass("select"), $(this).parent().addClass("select"), !1
    })
}), $(document).on("click", "[data-wishlist-new-input]", function() {
    var t = $(this).closest("[data-wishlist-new-scope]").find("[data-wishlist-new-radio]");
    $(t).trigger("click")
}), $(document).on("click", "[data-wishlist-new-radio]", function() {
    var t = $(this).closest("[data-wishlist-new-scope]").find("[data-wishlist-new-input]");
    $(t).trigger("focus")
}), $(document).on("submit", "[data-wishlist-ajax]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $("[data-wishlist-ajax]"),
        o = e.find('[data-button-loader="loader"]').closest("[data-wishlist-move-loader]");
    $.ajax({
        url: e.attr("action"),
        type: e.attr("method") ? e.attr("method") : "get",
        data: e.serialize(),
        beforeSend: function() {
            "move" == e.data("wishlist-ajax") && $.mlsWishList.moveListValidation(e) ? $.mshButtons.addLoader(o) : $.mlsAjax.preloaderShow({
                type: "frame",
                frame: a
            })
        },
        success: function(t) {
            "move" == e.data("wishlist-ajax") && $.mlsWishList.moveListValidation(e) ? location.assign(location.href) : ($.mlsAjax.loadResponseFrame(t, a), $.mlsAjax.transferData(t))
        }
    })
}), $(document).on("submit", "[data-wishlist-edit]", function(t) {
    t.preventDefault();
    var e = $(this),
        a = $("[data-wishlist-edit]"),
        o = $("[data-wishlist-edit]"),
        n = a.find("[data-wishlist-edit--button]"),
        r = a.find("[data-wishlist-edit--error]");
    $.ajax({
        url: e.attr("action"),
        type: e.attr("method") ? e.attr("method") : "get",
        data: e.serialize(),
        dataType: "json",
        beforeSend: function() {
            $.mlsAjax.preloaderShow({
                type: "frame",
                frame: o
            })
        },
        success: function(t) {
            "error" == t.answer ? r.removeClass("hidden").find("[data-wishlist-edit--error-message]").html(t.data[0]) : (r.addClass("hidden"), $.mshButtons.addLoader(n), location.assign(location.href))
        }
    })
}),
    function(t) {
        t(document).on("ajaxStop", function() {
            t.mlsAjax.preloaderHide()
        }), t("[data-global-doubletap]").doubleTapToGo()
    }(jQuery),
    function(t) {
        t('[data-slider="banner-simple"]').each(function() {
            var e = t(this);
            e.find("[data-slider-slides]").removeAttr("data-slider-nojs").slick({
                adaptiveHeight: !1,
                slidesToShow: 1,
                dots: e.data("dots"),
                arrows: e.data("arrows"),
                speed: e.data("speed"),
                autoplay: e.data("autoplay"),
                autoplaySpeed: e.data("autoplayspeed"),
                fade: e.data("fade"),
                infinite: e.data("infinite"),
                prevArrow: e.find("[data-slider-arrow-left]").removeClass("hidden"),
                nextArrow: e.find("[data-slider-arrow-right]").removeClass("hidden"),
                responsive: [{
                    breakpoint: 992,
                    settings: {
                        dots: !1
                    }
                }]
            })
        })
    }(jQuery),
    function() {
        var t = $('[data-slider="mainpage-brands"]'),
            e = $("[data-slider-slides]", t).attr("data-slider-slides");
        $("[data-slider-slides]", t).find("[data-slider-slide]").css("float", "left").end().slick({
            dots: !1,
            arrows: !0,
            adaptiveHeight: !1,
            slidesToShow: $.mlsSlider.getFirstCol(e),
            autoplay: !1,
            autoplaySpeed: 3e3,
            swipeToSlide: !0,
            mobileFirst: !0,
            prevArrow: $("[data-slider-arrow-left]", t).removeClass("hidden"),
            nextArrow: $("[data-slider-arrow-right]", t).removeClass("hidden"),
            responsive: $.mlsSlider.getCols(e)
        })
    }(), $(document).ready(function() {
    $(document).on("click", "[data-filter-toggle--btn]", function(t) {
        t.preventDefault(), $("[data-filter-toggle--filter]").toggleClass("hidden-xs"), $(this).find("[data-filter-toggle--btn-text]").toggleClass("hidden")
    }), $(document).on("click", "[data-filter-drop-handle]", function(t) {
        t.preventDefault(), $(this).closest("[data-filter-drop-scope]").find("[data-filter-drop-inner]").slideToggle(300), $(this).closest("[data-filter-drop-scope]").find("[data-filter-drop-ico]").toggleClass("hidden", 300)
    }), $("[data-filter-scroll]").each(function() {
        var t = $(this),
            e = t.find("[data-filter-control]:checked").first();
        if (e.size() > 0) {
            var a = e.offset().top - t.offset().top;
            t.scrollTop(a - (t.height() / 2 - e.height()))
        }
    }), $(document).on("change", "[data-filter-control]", function() {
        $("#catalog-form").submit()
    }), $("[data-filter-link]").on("click", function(t) {
        t.preventDefault(), $(this).closest("[data-filter-label]").trigger("click")
    }), $(document).on("submit", "#catalog-form", function() {
        var t = $("[data-filter]"),
            e = $("[data-catalog-default]"),
            a = $(this),
            o = t.find("[data-filter-price-min]"),
            n = t.find("[data-filter-price-max]");
        if (e.attr("disabled", !0), o.attr("data-filter-price-min") == o.val() && o.attr("disabled", !0), n.attr("data-filter-price-max") == n.val() && n.attr("disabled", !0), $.imcSeoUrl && $.imcSeoUrl.add({
                fields: t.find("[data-filter-control]:checked"),
                catUrl: t.attr("data-filter-category"),
                form: a
            }), "" == a.serialize()) return location.assign(a.attr("action")), !1
    }), $(document).on("click", "[data-filter-result]", function(t) {
        t.preventDefault();
        var e, a, o = $(this),
            n = $("[data-filter]"),
            r = '[data-filter-control="brand-' + o.attr("data-filter-result-value") + '"], [data-filter-control="property-' + o.attr("data-filter-result-value") + '"]';
        "checkbox" == o.attr("data-filter-result") && n.find(r).prop("checked", !1).trigger("change"), "price" == o.attr("data-filter-result") && (e = n.find("[data-filter-price-min]").attr("data-filter-price-min"), a = n.find("[data-filter-price-max]").attr("data-filter-price-max"), n.find("[data-filter-price-min]").val(e).end().find("[data-filter-price-max]").val(a), $("#catalog-form").submit())
    })
}),
    function(t) {
        var e, a, o, n = t('[data-autocomplete="header-search"]'),
            r = n.find("[data-autocomplete-frame]"),
            i = n.find("[data-autocomplete-product]").size(),
            l = n.find("[data-autocomplete-noitems]"),
            s = n.find("[data-autocomplete-view-all]"),
            c = function(e, a) {
                t(a).each(function(t, a) {
                    if (e.eq(t).size() > 0) e.eq(t).html(a);
                    else {
                        var o = e.eq(0).clone();
                        o.html(a), e.parent().append(o)
                    }
                })
            };
        t("[data-autocomplete-input]", n).autocomplete({
            source: function(d, u) {
                t.ajax({
                    url: n.attr("data-autocomplete-url"),
                    method: "post",
                    data: {
                        queryString: d.term
                    },
                    dataType: "json",
                    beforeSend: function() {},
                    success: function(d) {
                        (e = function(e) {
                            t.each(e, function(t, e) {
                                "queryString" != t && ((o = n.find('[data-autocomplete-product="' + t + '"]')).find("[data-autocomplete-product-name]").html(e.name), o.find("[data-autocomplete-product-price]").html(e.price), o.find("[data-autocomplete-product-img]").attr({
                                    src: e.smallImage,
                                    alt: e.name
                                }), o.attr("href", location.origin + "/" + e.url), c(o.find("[data-autocomplete-product-addition-price]"), e.nextCurrency), o.removeClass("hidden"))
                            }), a = Object.keys(e).length - 1;
                            for (var r = i; r >= a; r--) n.find('[data-autocomplete-product="' + r + '"]').addClass("hidden");
                            return r
                        }(d)) < 0 ? l.removeClass("hidden") : l.addClass("hidden"), e > 0 && s.removeClass("hidden"), r.removeClass("hidden");
                        var u = s.prop("href");
                        s.on("click", function() {
                            s.attr({
                                href: u + d.queryString
                            })
                        })
                    }
                })
            },
            minLength: 3,
            delay: 300
        }), t(document).on("click", function(e) {
            t(e.target).closest(r).size() > 0 ? e.stopPropagation() : r.addClass("hidden")
        })
    }(jQuery), $(document).on("ready", function() {
    function t(t, e) {
        var a = $("[" + r + "]"),
            o = a.find(".ui-slider-handle")[0],
            n = a.find(".ui-slider-handle")[1];
        $(o).empty(), $(n).empty(), $(o).append("<span class='range-slider__text'>" + t + "</span>"), $(n).append("<span class='range-slider__text'>" + e + "</span>")
    }
    var e = $("#catalog-form"),
        a = $("[data-filter]"),
        o = "data-filter-price-min",
        n = "data-filter-price-max",
        r = "data-range-slider",
        i = parseFloat(a.find("[" + o + "]").attr(o)),
        l = parseFloat(a.find("[" + n + "]").attr(n)),
        s = parseFloat(a.find("[" + o + "]").attr("value")),
        c = parseFloat(a.find("[" + n + "]").attr("value"));
    $("[" + r + "]").slider({
        min: i,
        max: l,
        values: [s, c],
        range: !0,
        slide: function(e, r) {
            var i = r.values[0],
                l = r.values[1];
            a.find("[" + o + "]").val(i), a.find("[" + n + "]").val(l), t(i, l)
        },
        change: function() {
            e.trigger("submit"), a.find("[" + o + "], [" + n + "]").attr("readOnly", !0)
        }
    }), t(s, c), a.find("[" + o + "], [" + n + "]").on("change", function() {
        $("[" + r + "]").slider("values", [a.find("[" + o + "]").val(), a.find("[" + n + "]").val()])
    })
}), $(document).ready(function() {
    $("[data-news-text-dot]").dotdotdot({
        ellipsis: "... ",
        wrap: "word",
        watch: !0,
        height: 80
    })
}), $(document).ready(function() {
    $("[data-pruct-cut-title]").dotdotdot({
        ellipsis: "... ",
        wrap: "word",
        watch: !0,
        height: 36
    })
}), $(document).ready(function() {
    $("[data-product-title-dot-lg]").dotdotdot({
        ellipsis: "... ",
        wrap: "word",
        watch: !0,
        height: 360
    })
}),
    function() {
        function t() {
            var t = new Date,
                e = $("[data-resend-btn]");
            t.setMinutes(5 + t.getMinutes()), $.cookie("BtnDis", "no", {
                expires: t,
                path: "/"
            }), $.cookie("BtnDis") ? (a.addClass("hidden"), o.removeClass("hidden"), $("[data-resend-btn]").attr("disabled", "disabled")) : (a.removeClass("hidden"), o.addClass("hidden"), $("[data-resend-btn]").removeAttr("disabled")), e.click(function() {
                $.cookie("BtnDis") ? $("[data-resend-btn]").attr("disabled", "disabled") : $("[data-resend-btn]").removeAttr("disabled")
            })
        }

        function e(e) {
            e.preventDefault();
            var a = $("[data-modal-submit]"),
                o = r.attr("data-main-form"),
                n = r.find("input[name=user_number]").val(),
                i = r.find("input[name=email]").val();
            $.ajax({
                url: o,
                type: "GET",
                data: {
                    user_number: n,
                    user_email: i
                },
                beforeSend: function() {
                    $.mshButtons.addLoader(l)
                },
                complete: function() {
                    $.mshButtons.removeLoader(l)
                },
                success: function(e) {
                    "error" == (e = JSON.parse(e)).status ? ($("[data-email-error]").removeClass("hidden"), $("[data-checking-email]").focus()) :
                        $.mlsModal({
                        src: a.attr("href"),
                        type: a.data("modal-type"),
                        data: {
                            template: a.data("modal")
                        }
                    }), t()
                }
            })
        }
        var a = $("[data-modal-submit]"),
            o = $("[data-open-popup-btn]");
        setInterval(function() {
            $.cookie("BtnDis") ? (a.addClass("hidden"), o.removeClass("hidden"), $("[data-resend-btn]").attr("disabled", "disabled")) : (a.removeClass("hidden"), o.addClass("hidden"), $("[data-resend-btn]").removeAttr("disabled"))
        }, 3e4), $("[data-visiable-input]").on("input", function() {
            var t = "";
            $("[data-visiable-input]").each(function() {
                t += $(this).val()
            }), $("[data-code-input]").val(t), $("[data-main-form-code-input]").val(t)
        });
        var n = $("[data-confirm-form]"),
            r = $("[data-main-form]"),
            i = $("[data-code-input]"),
            l = r.find("[data-vefication-button--loader]");
        n.submit(function(t) {
            t.preventDefault();
            var a = n.attr("action"),
                o = i.val(),
                l = r.find("input[name=user_number]").val(),
                s = $("[data-confirm-error-massage]");
            $.ajax({
                url: a,
                type: "GET",
                data: {
                    user_number: l,
                    user_code: o
                },
                beforeSend: function() {
                    $.mlsAjax.preloaderShow({
                        type: "frame",
                        frame: n
                    })
                },
                success: function(t) {
                    "error" == (t = JSON.parse(t)).status ? s.removeClass("hidden") : (r.off("submit", e), r.trigger("submit"), rrorMassage.addClass("hidden"))
                },
                error: function(t) {
                    s.removeClass("hidden")
                }
            })
        }), r.on("submit", e), $("[data-resend-btn]").on("click", e)
    }(), $("[data-open-popup-btn]").on("click", function() {
    var t = $("[data-modal-submit]");
    $.mlsModal({
        src: t.attr("href"),
        type: t.data("modal-type"),
        data: {
            template: t.data("modal")
        }
    })
}), $("[data-visiable-input]").on("keypress", function(t) {
    $(this).next().trigger("focus")
}),
    function(t) {
        t("[data-mobile-nav-link]", "[data-mobile-nav]").on("click", function(e) {
            e.preventDefault();
            var a = t(this),
                o = a.closest("[data-mobile-nav-item]").find(" > [data-mobile-nav-list]"),
                n = a.parents("[data-mobile-nav-list]");
            a.closest("[data-mobile-nav-list]");
            o.removeClass("hidden"), n.addClass("mobile-nav__list--is-moving"), t(".page__mobile").scrollTop(0)
        }), t("[data-mobile-nav-go-back]", "[data-mobile-nav]").on("click", function(e) {
            e.preventDefault();
            var a = t(this).closest("[data-mobile-nav-list]").parent().closest("[data-mobile-nav-list]"),
                o = a.find("[data-mobile-nav-list]");
            a.removeClass("mobile-nav__list--is-moving"), setTimeout(function() {
                o.addClass("hidden")
            }, 300)
        }), t("[data-mobile-nav-viewAll]").each(function() {
            var e = t(this),
                a = e.closest("[data-mobile-nav-list]").closest("[data-mobile-nav-item]").find("[data-mobile-nav-link]").attr("href");
            e.attr("href", a), e.closest("[data-mobile-nav-item]").removeClass("hidden")
        })
    }(jQuery), $(document).on("ready", function() {
    $.mlsMedia.magnificGalley(), $.mlsMedia.zoomImage()
}), $(document).on("click", "[data-product-photo-href]", function() {
    var t = $(this).attr("data-product-photo-href");
    location.assign(t)
}), $(document).on("click", "[data-product-photo-thumb]", function(t) {
    t.preventDefault();
    var e, a = $(this),
        o = a.closest("[data-product-photo-scope]"),
        n = a.closest("[data-magnific-galley]"),
        r = a.attr("href"),
        i = o.find("[data-product-photo-link]"),
        l = o.find("[data-product-photo]"),
        s = o.find("[data-zoom-image]");
    o.find("[data-product-photo-thumb]").removeAttr("data-product-photo-thumb-active"), a.attr("data-product-photo-thumb-active", ""), i.attr("href", r), l.attr("src", r), s.attr("data-zoom-image", r), $.mlsMedia.zoomImage(), e = o.find("[data-product-photo-thumb]").index(o.find("[data-product-photo-thumb-active]")), $.mlsMedia.magnificGalley(e, n)
}),
    function() {
        function t() {
            $.grep(a, function(t) {
                t.link.removeAttr("data-product-photo-thumb-active")
            })
        }

        function e(t) {
            t.link.attr("data-product-photo-thumb-active", !0), o.find("[data-product-photo-link]").attr("href", t.image), o.find("[data-product-photo]").attr("src", t.image), o.find("[data-product-photo]").attr("data-zoom-image", t.image), $.mlsMedia.zoomImage(), $.mlsMedia.magnificGalley(n, o)
        }
        var a = [],
            o = $("[data-product-photo-scope]"),
            n = 0;
        o.find("[data-product-photo-thumb]").each(function() {
            var t = $(this);
            a.push({
                link: t,
                image: t.attr("href")
            })
        }), o.on("click", "[data-product-slider-btn--left]", function() {
            var o = (--n < 0 && (n = a.length - 1), a[n]);
            t(), e(o)
        }), o.on("click", "[data-product-slider-btn--right]", function() {
            var o = (++n === a.length && (n = 0), a[n]);
            t(), e(o)
        })
    }(), $('[data-slider="widget-primary"]').each(function() {
    var t = $(this),
        e = $("[data-slider-slides]", t).attr("data-slider-slides");
    $("[data-slider-slides]", t).find("[data-slider-slide]").css("float", "left").end().slick({
        dots: !1,
        arrows: !0,
        infinite: !1,
        adaptiveHeight: !0,
        slidesToShow: $.mlsSlider.getFirstCol(e),
        autoplay: !1,
        autoplaySpeed: 3e3,
        swipeToSlide: !0,
        mobileFirst: !0,
        rows: 1,
        prevArrow: $("[data-slider-arrow-left]", t).removeClass("hidden"),
        nextArrow: $("[data-slider-arrow-right]", t).removeClass("hidden"),
        responsive: $.mlsSlider.getCols(e)
    })
}), $('[data-slider="widget-secondary"]').each(function() {
    var t = $(this),
        e = $("[data-slider-slides]", t).attr("data-slider-slides");
    $("[data-slider-slides]", t).find("[data-slider-slide]").css("float", "left").end().slick({
        dots: !1,
        arrows: !0,
        infinite: !1,
        adaptiveHeight: !0,
        slidesToShow: $.mlsSlider.getFirstCol(e),
        slidesToScroll: $.mlsSlider.getFirstCol(e),
        autoplay: !1,
        autoplaySpeed: 3e3,
        swipeToSlide: !0,
        mobileFirst: !0,
        rows: 1,
        prevArrow: $("[data-slider-arrow-left]", t).removeClass("hidden"),
        nextArrow: $("[data-slider-arrow-right]", t).removeClass("hidden"),
        responsive: $.mlsSlider.getCols(e)
    })
}),
    function t(e, a, o) {
        function n(i, l) {
            if (!a[i]) {
                if (!e[i]) {
                    var s = "function" == typeof require && require;
                    if (!l && s) return s(i, !0);
                    if (r) return r(i, !0);
                    var c = new Error("Cannot find module '" + i + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var d = a[i] = {
                    exports: {}
                };
                e[i][0].call(d.exports, function(t) {
                    var a = e[i][1][t];
                    return n(a || t)
                }, d, d.exports, t, e, a, o)
            }
            return a[i].exports
        }
        for (var r = "function" == typeof require && require, i = 0; i < o.length; i++) n(o[i]);
        return n
    }({
        1: [function(t, e, a) {
            "use strict";

            function o(t) {
                t.fn.perfectScrollbar = function(t) {
                    return this.each(function() {
                        if ("object" == typeof t || void 0 === t) {
                            var e = t;
                            r.get(this) || n.initialize(this, e)
                        } else {
                            var a = t;
                            "update" === a ? n.update(this) : "destroy" === a && n.destroy(this)
                        }
                    })
                }
            }
            var n = t("../main"),
                r = t("../plugin/instances");
            if ("function" == typeof define && define.amd) define(["jquery"], o);
            else {
                var i = window.jQuery ? window.jQuery : window.$;
                void 0 !== i && o(i)
            }
            e.exports = o
        }, {
            "../main": 7,
            "../plugin/instances": 18
        }],
        2: [function(t, e, a) {
            "use strict";
            a.add = function(t, e) {
                t.classList ? t.classList.add(e) : function(t, e) {
                    var a = t.className.split(" ");
                    a.indexOf(e) < 0 && a.push(e), t.className = a.join(" ")
                }(t, e)
            }, a.remove = function(t, e) {
                t.classList ? t.classList.remove(e) : function(t, e) {
                    var a = t.className.split(" "),
                        o = a.indexOf(e);
                    o >= 0 && a.splice(o, 1), t.className = a.join(" ")
                }(t, e)
            }, a.list = function(t) {
                return t.classList ? Array.prototype.slice.apply(t.classList) : t.className.split(" ")
            }
        }, {}],
        3: [function(t, e, a) {
            "use strict";
            var o = {};
            o.e = function(t, e) {
                var a = document.createElement(t);
                return a.className = e, a
            }, o.appendTo = function(t, e) {
                return e.appendChild(t), t
            }, o.css = function(t, e, a) {
                return "object" == typeof e ? function(t, e) {
                    for (var a in e) {
                        var o = e[a];
                        "number" == typeof o && (o = o.toString() + "px"), t.style[a] = o
                    }
                    return t
                }(t, e) : void 0 === a ? function(t, e) {
                    return window.getComputedStyle(t)[e]
                }(t, e) : function(t, e, a) {
                    return "number" == typeof a && (a = a.toString() + "px"), t.style[e] = a, t
                }(t, e, a)
            }, o.matches = function(t, e) {
                return void 0 !== t.matches ? t.matches(e) : void 0 !== t.matchesSelector ? t.matchesSelector(e) : void 0 !== t.webkitMatchesSelector ? t.webkitMatchesSelector(e) : void 0 !== t.mozMatchesSelector ? t.mozMatchesSelector(e) : void 0 !== t.msMatchesSelector ? t.msMatchesSelector(e) : void 0
            }, o.remove = function(t) {
                void 0 !== t.remove ? t.remove() : t.parentNode && t.parentNode.removeChild(t)
            }, o.queryChildren = function(t, e) {
                return Array.prototype.filter.call(t.childNodes, function(t) {
                    return o.matches(t, e)
                })
            }, e.exports = o
        }, {}],
        4: [function(t, e, a) {
            "use strict";
            var o = function(t) {
                this.element = t, this.events = {}
            };
            o.prototype.bind = function(t, e) {
                void 0 === this.events[t] && (this.events[t] = []), this.events[t].push(e), this.element.addEventListener(t, e, !1)
            }, o.prototype.unbind = function(t, e) {
                var a = void 0 !== e;
                this.events[t] = this.events[t].filter(function(o) {
                    return !(!a || o === e) || (this.element.removeEventListener(t, o, !1), !1)
                }, this)
            }, o.prototype.unbindAll = function() {
                for (var t in this.events) this.unbind(t)
            };
            var n = function() {
                this.eventElements = []
            };
            n.prototype.eventElement = function(t) {
                var e = this.eventElements.filter(function(e) {
                    return e.element === t
                })[0];
                return void 0 === e && (e = new o(t), this.eventElements.push(e)), e
            }, n.prototype.bind = function(t, e, a) {
                this.eventElement(t).bind(e, a)
            }, n.prototype.unbind = function(t, e, a) {
                this.eventElement(t).unbind(e, a)
            }, n.prototype.unbindAll = function() {
                for (var t = 0; t < this.eventElements.length; t++) this.eventElements[t].unbindAll()
            }, n.prototype.once = function(t, e, a) {
                var o = this.eventElement(t),
                    n = function(t) {
                        o.unbind(e, n), a(t)
                    };
                o.bind(e, n)
            }, e.exports = n
        }, {}],
        5: [function(t, e, a) {
            "use strict";
            e.exports = function() {
                function t() {
                    return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
                }
                return function() {
                    return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t()
                }
            }()
        }, {}],
        6: [function(t, e, a) {
            "use strict";

            function o(t) {
                return function(e, a) {
                    t(e, "ps--in-scrolling"), void 0 !== a ? t(e, "ps--" + a) : (t(e, "ps--x"), t(e, "ps--y"))
                }
            }
            var n = t("./class"),
                r = t("./dom"),
                i = a.toInt = function(t) {
                    return parseInt(t, 10) || 0
                },
                l = a.clone = function(t) {
                    if (t) {
                        if (Array.isArray(t)) return t.map(l);
                        if ("object" == typeof t) {
                            var e = {};
                            for (var a in t) e[a] = l(t[a]);
                            return e
                        }
                        return t
                    }
                    return null
                };
            a.extend = function(t, e) {
                var a = l(t);
                for (var o in e) a[o] = l(e[o]);
                return a
            }, a.isEditable = function(t) {
                return r.matches(t, "input,[contenteditable]") || r.matches(t, "select,[contenteditable]") || r.matches(t, "textarea,[contenteditable]") || r.matches(t, "button,[contenteditable]")
            }, a.removePsClasses = function(t) {
                for (var e = n.list(t), a = 0; a < e.length; a++) {
                    var o = e[a];
                    0 === o.indexOf("ps-") && n.remove(t, o)
                }
            }, a.outerWidth = function(t) {
                return i(r.css(t, "width")) + i(r.css(t, "paddingLeft")) + i(r.css(t, "paddingRight")) + i(r.css(t, "borderLeftWidth")) + i(r.css(t, "borderRightWidth"))
            }, a.startScrolling = o(n.add), a.stopScrolling = o(n.remove), a.env = {
                isWebKit: "undefined" != typeof document && "WebkitAppearance" in document.documentElement.style,
                supportsTouch: "undefined" != typeof window && ("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
                supportsIePointer: "undefined" != typeof window && null !== window.navigator.msMaxTouchPoints
            }
        }, {
            "./class": 2,
            "./dom": 3
        }],
        7: [function(t, e, a) {
            "use strict";
            var o = t("./plugin/destroy"),
                n = t("./plugin/initialize"),
                r = t("./plugin/update");
            e.exports = {
                initialize: n,
                update: r,
                destroy: o
            }
        }, {
            "./plugin/destroy": 9,
            "./plugin/initialize": 17,
            "./plugin/update": 21
        }],
        8: [function(t, e, a) {
            "use strict";
            e.exports = {
                handlers: ["click-rail", "drag-scrollbar", "keyboard", "wheel", "touch"],
                maxScrollbarLength: null,
                minScrollbarLength: null,
                scrollXMarginOffset: 0,
                scrollYMarginOffset: 0,
                suppressScrollX: !1,
                suppressScrollY: !1,
                swipePropagation: !0,
                swipeEasing: !0,
                useBothWheelAxes: !1,
                wheelPropagation: !1,
                wheelSpeed: 1,
                theme: "default"
            }
        }, {}],
        9: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.event.unbindAll(), n.remove(e.scrollbarX), n.remove(e.scrollbarY), n.remove(e.scrollbarXRail), n.remove(e.scrollbarYRail), o.removePsClasses(t), r.remove(t))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18
        }],
        10: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry"),
                r = t("../update-scroll");
            e.exports = function(t) {
                ! function(t, e) {
                    function a(t) {
                        return t.getBoundingClientRect()
                    }
                    var o = function(t) {
                        t.stopPropagation()
                    };
                    e.event.bind(e.scrollbarY, "click", o), e.event.bind(e.scrollbarYRail, "click", function(o) {
                        var i = o.pageY - window.pageYOffset - a(e.scrollbarYRail).top > e.scrollbarYTop ? 1 : -1;
                        r(t, "top", t.scrollTop + i * e.containerHeight), n(t), o.stopPropagation()
                    }), e.event.bind(e.scrollbarX, "click", o), e.event.bind(e.scrollbarXRail, "click", function(o) {
                        var i = o.pageX - window.pageXOffset - a(e.scrollbarXRail).left > e.scrollbarXLeft ? 1 : -1;
                        r(t, "left", t.scrollLeft + i * e.containerWidth), n(t), o.stopPropagation()
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        11: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                var a = null,
                    o = null,
                    n = function(n) {
                        ! function(o) {
                            var n = a + o * e.railXRatio,
                                i = Math.max(0, e.scrollbarXRail.getBoundingClientRect().left) + e.railXRatio * (e.railXWidth - e.scrollbarXWidth);
                            e.scrollbarXLeft = n < 0 ? 0 : n > i ? i : n;
                            var l = r.toInt(e.scrollbarXLeft * (e.contentWidth - e.containerWidth) / (e.containerWidth - e.railXRatio * e.scrollbarXWidth)) - e.negativeScrollAdjustment;
                            c(t, "left", l)
                        }(n.pageX - o), s(t), n.stopPropagation(), n.preventDefault()
                    },
                    l = function() {
                        r.stopScrolling(t, "x"), e.event.unbind(e.ownerDocument, "mousemove", n)
                    };
                e.event.bind(e.scrollbarX, "mousedown", function(s) {
                    o = s.pageX, a = r.toInt(i.css(e.scrollbarX, "left")) * e.railXRatio, r.startScrolling(t, "x"), e.event.bind(e.ownerDocument, "mousemove", n), e.event.once(e.ownerDocument, "mouseup", l), s.stopPropagation(), s.preventDefault()
                })
            }

            function n(t, e) {
                var a = null,
                    o = null,
                    n = function(n) {
                        ! function(o) {
                            var n = a + o * e.railYRatio,
                                i = Math.max(0, e.scrollbarYRail.getBoundingClientRect().top) + e.railYRatio * (e.railYHeight - e.scrollbarYHeight);
                            e.scrollbarYTop = n < 0 ? 0 : n > i ? i : n;
                            var l = r.toInt(e.scrollbarYTop * (e.contentHeight - e.containerHeight) / (e.containerHeight - e.railYRatio * e.scrollbarYHeight));
                            c(t, "top", l)
                        }(n.pageY - o), s(t), n.stopPropagation(), n.preventDefault()
                    },
                    l = function() {
                        r.stopScrolling(t, "y"), e.event.unbind(e.ownerDocument, "mousemove", n)
                    };
                e.event.bind(e.scrollbarY, "mousedown", function(s) {
                    o = s.pageY, a = r.toInt(i.css(e.scrollbarY, "top")) * e.railYRatio, r.startScrolling(t, "y"), e.event.bind(e.ownerDocument, "mousemove", n), e.event.once(e.ownerDocument, "mouseup", l), s.stopPropagation(), s.preventDefault()
                })
            }
            var r = t("../../lib/helper"),
                i = t("../../lib/dom"),
                l = t("../instances"),
                s = t("../update-geometry"),
                c = t("../update-scroll");
            e.exports = function(t) {
                var e = l.get(t);
                o(t, e), n(t, e)
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        12: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                var a = !1;
                e.event.bind(t, "mouseenter", function() {
                    a = !0
                }), e.event.bind(t, "mouseleave", function() {
                    a = !1
                });
                var o = !1;
                e.event.bind(e.ownerDocument, "keydown", function(i) {
                    if (!(i.isDefaultPrevented && i.isDefaultPrevented() || i.defaultPrevented)) {
                        var c = r.matches(e.scrollbarX, ":focus") || r.matches(e.scrollbarY, ":focus");
                        if (a || c) {
                            var d = document.activeElement ? document.activeElement : e.ownerDocument.activeElement;
                            if (d) {
                                if ("IFRAME" === d.tagName) d = d.contentDocument.activeElement;
                                else
                                    for (; d.shadowRoot;) d = d.shadowRoot.activeElement;
                                if (n.isEditable(d)) return
                            }
                            var u = 0,
                                f = 0;
                            switch (i.which) {
                                case 37:
                                    u = i.metaKey ? -e.contentWidth : i.altKey ? -e.containerWidth : -30;
                                    break;
                                case 38:
                                    f = i.metaKey ? e.contentHeight : i.altKey ? e.containerHeight : 30;
                                    break;
                                case 39:
                                    u = i.metaKey ? e.contentWidth : i.altKey ? e.containerWidth : 30;
                                    break;
                                case 40:
                                    f = i.metaKey ? -e.contentHeight : i.altKey ? -e.containerHeight : -30;
                                    break;
                                case 33:
                                    f = 90;
                                    break;
                                case 32:
                                    f = i.shiftKey ? 90 : -90;
                                    break;
                                case 34:
                                    f = -90;
                                    break;
                                case 35:
                                    f = i.ctrlKey ? -e.contentHeight : -e.containerHeight;
                                    break;
                                case 36:
                                    f = i.ctrlKey ? t.scrollTop : e.containerHeight;
                                    break;
                                default:
                                    return
                            }
                            s(t, "top", t.scrollTop - f), s(t, "left", t.scrollLeft + u), l(t), (o = function(a, o) {
                                var n = t.scrollTop;
                                if (0 === a) {
                                    if (!e.scrollbarYActive) return !1;
                                    if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                                }
                                var r = t.scrollLeft;
                                if (0 === o) {
                                    if (!e.scrollbarXActive) return !1;
                                    if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                                }
                                return !0
                            }(u, f)) && i.preventDefault()
                        }
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../../lib/dom"),
                i = t("../instances"),
                l = t("../update-geometry"),
                s = t("../update-scroll");
            e.exports = function(t) {
                o(t, i.get(t))
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        13: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a(a) {
                    var n = function(t) {
                            var e = t.deltaX,
                                a = -1 * t.deltaY;
                            return void 0 !== e && void 0 !== a || (e = -1 * t.wheelDeltaX / 6, a = t.wheelDeltaY / 6), t.deltaMode && 1 === t.deltaMode && (e *= 10, a *= 10), e != e && a != a && (e = 0, a = t.wheelDelta), t.shiftKey ? [-a, -e] : [e, a]
                        }(a),
                        l = n[0],
                        s = n[1];
                    (function(e, a) {
                        var o = t.querySelector("textarea:hover, select[multiple]:hover, .ps-child:hover");
                        if (o) {
                            var n = window.getComputedStyle(o);
                            if (![n.overflow, n.overflowX, n.overflowY].join("").match(/(scroll|auto)/)) return !1;
                            var r = o.scrollHeight - o.clientHeight;
                            if (r > 0 && !(0 === o.scrollTop && a > 0 || o.scrollTop === r && a < 0)) return !0;
                            var i = o.scrollLeft - o.clientWidth;
                            if (i > 0 && !(0 === o.scrollLeft && e < 0 || o.scrollLeft === i && e > 0)) return !0
                        }
                        return !1
                    })(l, s) || (o = !1, e.settings.useBothWheelAxes ? e.scrollbarYActive && !e.scrollbarXActive ? (s ? i(t, "top", t.scrollTop - s * e.settings.wheelSpeed) : i(t, "top", t.scrollTop + l * e.settings.wheelSpeed), o = !0) : e.scrollbarXActive && !e.scrollbarYActive && (l ? i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed) : i(t, "left", t.scrollLeft - s * e.settings.wheelSpeed), o = !0) : (i(t, "top", t.scrollTop - s * e.settings.wheelSpeed), i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed)), r(t), (o = o || function(a, o) {
                            var n = t.scrollTop;
                            if (0 === a) {
                                if (!e.scrollbarYActive) return !1;
                                if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                            }
                            var r = t.scrollLeft;
                            if (0 === o) {
                                if (!e.scrollbarXActive) return !1;
                                if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                            }
                            return !0
                        }(l, s)) && (a.stopPropagation(), a.preventDefault()))
                }
                var o = !1;
                void 0 !== window.onwheel ? e.event.bind(t, "wheel", a) : void 0 !== window.onmousewheel && e.event.bind(t, "mousewheel", a)
            }
            var n = t("../instances"),
                r = t("../update-geometry"),
                i = t("../update-scroll");
            e.exports = function(t) {
                o(t, n.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        14: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry");
            e.exports = function(t) {
                ! function(t, e) {
                    e.event.bind(t, "scroll", function() {
                        n(t)
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19
        }],
        15: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a() {
                    o && (clearInterval(o), o = null), n.stopScrolling(t)
                }
                var o = null,
                    s = {
                        top: 0,
                        left: 0
                    },
                    c = !1;
                e.event.bind(e.ownerDocument, "selectionchange", function() {
                    t.contains(function() {
                        var t = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
                        return 0 === t.toString().length ? null : t.getRangeAt(0).commonAncestorContainer
                    }()) ? c = !0 : (c = !1, a())
                }), e.event.bind(window, "mouseup", function() {
                    c && (c = !1, a())
                }), e.event.bind(window, "keyup", function() {
                    c && (c = !1, a())
                }), e.event.bind(window, "mousemove", function(e) {
                    if (c) {
                        var d = {
                                x: e.pageX,
                                y: e.pageY
                            },
                            u = {
                                left: t.offsetLeft,
                                right: t.offsetLeft + t.offsetWidth,
                                top: t.offsetTop,
                                bottom: t.offsetTop + t.offsetHeight
                            };
                        d.x < u.left + 3 ? (s.left = -5, n.startScrolling(t, "x")) : d.x > u.right - 3 ? (s.left = 5, n.startScrolling(t, "x")) : s.left = 0, d.y < u.top + 3 ? (s.top = u.top + 3 - d.y < 5 ? -5 : -20, n.startScrolling(t, "y")) : d.y > u.bottom - 3 ? (s.top = d.y - u.bottom + 3 < 5 ? 5 : 20, n.startScrolling(t, "y")) : s.top = 0, 0 === s.top && 0 === s.left ? a() : o || (o = setInterval(function() {
                                r.get(t) ? (l(t, "top", t.scrollTop + s.top), l(t, "left", t.scrollLeft + s.left), i(t)) : clearInterval(o)
                            }, 50))
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                o(t, r.get(t))
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        16: [function(t, e, a) {
            "use strict";

            function o(t, e, a, o) {
                function n(e, a) {
                    l(t, "top", t.scrollTop - a), l(t, "left", t.scrollLeft - e), i(t)
                }

                function s() {
                    y = !0
                }

                function c() {
                    y = !1
                }

                function d(t) {
                    return t.targetTouches ? t.targetTouches[0] : t
                }

                function u(t) {
                    return !(!t.targetTouches || 1 !== t.targetTouches.length) || !(!t.pointerType || "mouse" === t.pointerType || t.pointerType === t.MSPOINTER_TYPE_MOUSE)
                }

                function f(t) {
                    if (u(t)) {
                        w = !0;
                        var e = d(t);
                        m.pageX = e.pageX, m.pageY = e.pageY, v = (new Date).getTime(), null !== b && clearInterval(b), t.stopPropagation()
                    }
                }

                function p(a) {
                    if (!w && e.settings.swipePropagation && f(a), !y && w && u(a)) {
                        var o = d(a),
                            r = {
                                pageX: o.pageX,
                                pageY: o.pageY
                            },
                            i = r.pageX - m.pageX,
                            l = r.pageY - m.pageY;
                        n(i, l), m = r;
                        var s = (new Date).getTime(),
                            c = s - v;
                        c > 0 && (g.x = i / c, g.y = l / c, v = s),
                        function(a, o) {
                            var n = t.scrollTop,
                                r = t.scrollLeft,
                                i = Math.abs(a),
                                l = Math.abs(o);
                            if (l > i) {
                                if (o < 0 && n === e.contentHeight - e.containerHeight || o > 0 && 0 === n) return !e.settings.swipePropagation
                            } else if (i > l && (a < 0 && r === e.contentWidth - e.containerWidth || a > 0 && 0 === r)) return !e.settings.swipePropagation;
                            return !0
                        }(i, l) && (a.stopPropagation(), a.preventDefault())
                    }
                }

                function h() {
                    !y && w && (w = !1, e.settings.swipeEasing && (clearInterval(b), b = setInterval(function() {
                        r.get(t) && (g.x || g.y) ? Math.abs(g.x) < .01 && Math.abs(g.y) < .01 ? clearInterval(b) : (n(30 * g.x, 30 * g.y), g.x *= .8, g.y *= .8) : clearInterval(b)
                    }, 10)))
                }
                var m = {},
                    v = 0,
                    g = {},
                    b = null,
                    y = !1,
                    w = !1;
                a ? (e.event.bind(window, "touchstart", s), e.event.bind(window, "touchend", c), e.event.bind(t, "touchstart", f), e.event.bind(t, "touchmove", p), e.event.bind(t, "touchend", h)) : o && (window.PointerEvent ? (e.event.bind(window, "pointerdown", s), e.event.bind(window, "pointerup", c), e.event.bind(t, "pointerdown", f), e.event.bind(t, "pointermove", p), e.event.bind(t, "pointerup", h)) : window.MSPointerEvent && (e.event.bind(window, "MSPointerDown", s), e.event.bind(window, "MSPointerUp", c), e.event.bind(t, "MSPointerDown", f), e.event.bind(t, "MSPointerMove", p), e.event.bind(t, "MSPointerUp", h)))
            }
            var n = t("../../lib/helper"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                if (n.env.supportsTouch || n.env.supportsIePointer) {
                    o(t, r.get(t), n.env.supportsTouch, n.env.supportsIePointer)
                }
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        17: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/class"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = {
                    "click-rail": t("./handler/click-rail"),
                    "drag-scrollbar": t("./handler/drag-scrollbar"),
                    keyboard: t("./handler/keyboard"),
                    wheel: t("./handler/mouse-wheel"),
                    touch: t("./handler/touch"),
                    selection: t("./handler/selection")
                },
                s = t("./handler/native-scroll");
            e.exports = function(t, e) {
                e = "object" == typeof e ? e : {}, n.add(t, "ps");
                var a = r.add(t);
                a.settings = o.extend(a.settings, e), n.add(t, "ps--theme_" + a.settings.theme), a.settings.handlers.forEach(function(e) {
                    l[e](t)
                }), s(t), i(t)
            }
        }, {
            "../lib/class": 2,
            "../lib/helper": 6,
            "./handler/click-rail": 10,
            "./handler/drag-scrollbar": 11,
            "./handler/keyboard": 12,
            "./handler/mouse-wheel": 13,
            "./handler/native-scroll": 14,
            "./handler/selection": 15,
            "./handler/touch": 16,
            "./instances": 18,
            "./update-geometry": 19
        }],
        18: [function(t, e, a) {
            "use strict";

            function o(t) {
                return t.getAttribute("data-ps-id")
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("./default-setting"),
                l = t("../lib/dom"),
                s = t("../lib/event-manager"),
                c = t("../lib/guid"),
                d = {};
            a.add = function(t) {
                var e = c();
                return function(t, e) {
                    t.setAttribute("data-ps-id", e)
                }(t, e), d[e] = new function(t) {
                    function e() {
                        r.add(t, "ps--focus")
                    }

                    function a() {
                        r.remove(t, "ps--focus")
                    }
                    var o = this;
                    o.settings = n.clone(i), o.containerWidth = null, o.containerHeight = null, o.contentWidth = null, o.contentHeight = null, o.isRtl = "rtl" === l.css(t, "direction"), o.isNegativeScroll = function() {
                        var e = t.scrollLeft,
                            a = null;
                        return t.scrollLeft = -1, a = t.scrollLeft < 0, t.scrollLeft = e, a
                    }(), o.negativeScrollAdjustment = o.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, o.event = new s, o.ownerDocument = t.ownerDocument || document, o.scrollbarXRail = l.appendTo(l.e("div", "ps__scrollbar-x-rail"), t), o.scrollbarX = l.appendTo(l.e("div", "ps__scrollbar-x"), o.scrollbarXRail), o.scrollbarX.setAttribute("tabindex", 0), o.event.bind(o.scrollbarX, "focus", e), o.event.bind(o.scrollbarX, "blur", a), o.scrollbarXActive = null, o.scrollbarXWidth = null, o.scrollbarXLeft = null, o.scrollbarXBottom = n.toInt(l.css(o.scrollbarXRail, "bottom")), o.isScrollbarXUsingBottom = o.scrollbarXBottom == o.scrollbarXBottom, o.scrollbarXTop = o.isScrollbarXUsingBottom ? null : n.toInt(l.css(o.scrollbarXRail, "top")), o.railBorderXWidth = n.toInt(l.css(o.scrollbarXRail, "borderLeftWidth")) + n.toInt(l.css(o.scrollbarXRail, "borderRightWidth")), l.css(o.scrollbarXRail, "display", "block"), o.railXMarginWidth = n.toInt(l.css(o.scrollbarXRail, "marginLeft")) + n.toInt(l.css(o.scrollbarXRail, "marginRight")), l.css(o.scrollbarXRail, "display", ""), o.railXWidth = null, o.railXRatio = null, o.scrollbarYRail = l.appendTo(l.e("div", "ps__scrollbar-y-rail"), t), o.scrollbarY = l.appendTo(l.e("div", "ps__scrollbar-y"), o.scrollbarYRail), o.scrollbarY.setAttribute("tabindex", 0), o.event.bind(o.scrollbarY, "focus", e), o.event.bind(o.scrollbarY, "blur", a), o.scrollbarYActive = null, o.scrollbarYHeight = null, o.scrollbarYTop = null, o.scrollbarYRight = n.toInt(l.css(o.scrollbarYRail, "right")), o.isScrollbarYUsingRight = o.scrollbarYRight == o.scrollbarYRight, o.scrollbarYLeft = o.isScrollbarYUsingRight ? null : n.toInt(l.css(o.scrollbarYRail, "left")), o.scrollbarYOuterWidth = o.isRtl ? n.outerWidth(o.scrollbarY) : null, o.railBorderYWidth = n.toInt(l.css(o.scrollbarYRail, "borderTopWidth")) + n.toInt(l.css(o.scrollbarYRail, "borderBottomWidth")), l.css(o.scrollbarYRail, "display", "block"), o.railYMarginHeight = n.toInt(l.css(o.scrollbarYRail, "marginTop")) + n.toInt(l.css(o.scrollbarYRail, "marginBottom")), l.css(o.scrollbarYRail, "display", ""), o.railYHeight = null, o.railYRatio = null
                }(t), d[e]
            }, a.remove = function(t) {
                delete d[o(t)],
                    function(t) {
                        t.removeAttribute("data-ps-id")
                    }(t)
            }, a.get = function(t) {
                return d[o(t)]
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/event-manager": 4,
            "../lib/guid": 5,
            "../lib/helper": 6,
            "./default-setting": 8
        }],
        19: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                return t.settings.minScrollbarLength && (e = Math.max(e, t.settings.minScrollbarLength)), t.settings.maxScrollbarLength && (e = Math.min(e, t.settings.maxScrollbarLength)), e
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("../lib/dom"),
                l = t("./instances"),
                s = t("./update-scroll");
            e.exports = function(t) {
                var e = l.get(t);
                e.containerWidth = t.clientWidth, e.containerHeight = t.clientHeight, e.contentWidth = t.scrollWidth, e.contentHeight = t.scrollHeight;
                var a;
                t.contains(e.scrollbarXRail) || ((a = i.queryChildren(t, ".ps__scrollbar-x-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarXRail, t)), t.contains(e.scrollbarYRail) || ((a = i.queryChildren(t, ".ps__scrollbar-y-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarYRail, t)), !e.settings.suppressScrollX && e.containerWidth + e.settings.scrollXMarginOffset < e.contentWidth ? (e.scrollbarXActive = !0, e.railXWidth = e.containerWidth - e.railXMarginWidth, e.railXRatio = e.containerWidth / e.railXWidth, e.scrollbarXWidth = o(e, n.toInt(e.railXWidth * e.containerWidth / e.contentWidth)), e.scrollbarXLeft = n.toInt((e.negativeScrollAdjustment + t.scrollLeft) * (e.railXWidth - e.scrollbarXWidth) / (e.contentWidth - e.containerWidth))) : e.scrollbarXActive = !1, !e.settings.suppressScrollY && e.containerHeight + e.settings.scrollYMarginOffset < e.contentHeight ? (e.scrollbarYActive = !0, e.railYHeight = e.containerHeight - e.railYMarginHeight, e.railYRatio = e.containerHeight / e.railYHeight, e.scrollbarYHeight = o(e, n.toInt(e.railYHeight * e.containerHeight / e.contentHeight)), e.scrollbarYTop = n.toInt(t.scrollTop * (e.railYHeight - e.scrollbarYHeight) / (e.contentHeight - e.containerHeight))) : e.scrollbarYActive = !1, e.scrollbarXLeft >= e.railXWidth - e.scrollbarXWidth && (e.scrollbarXLeft = e.railXWidth - e.scrollbarXWidth), e.scrollbarYTop >= e.railYHeight - e.scrollbarYHeight && (e.scrollbarYTop = e.railYHeight - e.scrollbarYHeight),
                    function(t, e) {
                        var a = {
                            width: e.railXWidth
                        };
                        e.isRtl ? a.left = e.negativeScrollAdjustment + t.scrollLeft + e.containerWidth - e.contentWidth : a.left = t.scrollLeft, e.isScrollbarXUsingBottom ? a.bottom = e.scrollbarXBottom - t.scrollTop : a.top = e.scrollbarXTop + t.scrollTop, i.css(e.scrollbarXRail, a);
                        var o = {
                            top: t.scrollTop,
                            height: e.railYHeight
                        };
                        e.isScrollbarYUsingRight ? e.isRtl ? o.right = e.contentWidth - (e.negativeScrollAdjustment + t.scrollLeft) - e.scrollbarYRight - e.scrollbarYOuterWidth : o.right = e.scrollbarYRight - t.scrollLeft : e.isRtl ? o.left = e.negativeScrollAdjustment + t.scrollLeft + 2 * e.containerWidth - e.contentWidth - e.scrollbarYLeft - e.scrollbarYOuterWidth : o.left = e.scrollbarYLeft + t.scrollLeft, i.css(e.scrollbarYRail, o), i.css(e.scrollbarX, {
                            left: e.scrollbarXLeft,
                            width: e.scrollbarXWidth - e.railBorderXWidth
                        }), i.css(e.scrollbarY, {
                            top: e.scrollbarYTop,
                            height: e.scrollbarYHeight - e.railBorderYWidth
                        })
                    }(t, e), e.scrollbarXActive ? r.add(t, "ps--active-x") : (r.remove(t, "ps--active-x"), e.scrollbarXWidth = 0, e.scrollbarXLeft = 0, s(t, "left", 0)), e.scrollbarYActive ? r.add(t, "ps--active-y") : (r.remove(t, "ps--active-y"), e.scrollbarYHeight = 0, e.scrollbarYTop = 0, s(t, "top", 0))
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-scroll": 20
        }],
        20: [function(t, e, a) {
            "use strict";
            var o = t("./instances"),
                n = function(t) {
                    var e = document.createEvent("Event");
                    return e.initEvent(t, !0, !0), e
                };
            e.exports = function(t, e, a) {
                if (void 0 === t) throw "You must provide an element to the update-scroll function";
                if (void 0 === e) throw "You must provide an axis to the update-scroll function";
                if (void 0 === a) throw "You must provide a value to the update-scroll function";
                "top" === e && a <= 0 && (t.scrollTop = a = 0, t.dispatchEvent(n("ps-y-reach-start"))), "left" === e && a <= 0 && (t.scrollLeft = a = 0, t.dispatchEvent(n("ps-x-reach-start")));
                var r = o.get(t);
                "top" === e && a >= r.contentHeight - r.containerHeight && ((a = r.contentHeight - r.containerHeight) - t.scrollTop <= 1 ? a = t.scrollTop : t.scrollTop = a, t.dispatchEvent(n("ps-y-reach-end"))), "left" === e && a >= r.contentWidth - r.containerWidth && ((a = r.contentWidth - r.containerWidth) - t.scrollLeft <= 1 ? a = t.scrollLeft : t.scrollLeft = a, t.dispatchEvent(n("ps-x-reach-end"))), void 0 === r.lastTop && (r.lastTop = t.scrollTop), void 0 === r.lastLeft && (r.lastLeft = t.scrollLeft), "top" === e && a < r.lastTop && t.dispatchEvent(n("ps-scroll-up")), "top" === e && a > r.lastTop && t.dispatchEvent(n("ps-scroll-down")), "left" === e && a < r.lastLeft && t.dispatchEvent(n("ps-scroll-left")), "left" === e && a > r.lastLeft && t.dispatchEvent(n("ps-scroll-right")), "top" === e && a !== r.lastTop && (t.scrollTop = r.lastTop = a, t.dispatchEvent(n("ps-scroll-y"))), "left" === e && a !== r.lastLeft && (t.scrollLeft = r.lastLeft = a, t.dispatchEvent(n("ps-scroll-x")))
            }
        }, {
            "./instances": 18
        }],
        21: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = t("./update-scroll");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.negativeScrollAdjustment = e.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, n.css(e.scrollbarXRail, "display", "block"), n.css(e.scrollbarYRail, "display", "block"), e.railXMarginWidth = o.toInt(n.css(e.scrollbarXRail, "marginLeft")) + o.toInt(n.css(e.scrollbarXRail, "marginRight")), e.railYMarginHeight = o.toInt(n.css(e.scrollbarYRail, "marginTop")) + o.toInt(n.css(e.scrollbarYRail, "marginBottom")), n.css(e.scrollbarXRail, "display", "none"), n.css(e.scrollbarYRail, "display", "none"), i(t), l(t, "top", t.scrollTop), l(t, "left", t.scrollLeft), n.css(e.scrollbarXRail, "display", ""), n.css(e.scrollbarYRail, "display", ""))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-geometry": 19,
            "./update-scroll": 20
        }]
    }, {}, [1]), $(".custom-scrollbar").perfectScrollbar(),
    function t(e, a, o) {
        function n(i, l) {
            if (!a[i]) {
                if (!e[i]) {
                    var s = "function" == typeof require && require;
                    if (!l && s) return s(i, !0);
                    if (r) return r(i, !0);
                    var c = new Error("Cannot find module '" + i + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var d = a[i] = {
                    exports: {}
                };
                e[i][0].call(d.exports, function(t) {
                    var a = e[i][1][t];
                    return n(a || t)
                }, d, d.exports, t, e, a, o)
            }
            return a[i].exports
        }
        for (var r = "function" == typeof require && require, i = 0; i < o.length; i++) n(o[i]);
        return n
    }({
        1: [function(t, e, a) {
            "use strict";

            function o(t) {
                t.fn.perfectScrollbar = function(t) {
                    return this.each(function() {
                        if ("object" == typeof t || void 0 === t) {
                            var e = t;
                            r.get(this) || n.initialize(this, e)
                        } else {
                            var a = t;
                            "update" === a ? n.update(this) : "destroy" === a && n.destroy(this)
                        }
                    })
                }
            }
            var n = t("../main"),
                r = t("../plugin/instances");
            if ("function" == typeof define && define.amd) define(["jquery"], o);
            else {
                var i = window.jQuery ? window.jQuery : window.$;
                void 0 !== i && o(i)
            }
            e.exports = o
        }, {
            "../main": 7,
            "../plugin/instances": 18
        }],
        2: [function(t, e, a) {
            "use strict";
            a.add = function(t, e) {
                t.classList ? t.classList.add(e) : function(t, e) {
                    var a = t.className.split(" ");
                    a.indexOf(e) < 0 && a.push(e), t.className = a.join(" ")
                }(t, e)
            }, a.remove = function(t, e) {
                t.classList ? t.classList.remove(e) : function(t, e) {
                    var a = t.className.split(" "),
                        o = a.indexOf(e);
                    o >= 0 && a.splice(o, 1), t.className = a.join(" ")
                }(t, e)
            }, a.list = function(t) {
                return t.classList ? Array.prototype.slice.apply(t.classList) : t.className.split(" ")
            }
        }, {}],
        3: [function(t, e, a) {
            "use strict";
            var o = {};
            o.e = function(t, e) {
                var a = document.createElement(t);
                return a.className = e, a
            }, o.appendTo = function(t, e) {
                return e.appendChild(t), t
            }, o.css = function(t, e, a) {
                return "object" == typeof e ? function(t, e) {
                    for (var a in e) {
                        var o = e[a];
                        "number" == typeof o && (o = o.toString() + "px"), t.style[a] = o
                    }
                    return t
                }(t, e) : void 0 === a ? function(t, e) {
                    return window.getComputedStyle(t)[e]
                }(t, e) : function(t, e, a) {
                    return "number" == typeof a && (a = a.toString() + "px"), t.style[e] = a, t
                }(t, e, a)
            }, o.matches = function(t, e) {
                return void 0 !== t.matches ? t.matches(e) : void 0 !== t.matchesSelector ? t.matchesSelector(e) : void 0 !== t.webkitMatchesSelector ? t.webkitMatchesSelector(e) : void 0 !== t.mozMatchesSelector ? t.mozMatchesSelector(e) : void 0 !== t.msMatchesSelector ? t.msMatchesSelector(e) : void 0
            }, o.remove = function(t) {
                void 0 !== t.remove ? t.remove() : t.parentNode && t.parentNode.removeChild(t)
            }, o.queryChildren = function(t, e) {
                return Array.prototype.filter.call(t.childNodes, function(t) {
                    return o.matches(t, e)
                })
            }, e.exports = o
        }, {}],
        4: [function(t, e, a) {
            "use strict";
            var o = function(t) {
                this.element = t, this.events = {}
            };
            o.prototype.bind = function(t, e) {
                void 0 === this.events[t] && (this.events[t] = []), this.events[t].push(e), this.element.addEventListener(t, e, !1)
            }, o.prototype.unbind = function(t, e) {
                var a = void 0 !== e;
                this.events[t] = this.events[t].filter(function(o) {
                    return !(!a || o === e) || (this.element.removeEventListener(t, o, !1), !1)
                }, this)
            }, o.prototype.unbindAll = function() {
                for (var t in this.events) this.unbind(t)
            };
            var n = function() {
                this.eventElements = []
            };
            n.prototype.eventElement = function(t) {
                var e = this.eventElements.filter(function(e) {
                    return e.element === t
                })[0];
                return void 0 === e && (e = new o(t), this.eventElements.push(e)), e
            }, n.prototype.bind = function(t, e, a) {
                this.eventElement(t).bind(e, a)
            }, n.prototype.unbind = function(t, e, a) {
                this.eventElement(t).unbind(e, a)
            }, n.prototype.unbindAll = function() {
                for (var t = 0; t < this.eventElements.length; t++) this.eventElements[t].unbindAll()
            }, n.prototype.once = function(t, e, a) {
                var o = this.eventElement(t),
                    n = function(t) {
                        o.unbind(e, n), a(t)
                    };
                o.bind(e, n)
            }, e.exports = n
        }, {}],
        5: [function(t, e, a) {
            "use strict";
            e.exports = function() {
                function t() {
                    return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
                }
                return function() {
                    return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t()
                }
            }()
        }, {}],
        6: [function(t, e, a) {
            "use strict";

            function o(t) {
                return function(e, a) {
                    t(e, "ps--in-scrolling"), void 0 !== a ? t(e, "ps--" + a) : (t(e, "ps--x"), t(e, "ps--y"))
                }
            }
            var n = t("./class"),
                r = t("./dom"),
                i = a.toInt = function(t) {
                    return parseInt(t, 10) || 0
                },
                l = a.clone = function(t) {
                    if (t) {
                        if (Array.isArray(t)) return t.map(l);
                        if ("object" == typeof t) {
                            var e = {};
                            for (var a in t) e[a] = l(t[a]);
                            return e
                        }
                        return t
                    }
                    return null
                };
            a.extend = function(t, e) {
                var a = l(t);
                for (var o in e) a[o] = l(e[o]);
                return a
            }, a.isEditable = function(t) {
                return r.matches(t, "input,[contenteditable]") || r.matches(t, "select,[contenteditable]") || r.matches(t, "textarea,[contenteditable]") || r.matches(t, "button,[contenteditable]")
            }, a.removePsClasses = function(t) {
                for (var e = n.list(t), a = 0; a < e.length; a++) {
                    var o = e[a];
                    0 === o.indexOf("ps-") && n.remove(t, o)
                }
            }, a.outerWidth = function(t) {
                return i(r.css(t, "width")) + i(r.css(t, "paddingLeft")) + i(r.css(t, "paddingRight")) + i(r.css(t, "borderLeftWidth")) + i(r.css(t, "borderRightWidth"))
            }, a.startScrolling = o(n.add), a.stopScrolling = o(n.remove), a.env = {
                isWebKit: "undefined" != typeof document && "WebkitAppearance" in document.documentElement.style,
                supportsTouch: "undefined" != typeof window && ("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
                supportsIePointer: "undefined" != typeof window && null !== window.navigator.msMaxTouchPoints
            }
        }, {
            "./class": 2,
            "./dom": 3
        }],
        7: [function(t, e, a) {
            "use strict";
            var o = t("./plugin/destroy"),
                n = t("./plugin/initialize"),
                r = t("./plugin/update");
            e.exports = {
                initialize: n,
                update: r,
                destroy: o
            }
        }, {
            "./plugin/destroy": 9,
            "./plugin/initialize": 17,
            "./plugin/update": 21
        }],
        8: [function(t, e, a) {
            "use strict";
            e.exports = {
                handlers: ["click-rail", "drag-scrollbar", "keyboard", "wheel", "touch"],
                maxScrollbarLength: null,
                minScrollbarLength: null,
                scrollXMarginOffset: 0,
                scrollYMarginOffset: 0,
                suppressScrollX: !1,
                suppressScrollY: !1,
                swipePropagation: !0,
                swipeEasing: !0,
                useBothWheelAxes: !1,
                wheelPropagation: !1,
                wheelSpeed: 1,
                theme: "default"
            }
        }, {}],
        9: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.event.unbindAll(), n.remove(e.scrollbarX), n.remove(e.scrollbarY), n.remove(e.scrollbarXRail), n.remove(e.scrollbarYRail), o.removePsClasses(t), r.remove(t))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18
        }],
        10: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry"),
                r = t("../update-scroll");
            e.exports = function(t) {
                ! function(t, e) {
                    function a(t) {
                        return t.getBoundingClientRect()
                    }
                    var o = function(t) {
                        t.stopPropagation()
                    };
                    e.event.bind(e.scrollbarY, "click", o), e.event.bind(e.scrollbarYRail, "click", function(o) {
                        var i = o.pageY - window.pageYOffset - a(e.scrollbarYRail).top > e.scrollbarYTop ? 1 : -1;
                        r(t, "top", t.scrollTop + i * e.containerHeight), n(t), o.stopPropagation()
                    }), e.event.bind(e.scrollbarX, "click", o), e.event.bind(e.scrollbarXRail, "click", function(o) {
                        var i = o.pageX - window.pageXOffset - a(e.scrollbarXRail).left > e.scrollbarXLeft ? 1 : -1;
                        r(t, "left", t.scrollLeft + i * e.containerWidth), n(t), o.stopPropagation()
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        11: [function(t, e, a) {
            "use strict";
            var o = t("../../lib/helper"),
                n = t("../../lib/dom"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                var e = r.get(t);
                (function(t, e) {
                    function a(a) {
                        var n = r + a * e.railXRatio,
                            i = Math.max(0, e.scrollbarXRail.getBoundingClientRect().left) + e.railXRatio * (e.railXWidth - e.scrollbarXWidth);
                        e.scrollbarXLeft = n < 0 ? 0 : n > i ? i : n;
                        var s = o.toInt(e.scrollbarXLeft * (e.contentWidth - e.containerWidth) / (e.containerWidth - e.railXRatio * e.scrollbarXWidth)) - e.negativeScrollAdjustment;
                        l(t, "left", s)
                    }
                    var r = null,
                        s = null,
                        c = function(e) {
                            a(e.pageX - s), i(t), e.stopPropagation(), e.preventDefault()
                        },
                        d = function() {
                            o.stopScrolling(t, "x"), e.event.unbind(e.ownerDocument, "mousemove", c)
                        };
                    e.event.bind(e.scrollbarX, "mousedown", function(a) {
                        s = a.pageX, r = o.toInt(n.css(e.scrollbarX, "left")) * e.railXRatio, o.startScrolling(t, "x"), e.event.bind(e.ownerDocument, "mousemove", c), e.event.once(e.ownerDocument, "mouseup", d), a.stopPropagation(), a.preventDefault()
                    })
                })(t, e),
                    function(t, e) {
                        function a(a) {
                            var n = r + a * e.railYRatio,
                                i = Math.max(0, e.scrollbarYRail.getBoundingClientRect().top) + e.railYRatio * (e.railYHeight - e.scrollbarYHeight);
                            e.scrollbarYTop = n < 0 ? 0 : n > i ? i : n;
                            var s = o.toInt(e.scrollbarYTop * (e.contentHeight - e.containerHeight) / (e.containerHeight - e.railYRatio * e.scrollbarYHeight));
                            l(t, "top", s)
                        }
                        var r = null,
                            s = null,
                            c = function(e) {
                                a(e.pageY - s), i(t), e.stopPropagation(), e.preventDefault()
                            },
                            d = function() {
                                o.stopScrolling(t, "y"), e.event.unbind(e.ownerDocument, "mousemove", c)
                            };
                        e.event.bind(e.scrollbarY, "mousedown", function(a) {
                            s = a.pageY, r = o.toInt(n.css(e.scrollbarY, "top")) * e.railYRatio, o.startScrolling(t, "y"), e.event.bind(e.ownerDocument, "mousemove", c), e.event.once(e.ownerDocument, "mouseup", d), a.stopPropagation(), a.preventDefault()
                        })
                    }(t, e)
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        12: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                var a = !1;
                e.event.bind(t, "mouseenter", function() {
                    a = !0
                }), e.event.bind(t, "mouseleave", function() {
                    a = !1
                });
                var o = !1;
                e.event.bind(e.ownerDocument, "keydown", function(i) {
                    if (!(i.isDefaultPrevented && i.isDefaultPrevented() || i.defaultPrevented)) {
                        var c = r.matches(e.scrollbarX, ":focus") || r.matches(e.scrollbarY, ":focus");
                        if (a || c) {
                            var d = document.activeElement ? document.activeElement : e.ownerDocument.activeElement;
                            if (d) {
                                if ("IFRAME" === d.tagName) d = d.contentDocument.activeElement;
                                else
                                    for (; d.shadowRoot;) d = d.shadowRoot.activeElement;
                                if (n.isEditable(d)) return
                            }
                            var u = 0,
                                f = 0;
                            switch (i.which) {
                                case 37:
                                    u = i.metaKey ? -e.contentWidth : i.altKey ? -e.containerWidth : -30;
                                    break;
                                case 38:
                                    f = i.metaKey ? e.contentHeight : i.altKey ? e.containerHeight : 30;
                                    break;
                                case 39:
                                    u = i.metaKey ? e.contentWidth : i.altKey ? e.containerWidth : 30;
                                    break;
                                case 40:
                                    f = i.metaKey ? -e.contentHeight : i.altKey ? -e.containerHeight : -30;
                                    break;
                                case 33:
                                    f = 90;
                                    break;
                                case 32:
                                    f = i.shiftKey ? 90 : -90;
                                    break;
                                case 34:
                                    f = -90;
                                    break;
                                case 35:
                                    f = i.ctrlKey ? -e.contentHeight : -e.containerHeight;
                                    break;
                                case 36:
                                    f = i.ctrlKey ? t.scrollTop : e.containerHeight;
                                    break;
                                default:
                                    return
                            }
                            s(t, "top", t.scrollTop - f), s(t, "left", t.scrollLeft + u), l(t), (o = function(a, o) {
                                var n = t.scrollTop;
                                if (0 === a) {
                                    if (!e.scrollbarYActive) return !1;
                                    if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                                }
                                var r = t.scrollLeft;
                                if (0 === o) {
                                    if (!e.scrollbarXActive) return !1;
                                    if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                                }
                                return !0
                            }(u, f)) && i.preventDefault()
                        }
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../../lib/dom"),
                i = t("../instances"),
                l = t("../update-geometry"),
                s = t("../update-scroll");
            e.exports = function(t) {
                o(t, i.get(t))
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        13: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a(a) {
                    var n = function(t) {
                            var e = t.deltaX,
                                a = -1 * t.deltaY;
                            return void 0 !== e && void 0 !== a || (e = -1 * t.wheelDeltaX / 6, a = t.wheelDeltaY / 6), t.deltaMode && 1 === t.deltaMode && (e *= 10, a *= 10), e != e && a != a && (e = 0, a = t.wheelDelta), t.shiftKey ? [-a, -e] : [e, a]
                        }(a),
                        l = n[0],
                        s = n[1];
                    (function(e, a) {
                        var o = t.querySelector("textarea:hover, select[multiple]:hover, .ps-child:hover");
                        if (o) {
                            var n = window.getComputedStyle(o);
                            if (![n.overflow, n.overflowX, n.overflowY].join("").match(/(scroll|auto)/)) return !1;
                            var r = o.scrollHeight - o.clientHeight;
                            if (r > 0 && !(0 === o.scrollTop && a > 0 || o.scrollTop === r && a < 0)) return !0;
                            var i = o.scrollLeft - o.clientWidth;
                            if (i > 0 && !(0 === o.scrollLeft && e < 0 || o.scrollLeft === i && e > 0)) return !0
                        }
                        return !1
                    })(l, s) || (o = !1, e.settings.useBothWheelAxes ? e.scrollbarYActive && !e.scrollbarXActive ? (s ? i(t, "top", t.scrollTop - s * e.settings.wheelSpeed) : i(t, "top", t.scrollTop + l * e.settings.wheelSpeed), o = !0) : e.scrollbarXActive && !e.scrollbarYActive && (l ? i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed) : i(t, "left", t.scrollLeft - s * e.settings.wheelSpeed), o = !0) : (i(t, "top", t.scrollTop - s * e.settings.wheelSpeed), i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed)), r(t), (o = o || function(a, o) {
                            var n = t.scrollTop;
                            if (0 === a) {
                                if (!e.scrollbarYActive) return !1;
                                if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                            }
                            var r = t.scrollLeft;
                            if (0 === o) {
                                if (!e.scrollbarXActive) return !1;
                                if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                            }
                            return !0
                        }(l, s)) && (a.stopPropagation(), a.preventDefault()))
                }
                var o = !1;
                void 0 !== window.onwheel ? e.event.bind(t, "wheel", a) : void 0 !== window.onmousewheel && e.event.bind(t, "mousewheel", a)
            }
            var n = t("../instances"),
                r = t("../update-geometry"),
                i = t("../update-scroll");
            e.exports = function(t) {
                o(t, n.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        14: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry");
            e.exports = function(t) {
                ! function(t, e) {
                    e.event.bind(t, "scroll", function() {
                        n(t)
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19
        }],
        15: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a() {
                    s || (s = setInterval(function() {
                        return r.get(t) ? (l(t, "top", t.scrollTop + c.top), l(t, "left", t.scrollLeft + c.left), void i(t)) : void clearInterval(s)
                    }, 50))
                }

                function o() {
                    s && (clearInterval(s), s = null), n.stopScrolling(t)
                }
                var s = null,
                    c = {
                        top: 0,
                        left: 0
                    },
                    d = !1;
                e.event.bind(e.ownerDocument, "selectionchange", function() {
                    t.contains(function() {
                        var t = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
                        return 0 === t.toString().length ? null : t.getRangeAt(0).commonAncestorContainer
                    }()) ? d = !0 : (d = !1, o())
                }), e.event.bind(window, "mouseup", function() {
                    d && (d = !1, o())
                }), e.event.bind(window, "keyup", function() {
                    d && (d = !1, o())
                }), e.event.bind(window, "mousemove", function(e) {
                    if (d) {
                        var r = {
                                x: e.pageX,
                                y: e.pageY
                            },
                            i = {
                                left: t.offsetLeft,
                                right: t.offsetLeft + t.offsetWidth,
                                top: t.offsetTop,
                                bottom: t.offsetTop + t.offsetHeight
                            };
                        r.x < i.left + 3 ? (c.left = -5, n.startScrolling(t, "x")) : r.x > i.right - 3 ? (c.left = 5, n.startScrolling(t, "x")) : c.left = 0, r.y < i.top + 3 ? (c.top = i.top + 3 - r.y < 5 ? -5 : -20, n.startScrolling(t, "y")) : r.y > i.bottom - 3 ? (c.top = r.y - i.bottom + 3 < 5 ? 5 : 20, n.startScrolling(t, "y")) : c.top = 0, 0 === c.top && 0 === c.left ? o() : a()
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                o(t, r.get(t))
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        16: [function(t, e, a) {
            "use strict";
            var o = t("../../lib/helper"),
                n = t("../instances"),
                r = t("../update-geometry"),
                i = t("../update-scroll");
            e.exports = function(t) {
                if (o.env.supportsTouch || o.env.supportsIePointer) {
                    ! function(t, e, a, o) {
                        function l(a, o) {
                            var n = t.scrollTop,
                                r = t.scrollLeft,
                                i = Math.abs(a),
                                l = Math.abs(o);
                            if (l > i) {
                                if (o < 0 && n === e.contentHeight - e.containerHeight || o > 0 && 0 === n) return !e.settings.swipePropagation
                            } else if (i > l && (a < 0 && r === e.contentWidth - e.containerWidth || a > 0 && 0 === r)) return !e.settings.swipePropagation;
                            return !0
                        }

                        function s(e, a) {
                            i(t, "top", t.scrollTop - a), i(t, "left", t.scrollLeft - e), r(t)
                        }

                        function c() {
                            w = !0
                        }

                        function d() {
                            w = !1
                        }

                        function u(t) {
                            return t.targetTouches ? t.targetTouches[0] : t
                        }

                        function f(t) {
                            return !(!t.targetTouches || 1 !== t.targetTouches.length) || !(!t.pointerType || "mouse" === t.pointerType || t.pointerType === t.MSPOINTER_TYPE_MOUSE)
                        }

                        function p(t) {
                            if (f(t)) {
                                x = !0;
                                var e = u(t);
                                v.pageX = e.pageX, v.pageY = e.pageY, g = (new Date).getTime(), null !== y && clearInterval(y), t.stopPropagation()
                            }
                        }

                        function h(t) {
                            if (!x && e.settings.swipePropagation && p(t), !w && x && f(t)) {
                                var a = u(t),
                                    o = {
                                        pageX: a.pageX,
                                        pageY: a.pageY
                                    },
                                    n = o.pageX - v.pageX,
                                    r = o.pageY - v.pageY;
                                s(n, r), v = o;
                                var i = (new Date).getTime(),
                                    c = i - g;
                                c > 0 && (b.x = n / c, b.y = r / c, g = i), l(n, r) && (t.stopPropagation(), t.preventDefault())
                            }
                        }

                        function m() {
                            !w && x && (x = !1, e.settings.swipeEasing && (clearInterval(y), y = setInterval(function() {
                                return n.get(t) && (b.x || b.y) ? Math.abs(b.x) < .01 && Math.abs(b.y) < .01 ? void clearInterval(y) : (s(30 * b.x, 30 * b.y), b.x *= .8, void(b.y *= .8)) : void clearInterval(y)
                            }, 10)))
                        }
                        var v = {},
                            g = 0,
                            b = {},
                            y = null,
                            w = !1,
                            x = !1;
                        a ? (e.event.bind(window, "touchstart", c), e.event.bind(window, "touchend", d), e.event.bind(t, "touchstart", p), e.event.bind(t, "touchmove", h), e.event.bind(t, "touchend", m)) : o && (window.PointerEvent ? (e.event.bind(window, "pointerdown", c), e.event.bind(window, "pointerup", d), e.event.bind(t, "pointerdown", p), e.event.bind(t, "pointermove", h), e.event.bind(t, "pointerup", m)) : window.MSPointerEvent && (e.event.bind(window, "MSPointerDown", c), e.event.bind(window, "MSPointerUp", d), e.event.bind(t, "MSPointerDown", p), e.event.bind(t, "MSPointerMove", h), e.event.bind(t, "MSPointerUp", m)))
                    }(t, n.get(t), o.env.supportsTouch, o.env.supportsIePointer)
                }
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        17: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/class"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = {
                    "click-rail": t("./handler/click-rail"),
                    "drag-scrollbar": t("./handler/drag-scrollbar"),
                    keyboard: t("./handler/keyboard"),
                    wheel: t("./handler/mouse-wheel"),
                    touch: t("./handler/touch"),
                    selection: t("./handler/selection")
                },
                s = t("./handler/native-scroll");
            e.exports = function(t, e) {
                e = "object" == typeof e ? e : {}, n.add(t, "ps");
                var a = r.add(t);
                a.settings = o.extend(a.settings, e), n.add(t, "ps--theme_" + a.settings.theme), a.settings.handlers.forEach(function(e) {
                    l[e](t)
                }), s(t), i(t)
            }
        }, {
            "../lib/class": 2,
            "../lib/helper": 6,
            "./handler/click-rail": 10,
            "./handler/drag-scrollbar": 11,
            "./handler/keyboard": 12,
            "./handler/mouse-wheel": 13,
            "./handler/native-scroll": 14,
            "./handler/selection": 15,
            "./handler/touch": 16,
            "./instances": 18,
            "./update-geometry": 19
        }],
        18: [function(t, e, a) {
            "use strict";

            function o(t) {
                return t.getAttribute("data-ps-id")
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("./default-setting"),
                l = t("../lib/dom"),
                s = t("../lib/event-manager"),
                c = t("../lib/guid"),
                d = {};
            a.add = function(t) {
                var e = c();
                return function(t, e) {
                    t.setAttribute("data-ps-id", e)
                }(t, e), d[e] = new function(t) {
                    function e() {
                        r.add(t, "ps--focus")
                    }

                    function a() {
                        r.remove(t, "ps--focus")
                    }
                    var o = this;
                    o.settings = n.clone(i), o.containerWidth = null, o.containerHeight = null, o.contentWidth = null, o.contentHeight = null, o.isRtl = "rtl" === l.css(t, "direction"), o.isNegativeScroll = function() {
                        var e = t.scrollLeft,
                            a = null;
                        return t.scrollLeft = -1, a = t.scrollLeft < 0, t.scrollLeft = e, a
                    }(), o.negativeScrollAdjustment = o.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, o.event = new s, o.ownerDocument = t.ownerDocument || document, o.scrollbarXRail = l.appendTo(l.e("div", "ps__scrollbar-x-rail"), t), o.scrollbarX = l.appendTo(l.e("div", "ps__scrollbar-x"), o.scrollbarXRail), o.scrollbarX.setAttribute("tabindex", 0), o.event.bind(o.scrollbarX, "focus", e), o.event.bind(o.scrollbarX, "blur", a), o.scrollbarXActive = null, o.scrollbarXWidth = null, o.scrollbarXLeft = null, o.scrollbarXBottom = n.toInt(l.css(o.scrollbarXRail, "bottom")), o.isScrollbarXUsingBottom = o.scrollbarXBottom == o.scrollbarXBottom, o.scrollbarXTop = o.isScrollbarXUsingBottom ? null : n.toInt(l.css(o.scrollbarXRail, "top")), o.railBorderXWidth = n.toInt(l.css(o.scrollbarXRail, "borderLeftWidth")) + n.toInt(l.css(o.scrollbarXRail, "borderRightWidth")), l.css(o.scrollbarXRail, "display", "block"), o.railXMarginWidth = n.toInt(l.css(o.scrollbarXRail, "marginLeft")) + n.toInt(l.css(o.scrollbarXRail, "marginRight")), l.css(o.scrollbarXRail, "display", ""), o.railXWidth = null, o.railXRatio = null, o.scrollbarYRail = l.appendTo(l.e("div", "ps__scrollbar-y-rail"), t), o.scrollbarY = l.appendTo(l.e("div", "ps__scrollbar-y"), o.scrollbarYRail), o.scrollbarY.setAttribute("tabindex", 0), o.event.bind(o.scrollbarY, "focus", e), o.event.bind(o.scrollbarY, "blur", a), o.scrollbarYActive = null, o.scrollbarYHeight = null, o.scrollbarYTop = null, o.scrollbarYRight = n.toInt(l.css(o.scrollbarYRail, "right")), o.isScrollbarYUsingRight = o.scrollbarYRight == o.scrollbarYRight, o.scrollbarYLeft = o.isScrollbarYUsingRight ? null : n.toInt(l.css(o.scrollbarYRail, "left")), o.scrollbarYOuterWidth = o.isRtl ? n.outerWidth(o.scrollbarY) : null, o.railBorderYWidth = n.toInt(l.css(o.scrollbarYRail, "borderTopWidth")) + n.toInt(l.css(o.scrollbarYRail, "borderBottomWidth")), l.css(o.scrollbarYRail, "display", "block"), o.railYMarginHeight = n.toInt(l.css(o.scrollbarYRail, "marginTop")) + n.toInt(l.css(o.scrollbarYRail, "marginBottom")), l.css(o.scrollbarYRail, "display", ""), o.railYHeight = null, o.railYRatio = null
                }(t), d[e]
            }, a.remove = function(t) {
                delete d[o(t)],
                    function(t) {
                        t.removeAttribute("data-ps-id")
                    }(t)
            }, a.get = function(t) {
                return d[o(t)]
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/event-manager": 4,
            "../lib/guid": 5,
            "../lib/helper": 6,
            "./default-setting": 8
        }],
        19: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                return t.settings.minScrollbarLength && (e = Math.max(e, t.settings.minScrollbarLength)), t.settings.maxScrollbarLength && (e = Math.min(e, t.settings.maxScrollbarLength)), e
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("../lib/dom"),
                l = t("./instances"),
                s = t("./update-scroll");
            e.exports = function(t) {
                var e = l.get(t);
                e.containerWidth = t.clientWidth, e.containerHeight = t.clientHeight, e.contentWidth = t.scrollWidth, e.contentHeight = t.scrollHeight;
                var a;
                t.contains(e.scrollbarXRail) || ((a = i.queryChildren(t, ".ps__scrollbar-x-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarXRail, t)), t.contains(e.scrollbarYRail) || ((a = i.queryChildren(t, ".ps__scrollbar-y-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarYRail, t)), !e.settings.suppressScrollX && e.containerWidth + e.settings.scrollXMarginOffset < e.contentWidth ? (e.scrollbarXActive = !0, e.railXWidth = e.containerWidth - e.railXMarginWidth, e.railXRatio = e.containerWidth / e.railXWidth, e.scrollbarXWidth = o(e, n.toInt(e.railXWidth * e.containerWidth / e.contentWidth)), e.scrollbarXLeft = n.toInt((e.negativeScrollAdjustment + t.scrollLeft) * (e.railXWidth - e.scrollbarXWidth) / (e.contentWidth - e.containerWidth))) : e.scrollbarXActive = !1, !e.settings.suppressScrollY && e.containerHeight + e.settings.scrollYMarginOffset < e.contentHeight ? (e.scrollbarYActive = !0, e.railYHeight = e.containerHeight - e.railYMarginHeight, e.railYRatio = e.containerHeight / e.railYHeight, e.scrollbarYHeight = o(e, n.toInt(e.railYHeight * e.containerHeight / e.contentHeight)), e.scrollbarYTop = n.toInt(t.scrollTop * (e.railYHeight - e.scrollbarYHeight) / (e.contentHeight - e.containerHeight))) : e.scrollbarYActive = !1, e.scrollbarXLeft >= e.railXWidth - e.scrollbarXWidth && (e.scrollbarXLeft = e.railXWidth - e.scrollbarXWidth), e.scrollbarYTop >= e.railYHeight - e.scrollbarYHeight && (e.scrollbarYTop = e.railYHeight - e.scrollbarYHeight),
                    function(t, e) {
                        var a = {
                            width: e.railXWidth
                        };
                        e.isRtl ? a.left = e.negativeScrollAdjustment + t.scrollLeft + e.containerWidth - e.contentWidth : a.left = t.scrollLeft, e.isScrollbarXUsingBottom ? a.bottom = e.scrollbarXBottom - t.scrollTop : a.top = e.scrollbarXTop + t.scrollTop, i.css(e.scrollbarXRail, a);
                        var o = {
                            top: t.scrollTop,
                            height: e.railYHeight
                        };
                        e.isScrollbarYUsingRight ? e.isRtl ? o.right = e.contentWidth - (e.negativeScrollAdjustment + t.scrollLeft) - e.scrollbarYRight - e.scrollbarYOuterWidth : o.right = e.scrollbarYRight - t.scrollLeft : e.isRtl ? o.left = e.negativeScrollAdjustment + t.scrollLeft + 2 * e.containerWidth - e.contentWidth - e.scrollbarYLeft - e.scrollbarYOuterWidth : o.left = e.scrollbarYLeft + t.scrollLeft, i.css(e.scrollbarYRail, o), i.css(e.scrollbarX, {
                            left: e.scrollbarXLeft,
                            width: e.scrollbarXWidth - e.railBorderXWidth
                        }), i.css(e.scrollbarY, {
                            top: e.scrollbarYTop,
                            height: e.scrollbarYHeight - e.railBorderYWidth
                        })
                    }(t, e), e.scrollbarXActive ? r.add(t, "ps--active-x") : (r.remove(t, "ps--active-x"), e.scrollbarXWidth = 0, e.scrollbarXLeft = 0, s(t, "left", 0)), e.scrollbarYActive ? r.add(t, "ps--active-y") : (r.remove(t, "ps--active-y"), e.scrollbarYHeight = 0, e.scrollbarYTop = 0, s(t, "top", 0))
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-scroll": 20
        }],
        20: [function(t, e, a) {
            "use strict";
            var o = t("./instances"),
                n = function(t) {
                    var e = document.createEvent("Event");
                    return e.initEvent(t, !0, !0), e
                };
            e.exports = function(t, e, a) {
                if (void 0 === t) throw "You must provide an element to the update-scroll function";
                if (void 0 === e) throw "You must provide an axis to the update-scroll function";
                if (void 0 === a) throw "You must provide a value to the update-scroll function";
                "top" === e && a <= 0 && (t.scrollTop = a = 0, t.dispatchEvent(n("ps-y-reach-start"))), "left" === e && a <= 0 && (t.scrollLeft = a = 0, t.dispatchEvent(n("ps-x-reach-start")));
                var r = o.get(t);
                "top" === e && a >= r.contentHeight - r.containerHeight && ((a = r.contentHeight - r.containerHeight) - t.scrollTop <= 1 ? a = t.scrollTop : t.scrollTop = a, t.dispatchEvent(n("ps-y-reach-end"))), "left" === e && a >= r.contentWidth - r.containerWidth && ((a = r.contentWidth - r.containerWidth) - t.scrollLeft <= 1 ? a = t.scrollLeft : t.scrollLeft = a, t.dispatchEvent(n("ps-x-reach-end"))), void 0 === r.lastTop && (r.lastTop = t.scrollTop), void 0 === r.lastLeft && (r.lastLeft = t.scrollLeft), "top" === e && a < r.lastTop && t.dispatchEvent(n("ps-scroll-up")), "top" === e && a > r.lastTop && t.dispatchEvent(n("ps-scroll-down")), "left" === e && a < r.lastLeft && t.dispatchEvent(n("ps-scroll-left")), "left" === e && a > r.lastLeft && t.dispatchEvent(n("ps-scroll-right")), "top" === e && a !== r.lastTop && (t.scrollTop = r.lastTop = a, t.dispatchEvent(n("ps-scroll-y"))), "left" === e && a !== r.lastLeft && (t.scrollLeft = r.lastLeft = a, t.dispatchEvent(n("ps-scroll-x")))
            }
        }, {
            "./instances": 18
        }],
        21: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = t("./update-scroll");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.negativeScrollAdjustment = e.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, n.css(e.scrollbarXRail, "display", "block"), n.css(e.scrollbarYRail, "display", "block"), e.railXMarginWidth = o.toInt(n.css(e.scrollbarXRail, "marginLeft")) + o.toInt(n.css(e.scrollbarXRail, "marginRight")), e.railYMarginHeight = o.toInt(n.css(e.scrollbarYRail, "marginTop")) + o.toInt(n.css(e.scrollbarYRail, "marginBottom")), n.css(e.scrollbarXRail, "display", "none"), n.css(e.scrollbarYRail, "display", "none"), i(t), l(t, "top", t.scrollTop), l(t, "left", t.scrollLeft), n.css(e.scrollbarXRail, "display", ""), n.css(e.scrollbarYRail, "display", ""))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-geometry": 19,
            "./update-scroll": 20
        }]
    }, {}, [1]),
    function t(e, a, o) {
        function n(i, l) {
            if (!a[i]) {
                if (!e[i]) {
                    var s = "function" == typeof require && require;
                    if (!l && s) return s(i, !0);
                    if (r) return r(i, !0);
                    var c = new Error("Cannot find module '" + i + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var d = a[i] = {
                    exports: {}
                };
                e[i][0].call(d.exports, function(t) {
                    var a = e[i][1][t];
                    return n(a || t)
                }, d, d.exports, t, e, a, o)
            }
            return a[i].exports
        }
        for (var r = "function" == typeof require && require, i = 0; i < o.length; i++) n(o[i]);
        return n
    }({
        1: [function(t, e, a) {
            "use strict";
            var o = t("../main");
            "function" == typeof define && define.amd ? define(o) : (window.PerfectScrollbar = o, void 0 === window.Ps && (window.Ps = o))
        }, {
            "../main": 7
        }],
        2: [function(t, e, a) {
            "use strict";
            a.add = function(t, e) {
                t.classList ? t.classList.add(e) : function(t, e) {
                    var a = t.className.split(" ");
                    a.indexOf(e) < 0 && a.push(e), t.className = a.join(" ")
                }(t, e)
            }, a.remove = function(t, e) {
                t.classList ? t.classList.remove(e) : function(t, e) {
                    var a = t.className.split(" "),
                        o = a.indexOf(e);
                    o >= 0 && a.splice(o, 1), t.className = a.join(" ")
                }(t, e)
            }, a.list = function(t) {
                return t.classList ? Array.prototype.slice.apply(t.classList) : t.className.split(" ")
            }
        }, {}],
        3: [function(t, e, a) {
            "use strict";
            var o = {};
            o.e = function(t, e) {
                var a = document.createElement(t);
                return a.className = e, a
            }, o.appendTo = function(t, e) {
                return e.appendChild(t), t
            }, o.css = function(t, e, a) {
                return "object" == typeof e ? function(t, e) {
                    for (var a in e) {
                        var o = e[a];
                        "number" == typeof o && (o = o.toString() + "px"), t.style[a] = o
                    }
                    return t
                }(t, e) : void 0 === a ? function(t, e) {
                    return window.getComputedStyle(t)[e]
                }(t, e) : function(t, e, a) {
                    return "number" == typeof a && (a = a.toString() + "px"), t.style[e] = a, t
                }(t, e, a)
            }, o.matches = function(t, e) {
                return void 0 !== t.matches ? t.matches(e) : void 0 !== t.matchesSelector ? t.matchesSelector(e) : void 0 !== t.webkitMatchesSelector ? t.webkitMatchesSelector(e) : void 0 !== t.mozMatchesSelector ? t.mozMatchesSelector(e) : void 0 !== t.msMatchesSelector ? t.msMatchesSelector(e) : void 0
            }, o.remove = function(t) {
                void 0 !== t.remove ? t.remove() : t.parentNode && t.parentNode.removeChild(t)
            }, o.queryChildren = function(t, e) {
                return Array.prototype.filter.call(t.childNodes, function(t) {
                    return o.matches(t, e)
                })
            }, e.exports = o
        }, {}],
        4: [function(t, e, a) {
            "use strict";
            var o = function(t) {
                this.element = t, this.events = {}
            };
            o.prototype.bind = function(t, e) {
                void 0 === this.events[t] && (this.events[t] = []), this.events[t].push(e), this.element.addEventListener(t, e, !1)
            }, o.prototype.unbind = function(t, e) {
                var a = void 0 !== e;
                this.events[t] = this.events[t].filter(function(o) {
                    return !(!a || o === e) || (this.element.removeEventListener(t, o, !1), !1)
                }, this)
            }, o.prototype.unbindAll = function() {
                for (var t in this.events) this.unbind(t)
            };
            var n = function() {
                this.eventElements = []
            };
            n.prototype.eventElement = function(t) {
                var e = this.eventElements.filter(function(e) {
                    return e.element === t
                })[0];
                return void 0 === e && (e = new o(t), this.eventElements.push(e)), e
            }, n.prototype.bind = function(t, e, a) {
                this.eventElement(t).bind(e, a)
            }, n.prototype.unbind = function(t, e, a) {
                this.eventElement(t).unbind(e, a)
            }, n.prototype.unbindAll = function() {
                for (var t = 0; t < this.eventElements.length; t++) this.eventElements[t].unbindAll()
            }, n.prototype.once = function(t, e, a) {
                var o = this.eventElement(t),
                    n = function(t) {
                        o.unbind(e, n), a(t)
                    };
                o.bind(e, n)
            }, e.exports = n
        }, {}],
        5: [function(t, e, a) {
            "use strict";
            e.exports = function() {
                function t() {
                    return Math.floor(65536 * (1 + Math.random())).toString(16).substring(1)
                }
                return function() {
                    return t() + t() + "-" + t() + "-" + t() + "-" + t() + "-" + t() + t() + t()
                }
            }()
        }, {}],
        6: [function(t, e, a) {
            "use strict";

            function o(t) {
                return function(e, a) {
                    t(e, "ps--in-scrolling"), void 0 !== a ? t(e, "ps--" + a) : (t(e, "ps--x"), t(e, "ps--y"))
                }
            }
            var n = t("./class"),
                r = t("./dom"),
                i = a.toInt = function(t) {
                    return parseInt(t, 10) || 0
                },
                l = a.clone = function(t) {
                    if (t) {
                        if (Array.isArray(t)) return t.map(l);
                        if ("object" == typeof t) {
                            var e = {};
                            for (var a in t) e[a] = l(t[a]);
                            return e
                        }
                        return t
                    }
                    return null
                };
            a.extend = function(t, e) {
                var a = l(t);
                for (var o in e) a[o] = l(e[o]);
                return a
            }, a.isEditable = function(t) {
                return r.matches(t, "input,[contenteditable]") || r.matches(t, "select,[contenteditable]") || r.matches(t, "textarea,[contenteditable]") || r.matches(t, "button,[contenteditable]")
            }, a.removePsClasses = function(t) {
                for (var e = n.list(t), a = 0; a < e.length; a++) {
                    var o = e[a];
                    0 === o.indexOf("ps-") && n.remove(t, o)
                }
            }, a.outerWidth = function(t) {
                return i(r.css(t, "width")) + i(r.css(t, "paddingLeft")) + i(r.css(t, "paddingRight")) + i(r.css(t, "borderLeftWidth")) + i(r.css(t, "borderRightWidth"))
            }, a.startScrolling = o(n.add), a.stopScrolling = o(n.remove), a.env = {
                isWebKit: "undefined" != typeof document && "WebkitAppearance" in document.documentElement.style,
                supportsTouch: "undefined" != typeof window && ("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
                supportsIePointer: "undefined" != typeof window && null !== window.navigator.msMaxTouchPoints
            }
        }, {
            "./class": 2,
            "./dom": 3
        }],
        7: [function(t, e, a) {
            "use strict";
            var o = t("./plugin/destroy"),
                n = t("./plugin/initialize"),
                r = t("./plugin/update");
            e.exports = {
                initialize: n,
                update: r,
                destroy: o
            }
        }, {
            "./plugin/destroy": 9,
            "./plugin/initialize": 17,
            "./plugin/update": 21
        }],
        8: [function(t, e, a) {
            "use strict";
            e.exports = {
                handlers: ["click-rail", "drag-scrollbar", "keyboard", "wheel", "touch"],
                maxScrollbarLength: null,
                minScrollbarLength: null,
                scrollXMarginOffset: 0,
                scrollYMarginOffset: 0,
                suppressScrollX: !1,
                suppressScrollY: !1,
                swipePropagation: !0,
                swipeEasing: !0,
                useBothWheelAxes: !1,
                wheelPropagation: !1,
                wheelSpeed: 1,
                theme: "default"
            }
        }, {}],
        9: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.event.unbindAll(), n.remove(e.scrollbarX), n.remove(e.scrollbarY), n.remove(e.scrollbarXRail), n.remove(e.scrollbarYRail), o.removePsClasses(t), r.remove(t))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18
        }],
        10: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry"),
                r = t("../update-scroll");
            e.exports = function(t) {
                ! function(t, e) {
                    function a(t) {
                        return t.getBoundingClientRect()
                    }
                    var o = function(t) {
                        t.stopPropagation()
                    };
                    e.event.bind(e.scrollbarY, "click", o), e.event.bind(e.scrollbarYRail, "click", function(o) {
                        var i = o.pageY - window.pageYOffset - a(e.scrollbarYRail).top > e.scrollbarYTop ? 1 : -1;
                        r(t, "top", t.scrollTop + i * e.containerHeight), n(t), o.stopPropagation()
                    }), e.event.bind(e.scrollbarX, "click", o), e.event.bind(e.scrollbarXRail, "click", function(o) {
                        var i = o.pageX - window.pageXOffset - a(e.scrollbarXRail).left > e.scrollbarXLeft ? 1 : -1;
                        r(t, "left", t.scrollLeft + i * e.containerWidth), n(t), o.stopPropagation()
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        11: [function(t, e, a) {
            "use strict";
            var o = t("../../lib/helper"),
                n = t("../../lib/dom"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                var e = r.get(t);
                (function(t, e) {
                    function a(a) {
                        var n = r + a * e.railXRatio,
                            i = Math.max(0, e.scrollbarXRail.getBoundingClientRect().left) + e.railXRatio * (e.railXWidth - e.scrollbarXWidth);
                        e.scrollbarXLeft = n < 0 ? 0 : n > i ? i : n;
                        var s = o.toInt(e.scrollbarXLeft * (e.contentWidth - e.containerWidth) / (e.containerWidth - e.railXRatio * e.scrollbarXWidth)) - e.negativeScrollAdjustment;
                        l(t, "left", s)
                    }
                    var r = null,
                        s = null,
                        c = function(e) {
                            a(e.pageX - s), i(t), e.stopPropagation(), e.preventDefault()
                        },
                        d = function() {
                            o.stopScrolling(t, "x"), e.event.unbind(e.ownerDocument, "mousemove", c)
                        };
                    e.event.bind(e.scrollbarX, "mousedown", function(a) {
                        s = a.pageX, r = o.toInt(n.css(e.scrollbarX, "left")) * e.railXRatio, o.startScrolling(t, "x"), e.event.bind(e.ownerDocument, "mousemove", c), e.event.once(e.ownerDocument, "mouseup", d), a.stopPropagation(), a.preventDefault()
                    })
                })(t, e),
                    function(t, e) {
                        function a(a) {
                            var n = r + a * e.railYRatio,
                                i = Math.max(0, e.scrollbarYRail.getBoundingClientRect().top) + e.railYRatio * (e.railYHeight - e.scrollbarYHeight);
                            e.scrollbarYTop = n < 0 ? 0 : n > i ? i : n;
                            var s = o.toInt(e.scrollbarYTop * (e.contentHeight - e.containerHeight) / (e.containerHeight - e.railYRatio * e.scrollbarYHeight));
                            l(t, "top", s)
                        }
                        var r = null,
                            s = null,
                            c = function(e) {
                                a(e.pageY - s), i(t), e.stopPropagation(), e.preventDefault()
                            },
                            d = function() {
                                o.stopScrolling(t, "y"), e.event.unbind(e.ownerDocument, "mousemove", c)
                            };
                        e.event.bind(e.scrollbarY, "mousedown", function(a) {
                            s = a.pageY, r = o.toInt(n.css(e.scrollbarY, "top")) * e.railYRatio, o.startScrolling(t, "y"), e.event.bind(e.ownerDocument, "mousemove", c), e.event.once(e.ownerDocument, "mouseup", d), a.stopPropagation(), a.preventDefault()
                        })
                    }(t, e)
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        12: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                var a = !1;
                e.event.bind(t, "mouseenter", function() {
                    a = !0
                }), e.event.bind(t, "mouseleave", function() {
                    a = !1
                });
                var o = !1;
                e.event.bind(e.ownerDocument, "keydown", function(i) {
                    if (!(i.isDefaultPrevented && i.isDefaultPrevented() || i.defaultPrevented)) {
                        var c = r.matches(e.scrollbarX, ":focus") || r.matches(e.scrollbarY, ":focus");
                        if (a || c) {
                            var d = document.activeElement ? document.activeElement : e.ownerDocument.activeElement;
                            if (d) {
                                if ("IFRAME" === d.tagName) d = d.contentDocument.activeElement;
                                else
                                    for (; d.shadowRoot;) d = d.shadowRoot.activeElement;
                                if (n.isEditable(d)) return
                            }
                            var u = 0,
                                f = 0;
                            switch (i.which) {
                                case 37:
                                    u = i.metaKey ? -e.contentWidth : i.altKey ? -e.containerWidth : -30;
                                    break;
                                case 38:
                                    f = i.metaKey ? e.contentHeight : i.altKey ? e.containerHeight : 30;
                                    break;
                                case 39:
                                    u = i.metaKey ? e.contentWidth : i.altKey ? e.containerWidth : 30;
                                    break;
                                case 40:
                                    f = i.metaKey ? -e.contentHeight : i.altKey ? -e.containerHeight : -30;
                                    break;
                                case 33:
                                    f = 90;
                                    break;
                                case 32:
                                    f = i.shiftKey ? 90 : -90;
                                    break;
                                case 34:
                                    f = -90;
                                    break;
                                case 35:
                                    f = i.ctrlKey ? -e.contentHeight : -e.containerHeight;
                                    break;
                                case 36:
                                    f = i.ctrlKey ? t.scrollTop : e.containerHeight;
                                    break;
                                default:
                                    return
                            }
                            s(t, "top", t.scrollTop - f), s(t, "left", t.scrollLeft + u), l(t), (o = function(a, o) {
                                var n = t.scrollTop;
                                if (0 === a) {
                                    if (!e.scrollbarYActive) return !1;
                                    if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                                }
                                var r = t.scrollLeft;
                                if (0 === o) {
                                    if (!e.scrollbarXActive) return !1;
                                    if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                                }
                                return !0
                            }(u, f)) && i.preventDefault()
                        }
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../../lib/dom"),
                i = t("../instances"),
                l = t("../update-geometry"),
                s = t("../update-scroll");
            e.exports = function(t) {
                o(t, i.get(t))
            }
        }, {
            "../../lib/dom": 3,
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        13: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a(a) {
                    var n = function(t) {
                            var e = t.deltaX,
                                a = -1 * t.deltaY;
                            return void 0 !== e && void 0 !== a || (e = -1 * t.wheelDeltaX / 6, a = t.wheelDeltaY / 6), t.deltaMode && 1 === t.deltaMode && (e *= 10, a *= 10), e != e && a != a && (e = 0, a = t.wheelDelta), t.shiftKey ? [-a, -e] : [e, a]
                        }(a),
                        l = n[0],
                        s = n[1];
                    (function(e, a) {
                        var o = t.querySelector("textarea:hover, select[multiple]:hover, .ps-child:hover");
                        if (o) {
                            var n = window.getComputedStyle(o);
                            if (![n.overflow, n.overflowX, n.overflowY].join("").match(/(scroll|auto)/)) return !1;
                            var r = o.scrollHeight - o.clientHeight;
                            if (r > 0 && !(0 === o.scrollTop && a > 0 || o.scrollTop === r && a < 0)) return !0;
                            var i = o.scrollLeft - o.clientWidth;
                            if (i > 0 && !(0 === o.scrollLeft && e < 0 || o.scrollLeft === i && e > 0)) return !0
                        }
                        return !1
                    })(l, s) || (o = !1, e.settings.useBothWheelAxes ? e.scrollbarYActive && !e.scrollbarXActive ? (s ? i(t, "top", t.scrollTop - s * e.settings.wheelSpeed) : i(t, "top", t.scrollTop + l * e.settings.wheelSpeed), o = !0) : e.scrollbarXActive && !e.scrollbarYActive && (l ? i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed) : i(t, "left", t.scrollLeft - s * e.settings.wheelSpeed), o = !0) : (i(t, "top", t.scrollTop - s * e.settings.wheelSpeed), i(t, "left", t.scrollLeft + l * e.settings.wheelSpeed)), r(t), (o = o || function(a, o) {
                            var n = t.scrollTop;
                            if (0 === a) {
                                if (!e.scrollbarYActive) return !1;
                                if (0 === n && o > 0 || n >= e.contentHeight - e.containerHeight && o < 0) return !e.settings.wheelPropagation
                            }
                            var r = t.scrollLeft;
                            if (0 === o) {
                                if (!e.scrollbarXActive) return !1;
                                if (0 === r && a < 0 || r >= e.contentWidth - e.containerWidth && a > 0) return !e.settings.wheelPropagation
                            }
                            return !0
                        }(l, s)) && (a.stopPropagation(), a.preventDefault()))
                }
                var o = !1;
                void 0 !== window.onwheel ? e.event.bind(t, "wheel", a) : void 0 !== window.onmousewheel && e.event.bind(t, "mousewheel", a)
            }
            var n = t("../instances"),
                r = t("../update-geometry"),
                i = t("../update-scroll");
            e.exports = function(t) {
                o(t, n.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        14: [function(t, e, a) {
            "use strict";
            var o = t("../instances"),
                n = t("../update-geometry");
            e.exports = function(t) {
                ! function(t, e) {
                    e.event.bind(t, "scroll", function() {
                        n(t)
                    })
                }(t, o.get(t))
            }
        }, {
            "../instances": 18,
            "../update-geometry": 19
        }],
        15: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                function a() {
                    s || (s = setInterval(function() {
                        return r.get(t) ? (l(t, "top", t.scrollTop + c.top), l(t, "left", t.scrollLeft + c.left), void i(t)) : void clearInterval(s)
                    }, 50))
                }

                function o() {
                    s && (clearInterval(s), s = null), n.stopScrolling(t)
                }
                var s = null,
                    c = {
                        top: 0,
                        left: 0
                    },
                    d = !1;
                e.event.bind(e.ownerDocument, "selectionchange", function() {
                    t.contains(function() {
                        var t = window.getSelection ? window.getSelection() : document.getSelection ? document.getSelection() : "";
                        return 0 === t.toString().length ? null : t.getRangeAt(0).commonAncestorContainer
                    }()) ? d = !0 : (d = !1, o())
                }), e.event.bind(window, "mouseup", function() {
                    d && (d = !1, o())
                }), e.event.bind(window, "keyup", function() {
                    d && (d = !1, o())
                }), e.event.bind(window, "mousemove", function(e) {
                    if (d) {
                        var r = {
                                x: e.pageX,
                                y: e.pageY
                            },
                            i = {
                                left: t.offsetLeft,
                                right: t.offsetLeft + t.offsetWidth,
                                top: t.offsetTop,
                                bottom: t.offsetTop + t.offsetHeight
                            };
                        r.x < i.left + 3 ? (c.left = -5, n.startScrolling(t, "x")) : r.x > i.right - 3 ? (c.left = 5, n.startScrolling(t, "x")) : c.left = 0, r.y < i.top + 3 ? (c.top = i.top + 3 - r.y < 5 ? -5 : -20, n.startScrolling(t, "y")) : r.y > i.bottom - 3 ? (c.top = r.y - i.bottom + 3 < 5 ? 5 : 20, n.startScrolling(t, "y")) : c.top = 0, 0 === c.top && 0 === c.left ? o() : a()
                    }
                })
            }
            var n = t("../../lib/helper"),
                r = t("../instances"),
                i = t("../update-geometry"),
                l = t("../update-scroll");
            e.exports = function(t) {
                o(t, r.get(t))
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        16: [function(t, e, a) {
            "use strict";
            var o = t("../../lib/helper"),
                n = t("../instances"),
                r = t("../update-geometry"),
                i = t("../update-scroll");
            e.exports = function(t) {
                if (o.env.supportsTouch || o.env.supportsIePointer) {
                    ! function(t, e, a, o) {
                        function l(a, o) {
                            var n = t.scrollTop,
                                r = t.scrollLeft,
                                i = Math.abs(a),
                                l = Math.abs(o);
                            if (l > i) {
                                if (o < 0 && n === e.contentHeight - e.containerHeight || o > 0 && 0 === n) return !e.settings.swipePropagation
                            } else if (i > l && (a < 0 && r === e.contentWidth - e.containerWidth || a > 0 && 0 === r)) return !e.settings.swipePropagation;
                            return !0
                        }

                        function s(e, a) {
                            i(t, "top", t.scrollTop - a), i(t, "left", t.scrollLeft - e), r(t)
                        }

                        function c() {
                            w = !0
                        }

                        function d() {
                            w = !1
                        }

                        function u(t) {
                            return t.targetTouches ? t.targetTouches[0] : t
                        }

                        function f(t) {
                            return !(!t.targetTouches || 1 !== t.targetTouches.length) || !(!t.pointerType || "mouse" === t.pointerType || t.pointerType === t.MSPOINTER_TYPE_MOUSE)
                        }

                        function p(t) {
                            if (f(t)) {
                                x = !0;
                                var e = u(t);
                                v.pageX = e.pageX, v.pageY = e.pageY, g = (new Date).getTime(), null !== y && clearInterval(y), t.stopPropagation()
                            }
                        }

                        function h(t) {
                            if (!x && e.settings.swipePropagation && p(t), !w && x && f(t)) {
                                var a = u(t),
                                    o = {
                                        pageX: a.pageX,
                                        pageY: a.pageY
                                    },
                                    n = o.pageX - v.pageX,
                                    r = o.pageY - v.pageY;
                                s(n, r), v = o;
                                var i = (new Date).getTime(),
                                    c = i - g;
                                c > 0 && (b.x = n / c, b.y = r / c, g = i), l(n, r) && (t.stopPropagation(), t.preventDefault())
                            }
                        }

                        function m() {
                            !w && x && (x = !1, e.settings.swipeEasing && (clearInterval(y), y = setInterval(function() {
                                return n.get(t) && (b.x || b.y) ? Math.abs(b.x) < .01 && Math.abs(b.y) < .01 ? void clearInterval(y) : (s(30 * b.x, 30 * b.y), b.x *= .8, void(b.y *= .8)) : void clearInterval(y)
                            }, 10)))
                        }
                        var v = {},
                            g = 0,
                            b = {},
                            y = null,
                            w = !1,
                            x = !1;
                        a ? (e.event.bind(window, "touchstart", c), e.event.bind(window, "touchend", d), e.event.bind(t, "touchstart", p), e.event.bind(t, "touchmove", h), e.event.bind(t, "touchend", m)) : o && (window.PointerEvent ? (e.event.bind(window, "pointerdown", c), e.event.bind(window, "pointerup", d), e.event.bind(t, "pointerdown", p), e.event.bind(t, "pointermove", h), e.event.bind(t, "pointerup", m)) : window.MSPointerEvent && (e.event.bind(window, "MSPointerDown", c), e.event.bind(window, "MSPointerUp", d), e.event.bind(t, "MSPointerDown", p), e.event.bind(t, "MSPointerMove", h), e.event.bind(t, "MSPointerUp", m)))
                    }(t, n.get(t), o.env.supportsTouch, o.env.supportsIePointer)
                }
            }
        }, {
            "../../lib/helper": 6,
            "../instances": 18,
            "../update-geometry": 19,
            "../update-scroll": 20
        }],
        17: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/class"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = {
                    "click-rail": t("./handler/click-rail"),
                    "drag-scrollbar": t("./handler/drag-scrollbar"),
                    keyboard: t("./handler/keyboard"),
                    wheel: t("./handler/mouse-wheel"),
                    touch: t("./handler/touch"),
                    selection: t("./handler/selection")
                },
                s = t("./handler/native-scroll");
            e.exports = function(t, e) {
                e = "object" == typeof e ? e : {}, n.add(t, "ps");
                var a = r.add(t);
                a.settings = o.extend(a.settings, e), n.add(t, "ps--theme_" + a.settings.theme), a.settings.handlers.forEach(function(e) {
                    l[e](t)
                }), s(t), i(t)
            }
        }, {
            "../lib/class": 2,
            "../lib/helper": 6,
            "./handler/click-rail": 10,
            "./handler/drag-scrollbar": 11,
            "./handler/keyboard": 12,
            "./handler/mouse-wheel": 13,
            "./handler/native-scroll": 14,
            "./handler/selection": 15,
            "./handler/touch": 16,
            "./instances": 18,
            "./update-geometry": 19
        }],
        18: [function(t, e, a) {
            "use strict";

            function o(t) {
                return t.getAttribute("data-ps-id")
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("./default-setting"),
                l = t("../lib/dom"),
                s = t("../lib/event-manager"),
                c = t("../lib/guid"),
                d = {};
            a.add = function(t) {
                var e = c();
                return function(t, e) {
                    t.setAttribute("data-ps-id", e)
                }(t, e), d[e] = new function(t) {
                    function e() {
                        r.add(t, "ps--focus")
                    }

                    function a() {
                        r.remove(t, "ps--focus")
                    }
                    var o = this;
                    o.settings = n.clone(i), o.containerWidth = null, o.containerHeight = null, o.contentWidth = null, o.contentHeight = null, o.isRtl = "rtl" === l.css(t, "direction"), o.isNegativeScroll = function() {
                        var e = t.scrollLeft,
                            a = null;
                        return t.scrollLeft = -1, a = t.scrollLeft < 0, t.scrollLeft = e, a
                    }(), o.negativeScrollAdjustment = o.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, o.event = new s, o.ownerDocument = t.ownerDocument || document, o.scrollbarXRail = l.appendTo(l.e("div", "ps__scrollbar-x-rail"), t), o.scrollbarX = l.appendTo(l.e("div", "ps__scrollbar-x"), o.scrollbarXRail), o.scrollbarX.setAttribute("tabindex", 0), o.event.bind(o.scrollbarX, "focus", e), o.event.bind(o.scrollbarX, "blur", a), o.scrollbarXActive = null, o.scrollbarXWidth = null, o.scrollbarXLeft = null, o.scrollbarXBottom = n.toInt(l.css(o.scrollbarXRail, "bottom")), o.isScrollbarXUsingBottom = o.scrollbarXBottom == o.scrollbarXBottom, o.scrollbarXTop = o.isScrollbarXUsingBottom ? null : n.toInt(l.css(o.scrollbarXRail, "top")), o.railBorderXWidth = n.toInt(l.css(o.scrollbarXRail, "borderLeftWidth")) + n.toInt(l.css(o.scrollbarXRail, "borderRightWidth")), l.css(o.scrollbarXRail, "display", "block"), o.railXMarginWidth = n.toInt(l.css(o.scrollbarXRail, "marginLeft")) + n.toInt(l.css(o.scrollbarXRail, "marginRight")), l.css(o.scrollbarXRail, "display", ""), o.railXWidth = null, o.railXRatio = null, o.scrollbarYRail = l.appendTo(l.e("div", "ps__scrollbar-y-rail"), t), o.scrollbarY = l.appendTo(l.e("div", "ps__scrollbar-y"), o.scrollbarYRail), o.scrollbarY.setAttribute("tabindex", 0), o.event.bind(o.scrollbarY, "focus", e), o.event.bind(o.scrollbarY, "blur", a), o.scrollbarYActive = null, o.scrollbarYHeight = null, o.scrollbarYTop = null, o.scrollbarYRight = n.toInt(l.css(o.scrollbarYRail, "right")), o.isScrollbarYUsingRight = o.scrollbarYRight == o.scrollbarYRight, o.scrollbarYLeft = o.isScrollbarYUsingRight ? null : n.toInt(l.css(o.scrollbarYRail, "left")), o.scrollbarYOuterWidth = o.isRtl ? n.outerWidth(o.scrollbarY) : null, o.railBorderYWidth = n.toInt(l.css(o.scrollbarYRail, "borderTopWidth")) + n.toInt(l.css(o.scrollbarYRail, "borderBottomWidth")), l.css(o.scrollbarYRail, "display", "block"), o.railYMarginHeight = n.toInt(l.css(o.scrollbarYRail, "marginTop")) + n.toInt(l.css(o.scrollbarYRail, "marginBottom")), l.css(o.scrollbarYRail, "display", ""), o.railYHeight = null, o.railYRatio = null
                }(t), d[e]
            }, a.remove = function(t) {
                delete d[o(t)],
                    function(t) {
                        t.removeAttribute("data-ps-id")
                    }(t)
            }, a.get = function(t) {
                return d[o(t)]
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/event-manager": 4,
            "../lib/guid": 5,
            "../lib/helper": 6,
            "./default-setting": 8
        }],
        19: [function(t, e, a) {
            "use strict";

            function o(t, e) {
                return t.settings.minScrollbarLength && (e = Math.max(e, t.settings.minScrollbarLength)), t.settings.maxScrollbarLength && (e = Math.min(e, t.settings.maxScrollbarLength)), e
            }
            var n = t("../lib/helper"),
                r = t("../lib/class"),
                i = t("../lib/dom"),
                l = t("./instances"),
                s = t("./update-scroll");
            e.exports = function(t) {
                var e = l.get(t);
                e.containerWidth = t.clientWidth, e.containerHeight = t.clientHeight, e.contentWidth = t.scrollWidth, e.contentHeight = t.scrollHeight;
                var a;
                t.contains(e.scrollbarXRail) || ((a = i.queryChildren(t, ".ps__scrollbar-x-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarXRail, t)), t.contains(e.scrollbarYRail) || ((a = i.queryChildren(t, ".ps__scrollbar-y-rail")).length > 0 && a.forEach(function(t) {
                    i.remove(t)
                }), i.appendTo(e.scrollbarYRail, t)), !e.settings.suppressScrollX && e.containerWidth + e.settings.scrollXMarginOffset < e.contentWidth ? (e.scrollbarXActive = !0, e.railXWidth = e.containerWidth - e.railXMarginWidth, e.railXRatio = e.containerWidth / e.railXWidth, e.scrollbarXWidth = o(e, n.toInt(e.railXWidth * e.containerWidth / e.contentWidth)), e.scrollbarXLeft = n.toInt((e.negativeScrollAdjustment + t.scrollLeft) * (e.railXWidth - e.scrollbarXWidth) / (e.contentWidth - e.containerWidth))) : e.scrollbarXActive = !1, !e.settings.suppressScrollY && e.containerHeight + e.settings.scrollYMarginOffset < e.contentHeight ? (e.scrollbarYActive = !0, e.railYHeight = e.containerHeight - e.railYMarginHeight, e.railYRatio = e.containerHeight / e.railYHeight, e.scrollbarYHeight = o(e, n.toInt(e.railYHeight * e.containerHeight / e.contentHeight)), e.scrollbarYTop = n.toInt(t.scrollTop * (e.railYHeight - e.scrollbarYHeight) / (e.contentHeight - e.containerHeight))) : e.scrollbarYActive = !1, e.scrollbarXLeft >= e.railXWidth - e.scrollbarXWidth && (e.scrollbarXLeft = e.railXWidth - e.scrollbarXWidth), e.scrollbarYTop >= e.railYHeight - e.scrollbarYHeight && (e.scrollbarYTop = e.railYHeight - e.scrollbarYHeight),
                    function(t, e) {
                        var a = {
                            width: e.railXWidth
                        };
                        e.isRtl ? a.left = e.negativeScrollAdjustment + t.scrollLeft + e.containerWidth - e.contentWidth : a.left = t.scrollLeft, e.isScrollbarXUsingBottom ? a.bottom = e.scrollbarXBottom - t.scrollTop : a.top = e.scrollbarXTop + t.scrollTop, i.css(e.scrollbarXRail, a);
                        var o = {
                            top: t.scrollTop,
                            height: e.railYHeight
                        };
                        e.isScrollbarYUsingRight ? e.isRtl ? o.right = e.contentWidth - (e.negativeScrollAdjustment + t.scrollLeft) - e.scrollbarYRight - e.scrollbarYOuterWidth : o.right = e.scrollbarYRight - t.scrollLeft : e.isRtl ? o.left = e.negativeScrollAdjustment + t.scrollLeft + 2 * e.containerWidth - e.contentWidth - e.scrollbarYLeft - e.scrollbarYOuterWidth : o.left = e.scrollbarYLeft + t.scrollLeft, i.css(e.scrollbarYRail, o), i.css(e.scrollbarX, {
                            left: e.scrollbarXLeft,
                            width: e.scrollbarXWidth - e.railBorderXWidth
                        }), i.css(e.scrollbarY, {
                            top: e.scrollbarYTop,
                            height: e.scrollbarYHeight - e.railBorderYWidth
                        })
                    }(t, e), e.scrollbarXActive ? r.add(t, "ps--active-x") : (r.remove(t, "ps--active-x"), e.scrollbarXWidth = 0, e.scrollbarXLeft = 0, s(t, "left", 0)), e.scrollbarYActive ? r.add(t, "ps--active-y") : (r.remove(t, "ps--active-y"), e.scrollbarYHeight = 0, e.scrollbarYTop = 0, s(t, "top", 0))
            }
        }, {
            "../lib/class": 2,
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-scroll": 20
        }],
        20: [function(t, e, a) {
            "use strict";
            var o = t("./instances"),
                n = function(t) {
                    var e = document.createEvent("Event");
                    return e.initEvent(t, !0, !0), e
                };
            e.exports = function(t, e, a) {
                if (void 0 === t) throw "You must provide an element to the update-scroll function";
                if (void 0 === e) throw "You must provide an axis to the update-scroll function";
                if (void 0 === a) throw "You must provide a value to the update-scroll function";
                "top" === e && a <= 0 && (t.scrollTop = a = 0, t.dispatchEvent(n("ps-y-reach-start"))), "left" === e && a <= 0 && (t.scrollLeft = a = 0, t.dispatchEvent(n("ps-x-reach-start")));
                var r = o.get(t);
                "top" === e && a >= r.contentHeight - r.containerHeight && ((a = r.contentHeight - r.containerHeight) - t.scrollTop <= 1 ? a = t.scrollTop : t.scrollTop = a, t.dispatchEvent(n("ps-y-reach-end"))), "left" === e && a >= r.contentWidth - r.containerWidth && ((a = r.contentWidth - r.containerWidth) - t.scrollLeft <= 1 ? a = t.scrollLeft : t.scrollLeft = a, t.dispatchEvent(n("ps-x-reach-end"))), void 0 === r.lastTop && (r.lastTop = t.scrollTop), void 0 === r.lastLeft && (r.lastLeft = t.scrollLeft), "top" === e && a < r.lastTop && t.dispatchEvent(n("ps-scroll-up")), "top" === e && a > r.lastTop && t.dispatchEvent(n("ps-scroll-down")), "left" === e && a < r.lastLeft && t.dispatchEvent(n("ps-scroll-left")), "left" === e && a > r.lastLeft && t.dispatchEvent(n("ps-scroll-right")), "top" === e && a !== r.lastTop && (t.scrollTop = r.lastTop = a, t.dispatchEvent(n("ps-scroll-y"))), "left" === e && a !== r.lastLeft && (t.scrollLeft = r.lastLeft = a, t.dispatchEvent(n("ps-scroll-x")))
            }
        }, {
            "./instances": 18
        }],
        21: [function(t, e, a) {
            "use strict";
            var o = t("../lib/helper"),
                n = t("../lib/dom"),
                r = t("./instances"),
                i = t("./update-geometry"),
                l = t("./update-scroll");
            e.exports = function(t) {
                var e = r.get(t);
                e && (e.negativeScrollAdjustment = e.isNegativeScroll ? t.scrollWidth - t.clientWidth : 0, n.css(e.scrollbarXRail, "display", "block"), n.css(e.scrollbarYRail, "display", "block"), e.railXMarginWidth = o.toInt(n.css(e.scrollbarXRail, "marginLeft")) + o.toInt(n.css(e.scrollbarXRail, "marginRight")), e.railYMarginHeight = o.toInt(n.css(e.scrollbarYRail, "marginTop")) + o.toInt(n.css(e.scrollbarYRail, "marginBottom")), n.css(e.scrollbarXRail, "display", "none"), n.css(e.scrollbarYRail, "display", "none"), i(t), l(t, "top", t.scrollTop), l(t, "left", t.scrollLeft), n.css(e.scrollbarXRail, "display", ""), n.css(e.scrollbarYRail, "display", ""))
            }
        }, {
            "../lib/dom": 3,
            "../lib/helper": 6,
            "./instances": 18,
            "./update-geometry": 19,
            "./update-scroll": 20
        }]
    }, {}, [1]);