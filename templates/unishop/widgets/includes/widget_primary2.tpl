<section class="widget-primary" data-slider="widget-primary">
  <div class="title_block_inside"><h3 class="h3title_front" style="padding-top: 20px !important;">Найпопулярніші товари місяця</h3></div>
  <div class="widget-primary__inner">
    <div class="row">
      {foreach $parent_products as $product}
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 frontpage_products">
          {view('shop/includes/product/product_cut.tpl', [
          'model' => $product
          ])}
        </div>
      {/foreach}
    </div>
  </div>
  
</section>