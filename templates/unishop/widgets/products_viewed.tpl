{if $CI->core->core_data['data_type'] != 'main'}
{if count($products) > 0}
<div class="page__viewed">

  <div class="page__container">
  	<div class="title_block_inside"><h3 style="padding-top: 40px;">Ви нещодавно дивилися</h3></div>
    {view('widgets/includes/widget_primary.tpl', [
      'parent_products' => $products,
      'parent_title' => getWidgetTitle('products_viewed')
    ])}
  </div>
</div>
{/if}
{/if}